export interface VendorCodeItem {
  vendorName: string;
  vendorCode: string;
}

export interface BOPICItem {
  deptid?: string;
  emplid?: string;
  name_A?: string;
}

export interface COMPICItem {
  contact_Ao?: string;
  name?: string;
  name_A?: string;
  invest_No?: string;
}

export interface UploadFileItem {
  success: boolean;
  successCount: number;
  infoCount: number;
  warningCount: number;
  errorCount: number;
  MsgList: MsgList[];
}

export interface MsgList {
  msgType: string;
  cell: string;
  verifyMsg: string;
}

export interface CommonFile {
  file_Id?: number;
  attach_Batch?: number;
  file_Name?: string;
  file_Path?: string;
  file_Size?: number;
}

export interface PreVerifyItem {
  success: boolean;
  duplicateData: PreVerifyMsg[];
  containsCompanyFinancialsYearChange: PreVerifyMsg[];
}

export interface PreVerifyMsg {
  company: string;
  financialsYear: number;
  quarter: string;
}
