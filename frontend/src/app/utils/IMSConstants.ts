export const IMSConstants = {
  serverErrorMsg: "Sorry, the system meet an error. Please contact the system admin.",
  contactEmail: "IMS_Support.WHQ.Wistron@wistron.com",
  matomoURL: "https://matomo.wistron.com/",
  dateFormat: "yyyy-MM-dd",
  dateFormat4Tip: "YYYY-MM-DD",
  createSuccess: "Created successfully",
  createFailed: "Create failed",
  addSuccess: "Added successfully",
  addFailed: "Add failed",
  updateSuccess: "Update successful",
  updateFailed: "Update failed",
  deleteSuccess: "Deleted successfully",
  deleteFailed: "Delete failed",
  deleteConfirmMsg: "Are you sure you want to delete it?",

  Report_Type:
  {
    YTD: "001",
    Quarter: "002",
  },

  Quarter_Type:
  {
    Q4: "004",
    Q3: "003",
    Q2: "002",
    Q1: "001"
  },

  swalTitle: {
    success: "Success",
    warning: "Warning",
    error: "Error",
    unauth: "無系統使用權限",
  },

  sessionStorageKey: {
    selectedTabIndex: {
      viewCompany: "viewCompany_selectedTabIndex",
      viewFund: "viewFund_selectedTabIndex"
    }
  }
};
