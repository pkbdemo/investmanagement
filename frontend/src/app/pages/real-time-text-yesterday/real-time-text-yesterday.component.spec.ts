import { ComponentFixture, TestBed } from '@angular/core/testing'

import { RealTimeTextYesterdayViewComponent } from './real-time-text-yesterday.component'

describe('RealTimeTextYesterdayViewComponent', () => {
  let component: RealTimeTextYesterdayViewComponent
  let fixture: ComponentFixture<RealTimeTextYesterdayViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RealTimeTextYesterdayViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeTextYesterdayViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
