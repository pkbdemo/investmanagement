import { ComponentFixture, TestBed } from '@angular/core/testing'

import { SummaryTableHourViewComponent } from './summary-table-hour.component'

describe('SummaryTableHourViewComponent', () => {
  let component: SummaryTableHourViewComponent
  let fixture: ComponentFixture<SummaryTableHourViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SummaryTableHourViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryTableHourViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
