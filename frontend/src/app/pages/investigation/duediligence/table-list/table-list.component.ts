import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { environment } from '@environments/environment';

import { tableListItem, SelectValue, emitObj } from '../duediligenceModel';
import { AuthorityControlService } from 'src/app/service/authority-control.service';

interface DataItem {
  name: string;
  chinese: number;
  math: number;
  english: number;
}

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  @Input() invest_no: number;
  //用来控制通过公司或基金维护进入隐藏部分内容
  isShowView: boolean = true;

  //用来控制合法用户进入隐藏部分内容
  canCreateInvestigation: boolean = false;

  @Output() InProfessCount = new EventEmitter<emitObj>();

  listOfColumn = [
    {
      title: '建立日期',
      compare: (a: tableListItem, b: tableListItem) => a.build_Date_Format > b.build_Date_Format ? -1 : 1,
      priority: 1
    },
    {
      title: '調查名稱',
      compare: (a: tableListItem, b: tableListItem) => a.name > b.name ? -1 : 1,
      priority: 2
    },
    {
      title: '證券種類',
      compare: null,
      priority: false
    },
    {
      title: '投資部PIC',
      compare: null,
      priority: false
    },
    {
      title: 'Decision',
      compare: null,
      priority: false
    },
    {
      title: 'Decision日期',
      compare: null,
      priority: false
    }
  ];

  //Nav Search條件
  NavData: string = '';
  userId: string;
  //上下箭頭展示控制
  Notfrozen: number = 0;
  AllNotfrozen: number = 0;
  //下拉菜单數據源資料
  selectCompanyValud: SelectValue[] = [];
  selectStatusValud: SelectValue[] = [];

  //搜索资料
  listOfSecurityValue: string[] = [];
  searchValue: string;
  listOfStatusData: string;

  set listOfStatus(data) {
    this.listOfStatusData = data;
    this.search();
  }

  //TableList数据源資料
  listOfData: tableListItem[] = [];

  //组件外Nav条件限制，更改的数据源资料
  listOfExternalData: tableListItem[] = [];

  //组件外Header条件限制,更改的数据源资料
  listOfHeaderData: tableListItem[] = [];

  //TableList 过滤后展示资料
  listOfDisplayData: tableListItem[] = [];
  constructor(private httpClient: HttpClient,
    private authorityControlService: AuthorityControlService,
    private toastr: ToastrService,
    public router: Router) { }

  async ngOnInit() {
    this.httpClient.get<any>(environment.apiServerURL + "DDiligence/QueryAll").subscribe({
      next: (res) => {
        if (this.invest_no != null) {
          this.isShowView = false;
          res = res.filter((e) => e.invest_No == this.invest_no);
        }
        this.listOfData = [...this.listOfData, ...res];
        this.listOfHeaderData = [...this.listOfData]
        this.listOfExternalData = [...this.listOfHeaderData];
        this.listOfDisplayData = [...this.listOfExternalData];
        this.userId = sessionStorage.getItem("userID");
        if (!!this.userId && !this.invest_no) this.UpdateHeaderDate(this.userId); // listSelect = My List
        if (sessionStorage.getItem("list") === 'All List') this.UpdateHeaderDate(''); // listSelect = All list
        this.Notfrozen = 0;
        for (let item of this.listOfData) {
          if (item.frozen_Type == '001') this.Notfrozen++;
        }
        this.InProfessCount.emit({
          type: 'All',
          Notfrozen: this.Notfrozen
        });
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
    this.getCompany();
    this.getStatusData();
    this.authorityControlService.getUserRole().then(
      userRole => {
        if (userRole.isIT || (userRole.isIMDMember)) {
          this.canCreateInvestigation = true;
        }
      }
    );
  }

  getCompany() {
    let formData = new FormData();
    formData.set("Kind_id", "001");
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData).subscribe({
      next: (res) => {
        for (let item of res) {
          let data: SelectValue = {
            label: '',
            value: ''
          };
          data.label = item.code_Name;
          data.value = item.code_id;
          data.checked = false;
          data.disabled = false;
          this.selectCompanyValud = [...this.selectCompanyValud, data];
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getStatusData() {
    let formData1 = new FormData();
    formData1.set("Kind_id", "014");
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData1).subscribe({
      next: (res) => {
        for (let item of res) {
          let data: SelectValue = {
            label: '',
            value: ''
          };
          data.label = item.code_Name;
          data.value = item.code_id;
          this.selectStatusValud = [...this.selectStatusValud, data];
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  search(): void {
    this.searchValue = this.searchValue == null ? '' : this.searchValue;
    this.listOfDisplayData = this.listOfExternalData.filter((item: tableListItem) => {
      return (item.name.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1 || item.user_Name?.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1)
    });
    if (this.listOfSecurityValue.length > 0) {
      this.listOfDisplayData = this.listOfDisplayData.filter((item: tableListItem) => {
        return this.listOfSecurityValue.some((x) => {
          if (item.string_Agg == null) {
            return false;
          }
          return item.string_Agg.split(',').includes(x);
        }) && (item.name.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1 || item.user_Name?.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1)
      });
    }

    console.log(this.listOfStatusData);

    if (!!this.listOfStatusData) {
      this.listOfDisplayData = this.listOfDisplayData.filter((item: tableListItem) => {
        return (this.listOfStatusData.toUpperCase() === item.status_Name.toUpperCase()) && (item.name.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1 || item.user_Name.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1)
      });
    }


  }

  CompanyTransaction(data: SelectValue): void {
    if (data.checked) {
      this.listOfSecurityValue = [...this.listOfSecurityValue, data.label];
    } else {
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d !== data.label);
    }
    this.search();
  }

  //清空所选
  closeSelectValue(type: string) {
    if (type == 'Security' && this.listOfSecurityValue.length > 0) {
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d == '-1');
      for (let item of this.selectCompanyValud) {
        item.disabled = false;
        item.checked = false;
      }
    }
    if (type == 'Status' && this.listOfStatusData != null) {
      this.listOfStatusData = null;
    }
    if (type == 'All') {
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d == '-1');
      for (let item of this.selectCompanyValud) {
        item.disabled = false;
        item.checked = false;
      }
      this.searchValue = null;
      this.listOfStatusData = null;
    }
    this.search();
  }

  //Nav 操作元数据 listOfExternalData  =>  操作 Decision
  //因为多层操作 所以每次操作完成需要多层赋值
  UpdataNavData(data: string) {

    this.listOfExternalData = this.listOfHeaderData.filter((item: tableListItem) => {
      return item.frozen_Type.indexOf(data) !== -1
    });
    this.listOfDisplayData = [...this.listOfExternalData];
    this.search();
    this.NavData = data;
  }

  UpdateHeaderDate(data?: string) {
    if (data == 'My List') data = this.userId;
    this.listOfHeaderData = this.listOfData.filter((item: tableListItem) => {
      return item.contact_Ao.toUpperCase().indexOf(data.toUpperCase()) !== -1
    });
    this.listOfExternalData = [...this.listOfHeaderData];
    this.listOfDisplayData = [...this.listOfExternalData];

    this.Notfrozen = 0;
    for (let item of this.listOfDisplayData) {
      if (this.userId.toUpperCase() == item.contact_Ao.toUpperCase()) {
        if (item.frozen_Type == '001') this.Notfrozen++;
      }
    }
    this.InProfessCount.emit({
      type: '',
      Notfrozen: this.Notfrozen
    });
    this.UpdataNavData(this.NavData);
  }

  //路由跳转
  routingSkip(data: string) {
    console.log(data);
    if (data == '-1') this.router.navigate(['duediligence/ddOperate'])
    else this.router.navigate(['duediligence/ddView'], { queryParams: { dd_No: data, isShowBtn: !this.invest_no } });

  }
}
