import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';

import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Subscription } from 'rxjs';
import { codeNameItem } from 'src/app/pages/company-maintain/companyModel';
import { CompanyService } from 'src/app/pages/company-maintain/services/company.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { CommonFile, DDStage, mouseType, SurevyBack } from '../duediligenceModel';
import { DueDiligenceService } from '../services/due-diligence.service';

@Component({
  selector: 'app-insert-survey',
  templateUrl: './insert-survey.component.html',
  styleUrls: ['./insert-survey.component.css']
})
export class InsertSurveyComponent implements OnInit {
  @Output() back = new EventEmitter<SurevyBack>();
  data: DDStage = {};
  surveyValue: string[] = [];
  Type: number = 0;

  mouseType = mouseType;
  isVisible: boolean = false;

  //from表单声明
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '此欄位為必填項'
    },
    en: {
      required: '此欄位為必填項'
    },
    default: {}
  };

  //存放編輯時的attach_Batch
  attach_Batch: number;

  validateForm: FormGroup;

  selectScheduleSub: Subscription;
  selectScheduleValud: codeNameItem[] = [];    //進度
  listOfSelectSchedule: codeNameItem[] = [];

  dateFormat = IMSConstants.dateFormat4Tip;
  mouseSet = new Set<number>();
  url = environment.apiServerURL

  //阶段选择为kick-off时展示
  isKickOff: boolean = false;
  start_Date: string;
  end_Date: string;

  //页面固定展示内容
  TitleTxt: string;
  btnTxt: string;

  //返回的数据
  DataValue: DDStage = {};

  //附檔上傳
  fileList: NzUploadFile[] = [];
  fileNameList: string[] = [];

  _checkData: string;
  set checkData(data: string) {
    this._checkData = data;
    if (!!this.data?.status) {
      if (data == '001') {
        this.isKickOff = true;
        this.validateForm.get('Start_Date').disable();
        this.validateForm.get('End_Date').disable();
      } else {
        this.isKickOff = false;
        this.validateForm.get('Start_Date').enable();
        this.validateForm.get('End_Date').enable();
      }
    }
  }
  constructor(private companyService: CompanyService,
    private fb: FormBuilder,
    private message: NzMessageService) { }

  async ngOnInit() {
    this.start_Date = this.end_Date = new Date().toString();
    this.validateForm = this.fb.group({
      Start_Date: [null, [Validators.required]],
      End_Date: [null, [Validators.required]],
      Remark: ['', []],
    });

    await this.getScheduleData();
    this.validateForm.get('Start_Date').valueChanges.subscribe(data => {
      if (!!data && data != this.validateForm.value['Start_Date'])
        this.validateForm.get('End_Date').setValue(data);
    });
    this.validateForm.get('Start_Date').setValue(new Date());
  }

  getScheduleData() {
    this.companyService.getCodeName('014');
    this.selectScheduleSub = this.companyService.scheduleListObservable.subscribe(item => {
      this.selectScheduleValud = item as codeNameItem[];
      this.listOfSelectSchedule = item as codeNameItem[];
      this.selectScheduleValud.forEach(e => e.disabled = false);
    });
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    //儅不是重名文件時 才會去寫入
    if (this.fileNameList.some(e => e == file.name)) {
      this.fileList = this.fileList.filter(e => e.name != file.name);
      this.fileNameList = this.fileNameList.filter(e => e != file.name);
    }
    this.fileList = this.fileList.concat(file);
    let NameSet = new Set<string>();
    this.fileNameList = [...this.fileNameList, ...this.fileList.map(e => e.name)];
    this.fileNameList.forEach(e => NameSet.add(e));
    this.fileNameList = [...NameSet];
    //向CommonFile數據中寫入文件名
    this.fileNameList.forEach(item => {
      if (!this.DataValue.fileList) this.DataValue.fileList = [];
      if (!(this.DataValue.fileList.some(e => e.file_Name == item))) {
        let obj: CommonFile = {}
        obj.file_Name = item;
        this.DataValue.fileList.push(obj)
      }
    });
    return false;
  };

  removeFile(key: number) {
    if (this.fileList.length > 0) this.fileList.splice(key, 1);

    if (this.DataValue.fileList.length > 0) this.DataValue.fileList = this.DataValue.fileList.filter(e => e.file_Name != this.fileNameList[key])

    if (this.fileNameList.length > 0) this.fileNameList.splice(key, 1);

  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.validateForm.get('End_Date').value) {
      return false;
    }
    let date = (this.validateForm.get('End_Date').value as Date);
    return startValue.getTime() > new Date(date.getFullYear(), date.getMonth(), date.getDate(), 24, 59, 59).getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.validateForm.get('Start_Date').value) {
      return false;
    }
    let date = (this.validateForm.get('Start_Date').value as Date);
    return endValue.getTime() < new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
  };

  //捨棄彈窗
  showDeleteConfirm(e: MouseEvent): void {
    this.isVisible = false;
    this.back.emit({
      isShowView: false,
      data: null
    });
  }

  //鼠标经过档案事件
  mouseEvent(key: number, type: number) {
    if (type == mouseType.Add) {
      this.mouseSet.add(key);
    }
    if (type == mouseType.Delete) {
      this.mouseSet.delete(key);
    }
    this.mouseSet = new Set([...this.mouseSet]);
  }

  createMessage(type: string): void {
    this.message.create(type, `調查階段為必填項`);
  }

  submitForm() {
    Object.keys(this.validateForm.controls).forEach(key => {
      const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
      if (validationErrors != null) {
        Object.keys(validationErrors).forEach(keyError => {
          console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
        });
      }
    });
    if (this.validateForm.valid) {
      if (!this._checkData) {
        this.createMessage('warning');
        return;
      }
      this.DataValue.attach_Batch = this.attach_Batch;
      this.DataValue.status = this._checkData;
      this.DataValue.status_Name = this.selectScheduleValud.find(e => e.value == this._checkData).label;
      this.DataValue.start_Date = this.validateForm.get('Start_Date').value;
      this.DataValue.end_Date = this.validateForm.get('End_Date').value;
      this.DataValue.remark = this.validateForm.get('Remark').value;
      this.DataValue.remark = this.validateForm.get('Remark').value;
      this.DataValue.UploadFileList = this.fileList;
      this.back.emit({
        isShowView: false,
        data: this.DataValue
      });
      this.isVisible = false;
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //打开
  editVisible(Type: number, surveyValue?: string[], data?: DDStage) {
    this.isVisible = true;
    if (!!data) this.data = data;
    this.Type = Type;
    this.surveyValue = surveyValue;

    this.validateForm.get('Start_Date').setValue(new Date());

    if (this.Type == mouseType.View) {
      this.TitleTxt = '查看調查階段';
    } else if (!!this.data?.status) {
      this.TitleTxt = '編輯調查階段';
      this.btnTxt = '更新卡片';
    } else {
      this.TitleTxt = '新增調查階段';
      this.btnTxt = '加入卡片';
    }

    if (!!data && !!this.data?.status) {
      this.attach_Batch = this.data.attach_Batch;
      this.end_Date = this.data.end_Date.toString();
      this.start_Date = this.data.start_Date.toString();
      this.validateForm.get('Start_Date').setValue(new Date(this.data.start_Date));
      this.validateForm.get('End_Date').setValue(new Date(this.data.end_Date));
      this.validateForm.get('Remark').setValue(this.data.remark);
      if (!!this.data.UploadFileList && this.data?.UploadFileList.length > 0) {
        this.fileList = this.data.UploadFileList;
        this.fileNameList = this.data.UploadFileList.map(e => e.name);
      }
      if (!!this.data?.fileList && this.data?.fileList.length > 0) {
        this.fileNameList = this.data.fileList.map(e => e.file_Name);
        this.DataValue.fileList = this.data.fileList;
      }
      this.checkData = this.data.status;
    }

    //儅surveyValue不爲空時  需要篩選
    if (!!this.surveyValue && this.surveyValue.length > 0) {
      //儅surveyValue不爲空時  需要篩選
      if (this.surveyValue.length > 0) {
        if (!!this.data?.status) {
          this.surveyValue.forEach(e => {
            this.selectScheduleValud = this.listOfSelectSchedule.filter(item => item.value == this.data.status);
          });
        } else {
          this.selectScheduleValud = this.listOfSelectSchedule.filter(item => !this.surveyValue.some(e => e == item.value));

        }
      }
    } else this.selectScheduleValud = this.listOfSelectSchedule;
  }

  //關閉model事件
  nzAfterClose() {
    this.isKickOff = false;
    this.isVisible = false;
    this.start_Date = this.end_Date = new Date().toString();
    this.data = {};
    this.Type = 0;
    this.surveyValue = [];
    this.DataValue = {};
    this.fileList = [];
    this.fileNameList = [];
    console.log('model 關閉');
    //關閉是需要恢復狀態
    if (this.validateForm.contains('Quit_Date')) this.validateForm.get('Quit_Date').clearValidators();
    if (this.validateForm.contains('Quit_Reason')) this.validateForm.get('Quit_Reason').clearValidators();
    this.validateForm.enable();
    this.validateForm.reset();
  }
}
