import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { environment } from '@environments/environment';

import { LayoutService } from '../../../layout/services/layout.service';
import { emitObj } from '../duediligenceModel';

interface navBtnItem {
  Code_id: string;
  Code_Name: string;
}
@Component({
  selector: 'app-due-diligence',
  templateUrl: './due-diligence.component.html',
  styleUrls: ['./due-diligence.component.css']
})
export class DueDiligenceComponent implements OnInit {

  invest_no: number = null;
  userName: string;
  userId: string;
  navCheckBtn: number = 0;
  InProfressCount: number = 0;
  InProfressCountAll: number = 0;

  isShowVector: boolean = true;
  isShowUserVector: boolean = true;

  listSelected: string = '';

  navBtn: navBtnItem[] = [
    {
      Code_id: '',
      Code_Name: 'All'
    }];
  constructor(private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService) { }

  async ngOnInit() {
    this.userName = await sessionStorage.getItem("userName");

    if (!sessionStorage.getItem("list")) {
      this.listSelected = 'My List'
    } else {
      this.listSelected = sessionStorage.getItem('list')
    }

    let formData2 = new FormData();
    formData2.set("Kind_id", "015");
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData2).subscribe({
      next: (res) => {
        console.log(res);

        for (let item of res) {
          let navBtnItem: navBtnItem = {
            Code_id: '',
            Code_Name: ''
          };
          navBtnItem.Code_id = item.code_id;
          navBtnItem.Code_Name = item.code_Name;
          this.navBtn = [...this.navBtn, navBtnItem];
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  InProfessCount(data: emitObj) {
    if (data.type == 'All') {
      this.InProfressCountAll = data.Notfrozen;
    } else {
      this.layoutService.emitTitle(data.Notfrozen);
      this.InProfressCount = data.Notfrozen;
    }
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  updateListSelected(listSelected) {
    sessionStorage.setItem('list', listSelected)
  }

}
