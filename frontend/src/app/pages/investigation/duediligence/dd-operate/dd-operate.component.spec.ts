import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DdOperateComponent } from './dd-operate.component';

describe('DdOperateComponent', () => {
  let component: DdOperateComponent;
  let fixture: ComponentFixture<DdOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DdOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DdOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
