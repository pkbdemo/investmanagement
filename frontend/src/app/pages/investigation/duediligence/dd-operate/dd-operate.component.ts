import { Component, OnInit } from '@angular/core';

import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


//组件库导入
import { NzModalService } from 'ng-zorro-antd/modal';

import { LayoutService } from 'src/app/pages/layout/services/layout.service';
import { CompanyService } from '../../../company-maintain/services/company.service';
import { DueDiligenceService } from '../services/due-diligence.service';
import { CheckType, DDStage, DDView, DD_Stage_Model, mouseType, requiredType, SelectValue, StatusType, SurevyBack } from '../duediligenceModel';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { codeNameItem } from 'src/app/pages/company-maintain/companyModel';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { NzMessageService } from 'ng-zorro-antd/message';

interface RequestDDData {
  Name?: string;
  Files?: NzUploadFile[];
}
@Component({
  selector: 'app-dd-operate',
  templateUrl: './dd-operate.component.html',
  styleUrls: ['./dd-operate.component.css']
})
export class DdOperateComponent implements OnInit {

  //存放DD_No 用于修改完成返回DD_View
  dd_No: string;

  //用於區分頁面是新增還是修改
  mouseType = mouseType;

  Type: mouseType = 0;
  //調查階段類型
  serveyType: number = 0;
  categoryValue: SelectValue;

  dateFormat = IMSConstants.dateFormat4Tip;
  url = environment.apiServerURL
  //页面固定展示内容
  build_date: string;
  companyDataName: string;
  user_Name: string;
  user_Id: string;


  //防抖函數
  timer: any;

  //警告提示窗
  isVisible: boolean = false;
  //修改时的元数据
  listOfData: DDView;
  //标题
  companyName: string = '*未命名公司';
  statusName: string = 'In Progress';

  //欄位必選星號調整
  requiredType = requiredType;
  requiredTypeSet = new Set<number>();

  //控制新增或修改页面展示
  isAddOrEdit: boolean = true;
  isError: boolean = false; //控制证券种类是否展示异常

  //校驗彈窗信息
  IsModelVisible: boolean = false;
  CheckType = CheckType;
  CheckTypeData: CheckType;
  ModelTitle: string;
  ModelContent: string;
  ModelOk: string;

  //from表单声明
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '此欄位為必填項'
    },
    en: {
      required: '此欄位為必填項'
    },
    default: {}
  };

  validateForm: FormGroup;


  //表单内容声明
  panel = {
    active: true,
    disabled: false,
    name: '基本資料',
    customStyle: {
      background: '#ffffff',
      'border-radius': '4px',
      border: 'none'
    }
  }

  //下拉选单数据源
  listOfSecurityValue: SelectValue[] = [];  // 选中的證券種類
  selectCompanyValud: SelectValue[] = []; // 證券種類
  listOfSurveyValue: SelectValue[] = [];  //調查種類
  selectEmployeeValud: SelectValue[] = [];  //投資部PIC
  selectDeptValud: SelectValue[] = [];    //事業躰
  selectScheduleValud: SelectValue[] = [];    //進度
  selectCurrencyValud: SelectValue[] = [];    //幣別

  SurveySub: Subscription;
  SecuritySub: Subscription;
  selectScheduleSub: Subscription;
  boListSub: Subscription;
  CurrencySub: Subscription;

  SecurityValue: string[] = [];

  //附檔上傳
  fileList: NzUploadFile[] = [];

  isShowInsert: boolean = false;

  //操作調查階段
  isShowDecision: boolean = false;
  set SurveyData(data: DDStage[]) {
    this.SurveyDataList = data.sort((a, b) => this.ddService.SortData(a, b));
    this.SurveyValue = data.map(e => e.status);

    //控制結論區域的展示
    this.isShowDecision = data.some(e => e.status == '006');

    //控制投資款時間是否必選
    let value = this.validateForm.get('decision_Result').value
    this.editRequired(value);
  }
  SurveyDataList: DDStage[] = [];
  SurveyValue: string[] = [];
  dd_stage_item: DDStage = {};


  //頁面數據
  formData = new FormData();

  //页面初加载对金额进行基础校验
  AmountInitValidator = (control: FormControl): { [s: string]: boolean } => {
    let r = new RegExp('^[1-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?$');
    if (control.value) {
      if (!r.test(control.value.toString())) {
        return { confirm: true, error: true };
      }
    }
    return {};
  };

  //增加一個計時器用來限制快速點擊儲存
  isClickSave: boolean = false;

  constructor(private layoutService: LayoutService,
    private modal: NzModalService,
    public router: Router,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private companyService: CompanyService,
    private fb: FormBuilder,
    private ddService: DueDiligenceService,
    private toastr: ToastrService,
    private message: NzMessageService) { }

  ngOnInit(): void {
    this.build_date = new Date().toString();
    this.user_Name = sessionStorage.getItem("userName");
    this.user_Id = sessionStorage.getItem("userID").toUpperCase();
    this.validateForm = this.fb.group({
      Name: ['', [Validators.required]],
      Survey: ['', [Validators.required]],
      MEMO: ['', []],
      user_Name: ['', []],
      dept_No: ['', [Validators.required]],
      bo: ['', [Validators.required]],
      dD_Date: [null, []],
      review_Meeting_Date: [null, []],
      invest_Date: [null, []],
      currency: ['', []],
      invest_Amount: ['', [this.AmountInitValidator]],
      decision_Result: ['', []],
      decision_Date: [null, []],
      decision_Comment: ['', []],
      remark: ['', []],
    });
    setTimeout(() => {
      this.isClickSave = true;
    }, 1000);

    this.validateForm.get('Survey').valueChanges.subscribe(data => {
      this.surevyEdit();
    });

    this.validateForm.get('decision_Result').valueChanges.subscribe(data => {
      if (!!data && data != this.validateForm.value['decision_Result']) {
        this.editRequired(data);
      }
    })

    this.validateForm.get('Name').valueChanges.subscribe(data => {
      if (!!data) {

      }
    });

    Promise.all([
      this.getSurveyData(),
      this.getCompany(),
      this.getEmployee(),
      this.getDeptData(),
      this.getScheduleData(),
      this.getCurrencyData()]).then((result) => {
        this.route.queryParams.subscribe(async params => {
          if (params['dd_No'] != null) {
            this.isAddOrEdit = false;
            this.dd_No = params['dd_No'];
            this.Type = mouseType.Update;
            this.getMaintenance(this.dd_No);
          } else {
            this.Type = mouseType.Add;
            this.isAddOrEdit = true;
          }
        });
      });
  }

  //更改必選狀態
  editRequired(data: string) {
    if (this.isShowDecision) {
      this.requiredTypeSet.add(this.requiredType.Decision_Comment);
      this.requiredTypeSet.add(this.requiredType.Decision_Date);
      this.requiredTypeSet.add(this.requiredType.Decision_Result);
      this.validateForm.get('decision_Comment').setValidators([Validators.required]);
      this.validateForm.get('decision_Date').setValidators([Validators.required]);
      this.validateForm.get('decision_Result').setValidators([Validators.required]);
      if (data.trim() == 'Yes') {
        this.requiredTypeSet.add(this.requiredType.Invest_Date);
        this.requiredTypeSet.add(this.requiredType.Currency);
        this.requiredTypeSet.add(this.requiredType.Invest_Amount);
        this.validateForm.get('invest_Date').setValidators([Validators.required]);
        this.validateForm.get('currency').setValidators([Validators.required]);
        this.validateForm.get('invest_Amount').setValidators([Validators.required, this.AmountInitValidator]);
      } else {
        this.requiredTypeSet.delete(this.requiredType.Invest_Date);
        this.requiredTypeSet.delete(this.requiredType.Currency);
        this.requiredTypeSet.delete(this.requiredType.Invest_Amount);
        this.validateForm.get('invest_Date').clearValidators();
        this.validateForm.get('currency').clearValidators();
        this.validateForm.get('invest_Amount').clearValidators();
      }
    } else {
      this.requiredTypeSet.delete(this.requiredType.Decision_Comment);
      this.requiredTypeSet.delete(this.requiredType.Decision_Date);
      this.requiredTypeSet.delete(this.requiredType.Decision_Result);
      this.validateForm.get('decision_Comment').clearValidators();
      this.validateForm.get('decision_Date').clearValidators();
      this.validateForm.get('decision_Result').clearValidators();
    }
    this.validateForm.get('decision_Comment').updateValueAndValidity();
    this.validateForm.get('decision_Date').updateValueAndValidity();
    this.validateForm.get('decision_Result').updateValueAndValidity();
    this.validateForm.get('invest_Date').updateValueAndValidity();
    this.validateForm.get('currency').updateValueAndValidity();
    this.validateForm.get('invest_Amount').updateValueAndValidity();
  }

  //修改時獲取數據
  getMaintenance(dd_no: string) {
    this.ddService.getMaintenance(dd_no);
    this.ddService.DDViewValue$.subscribe(e => {
      this.listOfData = e as DDView;
      //页面写入数据
      this.WriteData(this.listOfData);
    });
  }

  //向頁面寫入數據
  async WriteData(data: DDView) {
    //建立日期
    this.build_date = data.build_Date;
    //标题公司名
    this.companyName = data.name;
    //标题状态
    this.statusName = data.frozen_Type_Name;
    //公司名
    this.companyDataName = data.name;
    this.validateForm.get('Name').disable();
    //调查种类
    this.validateForm.get('Survey').setValue(data.category);
    //证券种类
    if (!!data.dD_Type) {
      data.dD_Type.split(',').forEach(e => {
        this.CompanyTransaction(true, e);
      })
    }
    //memo
    this.validateForm.get('MEMO').setValue(data.memo);
    //投资款时间
    this.validateForm.get('invest_Date').setValue(data.invest_Date);
    //投资金额
    this.validateForm.get('invest_Amount').setValue(data.invest_Amount);
    //币别
    this.validateForm.get('currency').setValue(data.currency);
    //投资部PIC
    this.user_Name = data.user_Name
    this.validateForm.get('user_Name').disable();
    //工时挂账部门
    this.validateForm.get('dept_No').setValue(data.dept_No);
    //BO
    this.validateForm.get('bo').setValue(data.bo);
    if (!!data.ddStageList && data.ddStageList.length > 0) {
      this.SurveyData = data.ddStageList.map(e => {
        let obj: DDStage = {};
        obj.attach_Batch = e.attach_Batch;
        obj.end_Date = e.end_Date;
        obj.start_Date = e.start_Date;
        obj.fileList = e.fileList;
        obj.remark = e.remark;
        obj.status = e.status;
        obj.status_Name = e.status_Name;
        return obj;
      });
      if (data.ddStageList.some(e => e.status == '006')) {
        this.validateForm.get('decision_Result').setValue(data.decision_Result ? 'Yes' : 'No');
        this.validateForm.get('decision_Date').setValue(data.decision_Date);
        this.validateForm.get('decision_Comment').setValue(data.decision_Comment);
      }
    }

  }

  //獲取調查階段的返回的數據
  UpdateShow(data: SurevyBack) {
    if (!!data?.data?.status) {
      this.SurveyDataList = this.SurveyDataList.filter(e => e.status != data.data.status);
      this.SurveyData = [...this.SurveyDataList, data.data];
      if (data.data?.UploadFileList?.length > 0) {
        let FileName = this.MapStatusTtype(data.data.status);
        this.WriteFormFile(data.data.UploadFileList, FileName);
      }
    }
    this.dd_stage_item = {};
  }

  //根據Status不同來獲取對應的FileListName
  MapStatusTtype(Status: string): string {
    let FileName;
    switch (Status) {
      case StatusType.Kick_Off:
        FileName = 'Kick_Off_File';
        break;
      case StatusType.Data_Room:
        FileName = 'Data_Room_File';
        break;
      case StatusType.Pre_Meeting:
        FileName = 'Pre_Meeting_File';
        break;
      case StatusType.On_Site:
        FileName = 'On_Site_File';
        break;
      case StatusType.Review_Meeting:
        FileName = 'Review_Meeting_File';
        break;
      case StatusType.Close:
        FileName = 'Close_File';
        break;
    }
    return FileName;
  }

  //向FormData中寫入File
  WriteFormFile(FileList: NzUploadFile[], FileName: string) {
    if (this.formData.has(FileName)) this.formData.delete(FileName);
    FileList.forEach((file: any) => {
      this.formData.append(FileName, file);
    });
  }

  //初始化获取元数据
  getCompany() {
    this.companyService.getCodeName('001');
    this.SecuritySub = this.companyService.securityListObservable.subscribe(item => {
      this.selectCompanyValud = item as codeNameItem[];
      this.selectCompanyValud.forEach(e => e.disabled = false)
    });
  }

  //选择公司全称逻辑
  nameChange() {
    let name = this.validateForm.get('Name').value;
    if (!!name) {
      let formData = new FormData();
      formData.set('name', name);
      this.httpClient.post<any>(environment.apiServerURL + "DDiligence/VertifyName", formData).subscribe({
        next: (res) => {
          if (!res) {
            this.isVisible = true;
          } else {
            this.ddService.CheckCategory(name);
            this.ddService.CategoryValue$.subscribe(data => {
              if (!!data) {
                this.categoryValue = this.listOfSurveyValue.find(e => e.value == data as string);
                this.validateForm.get('Survey').setValue(data as string);
              } else {
                this.categoryValue = null;
                this.validateForm.get('Survey').setValue('');
              }
            });
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("request failed");
        }
      });
    }
  }

  handleOk() {
    this.isVisible = false;
    this.validateForm.get('Name').setValue('');
  }

  //选择证券种类后逻辑
  CompanyTransaction(check: boolean, data: string) {
    console.log(this.listOfSurveyValue, this.selectCompanyValud);
    switch (check) {
      case true:
        this.selectCompanyValud.forEach(e => {
          if (e.value == data) {
            this.SecurityValue.push(e.label);
            this.listOfSecurityValue.push(e);
            e.checked = true;
          }
        })
        break;
      case false:
        this.selectCompanyValud.forEach(e => {
          if (e.value == data) {
            this.SecurityValue = this.SecurityValue.filter(item => item != e.label);
            this.listOfSecurityValue = this.listOfSecurityValue.filter(item => item.label != e.label);
            e.checked = false;
          }
        })
        break;
    }
    this.surevyEdit();
  }

  //更改調查種類觸發證券種類邏輯
  surevyEdit() {
    let surveyValue = this.validateForm.get('Survey').value;
    if (!!surveyValue) {
      this.selectCompanyValud.forEach(e => {
        if (e.code_Extend_A != surveyValue) {
          e.disabled = true;
          e.checked = false;
          this.listOfSecurityValue = this.listOfSecurityValue.filter(item => item.label != e.label);
          this.SecurityValue = this.SecurityValue.filter(item => item != e.label);
        } else {
          e.disabled = false;
        }
      });
    } else {
      this.selectCompanyValud.forEach(e => {
        e.disabled = false;
      })
    }
  }

  //清空證券種類
  closeSelectValue() {
    if (this.listOfSecurityValue.length > 0) {
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d.value == '-1');
      this.SecurityValue = [];
      for (let item of this.selectCompanyValud) {
        item.disabled = false;
        item.checked = false;
      }
    }
  }

  //刪除調查階段
  removeSurvey(status: string) {
    this.SurveyData = this.SurveyDataList.filter(e => e.status != status);
    //刪除調查階段需刪除對應的FormData
    let FileName = this.MapStatusTtype(status);
    if (this.formData.has(FileName)) {
      this.formData.delete(FileName);
    }
  }

  //选择事业体逻辑
  deptForm(data: any) {
    this.selectDeptValud.forEach((e) => {
      if (e.value == data) this.validateForm.get('dept_No').setValue(e.code_Extend_A);
    })
  }

  debounce() {
    this.submitForm();
    if (!this.timer) {
      this.timer = setTimeout(() => {
        this.isClickSave = true;
        clearTimeout(this.timer)
        this.timer = null;
      }, 1000)
    }
  }


  submitForm() {
    if (this.isClickSave) {
      this.isClickSave = false;
      if (this.validateForm.status == "INVALID") {
        Object.keys(this.validateForm.controls).forEach(key => {
          const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
          if (validationErrors != null) {
            Object.keys(validationErrors).forEach(keyError => {
              console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
            });
          }
        });
      }
      if (this.listOfSecurityValue.length > 0) this.isError = false;
      else this.isError = true;
      if (this.validateForm.valid) {
        if (this.isError) return;
        this.CheckData1();
      } else {
        Object.values(this.validateForm.controls).forEach(control => {
          if (control.invalid) {
            control.markAsDirty();
            control.updateValueAndValidity({ onlySelf: true });
          }
        });
      }
    }
  }

  //數據處理
  HandleData(data) {
    this.formData.set('Category', data.Survey);
    this.formData.set('Name', data.Name || this.companyDataName);
    this.formData.set('build_date', this.ddService.timestampToTime(this.build_date));
    this.formData.set('DD_Type', this.listOfSecurityValue.map(e => e.value).toString());
    this.formData.set('Memo', !!data.MEMO ? data.MEMO : '');
    this.formData.set('DD_No', !!this.dd_No ? this.dd_No : '');
    this.formData.set('Invest_Date', !!data.invest_Date ? this.ddService.timestampToTime(data.invest_Date) : '');
    this.formData.set('Currency', !!data.currency ? data.currency : '');
    this.formData.set('Invest_Amount', !!data.invest_Amount ? data.invest_Amount : '');
    this.formData.set('Contact_Ao', this.user_Id);
    this.formData.set('Bo', data.bo);
    this.formData.set('Dept_No', data.dept_No);
    if (this.SurveyDataList.some(e => e.status == '006')) {
      this.formData.set('Decision_Date', this.ddService.timestampToTime(data.decision_Date));
      this.formData.set('Decision_Result', data.decision_Result);
      this.formData.set('Decision_Comment', data.decision_Comment);
      if (this.fileList?.length > 0) {
        if (this.formData.has('Decision_Files')) this.formData.delete('Decision_Files');
        this.WriteFormFile(this.fileList, 'Decision_Files');
      }
    }
    //組合DD_Stage_List
    if (this.SurveyDataList.length > 0) {
      let Object = this.SurveyDataList.map(item => {
        let obj: DD_Stage_Model = {};
        obj.status = item.status;
        obj.attach_Batch = item.attach_Batch;
        obj.start_Date = item.start_Date;
        obj.end_Date = item.end_Date;
        obj.fileList = item.fileList;
        obj.remark = item.remark;
        obj.status_Name = item.status_Name;
        return obj;
      });
      this.formData.set('DD_Stage_List', JSON.stringify(Object))
    }

  }

  // 儲存數據
  SaveData() {
    let apiUrl = this.Type == mouseType.Add ? 'Add' : 'Update';
    this.httpClient.post<any>(environment.apiServerURL + "DDiligence/" + apiUrl, this.formData).subscribe({
      next: (res) => {
        if (res != null) {
          if (res.is_Error) {
            this.createMessage('error', res.value);
          } else {
            this.createMessage('success', `已儲存變更`);
            this.router.navigate(['duediligence/ddView'], { queryParams: { dd_No: res.dD_No } });
          }
        }
      },
      error: (err) => {
        console.error(err);
      }
    });
  }

  //彈窗確認按鈕
  handleModelOk(value: CheckType) {
    switch (value) {
      case CheckType.Check1:
        this.CheckData2();
        break;
      case CheckType.Check2:
        this.HandleData(this.validateForm.value);
        this.SaveData();
        break;
    }
  }

  //儲存校驗
  CheckData1() {
    //1. 如果有Kick-off/NDA資料，需要校驗
    const isCheck = this.SurveyDataList.some(e => e.status == '001');
    if (isCheck) {
      let formData = new FormData();
      formData.set('status', '001');
      formData.set('name', this.companyDataName || this.validateForm.get('Name').value);
      formData.set('build_date', this.ddService.timestampToTime(this.build_date));
      this.httpClient.post<any>(environment.apiServerURL + "DDiligence/CheckData", formData).subscribe({
        next: (res) => {
          if (res) {
            this.IsModelVisible = true;
            this.CheckTypeData = CheckType.Check1
            this.ModelTitle = '注意！'
            this.ModelContent = 'Kick-off/NDA調查階段新增後，資料將不可刪除';
            this.ModelOk = '確認儲存';
          }
          else { this.CheckData2(); }
        },
        error: (err) => {
          console.error(err);
        }
      });
    } else {
      this.CheckData2();
    }
  }

  CheckData2() {
    //校驗2
    const isCheck = this.SurveyDataList.some(e => e.status == '006');
    if (isCheck) {
      this.IsModelVisible = true;
      this.CheckTypeData = CheckType.Check2
      this.ModelTitle = '是否凍結資料？'
      this.ModelContent = '請確認資料是否正確，若選擇凍結則無法再異動';
      this.ModelOk = '確認凍結';
    } else {
      this.HandleData(this.validateForm.value);
      this.SaveData();
    }
  }


  //結論區域文件
  beforeUpload = (file: NzUploadFile): boolean => {
    if (this.fileList.some(e => e.name == file.name)) this.fileList = this.fileList.filter(e => e.name != file.name);
    this.fileList = this.fileList.concat(file);
    return false;
  };

  //刪除結論區域文件
  removeFile = (file: NzUploadFile): boolean => {
    this.fileList = this.fileList.filter(e => e.uid != file.uid);
    return true;
  }

  Upload(e) {
    e.preventDefault();
  }

  getSurveyData() {
    this.companyService.getCodeName('018');
    this.SurveySub = this.companyService.SurveyListObservable.subscribe(item => {
      this.listOfSurveyValue = item as codeNameItem[];
      this.listOfSurveyValue.forEach(e => e.disabled = false)
    });
  }

  getDeptData() {
    this.ddService.Employee$.subscribe(item => this.selectEmployeeValud = item as SelectValue[]);
  }

  getEmployee() {
    this.companyService.getCodeName('004');
    this.boListSub = this.companyService.boListObservable.subscribe(item => {
      this.selectDeptValud = item as codeNameItem[];
      this.selectDeptValud.forEach(e => e.disabled = false)
    });
  }

  getScheduleData() {
    this.companyService.getCodeName('014');
    this.selectScheduleSub = this.companyService.scheduleListObservable.subscribe(item => {
      this.selectScheduleValud = item as codeNameItem[];
      this.selectScheduleValud.forEach(e => e.disabled = false)
    });
  }

  getCurrencyData() {
    this.CurrencySub = this.ddService.Currency$.subscribe(item => this.selectCurrencyValud = item as SelectValue[]);
  }

  handleCancel(e) {
    this.IsModelVisible = false;
    e.preventDefault();
  }



  //捨棄彈窗
  showDeleteConfirm(e: MouseEvent): void {
    e.preventDefault();
    this.modal.confirm({
      nzTitle: '是否捨棄資料?',
      nzContent: '<b style="color: red;">您未儲存變更，若選擇捨棄，講清除所有變更</b>',
      nzOkText: '捨棄',
      nzOkDanger: true,
      nzOnOk: () => {
        if (this.isAddOrEdit) {
          this.router.navigate(['/duediligence'])
        } else {
          this.router.navigate(['duediligence/ddView'], { queryParams: { dd_No: this.dd_No } });
        }
      },
      nzCancelText: '取消', nzCloseIcon: '',
      nzMaskClosable: false,
      nzClassName: 'DDModal',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  // openCollapse() {
  //   if (this.panel.active == false) {
  //     this.panel.active = true;
  //   } else {
  //     this.panel.active = false;
  //   }
  //   console.log(this.panel.active)
  // }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  createMessage(type: string, value: string): void {
    this.message.create(type, value);
  }

  //必须方法，不然下拉选单不能展开
  updateActive() {
  }
}
