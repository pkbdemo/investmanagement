import { TestBed } from '@angular/core/testing';

import { DueDiligenceService } from './due-diligence.service';

describe('DueDiligenceService', () => {
  let service: DueDiligenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DueDiligenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
