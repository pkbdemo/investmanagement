import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { SelectValue } from '../duediligenceModel';

@Injectable({
  providedIn: 'root'
})
export class DueDiligenceService {
  private _Employee$ = new Subject();
  private _Currency$ = new Subject();
  private _DDViewValue$ = new Subject();
  private _CategoryValue$ = new Subject();

  get DDViewValue$() {
    return this._DDViewValue$.asObservable();
  }

  get Employee$() {
    this.getEmployee();
    return this._Employee$.asObservable();
  }

  get Currency$() {
    this.getCurrencyData();
    return this._Currency$.asObservable();
  }

  get CategoryValue$() {
    return this._CategoryValue$.asObservable();
  }

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) {
  }

  getEmployee() {
    this.httpClient.get<any>(environment.apiServerURL + "DDiligence/QueryFindwHcmEmpByDept").subscribe({
      next: (res) => {
        if (!!res) {
          let data: SelectValue[] = [];
          res.forEach(e => {
            let item: SelectValue = {
              label: '',
              value: ''
            };
            item.label = e.name_a;
            item.value = e.emplid.toUpperCase();
            data = [...data, item];
          })
          this._Employee$.next(data);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getCurrencyData() {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryCurrencyList").subscribe({
      next: (res) => {
        if (!!res) {
          let data: SelectValue[] = [];
          res.forEach(e => {
            let item: SelectValue = {
              label: '',
              value: ''
            };
            item.label = e;
            item.value = e;
            item.code_Extend_A = e.code_Extend_A;
            item.checked = false;
            item.disabled = false;
            data = [...data, item];
          })
          this._Currency$.next(data);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //標準時間轉換
  timestampToTime(timestamp: string): string {
    if (timestamp != null && timestamp != '') {
      let date = new Date(timestamp);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let MM = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      let dd = d < 10 ? ('0' + d) : d;
      return y + '-' + MM + '-' + dd;
    }
    else {
      return null
    }
  }

  //数组排序
  SortData(a: any, b: any): number {
    let AStartDate = this.timestampToTime(a.start_Date.toString());
    let BStartDate = this.timestampToTime(b.start_Date.toString());
    let AEndDate = this.timestampToTime(a.end_Date.toString());
    let BEndDate = this.timestampToTime(b.end_Date.toString());
    if (Number(new Date(AStartDate)) != Number(new Date(BStartDate))) return Number(new Date(AStartDate)) > Number(new Date(BStartDate)) ? 0 : -1;
    else return Number(new Date(AEndDate)) > Number(new Date(BEndDate)) ? 0 : -1;
  }

  getMaintenance(dd_no: string) {
    let formData = new FormData();
    formData.set("dd_no", dd_no);
    this.httpClient.post<any>(environment.apiServerURL + "DDiligence/GetDDView", formData).subscribe({
      next: (res) => {
        console.log(res);
        if (res != null) {
          this._DDViewValue$.next(res);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //校驗調查名稱是否已維護
  CheckCategory(name: string) {
    let formData = new FormData();
    formData.set("name", name);
    this.httpClient.post<string>(environment.apiServerURL + "Utilities/CheckCategory", formData).subscribe({
      next: (res) => {
        console.log(res);
        this._CategoryValue$.next(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    })
  }
}
