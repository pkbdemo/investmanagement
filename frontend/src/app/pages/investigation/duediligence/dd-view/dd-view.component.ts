import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments/environment';
import { LayoutService } from 'src/app/pages/layout/services/layout.service';
import { ConclusionItem, DDStage, DDView, mouseType, SurevyBack } from '../duediligenceModel';
import { ToastrService } from 'ngx-toastr';
import { DueDiligenceService } from '../services/due-diligence.service';
import { InsertSurveyComponent } from '../insert-survey/insert-survey.component';

@Component({
  selector: 'app-dd-view',
  templateUrl: './dd-view.component.html',
  styleUrls: ['./dd-view.component.css']
})
export class DdViewComponent implements OnInit {

  isVisible = false;
  url = environment.apiServerURL

  dd_stage_item: DDStage;

  companyName: string = '*未命名公司';
  statusName: string = 'In Progress';
  userId: string = '';
  //存放點擊編輯時傳入的值
  dd_no: string = '';
  invest_no: number;
  listOfData: DDView;
  isShowBtn: boolean = true;
  isShowDelete: boolean = false;
  newConclusion: ConclusionItem = {};
  ConclusionList: ConclusionItem[] = [];
  isShowInsert: boolean = false;
  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  mouseType = mouseType;

  // @ViewChild('InsertSurvey') InsertSurvey!: InsertSurveyComponent;

  constructor(private layoutService: LayoutService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private location: Location,
    private toastr: ToastrService,
    public router: Router,
    private ddService: DueDiligenceService) { }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      this.isShowBtn = params['isShowBtn']! == 'false' ? false : true;
      if (!!params['dd_No']) {
        this.dd_no = params['dd_No'];
        await this.getMaintenance(params['dd_No']);
        // this.InsertSurvey.editVisible(mouseType.View, null, this.dd_stage_item);
      }
    });
  }
  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  getMaintenance(dd_no: string) {
    this.ddService.getMaintenance(dd_no);
    this.ddService.DDViewValue$.subscribe(e => {
      let res = e as DDView;
      this.invest_no = res.invest_No;
      this.listOfData = res;
      this.companyName = this.listOfData.name;
      this.statusName = this.listOfData.frozen_Type_Name;
      // 判断编辑按钮是否展示
      if (this.isShowBtn) {
        if (res?.frozen_Type == '001' && res?.contact_Ao.toUpperCase() == this.userId) {
          this.isShowBtn = true;
          if (!res?.ddStageList || res?.ddStageList.every(e => e.status != '001')) {
            this.isShowDelete = true;
          } else this.isShowDelete = false;
        }
        else this.isShowBtn = false;
      }
      if (!!this.listOfData.ddStageList && this.listOfData.ddStageList.length > 0) {
        this.listOfData.ddStageList = this.listOfData.ddStageList.sort((a, b) => this.ddService.SortData(a, b));
      }
      if (!!res.decision_Comment) {
        this.listOfData.decision_Comment_Arr = res.decision_Comment.split('\n');
      }
    });
  }

  UpdateShow(data: SurevyBack) {
    this.isShowInsert = data.isShowView;
    this.dd_stage_item = {};
  }

  goBack() {
    // this.location.back();
    this.router.navigate(['../duediligence/'])
  }

  //必须方法，不然下拉选单不能展开
  updateActive(data?: any) {
  }

  //路由跳转
  routingSkip() {
    // this.router.navigate(['duediligence/maintainArea'], { queryParams: { dd_No: this.dd_no } })
    this.router.navigate(['duediligence/ddOperate'], { queryParams: { dd_No: this.dd_no } })
  }

  //刪除取消框
  handleCancel(): void {
    this.isVisible = false;
  }

  //刪除確認框
  handleOk(): void {
    this.isVisible = false;
    let formData = new FormData();
    formData.set("DD_No", this.dd_no);
    formData.set("Invest_No", this.invest_no.toString());
    this.httpClient.post<any>(environment.apiServerURL + "DDiligence/DeleteData", formData).subscribe({
      next: (res) => {
        if (res) this.goback();
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //路由跳转 后退一步
  goback() {
    this.location.back();
  }
}
