import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DdViewComponent } from './dd-view.component';

describe('DdViewComponent', () => {
  let component: DdViewComponent;
  let fixture: ComponentFixture<DdViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DdViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DdViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
