import { NzUploadFile } from "ng-zorro-antd/upload";

export enum mouseType { Add = 0, Delete, Update, View }
export enum CheckType { Check1 = 0, Check2 }
export enum StatusType { Kick_Off = '001', Data_Room = '002', Pre_Meeting = '003', On_Site = '004', Review_Meeting = '005', Close = '006' }
export enum requiredType { Invest_Date, Currency, Invest_Amount, Decision_Result, Decision_Comment, Decision_Date }
export interface tableListItem {
  invest_No?: number;
  dD_No?: string;
  name?: string;
  category?: string;
  contact_Ao?: string;
  user_Id?: string;
  user_Name?: string;
  build_Date_Format?: string;
  status?: string;
  status_Name?: string;
  string_Agg?: string;
  ddType?: string;
  frozen_Type?: string;
  frozen_Type_Name?: string;
  decision_Result?: string;
  decision_Date_Format?: string;
}

export interface SearchCodeList {
  Code_id: string;
  Code_Name: string;
  Code_Extend_A: string;
  Code_Extend_B: string;
  Code_Extend_C: string;
}

export interface SelectValue {
  label: string;
  value: string;
  code_Extend_A?: string;
  checked?: boolean;
  disabled?: boolean;
}
export interface emitObj {
  type: string;
  Notfrozen: number;
}
export interface DDView {
  invest_No?: number;
  build_Date?: string;
  dD_No?: number;
  name?: string;
  user_Name?: string;
  dD_Type?: string;
  dD_Type_Name?: string;
  contact_Ao?: string;
  category?: string;
  category_Name?: string;
  memo?: string;
  invest_Date?: Date;
  frozen_Type?: string;
  frozen_Type_Name?: string;
  currency?: string;
  invest_Amount?: number;
  bo?: string;
  bo_Name?: string;
  dept_No?: string;
  decision_Result?: boolean;
  decision_Date?: Date;
  decision_Comment?: string;
  decision_Comment_Arr?: string[];
  attach_Batch?: number;
  ddStageList?: DDStage[];
  decisionFileList?: CommonFile[];
}

export interface DDStage extends DD_Stage_Model {
  UploadFileList?: NzUploadFile[];
}

export interface DD_Stage_Model {
  status?: string;
  status_Name?: string;
  start_Date?: Date;
  end_Date?: Date;
  remark?: string;
  attach_Batch?: number;
  fileList?: CommonFile[];
}

export interface CommonFile {
  file_Id?: number;
  attach_Batch?: number;
  file_Name?: string;
  file_Path?: string;
}

export interface SurevyBack {
  isShowView?: boolean;
  data?: DDStage;
}

export interface ReqMaintenance {
  build_date: string;
  Bo: string;
  name: string;
  ddType: string;
  ddType_Name: string;
  contact_Ao: string;
  user_Id: string;
  user_Name: string;
  dept_No: string;
  status: string;
  status_Name: string;
  dD_Date: string;
  review_Meeting_Date: string;
  invest_Date: string;
  currency: string;
  invest_Amount: number;
  newDecision_Result?: string;
  newDecision_Date?: string;
  newDecision_Comment?: string;
  decision_Result: string;
  decision_Date: string;
  decision_Comment: string;
  remark: string;
  frozen_Type: string;
  frozen_Type_Name?: string;
  code_Extend_A?: string;
  DeleteAmountRecord: string;
  DeleteAmountRecord_Value: string;
  DeleteStrikeRecord: string;
  DeleteStrikeRecord_Value: string;
  DeleteOtherRecord: string;
}
export interface ConclusionItem {
  decision_Result?: string;
  decision_Date?: string;
  decision_Comment?: string[];
  remark?: string[];
}
