import { ComponentFixture, TestBed } from '@angular/core/testing'

import { SummaryTableDayViewComponent } from './summary-table-day.component'

describe('SummaryTableDayViewComponent', () => {
  let component: SummaryTableDayViewComponent
  let fixture: ComponentFixture<SummaryTableDayViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SummaryTableDayViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryTableDayViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
