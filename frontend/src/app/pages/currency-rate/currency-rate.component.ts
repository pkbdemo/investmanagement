import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';

import { LayoutService } from '../layout/services/layout.service';

@Component({
  selector: 'app-currency-rate',
  templateUrl: './currency-rate.component.html',
  styleUrls: ['./currency-rate.component.css']
})

export class CurrencyRateComponent implements OnInit {

  expandSet = new Set<number>();
  listOfResultData: any[];
  listOfData: any[];
  listOfDataFilter: any[];
  listOfPicCompany: any[];
  listOfPic: any[];
  listOfCurrency: any[];
  date = null;
  listOfSecurityValue: string[] = [];
  searchValue: string = '最新匯率';
  ddlCurrencyFrom = '';
  ddlCurrencyTo = '';
  userName: string = '';
  userId: string = '';
  transferAreaIsVisible = false;
  currentDate = new Date();
  switchValueNTD = false;
  switchValueUSD = false;
  filterNTD = 'NTD';
  filterUSD = 'USD';
  resultVisible = false;
  dateRange = ['輸入時間 YYYY-MM', '輸入時間 YYYY-MM'];
  sortDirection;
  collapseActive = true;

  constructor(private httpClient: HttpClient,
    private layoutService: LayoutService,
    private toastr: ToastrService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.QueryNewest();
    this.QueryCurrency();
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  updateActive() { }

  CloseResult(result: boolean): void {
    this.resultVisible = result;
    this.listOfResultData = [];
    this.collapseActive = true;
  }

  sortOnChange(event) {
    if (event == "descend") {
      this.search("descend");
    }
    else if (event == "ascend") {
      this.search("ascend");
    }
    else {
      this.search("");
    }
  }

  QuerySwitchNTD(): void {
    if (this.switchValueNTD) {
      if (this.switchValueUSD)
        this.switchValueUSD = false;
      this.listOfData = this.listOfDataFilter;
      this.listOfData = this.listOfData.filter(item => item.currency_to.indexOf(this.filterNTD) !== -1);
    }
    else {
      if (this.switchValueUSD) {
        this.listOfData = this.listOfDataFilter;
        this.listOfData = this.listOfData.filter(item => item.currency_to.indexOf(this.filterUSD) !== -1);
      }
      else
        this.listOfData = this.listOfDataFilter;
    }
  }

  QuerySwitchUSD(): void {
    if (this.switchValueUSD) {
      if (this.switchValueNTD)
        this.switchValueNTD = false;
      this.listOfData = this.listOfDataFilter;
      this.listOfData = this.listOfData.filter(item => item.currency_to.indexOf(this.filterUSD) !== -1);
    }
    else {
      if (this.switchValueNTD) {
        this.listOfData = this.listOfDataFilter;
        this.listOfData = this.listOfData.filter(item => item.currency_to.indexOf(this.filterNTD) !== -1);
      }
      else
        this.listOfData = this.listOfDataFilter;
    }
  }

  QueryNewest(): void {
    this.httpClient.get<any>(environment.apiServerURL + "Currency/QueryNewestNTDUSD").subscribe({
      next: (res) => {
        this.listOfDataFilter = res;
        this.listOfData = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  QueryCurrency(): void {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryCurrencyList").subscribe({
      next: (res) => {
        if (res != null) this.listOfCurrency = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  search(sortDirection: string): void {
    //check required data
    if (this.date == null || this.date.length <= 0) {
      this.toastr.warning("起訖日不得為空");
      return;
    }

    let formData1 = new FormData();
    formData1.set("currencyFrom", this.ddlCurrencyFrom);
    formData1.set("currencyTo", this.ddlCurrencyTo);
    formData1.set("periodFrom", this.datePipe.transform(this.date[0], "yyyyMM"));
    formData1.set("periodTo", this.datePipe.transform(this.date[1], "yyyyMM"));

    this.httpClient.post<any>(environment.apiServerURL + "Currency/QueryCurrencyRate", formData1).subscribe({
      next: (res) => {
        if (res.length > 0) {
          if (sortDirection == 'ascend') {
            var sortedArray = res.sort((a, b) => a.period.localeCompare(b.period));
            this.resultVisible = true;
            this.listOfResultData = sortedArray;
          }
          else {
            this.resultVisible = true;
            this.listOfResultData = res;
          }
        }
        else {
          this.resultVisible = false;
          this.toastr.warning("查無匯率資料");
          return;
        }
        this.collapseActive = false;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  onExpandChange(contact_ao: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(contact_ao);
    } else {
      this.expandSet.delete(contact_ao);
    }
  }

  ExpandClick(contact_ao: number): void {
    if (this.expandSet.has(contact_ao)) {
      this.expandSet.delete(contact_ao);
    }
    else
      this.expandSet.add(contact_ao);
  }

  showTransferAreaModal(userid, name_a) {
    this.userId = userid;
    this.userName = name_a;
    let formData1 = new FormData();
    formData1.set("userid", userid);
    let formData2 = new FormData();
    formData2.set("userid", userid);
    formData2.set("deptid", 'WS1100');

    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryNameByContactAo", formData1).subscribe({
      next: (res) => {
        this.listOfPicCompany = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryFindwHcmEmpByDeptExcludeSearchId", formData2).subscribe({
      next: (res) => {
        this.listOfPic = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
    this.transferAreaIsVisible = true;
  }
}
