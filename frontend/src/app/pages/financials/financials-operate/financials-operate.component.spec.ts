import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialsOperateComponent } from './financials-operate.component';

describe('FinancialsOperateComponent', () => {
  let component: FinancialsOperateComponent;
  let fixture: ComponentFixture<FinancialsOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialsOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialsOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
