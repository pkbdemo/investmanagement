import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import { ActivatedRoute, Router } from '@angular/router';
import { toNumber } from 'ng-zorro-antd/core/util';
import { environment } from '@environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { LayoutService } from '../../layout/services/layout.service';
import { RequestService } from '../../amount-record/services/requestData.service';
import { CompanyService } from '../../company-maintain/services/company.service';
import { FinancialsService } from '../financials.service';
import { codeNameItem } from '../../amount-record/amountMaintainModel';
import { Account, AccountItem, FinancialsName, RequestFinancials } from '../financialsModel';
import moment from 'moment';
import { IMSConstants } from 'src/app/utils/IMSConstants';

@Component({
  selector: 'app-financials-operate',
  templateUrl: './financials-operate.component.html',
  styleUrls: ['./financials-operate.component.css']
})
export class FinancialsOperateComponent implements OnInit {
  financials_No: number;
  //為true => Add   false => Edit
  isAddOrEdit: boolean = true;

  //時間軸參數
  param = {
    invest_no: '',
    financials_no: ''
  };

  financialsName: FinancialsName = {
    name: "",
    nick_Name: "",
    invest_No: 0,
    fin_Year_Rule: ""
  }
  finanicalsNo: number;

  //捨棄窗口
  isVisible = false;

  //標題展示
  dataName: string = '媽呀咨詢';

  //表單下拉數據源
  resultsSub: Subscription;
  quarterList: codeNameItem[] = [];
  quarterSource: codeNameItem[] = [];
  quarterSub: Subscription;
  statementList: codeNameItem[] = [];
  statementSub: Subscription;
  currencyList: string[] = [];
  currencyListSub: Subscription;

  //編輯時的唯读内容
  endDateSource: string | null = null;
  startDateSource: string | null = null;
  financialsYearSource: string | null = null;
  quarterReadSource: string | null = null;

  //會計科目資料源
  AccountList: AccountItem[] = [];

  //新增時的唯讀內容
  endDateSourceWhenCreate: string | null = null;
  startDateSourceWhenCreate: string | null = null;
  nextReportStartDateWhenCreate: string | null = null;
  nextReportEndDateWhenCreate: string | null = null;

  //表单
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '該項為必填項'
    }
  };

  //當前可選最大日期
  maxDate: Date | null = null;

  //彈窗内容
  isShowModal: boolean = false;
  isShowFinYearRuleModal: boolean = false;
  modalTitle: string = '';
  modalHeader: string = '';
  modalDetail: string = '';
  oldFinancials_No: number | null = null;
  //校驗三符合開關
  isShowModal3: boolean = false;

  @ViewChild('endDatePicker') endDatePicker!: NzDatePickerComponent;
  //折叠面板樣式
  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  validateForm: FormGroup;

  get AccountControls(): FormGroup {
    if (this.validateForm.contains('Account')) {
      return this.validateForm.get('Account') as FormGroup;
    }
    return null;
  }

  set editDataSource(data: any) {
    if (!!data) {
      this.insertView(data);
      this.editDataSub.unsubscribe();
    }
  }

  editDataSub: Subscription;

  //歷史資料區域
  financialsService: FinancialsService;
  listOfColumn: any;
  accountList: string[] = [];
  listSource: any[] = [];

  //页面初加载对金额进行基础校验
  AmountInitValidator = (control: FormControl): { [s: string]: boolean } => {
    var r = new RegExp('^([0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)|([-][0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)$');
    if (control.value) {
      if (!r.test(control.value.toString())) {
        return { confirm: true, error: true };
      }
    }
    return {};
  };

  constructor(private route: ActivatedRoute,
    private layoutService: LayoutService,
    private companyService: CompanyService,
    public router: Router,
    private fb: FormBuilder,
    private requestService: RequestService,
    private message: NzMessageService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private location: Location) { }

  ngOnInit(): void {

    this.financialsService = new FinancialsService(this.httpClient, this.toastr);
    this.financialsService.listOfColumn$.subscribe(e => this.listOfColumn = e);
    this.financialsService.AccountList$.subscribe(e => this.accountList = e as string[]);
    this.financialsService.dataList$.subscribe(e => this.listSource = e);
    this.getQuarterType();
    this.getStatementType();
    this.getCurrency();
    this.getMaxDate();
    this.getAccounts();
    this.validateForm = this.fb.group({

      Quarter: ['', [Validators.required]],
      Financial_Year: ['', [Validators.required]],
      Financial_Statement: ['', [Validators.required]],
      Currency: ['', [Validators.required]],
      Unit: [1, [Validators.required]],
      Remark: ['', []]
      // Report_Start_Date: ['', []],
      // Report_End_Date: ['', []]
    });
    //由財年+Quarter+公司財年制度決定報表期初&報表期末&下季報表期初&下季報表期末
    this.validateForm.get('Quarter').valueChanges.subscribe(data => {
      if (!!data) {
        var ddl_Financial_Year = this.validateForm.get('Financial_Year').value;
        if (ddl_Financial_Year != "" && this.isAddOrEdit) {
          this.CalculateReportStartEndDate(ddl_Financial_Year, data);
        }
      }
      else if (this.isAddOrEdit) {
        this.startDateSourceWhenCreate = "";
        this.endDateSourceWhenCreate = "";
      }
    });

    this.validateForm.get('Financial_Year').valueChanges.subscribe(data => {
      if (!!data) {
        var ddl_Quarter = this.validateForm.get('Quarter').value;
        if (ddl_Quarter != "" && this.isAddOrEdit) {
          this.CalculateReportStartEndDate(data, ddl_Quarter);
        }
      }
      else if (this.isAddOrEdit) {
        this.startDateSourceWhenCreate = "";
        this.endDateSourceWhenCreate = "";
      }
    });

    //根據financials_No 是否有值來判斷為新增還是編輯
    this.route.queryParams.subscribe(params => {
      let sFinancialsNo = params["financials_No"];
      let invest_No = params["invest_no"]
      this.param.invest_no = invest_No;
      if (!!invest_No) {
        Promise.all([
          this.financialsService.getFinancialsName(toNumber(invest_No.trim()))]).then((result) => {
            this.financialsService.objName$.subscribe(e => {
              this.financialsName = e;
              this.param.invest_no = e.invest_No.toString();
              //根據invest_no 來獲取歷史資料
              this.financialsService.search(e.invest_No);
            });
          });
      }

      if (!!sFinancialsNo) {
        this.isAddOrEdit = false;
        this.financials_No = toNumber(sFinancialsNo.trim());
        this.param.financials_no = this.financials_No.toString();
        this.financialsService.getFinancialsView(this.financials_No);
        this.getFinancialsSource();
      } else this.isAddOrEdit = true;
    });
  }

  //計算報表期初&報表期末&下季報表期初&下季報表期末，四個日期
  CalculateReportStartEndDate(ddl_Financial_Year: any, data: any): void {
    let formData = new FormData();
    formData.set("invest_no", this.param.invest_no);
    //檢查是否已經有存在invest_no, 不可有重複的invest_no資料
    this.httpClient.post<any>(environment.apiServerURL + "Financials/QueryFinYearRule", formData).subscribe({
      next: (res) => {
        //YYYY-MM-DD HH:mm:ss
        //YYYY-MM-DD
        var StartDateFormat = "YYYY-MM-DD";
        var EndDateFormat = "YYYY-MM-";
        //公司財年制度: 001:一般財年  002:二月財年  003:四月財年
        switch (res) {
          case "001"://一般財年
            //001:Q1  002:Q2  003:Q3  004:Q4
            if (data == "001" || data == "Q1") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).startOf('Q').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q')).daysInMonth();
            }
            if (data == "002" || data == "Q2") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q')).daysInMonth();
            }
            if (data == "003" || data == "Q3") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q')).daysInMonth();
            }
            if (data == "004" || data == "Q4") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).startOf('Q').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).endOf('Q').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q')).daysInMonth();
            }

            break;
          case "002"://二月財年
            //001:Q1  002:Q2  003:Q3  004:Q4
            if (data == "001" || data == "Q1") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q').add(1, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q').add(1, 'M')).daysInMonth();
            }
            if (data == "002" || data == "Q2") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q').add(1, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q').add(1, 'M')).daysInMonth();
            }
            if (data == "003" || data == "Q3") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q').add(1, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q').add(1, 'M')).daysInMonth();
            }
            if (data == "004" || data == "Q4") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q').add(1, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).startOf('Q').add(1, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).endOf('Q').add(1, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q').add(1, 'M')).daysInMonth();
            }
            break;
          case "003"://四月財年
            //001:Q1  002:Q2  003:Q3  004:Q4
            if (data == "001" || data == "Q1") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(1).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q').add(3, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q').add(3, 'M')).daysInMonth();
            }
            if (data == "002" || data == "Q2") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(2).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(2).endOf('Q').add(3, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q').add(3, 'M')).daysInMonth();
            }
            if (data == "003" || data == "Q3") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(3).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(3).endOf('Q').add(3, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q').add(3, 'M')).daysInMonth();
            }
            if (data == "004" || data == "Q4") {
              this.startDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.endDateSourceWhenCreate = moment(ddl_Financial_Year).quarter(4).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(4).endOf('Q').add(3, 'M')).daysInMonth();
              this.nextReportStartDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).startOf('Q').add(3, 'M').format(StartDateFormat);
              this.nextReportEndDateWhenCreate = moment(ddl_Financial_Year).add(1, 'y').quarter(1).endOf('Q').add(3, 'M').format(EndDateFormat) + moment(moment(ddl_Financial_Year).quarter(1).endOf('Q').add(3, 'M')).daysInMonth();
            }
            break;
        }
        if (!this.isAddOrEdit)
          this.CheckFinYearRule(this.startDateSourceWhenCreate, this.endDateSourceWhenCreate);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  customPrecisionFn(value: number, precision: number): number {
    console.log(value, precision);
    console.log(+Number(value).toFixed(precision + 1));
    return +Number(value).toFixed(precision + 1);
  }

  //獲取當前最大日期
  getMaxDate() {
    let date = new Date(Date.now());
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDay();
    let maxD = this.getDay(m, y);
    if (d != maxD) {
      if (m == 12) {
        y++;
        m = 1;
      }
      this.maxDate = new Date(Date.parse(y + '-' + m + '-' + '01'));
    } else this.maxDate = date;
  }

  //獲取每月最大天數
  getDay(month: number, year: number): number {
    let day1 = [1, 3, 5, 7, 8, 10, 12];

    let set1 = new Set<number>();
    day1.forEach(e => set1.add(e));
    if (set1.has(month)) return 31;
    if (year % 4 != 0 && month == 2) return 28;
    if (year % 4 == 0 && year % 100 != 0 && month == 2) return 29;
    if (year % 4 == 0 && year % 100 == 0 && month == 2) return 29;
    return 30
  }

  submitForm(): void {
    if (this.validateForm.status == "INVALID") {
      Object.keys(this.validateForm.controls).forEach(key => {
        const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
        if (validationErrors != null) {
          Object.keys(validationErrors).forEach(keyError => {
            console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
          });
        }
      });
    }
    if (this.validateForm.valid) {
      this.checkEndDate(this.validateForm.value);

    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach(item => {
              if (item.invalid) {
                item.markAsDirty();
                item.updateValueAndValidity({ onlySelf: true });
              }
            });
          }
        }
      });
    }
  }

  //儲存校驗邏輯1
  /**
   * 校驗1.1
   * 檢核company_financials_ytd；
   * 校驗1.2
   * 檢核輸入的資料，是否DB Table有重複的invest_no、financial_year、quarter資料
   */
  checkEndDate(data) {
    let request = {
      Financials_No: this.finanicalsNo,
      IsAddOrEdit: this.isAddOrEdit,
      // Type: data.Report_TY,
      Invest_no: this.financialsName.invest_No,
      Report_End_Date: this.financialsService.timestampToTime(data.Report_End_Date),
      Financial_Year: data.Financial_Year.getFullYear().toString(),
      Quarter: data.Quarter
    }

    this.httpClient.post<any>(environment.apiServerURL + "Financials/CheckEndDate", request).subscribe({
      next: (res) => {

        if (res.type != '0') {
          this.isShowModal = true;
          this.isShowModal3 = false;
          this.modalTitle = '已存在此筆資料，無法存儲';
          this.modalHeader = '以下重複資料已存在：';
          switch (res.type) {
            //儲存校驗邏輯2
            /**
             * 校驗2
             * 檢核是否有invest_no、financial_year、quarter重複資料
             */
            case '1':
              this.modalTitle = '已存在此筆，無法儲存';
              this.modalHeader = '以下重複資料已存在：';
              this.oldFinancials_No = res.value;
              this.isShowModal3 = true;
              this.quarterList.forEach(e => {
                if (e.value == res.quarter) this.modalDetail = `財年：${request.Financial_Year}，Quarter：${e.label}`;
              });
              break;
          }
        } else {
          this.oldFinancials_No = res.value;
          this.EditData(this.validateForm.value);
        }
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

  EditData(data) {
    let request = this.dataMate(data);
    console.log(request, '>>>>>>>>>>>>>');

    this.httpClient.post<any>(environment.apiServerURL + "Financials/DataEdit", request).subscribe({
      next: (res) => {
        console.log(res);
        if (res == "") { this.createMessage('success', '已儲存變更'); this.goback(); }
        else { this.message.create('error', res.toString()); }
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

  //數據匹配
  dataMate(data): RequestFinancials {
    let request: RequestFinancials = {};
    request.OldFinancials_No = this.oldFinancials_No;
    request.Invest_No = this.financialsName.invest_No;
    request.Report_Start_Date = this.financialsService.timestampToTime(this.startDateSourceWhenCreate);
    request.Report_End_Date = this.financialsService.timestampToTime(this.endDateSourceWhenCreate);
    request.Next_Report_Start_Date = this.financialsService.timestampToTime(this.nextReportStartDateWhenCreate);
    request.Next_Report_End_Date = this.financialsService.timestampToTime(this.nextReportEndDateWhenCreate);
    request.Quarter = data.Quarter;
    request.Financial_Year = data.Financial_Year?.getFullYear();
    request.Currency = data.Currency;
    request.Financial_Statement = data.Financial_Statement;
    request.Unit = data.Unit;
    request.Concerns = data.Remark;
    let AccountList: Account[] = [];
    Object.getOwnPropertyNames(data.Account).forEach((item, index) => {
      let AccountItem: Account = {};
      AccountItem.Account_Id = parseInt(item.replace(/[^\d]/g, ' '));
      AccountItem.Amount = parseInt(data.Account[item]);
      AccountList.push(AccountItem);
    });
    request.AccountList = [...AccountList];
    return request;
  }

  //編輯時獲取元數據
  getFinancialsSource() {
    this.editDataSub = this.financialsService.Financials$.subscribe(e => this.editDataSource = e);
  }

  //元數據填充頁面
  insertView(dataSource) {
    //頁面寫入數值
    this.financialsName = {
      name: dataSource.name,
      nick_Name: dataSource.nickName,
      invest_No: dataSource.invest_No,
      fin_Year_Rule: this.financialsName.fin_Year_Rule
    };
    this.finanicalsNo = dataSource.financials_No;
    // this.reportSource = dataSource.report_Type_NM;
    // this.validateForm.get('Report_TY').setValue(dataSource.report_Type);
    this.endDateSource = this.financialsService.timestampToTime(dataSource.report_End_Date);
    // this.validateForm.get('Report_End_Date').setValue(new Date(Date.parse(dataSource.report_End_Date)));
    this.startDateSource = this.financialsService.timestampToTime(dataSource.report_Start_Date);
    // this.validateForm.get('Report_Start_Date').setValue(new Date(Date.parse(dataSource.report_Start_Date)));
    this.quarterReadSource = dataSource.quarter;
    this.validateForm.get('Quarter').setValue(dataSource.quarter);
    switch (dataSource.quarter) {
      case IMSConstants.Quarter_Type.Q1:
        this.quarterReadSource = "Q1";
        break;
      case IMSConstants.Quarter_Type.Q2:
        this.quarterReadSource = "Q2";
        break;
      case IMSConstants.Quarter_Type.Q3:
        this.quarterReadSource = "Q3";
        break;
      case IMSConstants.Quarter_Type.Q4:
        this.quarterReadSource = "Q4";
        break;
    }
    this.financialsYearSource = dataSource.financial_Year;
    this.validateForm.get('Financial_Year').setValue(new Date(Date.parse(dataSource.financial_Year)));
    this.validateForm.get('Financial_Statement').setValue(dataSource.financial_Statement);
    this.validateForm.get('Currency').setValue(dataSource.currency);
    this.validateForm.get('Unit').setValue(dataSource.unit);
    this.validateForm.get('Remark').setValue(dataSource.remark);
    this.CalculateReportStartEndDate(dataSource.financial_Year, dataSource.quarter);
    setTimeout(() => {
      if (this.validateForm.contains('Account')) {
        dataSource.accountList.forEach(e => {
          (this.validateForm.get('Account') as FormGroup).get(e.control_NM).setValue(e.amount);
        });
      }
    }, 500);
  }

  //編輯按鈕進入時，若報表期初、報表期末與上表結果不同時，表示[公司維護]更新公司財年制度欄位，需跳出警語，點選OK後變更畫面報表期初、報表期末、下季報表期初、下季報表期末欄位
  CheckFinYearRule(startDate: string, endDate: string) {
    if (startDate != this.startDateSource && endDate != this.endDateSource) {
      this.startDateSource = startDate;
      this.endDateSource = endDate;
      this.isShowFinYearRuleModal = true;
    }
  }
  //路由跳转
  goback() {
    this.location.back();
  }

  //獲取Quarter
  getQuarterType() {
    this.companyService.getCodeName('010');
    this.quarterSub = this.companyService.quarterListObservable.subscribe(item => {
      this.quarterSource = item as codeNameItem[]
      this.quarterList = item as codeNameItem[]
    });
  }

  //獲取Single or Con.
  getStatementType() {
    this.companyService.getCodeName('011');
    this.statementSub = this.companyService.statementListObservable.subscribe(item =>
      this.statementList = item as codeNameItem[]
    );
  }

  //獲取幣別
  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  //獲取Accounts
  getAccounts() {
    this.financialsService.getAccountS();
    this.financialsService.Account$.subscribe(e => {
      this.AccountList = e as AccountItem[];
      let obj = {};
      (e as AccountItem[]).forEach(item => {
        obj[item.control_NM] = [null, [item.required ? Validators.required : null, this.AmountInitValidator]];
      });
      this.validateForm.addControl('Account', this.fb.group(obj));
      this.validateForm.get('Account').updateValueAndValidity();
    });
  }

  //折叠面板点击事件
  updateActive() { }

  //捨棄事件
  giveUp(event) {
    event.preventDefault();
    this.isVisible = true;
  }

  //捨棄取消框
  handleCancel(): void {
    this.isVisible = false;
  }

  //捨棄確認框
  handleOk(): void {
    this.isVisible = false;
    this.goback();
  }

  removeOk() {
    this.EditData(this.validateForm.value);
  }

  //路由跳转
  routingSkip() {
    this.router.navigate(['companyMaintain/list']);
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //提示框
  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

}
