import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datePip'
})
export class DatePipPipe implements PipeTransform {

  transform(value: unknown, type?: string): string {
    if (!!value) {
      if (typeof value == 'string') {
        if (value == '無') return '無';
        if (value.indexOf('-') < 0 && type == 'transition') value = (value as string).substring(0, 4) + '-' + (value as string).substring(4, 6) + '-' + (value as string).substring(6, 8);
      }
      let date = new Date(value as string);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let d = date.getDate();
      let newDate = y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
      return newDate;
    } else return null;
  }

}
