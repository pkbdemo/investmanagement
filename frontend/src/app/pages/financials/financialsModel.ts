export interface FilterConditionItem {
  name: string;
  value: any;
}

export interface AccountItem {
  account_Id: number;
  control_NM: string;
  account_Type: string;
  account_Name: string;
  required: boolean;
}

export interface FinancialsName {
  name?: string;
  nick_Name?: string;
  invest_No?: number;
  fin_Year_Rule: string;
}

export interface RequestFinancials {
  OldFinancials_No?: number;
  Invest_No?: number;
  Report_TY?: string;
  Report_End_Date?: string;
  Quarter?: string;
  Report_Start_Date?: string;
  Financial_Year?: number;
  Currency?: string;
  Financial_Statement?: string;
  Unit?: number;
  Concerns?: string;
  AccountList?: Account[];
  Next_Report_End_Date?: string;
  Next_Report_Start_Date?: string;
}

export interface Account {
  Account_Id?: number;
  Control_NM?: string;
  Account_Type?: string;
  Account_Name?: string;
  Amount?: number;
}

export interface FinancialsListItem {
  invest_No: number;
  name: string;
  nickName: string;
  financials_No: number;
  report_End_Date: Date;
  report_Start_Date: Date;
  financial_Statement: string;
  fund_Type: string;
  financial_Year: string;
  currency: string;
  unit: string;
  quarter: string;
  quarter_NM: string;
  headquarter: string;
  remark: string;
  accountList: Array<AccountList>;
  contact_Ao: string;

}

export interface AccountList {
  account_Id: number;
  control_NM: string;
  account_Type: string;
  account_Name: string;
  amount: number;
}
