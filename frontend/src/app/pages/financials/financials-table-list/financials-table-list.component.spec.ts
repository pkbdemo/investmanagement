import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialsTableListComponent } from './financials-table-list.component';

describe('FinancialsTableListComponent', () => {
  let component: FinancialsTableListComponent;
  let fixture: ComponentFixture<FinancialsTableListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialsTableListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialsTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
