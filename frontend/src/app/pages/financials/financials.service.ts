import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from '@environments/environment';

import { FilterConditionItem, FinancialsName } from './financialsModel'

interface SearchResult {
  dataList: any[];
  total: number;
}


@Injectable({
  providedIn: 'root'
})
export class FinancialsService {

  private _pseudoPaging: boolean = false;
  private _dataSourse = null;
  private filterConditionValue: FilterConditionItem[] = [];
  private _listOfColumn$ = new Subject();
  private _AccountList$ = new Subject();
  private _dataList$ = new BehaviorSubject<any[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private _Account$ = new Subject();
  private _ConcatTo$ = new Subject();

  //存儲公司全程、簡稱、invest_no
  private _objName$ = new Subject<FinancialsName>();

  //存放編輯時元數據
  private _FinancialsSource$ = new Subject<any>();

  get dataList$() { return this._dataList$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get listOfColumn$() { return this._listOfColumn$.asObservable() }
  get Account$() { return this._Account$.asObservable(); }
  get AccountList$() { return this._AccountList$.asObservable(); }
  get objName$() { return this._objName$.asObservable(); }
  get ConcatTo$() { return this._ConcatTo$.asObservable(); }
  get Financials$() { return this._FinancialsSource$.asObservable(); }
  set filterCondition(filterCondition: FilterConditionItem[]) {
    this.filterConditionValue = filterCondition;
    if (filterCondition.length > 0) {
      this._pseudoPaging = true;
      this.search();
    }
    else this._pseudoPaging = false;
  }
  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) {
  }
  uploadRefresh(invest_no?: number) {
    this._dataSourse = null
    this.search(invest_no);
  }
  search(invest_no?: number) {
    let dataList: any[];
    let total: number;
    let needToGetData = this._pseudoPaging == false || this._dataSourse == null;
    if (needToGetData) {
      if (!invest_no) invest_no = -1;
      this.httpClient.post<any>(environment.apiServerURL + "Financials/QueryMaintenance", invest_no).subscribe({
        next: (res) => {
          let maxAccount = 0;
          let maxAccountItem;
          if (!!res) {
            res.forEach(e => {
              let obj = {};
              e.accountList.forEach(item => {
                if (!item[item.control_NM]) obj[item.control_NM] = item.amount;
              });
              e['Account'] = obj;
              maxAccountItem = e.accountList?.length > maxAccount ? e : maxAccountItem;
            });
          }
          this.getAccount(maxAccountItem);

          this._dataSourse = res;


          if (this._pseudoPaging == false) {
            dataList = res;
            total = res.length;
            this._total$.next(total);
          } else {
            dataList = this.getData_PseudoPaging();
          }
          this._dataList$.next(dataList);
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("request failed");
        }
      });
    }
    else {
      dataList = this.getData_PseudoPaging();
      this._dataList$.next(dataList);
    }

  }

  getAccount(maxAccountItem: any) {
    ///生成TableList 標題欄為
    let listOfColumn = [
      {
        title: '公司簡稱',
        compare: (a, b) => a.nickName > b.nickName ? 1 : -1,
        priority: 1
      },
      {
        title: '財年',
        compare: (a, b) => a.financial_Year > b.financial_Year ? 1 : -1,
        priority: 1
      },
      {
        title: 'Quarter',
        compare: (a, b) => a.quarter_NM > b.quarter_NM ? 1 : -1,
        priority: 1
      },
      {
        title: '報表期初',
        compare: (a, b) => a.report_Start_Date > b.report_Start_Date ? 1 : -1,
        priority: 1
      },
      {
        title: '報表期末',
        compare: (a, b) => a.report_End_Date > b.report_End_Date ? 1 : -1,
        priority: 1
      },
      {
        title: 'Single Or Con.',
        compare: (a, b) => a.financial_Statement_NM > b.financial_Statement_NM ? 1 : -1,
        priority: 1
      },
      {
        title: 'Currency',
        compare: (a, b) => a.currency > b.currency ? 1 : -1,
        priority: 1
      },
      {
        title: '金額單位',
        compare: (a, b) => a.unit > b.unit ? 1 : -1,
        priority: 1
      }
    ];
    let AccountList: string[] = [];
    maxAccountItem?.accountList?.forEach(e => {
      AccountList.push(e.control_NM);
      let obj;
      if (e.account_Id == 6) {
        obj = {
          title: e.account_Name,
          compare: null,
          titleName: e.control_NM,
          priority: 1
        }
      } else {
        obj = {
          title: e.account_Name,
          compare: (a, b) => a.Account[e.control_NM] > b.Account[e.control_NM] ? 1 : -1,
          titleName: e.control_NM,
          priority: 1
        }
      }
      listOfColumn = [...listOfColumn, obj];
    });
    listOfColumn = [...listOfColumn, {
      title: '關注事項',
      compare: null,
      priority: 0
    }];
    //發送 標題欄為以及 動態的金額欄位
    this._AccountList$.next(AccountList);
    this._listOfColumn$.next(listOfColumn);
  }

  getFinancialsName(invest_no: number) {
    this.httpClient.post<any>(environment.apiServerURL + "Financials/GetFinancialsName", invest_no).subscribe({
      next: (res) => {
        if (!!res) {
          this._ConcatTo$.next(res.contact_Ao);
          let obj: FinancialsName = {
            name: res.name,
            nick_Name: res.nickName,
            invest_No: res.invest_No,
            fin_Year_Rule: res.fin_Year_Rule
          }
          this._objName$.next(obj);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getFinancialsView(financials_no: number) {
    this.httpClient.post<any>(environment.apiServerURL + "Financials/GetFinancialsView", financials_no).subscribe({
      next: (res) => {
        if (!!res) this._FinancialsSource$.next(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  private getData_PseudoPaging(): any[] {
    let myData: any[] = this._dataSourse;
    let total = 0;
    if (myData != null) {
      total = myData.length;
      // 1. filter
      if (this.filterConditionValue != null && this.filterConditionValue.length > 0) {
        myData = this.filterConditionValue.reduce((prev, cur) => {
          if (cur.name == 'nickName') prev = prev.filter(data => data.nickName.toUpperCase().replace(/\s*/g, "").indexOf(cur.value.toUpperCase().replace(/\s*/g, "")) != -1);
          if (cur.name == 'currency') prev = prev.filter(data => data.currency == cur.value);
          if (cur.name == 'Date') prev = prev.filter(data => data.financial_Year >= cur.value.startDate && data.financial_Year <= cur.value.endDate);
          if (cur.name == 'quarter') {
            this.filterConditionValue.forEach(e => {
              if (e.name == 'Date') {
                prev = prev.filter(data => ((data.quarter >= cur.value.startQuarter && data.financial_Year >= e.value.startDate) && (data.quarter <= cur.value.endQuarter && data.financial_Year <= e.value.endDate)));
              }
            });
          }
          if (cur.name == 'userId') prev = prev.filter(data => data.contact_Ao.toUpperCase() == cur.value);
          total = prev.length;
          return prev;
          //
        }, myData);
        total = myData.length;
      }
    }

    this._total$.next(total);
    return myData;
  }

  getAccountS() {
    this.httpClient.get<any>(environment.apiServerURL + "Financials/GetAccounts").subscribe({
      next: (res) => {
        if (!!res) this._Account$.next(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //標準時間轉換
  timestampToTime(timestamp: string): string {
    if (timestamp != null && timestamp != '') {

      let date = new Date(timestamp);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let MM = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      let dd = d < 10 ? ('0' + d) : d;
      return y + '-' + MM + '-' + dd;
    }
    else {
      return null
    }
  }
}
