import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '@environments/environment';
import { Subscription } from 'rxjs';

import { Account, FinancialsListItem } from '../financialsModel';
import { CompanyService } from '../../company-maintain/services/company.service';
import { LayoutService } from '../../layout/services/layout.service';
import { codeNameItem } from '../../amount-record/amountMaintainModel';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-financials-view',
  templateUrl: './financials-view.component.html',
  styleUrls: ['./financials-view.component.css']
})
export class FinancialsViewComponent implements OnInit {

  //時間軸參數
  param = {
    invest_no: '',
    financials_no: ''
  };

  statementSub: Subscription;
  userId: string = '';
  isVisible = false;
  isShowBtn: boolean = false;

  //用於編輯頁面展示Table List展示
  invest_no: string;
  financials_no: string;
  click_from_company: string;

  financials_year: string;

  tableListData: FinancialsListItem = {
    invest_No: 0,
    name: '',
    nickName: '',
    financials_No: 0,
    report_End_Date: undefined,
    report_Start_Date: undefined,
    financial_Statement: '',
    fund_Type: '',
    financial_Year: '',
    currency: '',
    unit: '',
    quarter: '',
    quarter_NM: '',
    headquarter: '',
    remark: '',
    accountList: undefined,
    contact_Ao: ''
  };

  contact_Ao: Account[] = [];
  statementList: codeNameItem[] = [];

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel1 =
    {
      active: true,
      disabled: false,
      name: '會計科目',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel2 =
    {
      active: true,
      disabled: false,
      name: '備註',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel3 =
    {
      active: true,
      disabled: false,
      name: '財報資料時程',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  constructor(
    private layoutService: LayoutService,
    private companyService: CompanyService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    public router: Router,
    private location: Location,
    private message: NzMessageService) { }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      let sFinancialsNo = params['financials_No'];
      let invest_No = params['invest_no']
      this.param.invest_no = invest_No;
      this.invest_no = invest_No;
      if (!!sFinancialsNo) {
        this.param.financials_no = sFinancialsNo.toString();
        this.financials_no = sFinancialsNo.toString();
      }
      this.financials_no = params['financials_No'];
      this.click_from_company = params['clickFromCompany'];
      if (params['financials_No'] != null) this.getFinancialsViewList(params['financials_No']);
    });
  }

  getFinancialsViewList(financials_no: string) {
    //!開始把前面的html Detail 內容一個個放上去
    this.httpClient.post<any>(environment.apiServerURL + "Financials/GetFinancialsView", Number(financials_no)).subscribe({
      next: (res) => {
        this.tableListData = res;
        if (res.contact_Ao.toUpperCase() == this.userId && this.click_from_company == "true") this.isShowBtn = true;
        else this.isShowBtn = false;

        //獲取Single or Con.
        this.companyService.getCodeName('011');
        this.statementSub = this.companyService.statementListObservable.subscribe(item => {
          this.statementList = item as codeNameItem[];
          this.statementList = this.statementList.filter((e) => e.value == res.financial_Statement);
        });

        this.financials_year = res.financial_Year;
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

  updateActive() { }
  //刪除確認框
  handleOk(): void {
    let formData = new FormData();
    formData.set("financials_no", this.financials_no);
    formData.set("invest_no", this.invest_no);
    formData.set("financials_year", this.financials_year);

    console.log('Button ok clicked!');
    this.isVisible = false;
    this.httpClient.post<any>(environment.apiServerURL + "Financials/DeleteFinancials", formData).subscribe({
      next: (res) => {
        if (res == "") {
          this.message.create('success', "刪除成功!");
          this.goback();
        }
        else {
          this.message.create('error', res.toString());
        }
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

  //刪除取消框
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  //路由跳转
  goback() {
    this.location.back();
  }

  //路由跳轉到操作頁面
  routingOperate() {
    this.router.navigate(['financials/operate'], { queryParams: { "financials_No": this.financials_no, "invest_no": this.invest_no } });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }
}
