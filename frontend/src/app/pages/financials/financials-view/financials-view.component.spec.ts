import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialsViewComponent } from './financials-view.component';

describe('FinancialsViewComponent', () => {
  let component: FinancialsViewComponent;
  let fixture: ComponentFixture<FinancialsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FinancialsViewComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
