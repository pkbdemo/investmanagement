import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialsHeaderComponent } from './financials-header.component';

describe('FinancialsHeaderComponent', () => {
  let component: FinancialsHeaderComponent;
  let fixture: ComponentFixture<FinancialsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialsHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
