import { Component, OnInit } from '@angular/core';

import { LayoutService } from '../../layout/services/layout.service';

@Component({
  selector: 'app-financials-header',
  templateUrl: './financials-header.component.html',
  styleUrls: ['./financials-header.component.css']
})
export class FinancialsHeaderComponent implements OnInit {


  isShowVector: boolean = true;
  isShowUserVector: boolean = true;

  //儅選擇MyList 或 AllList 是需要傳入  否則為 '' 
  listSelected: string = 'My List';

  constructor(private layoutService: LayoutService,) { }

  ngOnInit(): void {
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }
}
