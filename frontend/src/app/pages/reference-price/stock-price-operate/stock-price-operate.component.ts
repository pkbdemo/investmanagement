import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from '../../layout/services/layout.service';
import { Location } from '@angular/common';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { distinctUntilChanged, ignoreElements, throttleTime } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@environments/environment';
import { PopupComponent } from '../../Utilities/popup/popup.component';
import { SaveDataItem } from '../stockPriceModel';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-stock-price-operate',
  templateUrl: './stock-price-operate.component.html',
  styleUrls: ['./stock-price-operate.component.css']
})
export class StockPriceOperateComponent implements OnInit {

  user_Id: string;
  user_Name: string;
  companyFullName: string;
  invest_No: string;

  dateFormat = IMSConstants.dateFormat4Tip;

  //页面唯读栏位
  transaction_Date: string;
  public_Suggest_Currency: string;
  public_Suggest_Price: string;
  private_Suggest_Currency: string;
  private_Suggest_Price: string;

  amount_Currency: string;
  amount: string;
  history_Currency: string;
  history_Price: string;

  stock_Price_No: number;

  set _amount(data: string) {
    if (!!data && !!this.history_Price) {
      this.amount = data;
      if (this.requiredSet.has('date')) this.requiredSet.delete('date');
      this.validateForm.get('date').disable();
    } else {
      if (!this.requiredSet.has('date')) this.requiredSet.add('date');
    }
  }

  set _history_Price(data: string) {
    if (!!data && !!this.amount) {
      this.amount = data;
      if (this.requiredSet.has('date')) this.requiredSet.delete('date');

      this.validateForm.get('date').disable();
    } else {
      if (!this.requiredSet.has('date')) this.requiredSet.add('date');
    }
  }

  //校验窗口展示
  checkContent: string;
  //页面下拉数据源
  currencyList: string[] = [];

  //页面必填标志
  requiredSet = new Set<string>();

  //表单
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '該項為必填項'
    }
  };

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  validateForm: FormGroup;
  @ViewChild('popup1') popup1: PopupComponent;
  constructor(private layoutService: LayoutService,
    private fb: FormBuilder,
    public router: Router,
    private location: Location,
    private commonMethodService: CommonMethodService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private route: ActivatedRoute,) {
    this.user_Name = sessionStorage.getItem("userName").toUpperCase();
    this.user_Id = sessionStorage.getItem("userID").toUpperCase();

    this.validateForm = this.fb.group({
      transaction_Date: [null, [Validators.required]],
      public_Actual_Currency: ['', []],
      public_Actual_Price: ['', []],
      private_Actual_Currency: ['', []],
      private_Actual_Price: ['', []],
      discount: ['', [Validators.required]],
      limit_Currency: ['', []],
      limit_Price: ['', []]
    });
    this.requiredSet.add('date');
    this.getCurrency();
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.companyFullName = params['companyFullName'];
      this.invest_No = params['invest_No'];
      this.stock_Price_No = params["stock_Price_No"];
      if (!!this.stock_Price_No) {
        this.getView(this.stock_Price_No);
      }
    });

    this.validateForm.get('public_Actual_Currency').valueChanges
      .pipe(distinctUntilChanged())
      .subscribe(data => {
        if (!!data) {
          if (!this.validateForm.get('public_Actual_Price').validator) {
            this.requiredSet.add('publicPrice');
            this.validateForm.get('public_Actual_Price').setValidators(Validators.required);
            this.validateForm.get('public_Actual_Price').updateValueAndValidity();
          }
        } else {
          this.requiredSet.delete('publicPrice');
          this.validateForm.get('public_Actual_Price').clearValidators();
          this.validateForm.get('public_Actual_Price').updateValueAndValidity();
        }
      });

    this.validateForm.get('public_Actual_Price').valueChanges
      .pipe(distinctUntilChanged()).subscribe(data => {
        if (!!data) {
          if (!this.validateForm.get('public_Actual_Currency').validator) {
            this.requiredSet.add('publicCurrency');
            this.validateForm.get('public_Actual_Currency').setValidators(Validators.required);
            this.validateForm.get('public_Actual_Currency').updateValueAndValidity();
          }
        } else {
          if (data != this.validateForm.value['public_Actual_Currency']) { }
          this.requiredSet.delete('publicCurrency');
          this.validateForm.get('public_Actual_Currency').clearValidators();
          this.validateForm.get('public_Actual_Currency').updateValueAndValidity();
        }
      });

    this.validateForm.get('private_Actual_Currency').valueChanges.pipe(distinctUntilChanged()).subscribe(data => {
      if (!!data) {
        if (!this.validateForm.get('private_Actual_Price').validator) {
          this.requiredSet.add('privatePrice');
          this.validateForm.get('private_Actual_Price').setValidators(Validators.required);
        }
      } else {
        this.requiredSet.delete('privatePrice');
        this.validateForm.get('private_Actual_Price').clearValidators();
      }
      this.validateForm.get('private_Actual_Price').updateValueAndValidity();

    });

    this.validateForm.get('private_Actual_Price').valueChanges.pipe(distinctUntilChanged()).subscribe(data => {
      if (!!data) {
        if (!this.validateForm.get('private_Actual_Currency').validator) {
          this.requiredSet.add('privateCurrency');
          this.validateForm.get('private_Actual_Currency').setValidators(Validators.required);
        }
      } else {
        this.requiredSet.delete('privateCurrency');
        this.validateForm.get('private_Actual_Currency').clearValidators();
      }
      this.validateForm.get('private_Actual_Currency').updateValueAndValidity();

    });

    this.validateForm.get('limit_Price').valueChanges.pipe(distinctUntilChanged()).subscribe(data => {
      if (!!data) {
        if (!this.validateForm.get('limit_Currency').validator) {
          this.requiredSet.add('limit_Currency');
          this.validateForm.get('limit_Currency').setValidators(Validators.required);
        }
      } else {
        this.requiredSet.delete('limit_Currency');
        this.validateForm.get('limit_Currency').clearValidators();
      }
      this.validateForm.get('limit_Currency').updateValueAndValidity();
    });

    this.validateForm.get('limit_Currency').valueChanges.pipe(distinctUntilChanged()).subscribe(data => {
      if (!!data) {
        if (!this.validateForm.get('limit_Price').validator) {
          this.requiredSet.add('limit_Price');
          this.validateForm.get('limit_Price').setValidators(Validators.required);
        }
      } else {
        this.requiredSet.delete('limit_Price');
        this.validateForm.get('limit_Price').clearValidators();
      }
      this.validateForm.get('limit_Price').updateValueAndValidity();

    });
  }

  getView(stock_Price_No: number) {
    let httpParams = new HttpParams().set("stock_Price_No", stock_Price_No);
    //Attention!!! If have multi parameters, it should be
    //let httpParams = new HttpParams().set("param1", "value1").set("param2", "value2").set("param3", "value3");

    let options = { params: httpParams };
    this.httpClient.get<any>(environment.apiServerURL + 'ReferencePrice/FindView', options).subscribe({
      next: (res) => {
        if (!!res) {
          console.log(res);
          this.writeData(res);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  //数据写入逻辑
  writeData(data: any) {
    this.invest_No = data.invest_No;
    this.companyFullName = data.company_Full_Name;
    this.transaction_Date = data.transaction_Date;
    this.validateForm.get('transaction_Date').setValue(data.transaction_Date);
    this.public_Suggest_Currency = data.public_Suggest_Currency;
    this.public_Suggest_Price = data.public_Suggest_Price;
    this.validateForm.get('public_Actual_Currency').setValue(data.public_Actual_Currency);
    this.validateForm.get('public_Actual_Price').setValue(data.public_Actual_Price);
    this.private_Suggest_Currency = data.private_Suggest_Currency;
    this.private_Suggest_Price = data.private_Suggest_Price;
    this.validateForm.get('private_Actual_Currency').setValue(data.private_Actual_Currency);
    this.validateForm.get('private_Actual_Price').setValue(data.private_Actual_Price);
    this.validateForm.get('discount').setValue(!!data.discount ? data.discount * 100 : null);
    this.amount_Currency = data.net_Worth_Currency;
    this.amount = data.net_Worth_Price;
    this.history_Currency = data.history_Currency;
    this.history_Price = data.history_Price;
    this.validateForm.get('limit_Currency').setValue(data.limit_Currency);
    this.validateForm.get('limit_Price').setValue(data.limit_Price);
  }

  submitForm(): void {
    if (this.validateForm.status == "INVALID") {
      Object.keys(this.validateForm.controls).forEach(key => {
        const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
        if (validationErrors != null) {
          Object.keys(validationErrors).forEach(keyError => {
            console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
          });
        }
      });
    }
    if (this.validateForm.valid) {
      this.checkSaveData(this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //栏位校验
  checkSaveData(data: any) {
    let formData = new FormData();
    formData.set('stock_price_no', !!this.stock_Price_No ? this.stock_Price_No.toString() : null);
    formData.set('invetstNo', this.invest_No);
    formData.set('transaction_Date', this.commonMethodService.timestampToTime(data.transaction_Date || this.transaction_Date));
    this.httpClient.post<boolean>(environment.apiServerURL + "ReferencePrice/CheckSaveData", formData).subscribe({
      next: (res) => {
        if (res) {
          this.checkContent = data.transaction_Date || this.transaction_Date;
          this.popup1.openModel();
        } else {
          this.saveData(data);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  //儲存邏輯
  saveData(data: any) {
    let request = this.dataJoin(data);
    console.log(request);

    const url = !!this.stock_Price_No ? 'ReferencePrice/Update' : 'ReferencePrice/Create';
    this.httpClient.post<number>(environment.apiServerURL + url, request).subscribe({
      next: (res) => {
        if (res != 0) {
          this.createMessage('success', '已儲存變更');
          this.routingViewSkip(data);
        } else {
          this.message.create('error', '儲存失敗');
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    })
  }

  //數據拼接
  dataJoin(data: any) {
    let dataItem: SaveDataItem = {};
    dataItem.Stock_Price_No = !!this.stock_Price_No ? this.stock_Price_No : null;
    dataItem.Invest_No = this.invest_No.toString();
    dataItem.Transaction_Date = this.commonMethodService.timestampToTime(data.transaction_Date || this.transaction_Date);
    dataItem.Public_Actual_Currency = data.public_Actual_Currency;
    dataItem.Public_Actual_Price = !!data.public_Actual_Price ? data.public_Actual_Price : null;
    dataItem.Private_Actual_Currency = data.private_Actual_Currency;
    dataItem.Private_Actual_Price = !!data.private_Actual_Price ? data.private_Actual_Price : null;
    dataItem.Discount = !!data.discount ? data.discount : null;
    dataItem.Limit_Currency = data.limit_Currency;
    dataItem.Limit_Price = !!data.limit_Price ? data.limit_Price : null;
    return dataItem;
  }

  //獲取幣別
  getCurrency() {
    this.commonMethodService.RequestCurrencyList();
    this.commonMethodService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }


  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //路由跳转
  goback() {
    this.location.back();
  }

  routingViewSkip(data: any) {
    // this.router.navigate(['referencePrice/view'], { queryParams: { "stock_Price_No": data.stock_Price_No} });
    this.location.back();
  }

  //提示框
  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  updateActive() { }
}
