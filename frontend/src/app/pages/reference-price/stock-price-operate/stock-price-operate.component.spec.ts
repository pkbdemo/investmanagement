import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockPriceOperateComponent } from './stock-price-operate.component';

describe('StockPriceOperateComponent', () => {
  let component: StockPriceOperateComponent;
  let fixture: ComponentFixture<StockPriceOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockPriceOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockPriceOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
