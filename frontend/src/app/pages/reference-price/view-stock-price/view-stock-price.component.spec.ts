import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStockPriceComponent } from './view-stock-price.component';

describe('ViewStockPriceComponent', () => {
  let component: ViewStockPriceComponent;
  let fixture: ComponentFixture<ViewStockPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewStockPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStockPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
