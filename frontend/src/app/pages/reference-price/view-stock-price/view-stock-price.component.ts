import { HttpClient, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import { Component, OnInit, Type } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { environment } from '@environments/environment';
import { KeycloakService } from 'keycloak-angular';
import { toNumber } from 'ng-zorro-antd/core/util';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { NzMessageService } from 'ng-zorro-antd/message';

import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { LayoutService } from '../../layout/services/layout.service';
import { StockPriceDetail } from '../stockPriceModel';
import { StockPriceItem } from '../stockPriceModel';
import { SaveDataItem } from '../stockPriceModel';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { timestamp } from 'rxjs';

@Component({
  selector: 'app-view-stock-price',
  templateUrl: './view-stock-price.component.html',
  styleUrls: ['./view-stock-price.component.css']
})
export class ViewStockPriceComponent implements OnInit {

  stockPriceDetail: StockPriceDetail;
  loginUserId = "";
  investNo: number;
  stock_Price_No: number;
  companyInfo: any;
  unitPic: string;
  canRenameCompany = false;
  canEditPrice = false;
  statusList = [];
  tabTitle_company = "Company";
  tabTitle_financials = "Financials";
  tabTitle_FMV = "FMV&%";
  tabTitle_Shares = "參考股價";
  tabTitle_amountRecord = "金額紀錄";
  tabTitle_DDHistory = "DD History";
  tabTitle_companyFinanceMapping = "公司財務對應表";
  sessionStorageKey_selectedTabIndex = IMSConstants.sessionStorageKey.selectedTabIndex.viewCompany;
  selectedTabIndex = 0;
  selectedTabTitle = this.tabTitle_company;
  selectedCompany = true;
  detail: StockPriceItem = {}

  //控制新增或修改页面展示
  isAddOrEdit: boolean = true;

  companyType = Type;
  userRole: any;

  companyPanel_1 = { active: true }
  companyPanel_2 = { active: true }
  companyPanel_3 = { active: true }
  companyPanel_4 = { active: true }

  constructor(private layoutService: LayoutService,
    private modal: NzModalService,
    private route: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private keycloakService: KeycloakService,
    private authorityControlService: AuthorityControlService,
    private commonMethodService: CommonMethodService,
    private message: NzMessageService,
    private location: Location) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  async ngOnInit(): Promise<void> {
    if (await this.keycloakService.isLoggedIn()) {
      await this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }

    this.authorityControlService.getUserRole().then(
      userRole => {
        this.userRole = userRole;
      }
    );



    this.route.queryParams.subscribe(params => {
      this.stock_Price_No = params["stock_Price_No"];
      this.investNo = params["investNo"];
      let sInvestNo = params["investNo"];
      if (sInvestNo != null && sInvestNo.trim() != '') {
        this.investNo = toNumber(sInvestNo.trim());
      }
    });

    await this.getStockPrice();

    let lastSelectedTabIndex = sessionStorage.getItem(this.sessionStorageKey_selectedTabIndex);
    if (lastSelectedTabIndex != null && lastSelectedTabIndex != "") {
      this.selectedTabIndex = Number.parseInt(lastSelectedTabIndex);
    }
  }
  getStockPrice() {
    let httpParams = new HttpParams().set("stock_Price_No", this.stock_Price_No);
    //Attention!!! If have multi parameters, it should be
    //let httpParams = new HttpParams().set("param1", "value1").set("param2", "value2").set("param3", "value3");

    let options = { params: httpParams };
    this.httpClient.get<any>(environment.apiServerURL + 'ReferencePrice/FindView', options).subscribe({
      next: (res) => {
        this.detail = res

        this.authorityControlService.getUserRole().then(
          userRole => {
            if (this.loginUserId == this.detail.contact_Ao) {
              this.canEditPrice = true;
            }
          }
        );
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  goBack() {
    this.location.back();
  }

  updateActive() {

  }

  editCompany(stock_Price_No: number) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "stock_Price_No": stock_Price_No
      }
    }
    this.router.navigate(['referencePrice/operate'], navigationExtras);
  }

  getCompanyInfo() {
    this.httpClient.get<any>(environment.apiServerURL + "Company/GetCompanyDetail/" + this.investNo).subscribe({
      next: (res) => {
        console.log(res)
        this.companyInfo = res;
        if (!!res.unitPicList && res.unitPicList.length > 0) {
          this.unitPic = res.unitPicList.map(e => e.name_A).toString();
        }
        this.authorityControlService.getUserRole().then(
          userRole => {
            if (userRole.isIT || userRole.isManagerOfIMD) {
              this.canRenameCompany = true;
            }
            
            if (this.loginUserId == this.companyInfo.contact_Ao) {
            }
          }
        );
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  updateCompanyName(data: string) {
    if (this.companyInfo != null) {
      this.companyInfo.name = data;
    }
  }

  getCompanyStatusList() {
    this.httpClient.get<any>(environment.apiServerURL + "Company/GetCompanyStatusList/" + this.investNo).subscribe({
      next: (res) => {
        this.statusList = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(err.error.title);
      }
    });
  }

  selectedIndexChange(index: number): void {
    if (index == 0) {
      this.selectedCompany = true;
      this.selectedTabTitle = this.tabTitle_company;
    } else if (index == 1) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_financials;
    } else if (index == 2) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_amountRecord;
    } else if (index == 3) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_DDHistory;
    }

    sessionStorage.setItem(this.sessionStorageKey_selectedTabIndex, index.toString());
  }

  //清除彈窗
  showDeleteConfirm(e: MouseEvent): void {
    e.preventDefault();
    this.modal.confirm({
      nzTitle: '是否清除此筆資料的紀錄內容?',
      nzContent: '<span style="color: #3F4852;">清除的紀錄內容包含：<br>一般股價-實際，私募股價-實際，<br>私/上市(%)，指定股價</span>',
      nzOkText: '清除紀錄',
      nzOkDanger: true,
      nzOnOk: () => {
        this.saveData(this.detail.stock_Price_No);
        this.router.navigate(['referencePrice/view'], { queryParams: { "stock_Price_No": this.detail.stock_Price_No } });
      },
      nzCancelText: '取消', nzCloseIcon: '',
      nzMaskClosable: false,
      nzClassName: 'DDModal',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  saveData(stock_Price_No: number) {
    let dataForm = new FormData();
    dataForm.set("stock_price_no", stock_Price_No.toString())
    this.httpClient.post<number>(environment.apiServerURL + "ReferencePrice/Clear", dataForm).subscribe({
      next: (res) => {
        if (res != 0) {
          this.createMessage('success', '已清除');
        } else {
          this.message.create('error', '清除失敗');
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    })
  }


  //提示框
  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

}
