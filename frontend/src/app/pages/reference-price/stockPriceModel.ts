import { StringMappingType } from "typescript";

export interface StockPriceItem {
    stock_Price_No?: number;
    company_Full_Name?: string;
    discount4display?: number;
    invest_No?: number;
    emp_Name?: string;
    pic?: string;
    transaction_Date?: Date;
    public_Actual_Currency?: string;
    public_Actual_Price?: number;
    private_Actual_Currency?: string;
    private_Actual_Price?: number;
    discount?: number;
    public_Suggest_Currency?: string;
    public_Suggest_Price?: number;
    private_Suggest_Currency?: string;
    private_Suggest_Price?: number;
    net_Worth_Currency?: string;
    net_Worth_Price?: number;
    history_Currency?: string;
    history_Price?: number;
    limit_Currency?: string;
    limit_Price?: number;
    deptid?: string;
    contact_Ao?: string;
    create_User?: string;
    create_Date?: Date;
    update_User?: string;
    update_Date?: Date;

}


export interface StockPriceDetail {
    compayFullaName?: string;
    invest_No?: number;
    emp_Name?: string;//英文名
    transaction_Date?: Date;//交易日期
    //一般股价
    public_Actual_Currency?: string;
    public_Actual_Price?: number;
    //私募股價
    private_Actual_Currency?: string;
    private_Actual_Price?: number;

    discount?: number;
    net_Worth_Currency?: string;
    net_Worth_Price?: number;

    //历史股价
    history_Currency?: string;
    history_Price?: number;
    //指定股价
    limit_currency?: string;
    limit_Price?: number;

    create_User?: string;
    create_Date?: Date;
    update_User?: string;
    update_Date?: Date;
}

export interface SaveDataItem {
    Stock_Price_No?: number;
    Invest_No?: string;
    Transaction_Date?: string;
    Public_Actual_Currency?: string;
    Public_Actual_Price?: number;
    Private_Actual_Currency?: string;
    Private_Actual_Price?: number;
    Discount?: number;
    Limit_Currency?: string;
    Limit_Price?: number;
}