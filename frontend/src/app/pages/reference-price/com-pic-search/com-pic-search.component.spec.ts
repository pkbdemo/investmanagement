import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComPicSearchComponent } from './com-pic-search.component';

describe('ComPicSearchComponent', () => {
  let component: ComPicSearchComponent;
  let fixture: ComponentFixture<ComPicSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComPicSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComPicSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
