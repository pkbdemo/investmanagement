import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2';
import { environment } from '@environments/environment';

import { COMPICItem } from '../../../utils/utility-component-model';
import { IMSConstants } from 'src/app/utils/IMSConstants';
@Component({
  selector: 'app-com-pic-search',
  templateUrl: './com-pic-search.component.html',
  styleUrls: ['./com-pic-search.component.css']
})
export class ComPicSearchComponent implements OnInit {
  @Output() chosenUnitPICItem = new EventEmitter<COMPICItem[]>();

  selectVisible = false;
  alertVisible = false;
  boPICListVisible = false;
  // boPICList: BOPICItem[] = [];
  comPICList: COMPICItem[] = []
  set checkListValue(data: COMPICItem[]) {
    console.log(data);
    this.chosenBOPIC = data;
    this.setOfCheckedEmplid.clear();
    data.forEach(e => {
      this.setOfCheckedEmplid.add(e);
    })
  }
  get checkListValue() {
    return this.chosenBOPIC;
  }
  chosenBOPIC: COMPICItem[] = [];

  searchDeptid: string = '';
  searchEmplidorEnName: string = '';
  setOfCheckedEmplid = new Set<COMPICItem>();

  searchValue: string = '';

  searchSimple: boolean = false;
  checked = false;
  _this = this;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {

  }

  AfterOpen = () => {
    this.checkListValue = this.checkListValue;
  }

  initBOPIC(data?: COMPICItem[]) {
    this.selectVisible = true;
    this.checkListValue = data;
    this.checked = false;
  }

  handleOk(): void {
    if (this.checkListValue.length <= 0) {
      this.selectVisible = false;
      Swal.fire(IMSConstants.swalTitle.error, "你沒選擇任何事業體窗口", "error").then((result) => { this.selectVisible = true });
    }

    this.chosenUnitPICItem.emit(this.checkListValue);
    this.selectVisible = false;
  }

  handleCancel(): void {
    this.selectVisible = false;
  }

  onSearch(): void {
    this.searchSimple = true;
    this.getVendorList(this.searchValue);
  }

  onBOPICChecked(boPICItem: COMPICItem, checked: boolean): void {
    console.log(boPICItem, checked);
    if (checked && !this.checkListValue.some(e => e == boPICItem)) {
      this.setOfCheckedEmplid.add(boPICItem);
      this.checkListValue.push(boPICItem)
    }
    else {
      this.setOfCheckedEmplid.delete(boPICItem);
      this.checkListValue = this.checkListValue.filter(e => e != boPICItem);
    }
  }

  getVendorList(searchValue: string) {
    let formData1 = new FormData();
    formData1.set("searchValue", searchValue);
    this.httpClient.post<any>(environment.apiServerURL + "ReferencePrice/GetComPICList", formData1).subscribe({
      next: (res) => {
        this.searchSimple = false
        console.log(res, '>>>>>>>.');

        this.comPICList = [...res];
        console.log(this.comPICList);
        if (this.comPICList.length == 0)
          this.boPICListVisible = false;
        else
          this.boPICListVisible = true;
      },
      error: (err) => {
        console.error(err);
      }
    });
  }
  //刪除
  deleteUnit(comPicItem: COMPICItem) {
    if (comPicItem != null) {
      this.checkListValue = this.checkListValue.filter(e => e != comPicItem);
    }
  }


  onAllChecked(checked: boolean): void {
    this.checkListValue = [];
    this.setOfCheckedEmplid.clear();

    if (checked) {
      let fixedTable = this.comPICList.slice(0, 100);
      console.log(fixedTable);
      fixedTable.forEach(value => {
        console.log(value);
        this.setOfCheckedEmplid.add(value);
        this.checkListValue.push(value)
      });
    }
  }
}
