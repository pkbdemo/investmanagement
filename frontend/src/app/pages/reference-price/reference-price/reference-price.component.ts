import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LayoutService } from '../../layout/services/layout.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { COMPICItem } from 'src/app/utils/utility-component-model';
import { differenceInCalendarDays, setHours } from 'date-fns';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import moment from 'moment';
import * as FileSaver from 'file-saver';
import { Workbook } from 'exceljs/dist/exceljs.min.js'

@Component({
  selector: 'app-reference-price',
  templateUrl: './reference-price.component.html',
  styleUrls: ['./reference-price.component.css']
})
export class ReferencePriceComponent implements OnInit {

  today: Date = null;
  validateForm!: FormGroup;
  footerRender = (): string => '查詢時間區段限制最大為一年'

  date = null;
  dateFormat = IMSConstants.dateFormat4Tip;
  unitPicList: COMPICItem[] = [];//

  constructor(
    private layoutService: LayoutService,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      dateValue: [[], [Validators.required]],
      searchName: [null, []]
    });
  }

  disabledDate = (current: Date): boolean => {
    if (!!this.today) {
      return differenceInCalendarDays(current, this.today) > 0;
    }
    return false;
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  onChange(result: Date[]): void {
    if (!!result[0]) {
      this.today = new Date(result[0].getFullYear() + 1, result[0].getMonth(), result[0].getDate());
      console.log(this.validateForm.get('dateValue').value);
      if (!!this.validateForm.get('dateValue').value[1]) {
        let newDate = this.validateForm.get('dateValue').value[1];
        let oldDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()).getTime();
        if (oldDate > this.today.getTime()) {
          this.validateForm.get('dateValue').setValue([result[0], this.today]);
        }
      }
    }
    console.log('onChange: ', result);
  }

  chosenUnitPIC(data: any) {
    console.log(data);
    if (!!data) {
      this.unitPicList = data;
      this.checkSearchName();
    }
  }

  deleteUnit(item: COMPICItem) {
    if (item != null) {
      console.log(item);
      this.unitPicList.splice(this.unitPicList.indexOf(item), 1);
      this.checkSearchName();
    }
  }

  checkSearchName(): void {
    Object.keys(this.validateForm.controls).forEach(key => {
      if (key == "searchName" && this.unitPicList.length <= 0) {
        this.validateForm.get(key).setValidators(Validators.required);
        this.validateForm.get(key).markAsDirty();
        this.validateForm.get(key).updateValueAndValidity();
        return;
      }
      else if (key == "searchName" && this.unitPicList.length > 0) {
        this.validateForm.get(key).clearValidators();
        this.validateForm.get(key).updateValueAndValidity();
      }
    });
  }

  downloadExcel(excelData) {
    //Title, Header & Data
    const title = excelData.title;
    var headerRowContent = [];
    headerRowContent.push("公司全稱\r\nCompanyName");
    headerRowContent.push("投資部PIC\r\nInvestmentDepartmentPIC");
    headerRowContent.push("  日期  \r\n  Date  ");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("建議\r\nSuggestedPrice");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("實際\r\nPricingPractice");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("建議\r\nSuggestedPrice");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("實際\r\nPricingPractice");
    headerRowContent.push("私/上市(%)");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("淨值\r\nTotalLiabilitiesAndEquity");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("歷史\r\nHistoricalPrice");
    headerRowContent.push("幣別\r\nCurrency");
    headerRowContent.push("指定股價\r\nDesignatedPrice");
    headerRowContent.push("公司全稱");
    headerRowContent.push("公司全稱");
    headerRowContent.push("公司全稱");
    headerRowContent.push("公司全稱");
    headerRowContent.push("公司全稱");

    const header = headerRowContent
    const data = excelData.data;

    //Create a workbook with a worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('參考股價');

    //Add Row and formatting
    //一般股價
    worksheet.mergeCells('D1', 'G1');
    let titleRow = worksheet.getCell('D1');
    titleRow.value = "一般股價\r\nPrice"
    titleRow.font = {
      name: 'Calibri',
      size: 12,
      bold: true
    }
    titleRow.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true }


    // 私募股價
    worksheet.mergeCells('H1:K1');
    let dateCell = worksheet.getCell('H1');
    dateCell.value = "私募股價\r\nPrivatePlacementPrice";
    dateCell.font = {
      name: 'Calibri',
      size: 12,
      bold: true
    }
    dateCell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true }

    //Blank Row
    // worksheet.addRow([]);

    //Adding Header Row
    let headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true }
    })

    // Adding Data with Conditional Formatting
    data.forEach(d => {
      let row = worksheet.addRow(d);
      row.eachCell((cell, number) => {
        cell.font = {
          name: 'Calibri',
          size: 11
        }
        cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true }
      })
      let date = row.getCell(3);
      date.value = moment(date.value).format("YYYY-MM-DD");
    }
    );
    worksheet.spliceColumns(19, 5);
    this.AdjustColumnWidth(worksheet);
    worksheet.getColumn(1).width = 18;
    worksheet.getColumn(12).width = 15;
    worksheet.getRow(1).height = 50;
    //Generate & Save Excel File
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, title + '.xlsx');
    })

  }

  private AdjustColumnWidth(worksheet) {
    worksheet.columns.forEach(column => {
      const lengths = column.values.map(v => v.toString().length);
      const maxLength = Math.max(...lengths.filter(v => typeof v === 'number'));
      column.width = maxLength;
    });
  }

  submitForm(): void {
    if (this.validateForm.valid) {
      let formData = new FormData();
      var startEndDate = this.validateForm.get('dateValue').value;
      var startDateFormate = moment(startEndDate[0]).format("YYYY-MM-DD");
      var endDateFormate = moment(startEndDate[1]).format("YYYY-MM-DD");
      this.unitPicList.forEach(x => {
        formData.append("arrInvestNo", x.invest_No);
      })
      formData.set("startDate", startDateFormate);//"2022-07-10"
      formData.set("endDate", endDateFormate);//"2022-07-31"

      this.httpClient.post<any>(environment.apiServerURL + "ReferencePrice/DownloadReport", formData).subscribe({
        next: (res) => {
          if (!!res) {
            //接收返回資訊，整理成reportData Model
            var dataForExcel = [];
            res.forEach((row: any) => {
              dataForExcel.push(Object.values(row))
            })
            let reportData = {
              title: '參考股價Report ' + startDateFormate + '_' + endDateFormate,
              data: dataForExcel
              // headers: Object.keys(res[0])
            }
            this.downloadExcel(reportData);
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("request failed");
        }
      });
    } else {
      this.checkSearchName();
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          console.log(control);
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach(controlSon => {
              if (controlSon.invalid) {
                controlSon.markAsDirty();
                controlSon.updateValueAndValidity({ onlySelf: true });
                if (controlSon instanceof FormArray) {
                  Object.values(controlSon.controls).forEach(controlArray => {
                    if (controlArray.invalid) {
                      if (controlArray instanceof FormGroup) {
                        Object.values(controlArray.controls).forEach(o => {
                          if (o.invalid) {
                            o.markAsDirty();
                            o.updateValueAndValidity({ onlySelf: true });
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          }
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

}
