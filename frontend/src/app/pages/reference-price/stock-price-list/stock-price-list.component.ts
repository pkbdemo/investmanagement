import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { environment } from '@environments/environment';
import { KeycloakService } from 'keycloak-angular';

import { IMSConstants } from 'src/app/utils/IMSConstants';
import { StockPriceItem } from '../stockPriceModel';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { differenceInCalendarDays, setHours } from 'date-fns';

@Component({
  selector: 'app-stock-price-list',
  templateUrl: './stock-price-list.component.html',
  styleUrls: ['./stock-price-list.component.css']
})
export class StockPriceListComponent implements OnInit {

  @Input() invest_no: number;
  @Input() companyFullName: string;

  dateFormat = IMSConstants.dateFormat4Tip;
  isBoardSecretariatMember = false;
  loginUserId: string;
  canAddNewPrice = false;
  date: Date[] = [];
  today: Date = null;
  footerRender = (): string => '查詢時間區段限制最大為一年';
  listOfColumn = [
    {
      title: '日期',
      compare: (a, b) => a.calculate_Date > b.calculate_Date ? 1 : -1,
      priority: 1,
      col: 1
    },
    {
      title: '一般股價',
      priority: 1,
      col: 4
    },
    {
      title: '私募股價',
      priority: 1,
      col: 4
    },
    {
      title: '私/上市(%)',
      priority: 1,
      col: 1
    },
    {
      title: '淨值',
      priority: 1,
      col: 2
    }
  ];

  stockPriceList: StockPriceItem[];

  constructor(
    private httpClient: HttpClient,
    private authorityControlService: AuthorityControlService,
    private keycloakService: KeycloakService,
    private router: Router) { }

    async ngOnInit(): Promise<void> {

    if (await this.keycloakService.isLoggedIn()) {
      await this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }

    this.authorityControlService.getUserRole().then(
      userRole => {
        if (userRole.isBoardSecretariatMember) {
          this.isBoardSecretariatMember = true;
        }
      }
    );
    let formData = new FormData();
    formData.set("invetstNo", this.invest_no.toString());
    this.httpClient.post<any>(environment.apiServerURL + "ReferencePrice/FindInitSuggestedPrice", formData).subscribe({
      next: (res) => {
        this.stockPriceList = res;
        this.authorityControlService.getUserRole().then(
          userRole => {
            console.log(this.loginUserId);
            console.log(this.stockPriceList[0].contact_Ao);
            if (this.loginUserId == this.stockPriceList[0].contact_Ao) {
              this.canAddNewPrice = true;
            }
          }
        );
      },
      error: (err) => {
        console.error(err);
      }
    });



  }

  onChange(result: Date[]): void {
    if (!!result[0]) {
      this.today = new Date(result[0].getFullYear() + 1, result[0].getMonth(), result[0].getDate());
      console.log(this.date);
      if (!!this.date[1]) {
        let newDate = this.date[1];
        let oldDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()).getTime();
        if (oldDate > this.today.getTime()) {
          this.date = [result[0], this.today];
        }
      }
    }
    console.log('onChange: ', result);
  }

  disabledDate = (current: Date): boolean => {
    if (!!this.today) {
      return differenceInCalendarDays(current, this.today) > 0;
    }
    return false;
  }

  closeSelectValue() {
  }

  //跳轉到新增操作頁面
  routingOperate() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "invest_No": this.invest_no,
        "companyFullName": this.companyFullName
      },
      fragment: 'anchor'
    }
    this.router.navigate(['referencePrice/operate'], navigationExtras);
  }


  //跳转到详情
  viewStockPrice(detail: StockPriceItem) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "stock_Price_No": detail.stock_Price_No
      }
    }
    this.router.navigate(['referencePrice/view'], navigationExtras);
  }

}
