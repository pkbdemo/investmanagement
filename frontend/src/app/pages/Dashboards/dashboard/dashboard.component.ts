import { Component, OnInit } from '@angular/core';
import { environment } from '@environments/environment';

import { PowerbiService } from 'src/app/service/powerbi.service';
import { LayoutService } from '../../layout/services/layout.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private layoutService: LayoutService,
    private powerbiService: PowerbiService) { }

  ngOnInit(): void {
    this.powerbiService.showDashboard("report-container-dashboard", environment.dashboard_reportId);
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }
}
