export enum FundType { Add, Update, Delete, View, Custom }
export enum SecurityRequired { Stock_Type, Dd_Close_Date, Quit_Date, Quit_Reason }
export interface codeNameItem {
  name?: string;
  label: string;
  value: string;
  code_Extend_A?: string;
  code_Extend_B?: string;
  code_Extend_C?: string;
  checked?: boolean;
  disabled?: boolean;
}
export interface SecurityCodeItem {
  label?: string;
  value?: string;
  showDelete?: boolean;
}
export interface fundTableListItem {
  invest_No: number;
  name: string;
  contact_Ao: string;
  emp_Name: string;
  full_Name: string;
  commited_Currency: string;
  total_Commited_Amount: number;
  now_Status_Nm: string;
  status: string;
  first_Trans_Date: Date;
}
export interface tableListItem {
  invest_No: number;
  name: string;
  contact_Ao: string;
  emp_Name: string;
  nickname: string;
  fund_Period: string;
  fund_Period_Nm: string;
  commited_Currency: string;
  commited_Amount: string;
}
export interface fundListItem {
  invest_No: number;
  name: string;
  contact_Ao: string;
  emp_Name: string;
  nickname: string;
  fund_Period: string;
  fund_Period_Nm: string;
  commited_Currency: string;
  commited_Amount: string;
  vendor_Code: string;
  fund_Type: string;
  bo: string;
  bo_Pic: string;
  bo_Pic_Name: string;
  quit_Date: Date;
  quit_Reason: string;
  liability_Type: string;
  headquarter: string;
  remark: string;
}
export interface RequestFundModify {
  Invest_No?: number;
  Emp_Name?: string;
  Fin_Year_Rule?: string;
  Name?: string;
  Full_Name?: string;
  Initial_Holding_Ratio?: number;
  Fund_Period?: number;
  Investment_Period?: number;
  Harvest_Period?: number;
  Fund_Type?: string;
  Quit_Date?: string;
  Quit_Reason?: string;
  Liability_Type?: string;
  Headquarter?: string;
  Unit_Pic?: string[];
  SecurityList?: FundSecurity[];
  Close_Investment?: string;
}
export interface FundSecurity {
  Security_No?: string;
  Invest_No?: number;
  Effective_Date?: string;
  DD_Close_Date?: string;
  Commited_Currency?: string;
  Commited_Amount?: string;
  Remark?: string;
}
