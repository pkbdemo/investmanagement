import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments/environment';

import { fundListItem } from '../fundMaintainModel'; //RequestFundItem,
import { LayoutService } from '../../layout/services/layout.service';
import { CompanyService } from '../../company-maintain/services/company.service';
import { RequestService } from '../../amount-record/services/requestData.service';
import { codeNameItem } from '../../company-maintain/companyModel';


@Component({
  selector: 'app-fund-update',
  templateUrl: './fund-update.component.html',
  styleUrls: ['./fund-update.component.css']
})


export class FundUpdateComponent implements OnInit {



  boList: codeNameItem[] = []; boListSub: Subscription;
  fundPeriodList: codeNameItem[] = []; fundPeriodListSub: Subscription;
  liabilityTypeList: codeNameItem[] = []; liabilityTypeListSub: Subscription;
  headquarterList: string[] = []; headquarterListSub: Subscription;
  currencyList: string[] = []; currencyListSub: Subscription;

  userId: string = '';
  userName: string = '系統依據所選基金帶入';
  fundName: string = '';
  fundType: string = '系統依據所選基金帶入';
  fundTypeCode: string = '';
  listOfData: any[] = [];
  vendor_Code: string = '';
  modeName: string = '基金';
  invest_no: string = '';
  chosenBOPICEmplid: string = '';
  bo_pic: string = '';
  //刪除請求内容
  isVisible = false;
  tableListData: fundListItem = {
    invest_No: 0,
    name: '',
    contact_Ao: '',
    emp_Name: '',
    nickname: '',
    fund_Period: '',
    fund_Period_Nm: '',
    commited_Currency: '',
    commited_Amount: '',
    vendor_Code: '',
    fund_Type: '',
    bo: '',
    bo_Pic: '',
    bo_Pic_Name: '',
    quit_Date: undefined,
    quit_Reason: '',
    liability_Type: '',
    headquarter: '',
    remark: ''
  };

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel1 =
    {
      active: true,
      disabled: false,
      name: '進度狀況',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel2 =
    {
      active: true,
      disabled: false,
      name: '認購資訊',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  validateForm: FormGroup;

  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '該項為必填項'
    }
  };

  //驗證表單，判斷是否可以新增
  submitForm(): void {

    if (this.validateForm.valid) {
      // 驗證成功，用一個interface把資料承接完後，開始做Update
      // let data: RequestFundItem = {
      //   //想辦法把invest_no改成數值
      //   Invest_No: Number(this.invest_no),
      //   Nickname: '',
      //   Vendor_Code: (this.tableListData.vendor_Code != '' && this.tableListData.vendor_Code != null) ? this.tableListData.vendor_Code : (this.validateForm.get('vendor_Code').value != '') ? this.validateForm.get('vendor_Code').value : null,
      //   Fund_Type: '',
      //   Fund_Period: this.validateForm.get('fundPeriod').value,
      //   Commited_Currency: this.validateForm.get('currencyList').value,
      //   Commited_Amount: (this.validateForm.get('commitedAmount').value != null && this.validateForm.get('commitedAmount').value != '') ? this.validateForm.get('commitedAmount').value.toString() : null,
      //   Liability_Type: this.validateForm.get('liabilityType').value,
      //   Headquarter: this.validateForm.get('headquarter').value,
      //   Quit_Date: this.validateForm.get('quitDate').value == '' ? null : this.timestampToTime(this.validateForm.get('quitDate').value),
      //   Quit_Reason: this.validateForm.get('quitReason').value,
      //   Remark: this.validateForm.get('remark').value,
      //   Bo: this.validateForm.get('bo').value,
      //   Bo_Pic: (this.chosenBOPICEmplid != null && this.chosenBOPICEmplid != '') ? this.chosenBOPICEmplid.toString() : (this.bo_pic != '') ? this.bo_pic : null,

      //   Name: '',
      //   Contact_Ao: '',
      //   Emp_Name: '',
      //   Fund_Period_Nm: '',
      //   Bo_Pic_Name: ''
      // }
      // this.UpdateFund(data);

      console.log('submit', this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //修改
  // UpdateFund(data: RequestFundItem) {
  //   console.log(data);
  //   this.httpClient.post<any>(environment.apiServerURL + "Fund/Update", data).subscribe({
  //     next: (res) => {
  //       console.log(res);
  //       if (res) this.createMessage('success'); this.routingViewSkip(res);
  //     },
  //     error: (err) => {
  //       console.error(err);
  //       this.toastr.error("request failed");
  //     }
  //   });
  // }

  createMessage(type: string): void {
    this.message.create(type, `已儲存變更`);
  }

  constructor(
    private layoutService: LayoutService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    public router: Router,
    private location: Location,
    private fb: FormBuilder,
    private companyService: CompanyService,
    private requestService: RequestService,
    private message: NzMessageService) {
  }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      console.log(params['invest_no']);
      this.invest_no = params['invest_no'];
      if (params['invest_no'] != null) this.getFundTableList(params['invest_no']);
    });

    this.validateForm = this.fb.group({
      vendor_Code: ['', []],
      bo: ['', [Validators.required]],
      bo_pic: ['', []],
      fundPeriod: ['', [Validators.required]],
      quitDate: ['', []],
      quitReason: ['', []],
      currencyList: ['', []],
      commitedAmount: ['', []],
      liabilityType: ['', []],
      headquarter: ['', []],
      remark: ['', []]
    });
    this.getNameList();
    this.getBoList();
    this.getFundPeriodList();
    this.getLiabilityTypeList();
    this.getheadquarter();
    this.getCurrency();
  }

  updateActive() { }

  //Vender Code
  chosenVendorCode(data: string) {
    this.validateForm.get('vendor_Code').setValue(data);
  }

  //bo_pic
  chosenBOPIC(data: any) {
    console.log(data);
    this.chosenBOPICEmplid = data.emplid;
    this.validateForm.get('bo_pic').setValue(data.name_A);
  }

  //捨棄事件
  giveUp(event) {
    event.preventDefault();
    this.isVisible = true;
  }

  //捨棄取消框
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  //捨棄確認框
  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    this.routingSkip();
  }

  //路由跳转
  routingSkip() {
    // this.router.navigate(['fundMaintain/list']);
    this.location.back();
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  getNameList() {
    this.httpClient.get<any>(environment.apiServerURL + "DDiligence/QueryAll").subscribe({
      next: (res) => {
        if (res != null) {
          let objList = res.reduce((item, next) => {
            if (next.frozen_Type == '001' && next.ddType.search('007') == -1 && next.status == '006' && next.decision_Result == 'True' && next.category == 'Fund' && next.contact_Ao == this.userId.toUpperCase()) {

              item.push(next);
            }
            return item;
          }, []);
          this.listOfData = objList;

        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  nameChange(data: string) {
    this.fundName = data.split("|")[0];
    this.listOfData.forEach(e => {
      if (e.name == data.split("|")[0]) {
        this.userName = e.user_Name;
        this.fundType = e.string_Agg;
        this.fundTypeCode = e.ddType;
      }
      else {
        this.userName = "系統依據所選基金帶入";
        this.fundType = "系統依據所選基金帶入";
        this.fundTypeCode = "";
      }
    });
  }

  getBoList() {
    this.companyService.getCodeName('004');
    this.boListSub = this.companyService.boListObservable.subscribe(item => this.boList = item as codeNameItem[]);
  }
  getFundPeriodList() {
    this.companyService.getCodeName('007');
    this.fundPeriodListSub = this.companyService.fundPeriodObservable.subscribe(item => this.fundPeriodList = item as codeNameItem[]);
  }
  getLiabilityTypeList() {
    this.companyService.getCodeName('013');
    this.liabilityTypeListSub = this.companyService.liabilityTypeObservable.subscribe(item => this.liabilityTypeList = item as codeNameItem[]);
  }

  getheadquarter() {
    this.companyService.getheadquarter();
    this.headquarterListSub = this.companyService.headquarterListObservable.subscribe(item => this.headquarterList = item as string[]
    );
  }

  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  ngOnDestory() {
    this.boListSub.unsubscribe();
    this.fundPeriodListSub.unsubscribe();
    this.liabilityTypeListSub.unsubscribe();
    this.headquarterListSub.unsubscribe();
    this.currencyListSub.unsubscribe();
  }

  CheckQuitDate(date: Date) {
    if (date != null) {
      this.validateForm.get('quitReason').setValidators([Validators.required]);
      this.validateForm.get('quitReason').updateValueAndValidity();
    }
    else {
      this.validateForm.get('quitReason').clearValidators();
      this.validateForm.get('quitReason').updateValueAndValidity();
    }
  }

  checkAmount(num: number) {
    if (num != null && num > 0) {
      this.validateForm.get('currencyList').setValidators([Validators.required]);
      this.validateForm.get('currencyList').updateValueAndValidity();
    }
    else {
      this.validateForm.get('currencyList').clearValidators();
      this.validateForm.get('currencyList').updateValueAndValidity();
    }
  }

  //路由跳转
  routingViewSkip(data: number) {
    console.log(data);
    this.router.navigate(['fundMaintain/view'], { queryParams: { invest_no: this.invest_no, isShowBtn: true } })
  }

  //標準時間轉換
  timestampToTime(timestamp: string): string {
    console.log(timestamp);
    if (timestamp != null && timestamp != '') {

      let date = new Date(timestamp);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let MM = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      let dd = d < 10 ? ('0' + d) : d;
      return y + '-' + MM + '-' + dd;
    }
    else {
      return null
    }
  }

  getFundTableList(invest_no: string) {
    let formData = new FormData();
    formData.set("invest_no", invest_no);
    this.httpClient.post<any>(environment.apiServerURL + "Fund/QueryDetail", formData).subscribe({
      next: (res) => {
        if (res != null) {
          console.log(res);
          this.tableListData = res[0];
          this.validateForm.get('bo').setValue(res[0].bo_Code);
          this.bo_pic = res[0].bo_Pic;
          this.validateForm.get('bo_pic').setValue(res[0].bo_Pic_Name);
          this.validateForm.get('fundPeriod').setValue(res[0].fund_Period);
          this.validateForm.get('quitDate').setValue(res[0].quit_Date);
          this.validateForm.get('quitReason').setValue(res[0].quit_Reason);
          this.validateForm.get('currencyList').setValue(res[0].commited_Currency);
          this.validateForm.get('commitedAmount').setValue(res[0].commited_Amount);
          this.validateForm.get('liabilityType').setValue(res[0].liability_Type_Code);
          this.validateForm.get('headquarter').setValue(res[0].headquarter);
          this.validateForm.get('remark').setValue(res[0].remark);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }


}
