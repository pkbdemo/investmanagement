import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundUpdateComponent } from './fund-update.component';

describe('FundUpdateComponent', () => {
  let component: FundUpdateComponent;
  let fixture: ComponentFixture<FundUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FundUpdateComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
