import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments/environment';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { fundListItem, FundSecurity, FundType } from '../fundMaintainModel';
import { LayoutService } from '../../layout/services/layout.service';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import moment from 'moment';

@Component({
  selector: 'app-fund-view',
  templateUrl: './fund-view.component.html',
  styleUrls: ['./fund-view.component.css']
})
export class FundViewComponent implements OnInit {

  //刪除請求内容
  request: number[] = [];
  userId: string = '';
  isVisible = false;
  fundTitle: string = 'Fund';
  currencyTitle: string = '金額紀錄';
  ddHistoryTitle: string = 'DD History';
  CompanyFinanceMappingTitle: string = '公司財務對應表';
  FundFinancials: string = '基金淨值';
  FMVTitle: string = 'FMV&%';
  tabType: string = 'Fund';
  canRenameFund = false;
  canEditFund = false;
  selectedFund = true;
  sessionStorageKey_selectedTabIndex = IMSConstants.sessionStorageKey.selectedTabIndex.viewFund;
  selectedTabIndex = 0;
  fundInfo: any;
  unitPic: string;
  securityList: FundSecurity[] = [];
  fundType = FundType;
  statusList = [];
  tableListData: fundListItem = {
    invest_No: 0,
    name: '',
    contact_Ao: '',
    emp_Name: '',
    nickname: '',
    fund_Period: '',
    fund_Period_Nm: '',
    commited_Currency: '',
    commited_Amount: '',
    vendor_Code: '',
    fund_Type: '',
    bo: '',
    bo_Pic: '',
    bo_Pic_Name: '',
    quit_Date: undefined,
    quit_Reason: '',
    liability_Type: '',
    headquarter: '',
    remark: ''
  };

  //用於編輯頁面展示Table List展示
  invest_no: string = '';
  invest_no2: number = null;

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel1 =
    {
      active: true,
      disabled: false,
      name: '認購資訊',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  panel2 =
    {
      active: true,
      disabled: false,
      name: '單次認購',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  constructor(
    private layoutService: LayoutService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    public router: Router,
    private authorityControlService: AuthorityControlService,
    private location: Location) { }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      console.log(params['invest_no']);
      this.request.push(params['invest_no']);
      this.invest_no = params['invest_no'];
      this.invest_no2 = params['invest_no'];
      if (params['invest_no'] != null) this.getFundInfo(params['invest_no']);
    });

    let lastSelectedTabIndex = sessionStorage.getItem(this.sessionStorageKey_selectedTabIndex);
    if (lastSelectedTabIndex != null && lastSelectedTabIndex != "") {
      this.selectedTabIndex = Number.parseInt(lastSelectedTabIndex);
    }
  }

  getFundInfo(invest_no: string) {
    let formData = new FormData();
    formData.set("invest_no", invest_no);
    this.httpClient.post<any>(environment.apiServerURL + "Fund/GetFundDetail/", formData).subscribe({
      next: (res) => {
        console.log(res, '>>>>>>>>>>>')
        this.fundInfo = res;
        this.fundInfo.quit_Date = moment(this.fundInfo.quit_Date).format("YYYY-MM-DD")
        if (!!res.unitPicList && res.unitPicList.length > 0) {
          this.unitPic = res.unitPicList.map(e => e.name_A).toString();
        }
        this.authorityControlService.getUserRole().then(
          userRole => {
            if (userRole.isIT || userRole.isManagerOfIMD) {
              this.canRenameFund = true;
            }
            if ((userRole.isIT || res.contact_Ao.toUpperCase() == this.userId) && res.status != "003")
              this.canEditFund = true;
            else
              this.canEditFund = false;
          }
        );
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });

    this.httpClient.get<any>(environment.apiServerURL + "Fund/GetFundSecurityList/" + invest_no).subscribe({
      next: (res) => {
        console.log(res);
        if (!!res) {
          res.forEach(element => {
            const obj: FundSecurity = {}
            obj.Security_No = element.security_No;
            obj.Invest_No = element.invest_No;
            obj.Effective_Date = element.effective_Date;
            obj.DD_Close_Date = moment(element.dd_Close_Date, "YYYYMMDD").format("YYYY-MM-DD");
            obj.Commited_Currency = element.commited_Currency;
            obj.Commited_Amount = element.commited_Amount;
            obj.Remark = element.remark;

            this.securityList.push(obj);
          });
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  getFundTableList(invest_no: string) {
    let formData = new FormData();
    formData.set("invest_no", invest_no);
    this.httpClient.post<any>(environment.apiServerURL + "Fund/QueryDetail", formData).subscribe({
      next: (res) => {
        if (res != null) {
          this.authorityControlService.getUserRole().then(
            userRole => {
              if (userRole.isIT || userRole.isManagerOfIMD) {
                this.canRenameFund = true;
              }
              if (userRole.isIT || res[0].contact_Ao.toUpperCase() == this.userId)
                this.canEditFund = true;
              else
                this.canEditFund = false;
            }
          );
          console.log(res);
          this.tableListData = res[0];
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  updateActive() { }

  SelectedIndexChange(index: number): void {
    if (index == 0) {
      this.selectedFund = true;
      this.tabType = this.fundTitle;
    }
    else if (index == 1) {
      this.selectedFund = false;
      this.tabType = this.ddHistoryTitle;
    }
    else if (index == 2) {
      this.selectedFund = false;
      this.tabType = this.currencyTitle;
    }
    else if (index == 3) {
      this.selectedFund = false;
      this.tabType = this.FundFinancials;
    }
    else if (index == 4) {
      this.selectedFund = false;
      this.tabType = this.FMVTitle;
    }
    else if (index == 5) {
      this.selectedFund = false;
      this.tabType = this.CompanyFinanceMappingTitle;
    }

    sessionStorage.setItem(this.sessionStorageKey_selectedTabIndex, index.toString());
  }

  //路由跳转
  routingSkip() {
    // this.router.navigate(['fundMaintain/list']);
    this.location.back();
  }

  //路由跳轉到操作頁面
  routingUpdate() {
    this.router.navigate(['fundMaintain/operate'], { queryParams: { invest_no: this.invest_no } });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  updateFundName(data: string) {
    if (this.fundInfo != null) {
      this.fundInfo.name = data;
    }
  }

  getFundStatusList() {
    this.httpClient.get<any>(environment.apiServerURL + "Fund/GetFundStatusList/" + this.invest_no).subscribe({
      next: (res) => {
        this.statusList = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(err.error.title);
      }
    });
  }
}
