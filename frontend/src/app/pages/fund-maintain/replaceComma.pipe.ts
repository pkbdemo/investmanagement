import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceComma'
})
@Injectable()
export class ReplaceCommaPipe implements PipeTransform {

  transform(val: string): any {
    return val && val.replace(/,/g, '') || '';
  }
}
