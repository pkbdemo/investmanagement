import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'
import { codeNameItem } from '../../company-maintain/companyModel';
import { LayoutService } from '../../layout/services/layout.service';
import { environment } from 'src/environments/environment';
import { fundTableListItem } from '../fundMaintainModel';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { CompanyService } from '../../company-maintain/services/company.service';

@Component({
  selector: 'app-fund-list',
  templateUrl: './fund-list.component.html',
  styleUrls: ['./fund-list.component.css']
})
export class FundListComponent implements OnInit {
  listOfColumn = [
    {
      title: '基金簡稱',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.name > b.name ? -1 : 1,
      priority: 1
    },
    {
      title: '基金全稱',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.full_Name > b.full_Name ? -1 : 1,
      priority: 2
    },
    {
      title: '投資部PIC',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.emp_Name > b.emp_Name ? -1 : 1,
      priority: 3
    },
    {
      title: '投資階段',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.now_Status_Nm > b.now_Status_Nm ? -1 : 1,
      priority: 4
    },
    {
      title: '認購幣別',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.commited_Currency > b.commited_Currency ? -1 : 1,
      priority: 5
    },
    {
      title: '認購金額',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.total_Commited_Amount > b.total_Commited_Amount ? -1 : 1,
      priority: 6
    }
    ,
    {
      title: '初次投資日',
      compare: (a: fundTableListItem, b: fundTableListItem) => a.first_Trans_Date > b.first_Trans_Date ? -1 : 1,
      priority: 7
    }

  ];
  listSelected: string = 'My List';
  isShowVector: boolean = true;
  isShowUserVector: boolean = true;
  listOfData: any[] = [];
  listOfTmpData: any[] = [];
  // condition: string = '';
  searchValue: string = '';
  userId: string;
  canCreateFund = false;
  arrAccountTitleId: string[] = [];
  accountTitleList: codeNameItem[] = [];
  nowListStatus: string = "My List";
  // firstLoad = true;
  constructor(private httpClient: HttpClient,
    private layoutService: LayoutService,
    private toastr: ToastrService,
    private authorityControlService: AuthorityControlService,
    private companyService: CompanyService,
    public router: Router) { }

  ngOnInit(): void {
    this.QueryAll(this.listSelected);
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.authorityControlService.getUserRole().then(
      userRole => {
        if (userRole.isIT || (userRole.isIMDMember && !userRole.isManagerOfIMD)) {
          this.canCreateFund = true;
        }
      }
    );
    this.getKindList();
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  QueryAll(ListType: string): void {
    let formData = new FormData();
    formData.set("condition", this.searchValue);
    this.httpClient.post<any>(environment.apiServerURL + "Fund/Query", formData).subscribe({
      next: (res) => {
        this.listOfData = res;
        this.listOfTmpData = res;
        this.search(ListType);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  //My 或 All 觸發方法
  UpdateHeader(data?: string) {
    if (data == 'My List') {
      this.nowListStatus = "My List";
      data = this.userId;
    }
    else {
      this.nowListStatus = "All List";
      data = '';
    }
    this.listOfData = this.listOfTmpData.filter((item: fundTableListItem) => {
      return item.contact_Ao.toUpperCase().indexOf(data.toUpperCase()) !== -1
    });
  }

  //路由跳轉
  routingSkip(data: string) {
    console.log(data);
    this.router.navigate(['fundMaintain/view'], { queryParams: { invest_no: data, isShowBtn: true } })
  }

  //路由跳轉到新增頁面
  routingOperate() {
    this.router.navigate(['fundMaintain/operate']);
  }

  clearSelectedSecurityType() {
    this.arrAccountTitleId = [];
    this.accountTitleList.forEach(x => {
      x.checked = false;
    });
    this.UpdateHeader(this.nowListStatus);
  }

  AccountTitleSelect(data: codeNameItem) {
    if (data.checked) {
      this.arrAccountTitleId = [...this.arrAccountTitleId, data.label];
    } else {
      this.arrAccountTitleId = this.arrAccountTitleId.filter(d => d !== data.label);
    }
    if (this.arrAccountTitleId.length > 0)
      this.search(this.nowListStatus);
    else
      this.UpdateHeader(this.nowListStatus);
  }

  search(data?: string) {
    if (data == 'My List') data = this.userId;
    else data = '';
    this.listOfData = this.listOfTmpData.filter((item: fundTableListItem) => {
      var result = false;
      if (this.arrAccountTitleId.length > 0) {
        result = item.contact_Ao.toUpperCase().indexOf(data.toUpperCase()) !== -1 && this.arrAccountTitleId.indexOf(item.now_Status_Nm) !== -1;
      }
      else
        result = item.contact_Ao.toUpperCase().indexOf(data.toUpperCase()) !== -1;

      return result;
    });
  }

  getKindList() {
    this.companyService.getCodeName('007');
    this.companyService.fundPeriodList.subscribe(item => this.accountTitleList = item as codeNameItem[]);
  }
}
