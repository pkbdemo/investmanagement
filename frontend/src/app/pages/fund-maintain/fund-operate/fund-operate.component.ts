
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpClient, HttpParams } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { NzMessageService } from 'ng-zorro-antd/message';
import { environment } from '@environments/environment';

import { CompanyService } from '../../company-maintain/services/company.service';
import { LayoutService } from '../../layout/services/layout.service';
import { tableListItem } from '../../investigation/duediligence/duediligenceModel';
import { codeNameItem, RequestFundModify, FundSecurity, SecurityCodeItem, FundType } from '../fundMaintainModel';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { BOPICItem } from 'src/app/utils/utility-component-model';
import { FundSecurityComponent } from '../fund-security/fund-security.component';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import moment from 'moment';
import { PopupComponent } from '../../Utilities/popup/popup.component';

@Component({
  selector: 'app-fund-operate',
  templateUrl: './fund-operate.component.html',
  styleUrls: ['./fund-operate.component.css']
})
export class FundOperateComponent implements OnInit {
  getNeedToDeleteSecurity = 0;
  dataName: string = '*未命名基金';
  modeName: string = '基金';
  dateFormat = IMSConstants.dateFormat4Tip;
  user_Name: string;
  user_Id: string;
  userRole: any = {};

  @ViewChild('popup1') popup1: PopupComponent;
  @ViewChild('popup2') popup2: PopupComponent;
  //捨棄窗口
  isVisible = false;
  isCheckVisible = false;
  CheckDataVisible = false;
  isClickCard = false;

  //校驗彈窗内容
  checkDataType = new Set<number>();  //用來存放校驗步驟：1，2，3
  checkDataTitle: string;
  checkDataContent: string;
  checkDataContentList: string[] = [];

  //刪除卡片窗口
  isDelete = false;

  //表单
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '不是合法Email格式'
    }
  };
  validateForm: FormGroup;

  //修改状态下展示的内容
  name: string = '';
  name_a: string = '無';
  full_Name: string = '';
  fund_Period: string = '';
  commited_Currency: string = '';
  total_Commited_Amount: number = 0;
  first_Trans_Date: string = '';
  securityList_View: codeNameItem;
  isChecked: boolean = false;
  isShowCloseCheckBox = false;
  commited_Currency_Add: string = '-';
  commited_Amount_Add: number = 0;
  //建議Status
  suggest_status: string;
  set _suggest_status(data: string) {
    if (!!data) {
      this.suggest_status = data
      this.validateForm.get('status').setValidators(Validators.required);
      this.validateForm.get('status').setValue(data);
    }
  }
  invest_no: number = 0;

  //折叠面板
  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  //表单下拉选单元數據
  fundNameList: tableListItem[] = [];
  liabilityTypeList: codeNameItem[] = [];
  boList: codeNameItem[] = [];
  stageList: codeNameItem[] = [];
  headquarterList: string[] = [];
  finYearList: codeNameItem[] = [];

  fundNameListSub: Subscription;
  liabilityTypeListSub: Subscription;
  boListSub: Subscription;
  stageListSub: Subscription;
  headquarterListSub: Subscription;

  marketListSub: Subscription;
  finYearListSub: Subscription;
  fundTypeSub: Subscription;

  //Security 下拉選單樣式
  securityList: codeNameItem[] = [];
  securityListSub: Subscription;

  //存放選擇的新增的Security
  selectSecurityValue: SecurityCodeItem[] = [];

  setOfCheckedId = new Set<string>();
  //判斷修改Or新增 默認為新增
  operateType: FundType;
  FundType = FundType;

  hasQuitDate: boolean = false;
  hasQuiteDateAndAddSecurityValue: string = "";
  checkContent: string[] = [];
  //儲存數據
  unitPicList: BOPICItem[] = [];
  securityDataList: FundSecurity[] = [];
  securityDataListForCalculateAmount: FundSecurity[] = [];
  set _securityDataList(data: FundSecurity[]) {
    this.securityDataList = data.sort((a, b) => this.fundService.SortData(a, b));
  }

  get _securityDataList() {
    return this.securityDataList;
  }

  @ViewChild('fundSecurity') fundSecurity!: FundSecurityComponent;
  constructor(private layoutService: LayoutService,
    public router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private fundService: CompanyService,
    private authorityControlService: AuthorityControlService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService) {

  }

  ngOnInit(): void {
    this.name_a = sessionStorage.getItem("userName").toUpperCase();
    this.user_Id = sessionStorage.getItem("userID").toUpperCase();
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      full_Name: ['', [Validators.required]],
      initial_Holding_Ratio: ['', []],
      headquarter: ['', []],
      liabilityType: ['', []],
      fin_Year_Rule: ['', []],
      unit_Pic: ['', []],
      fund_Period: ['', [Validators.required]],
      investment_Period: ['', [Validators.required]],
      harvest_Period: ['', [Validators.required]],
      quit_Date: ['', []],
      quit_Reason: ['', []],
      close_Check: ['', []],
    });

    this.getFundNameList();
    this.getLiabilityTypeList();
    this.getBoList();
    this.getStageList();
    this.getheadquarter();
    this.getSecurity();
    this.getFinYear();

    this.validateForm.get('name').valueChanges.subscribe(data => {
      this.name = data;
      this.validateForm.get('full_Name').setValue(data);
      if (this.securityDataList.length > 0 && this.operateType == FundType.Add) {
        this.popup1.openModel();
        this.securityDataList = [];
        this._securityDataList = [];
      }

    });

    this.validateForm.get('close_Check').valueChanges.subscribe(data => {
      if (data == true) {
        this.isChecked = true;
        this.turnOnQuit();
      }
      else {
        this.isChecked = false;
        this.turnOffQuit();
      }
    });

    this.authorityControlService.getUserRole().then(
      userRole => {
        console.log(userRole);
        this.userRole = userRole;
        this.getFundNameList();
        this.route.queryParams.subscribe(async params => {
          if (params['invest_no'] != null && params['invest_no'] != '') {
            this.operateType = FundType.Update
            this.invest_no = params['invest_no'];
            this.getFundData(params['invest_no']);

          }
          else {
            this.operateType = FundType.Add
            this.validateForm.get('close_Check').setValue(false);
            this.turnOffQuit();
          }
        });
      }
    );
  }

  ngOnDestory() {
    this.fundNameListSub.unsubscribe();
    this.boListSub.unsubscribe();
    this.stageListSub.unsubscribe();
    this.headquarterListSub.unsubscribe();
  }

  //显示组件回传信息
  modifySecurity(data) {
    console.log(data);
    switch (data.type) {
      case FundType.Custom:
        this.fundSecurity.editVisible(true, FundType.Custom, this.userRole, this.name, data.value, data.index);
        break;
      case FundType.Update:
        this.fundSecurity.editVisible(true, FundType.Update, this.userRole, this.name, data.value, data.index);
        break;
      case FundType.Delete:
        this._securityDataList.splice(data.index, 1);
        if (this._securityDataList.length > 0)
          this.isShowCloseCheckBox = true;
        else {
          this.isShowCloseCheckBox = false;
          this.isChecked = false;
          this.turnOffQuit();
        }
        this.calculateCommitedAmount();
        break;
    }

  }

  //security的返回信息
  goBackData(data: any) {
    console.log(data, '新增返回');
    if (data.index == 0 || !!data.index) this.securityDataList.splice(data.index, 1);
    this._securityDataList.push(data.value);
    if (this._securityDataList.length > 0)
      this.isShowCloseCheckBox = true;
    else {
      this.isShowCloseCheckBox = false;
      this.isChecked = false;
      this.turnOffQuit();
    }
    this.calculateCommitedAmount();
  }

  calculateCommitedAmount() {
    this.isClickCard = true;
    if (this.securityDataList.length > 0) {
      var JsonString = JSON.stringify(this.securityDataList);
      let formData = new FormData();
      formData.append('securityDataList', JsonString);
      this.httpClient.post<any>(environment.apiServerURL + "Fund/CalculateAmount", formData).subscribe({
        next: async (res) => {
          if (res != null) {
            if (res.commited_Amount < 0) {
              this.checkContent = [];
              res.errorCurrencyList.forEach(element => {
                this.checkContent.push("There is not rate of[" + element.error_Ori_Currency + "] -> [" + element.error_New_Currency + "] at {" + element.error_Effective_Date + "}");
              });
              this.popup2.openModel();
            }
            else {
              this.commited_Currency_Add = res.commited_Currency;
              this.commited_Amount_Add = res.commited_Amount;
            }
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("Failed");
        }
      });
    }
    else {
      this.commited_Currency_Add = "-";
      this.commited_Amount_Add = 0;
    }
  }

  turnOnQuit() {
    this.validateForm.get('quit_Date').setValidators([Validators.required]);
    this.validateForm.get('quit_Reason').setValidators([Validators.required]);
    this.validateForm.get('quit_Date').enable();
    this.validateForm.get('quit_Reason').enable();
    this.validateForm.updateValueAndValidity();
  }

  turnOffQuit() {
    this.validateForm.get('quit_Date').setValue(null);
    this.validateForm.get('quit_Reason').setValue(null);
    this.validateForm.get('quit_Date').clearValidators();
    this.validateForm.get('quit_Reason').clearValidators();
    this.validateForm.get('quit_Date').disable();
    this.validateForm.get('quit_Reason').disable();
    this.validateForm.updateValueAndValidity();
  }

  //刪除事業單位
  deleteUnit(index: number) {
    if (index != null) {
      console.log(index);
      this.unitPicList.splice(index, 1);
    }
  }

  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  //修改時獲取信息
  getFundData(invest_no: number) {
    let formData = new FormData();
    formData.set("invest_no", invest_no.toString());
    this.httpClient.post<any>(environment.apiServerURL + "Fund/GetFundModifyModel", formData).subscribe({
      next: (res) => {
        console.log(res);
        if (res != null) {
          this.dataName = res.name;
          this.name = res.name;
          this.invest_no = res.invest_No;
          this.validateForm.get('name').setValue(res.name);
          this.validateForm.get('name').disable();   //唯读
          this.full_Name = res.full_Name;
          this.validateForm.get('full_Name').setValue(res.full_Name);
          // this.validateForm.get('full_Name').disable();   //唯读
          // this.name_a = res.contact_Ao_Nm;
          this.fund_Period = res.fund_Period;
          this.validateForm.get('fund_Period').setValue(res.fund_Period);
          if (!!res.unitPicList && res.unitPicList.length > 0) {
            this.unitPicList = res.unitPicList.map(e => {
              let obj: BOPICItem = {}
              obj.emplid = e.item_Value;
              obj.name_A = e.name_A;
              return obj;
            });
          }
          this.validateForm.get('initial_Holding_Ratio').setValue((res.initial_Holding_Ratio * 100).toFixed(2));
          this.validateForm.get('headquarter').setValue(res.headquarter);
          this.validateForm.get('liabilityType').setValue(res.liability_Type);
          this.validateForm.get('fin_Year_Rule').setValue(res.fin_Year_Rule);
          this.validateForm.get('investment_Period').setValue(res.investment_Period);
          this.validateForm.get('harvest_Period').setValue(res.harvest_Period);
          this.validateForm.get('quit_Date').setValue(res.quit_Date);
          this.validateForm.get('quit_Reason').setValue(res.quit_Reason);
          if (res.length > 0)
            this.validateForm.get('close_Check').setValue(true);
          else
            this.validateForm.get('close_Check').setValue(false);
          this.commited_Currency = res.commited_Currency;
          this.total_Commited_Amount = res.total_Commited_Amount;
          this.first_Trans_Date = res.first_Trans_Date;
          this.securityDataList = res.securityList.map(e => {
            let obj: FundSecurity = {};
            obj.Security_No = e.security_No;
            obj.Invest_No = e.invest_No;
            obj.Effective_Date = e.effective_Date;
            obj.DD_Close_Date = e.dd_Close_Date.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
            obj.Commited_Currency = e.commited_Currency;
            obj.Commited_Amount = e.commited_Amount;
            obj.Remark = e.remark;
            return obj;
          });
          if (this.securityDataList.length > 0) {
            this.isShowCloseCheckBox = true;
            if (res.status == "003") {
              this.isChecked = true;
            }
            else {
              this.isChecked = false;
              this.validateForm.get('close_Check').setValue(false);
              this.turnOffQuit();
            }

          }
          else {
            this.isShowCloseCheckBox = false;
            this.isChecked = false;
            this.turnOffQuit();
          }

        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //獲取基金全稱
  getFundNameList() {
    if (this.userRole.isIMDMember || this.userRole.isIT) {
      this.fundService.getFundNameList();
      this.fundNameListSub = this.fundService.fundNameListObservable.subscribe(item =>
        this.fundNameList = item as tableListItem[]
      );
    }
  }

  //獲取事業躰
  getBoList() {
    this.fundService.getCodeName('004');
    this.boListSub = this.fundService.boListObservable.subscribe(item => this.boList = item as codeNameItem[]);
  }

  //獲取公司所處階段
  getStageList() {
    this.fundService.getCodeName('012');
    this.stageListSub = this.fundService.stageListObservable.subscribe(item => this.stageList = item as codeNameItem[]);
  }

  //獲取國家
  getheadquarter() {
    this.fundService.getheadquarter();
    this.headquarterListSub = this.fundService.headquarterListObservable.subscribe(item => this.headquarterList = item as string[]
    );
  }

  //獲取基金種類
  getSecurity() {
    this.fundService.getCodeName('001');
    this.securityListSub = this.fundService.securityListObservable.subscribe(item => {
      this.securityList = item as codeNameItem[];
      this.securityList_View = (item as codeNameItem[]).filter(e => e.code_Extend_A == '002')[0]
    });
  }

  //獲取公司財年制度
  getFinYear() {
    this.fundService.getCodeName('020');
    this.finYearListSub = this.fundService.FinYearListObservable.subscribe(item => this.finYearList = item as codeNameItem[]
    );
  }

  //獲取設立形式
  getLiabilityTypeList() {
    this.fundService.getCodeName('013');
    this.fundTypeSub = this.fundService.liabilityTypeObservable.subscribe(item =>
      this.liabilityTypeList = item as codeNameItem[]);

  }

  submitForm(): void {
    if (this.validateForm.valid) {
      this.checkData1();
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          console.log(control);
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach(controlSon => {
              if (controlSon.invalid) {
                controlSon.markAsDirty();
                controlSon.updateValueAndValidity({ onlySelf: true });
                if (controlSon instanceof FormArray) {
                  Object.values(controlSon.controls).forEach(controlArray => {
                    if (controlArray.invalid) {
                      if (controlArray instanceof FormGroup) {
                        Object.values(controlArray.controls).forEach(o => {
                          if (o.invalid) {
                            o.markAsDirty();
                            o.updateValueAndValidity({ onlySelf: true });
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          }
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //1.檢核是否此公司有重複的合約生效日資料
  checkData1() {
    this.checkDataContentList = [];
    this.checkDataType.clear()
    let bool: boolean = false;
    this.checkDataTitle = '已存在此筆資料，無法儲存'
    this.checkDataContent = '以下重複資料已存在：'
    let contentList = [];
    this._securityDataList.forEach(item => {
      let newLength = this._securityDataList.filter(e => moment(e.Effective_Date).format("YYYYMMDD") == moment(item.Effective_Date).format("YYYYMMDD")).length;
      if (newLength > 1) {
        bool = true;
        if (!contentList.some(e => moment(e.Effective_Date).format("YYYYMMDD") == moment(item.Effective_Date).format("YYYYMMDD"))) {
          contentList.push({
            Effective_Date: item.Effective_Date
          })
        }
      }
    });
    if (contentList.length > 0) {
      contentList.forEach(item => {
        // ${this.fundService.timestampToTime(item.Effective_Date)}
        this.checkDataContentList.push(`合約生效日：${moment(item.Effective_Date).format("YYYY-MM-DD")}`);
      });
    }
    this.CheckDataVisible = bool;
    if (bool) this.checkDataType.add(1);
    else this.checkData2();

  }

  //2.檢核是否有尚未新增進DB的卡片，需提示訊息
  checkData2() {
    this.checkDataType.clear()
    var checkBool = false;
    if (this.securityDataList.length > 0) {
      this.securityDataList.forEach(item => {
        if (!item.Security_No)
          checkBool = true;
      });
      if (checkBool) {
        this.checkDataTitle = '注意！'
        this.checkDataContent = '單次認購卡片新增後，資料將不可刪除'
        this.checkDataType.add(2);
        this.CheckDataVisible = true;
      }
      else this.checkData3();
    }
    else {
      this.checkData3();
    }
  }

  //3.檢核是否有勾選Close Checkbox，需跳出凍結資料警語
  checkData3() {
    this.checkDataType.clear()
    if (this.isChecked == true) {
      this.checkDataTitle = '是否凍結資料？'
      this.checkDataContent = '請確認資料是否正確，若選擇凍結則無法再異動'
      this.checkDataType.add(3);
      this.CheckDataVisible = true;
    }
    else {
      this.saveFundData();
    }

  }

  handCheckCancel() {
    this.CheckDataVisible = false;
  }

  handCheckCloseOk() {
    this.CheckDataVisible = false;
    if (this.checkDataType.has(2)) this.checkData3();
  }

  handCheckOk() {
    this.CheckDataVisible = false;
    if (this.checkDataType.has(3)) { this.saveFundData(); }
  }

  //儲存事件
  saveFundData() {
    let request = this.saveDataJoin(this.validateForm.value);
    let url: string;
    switch (this.operateType) {
      case FundType.Add:
        url = 'Fund/Create';
        break
      case FundType.Update:
        url = 'Fund/Update';
        break;
    }
    this.httpClient.post(environment.apiServerURL + url, request).subscribe({
      next: (res) => {
        if (res) {
          this.createMessage('success', '已儲存變更');
          this.routingViewSkip((this.operateType == this.FundType.Update) ? this.invest_no : this.fundNameList.filter(x => x.name == this.validateForm.get('name').value)[0].invest_No);
        }
      },
      error: (err) => {
        this.toastr.error("request failed");
      }
    })
  }

  saveDataJoin(Formdata): RequestFundModify {
    let data: RequestFundModify = {};
    data.Invest_No = (this.operateType == this.FundType.Update) ? this.invest_no : this.fundNameList.filter(x => x.name == this.validateForm.get('name').value)[0].invest_No;
    data.Name = Formdata.name || this.name;
    data.Full_Name = Formdata.full_Name || this.full_Name;
    data.Initial_Holding_Ratio = Formdata.initial_Holding_Ratio / 100;
    data.Headquarter = Formdata.headquarter;
    data.Liability_Type = Formdata.liabilityType;
    data.Unit_Pic = this.unitPicList.map(e => e.emplid);
    data.Fin_Year_Rule = Formdata.fin_Year_Rule;
    data.Fund_Type = this.securityList_View.value;
    data.Fund_Period = Formdata.fund_Period;
    data.SecurityList = this._securityDataList;
    data.Investment_Period = Formdata.investment_Period;
    data.Harvest_Period = Formdata.harvest_Period;
    data.Emp_Name = this.name_a;
    data.Quit_Date = (Formdata.quit_Date == undefined) ? undefined : moment(Formdata.quit_Date).format("YYYY-MM-DD HH:mm:ss");
    data.Quit_Reason = Formdata.quit_Reason;
    data.Close_Investment = this.isChecked.toString();
    console.log(data);
    return data;
  }

  routingViewSkip(data: any) {
    this.router.navigate(['fundMaintain/view'], { queryParams: { invest_no: data } });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //捨棄事件
  giveUp(event) {
    event.preventDefault();
    this.isVisible = true;
  }

  //捨棄取消框
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  //捨棄確認框
  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    this.routingSkip();
  }

  handleCheckOk() {
    this.isCheckVisible = false;
  }

  //刪除卡片事件
  giveDelete(event, index: number) {
    this.getNeedToDeleteSecurity = index
    event.preventDefault();
    this.isDelete = true;
  }

  //路由跳转
  routingSkip() {
    this.router.navigate(['fundMaintain/list']);
  }

  //折叠面板点击事件
  updateActive() { }

  //bo_pic
  chosenUnitPIC(data: any) {
    console.log(data);
    if (!!data) {
      this.unitPicList = data;
    }
  }
}
