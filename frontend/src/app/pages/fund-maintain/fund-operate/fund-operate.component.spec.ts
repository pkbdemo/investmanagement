import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundOperateComponent } from './fund-operate.component';

describe('FundOperateComponent', () => {
  let component: FundOperateComponent;
  let fixture: ComponentFixture<FundOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FundOperateComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
