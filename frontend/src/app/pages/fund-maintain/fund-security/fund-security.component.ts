import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subscription } from 'rxjs';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { codeNameItem, FundSecurity, SecurityRequired, FundType } from '../fundMaintainModel';
import { CompanyService } from '../../company-maintain/services/company.service';
import { RequestService } from '../../amount-record/services/requestData.service';

@Component({
  selector: 'app-fund-security',
  templateUrl: './fund-security.component.html',
  styleUrls: ['./fund-security.component.css']
})
export class FundSecurityComponent implements OnInit {
  @Output() goBackData = new EventEmitter();

  //用來存放編輯是編輯的那一筆
  index: number;
  title: string = '';
  type: string;
  type_Name: string;
  security_No: string;

  //区分user
  userRole: any = {};

  dateFormat = IMSConstants.dateFormat4Tip;

  isVisible: boolean = false;
  handOkTxt: string = '加入卡片';
  stockList: codeNameItem[] = [];
  stockListOf: codeNameItem[] = [];
  closeDateList: string[] = [];
  // stockListSub: Subscription;
  visibleType: FundType;
  FundTypeList = FundType;
  isShowStrike: boolean = false;
  currencyList: string[] = [];
  currencyListSub: Subscription;

  //股票形式不可选择列表
  disableStockType: string;

  //接受到的數據
  SecurityList: FundSecurity = {};

  //表单
  validateForm: FormGroup;
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '不是合法Email格式'
    }
  };

  //固定展示
  first_Trans_Date: string;

  get NAgreements() {
    return this.validateForm.get('Agreement') as FormArray;
  }

  constructor(private companyService: CompanyService,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private message: NzMessageService,
    private commonService: CommonMethodService,
    private requestService: RequestService) {
    this.validateForm = this.fb.group({
      commited_Currency: ['', [Validators.required]],
      commited_Amount: ['', [Validators.required]],
      effective_Date: ['', [Validators.required]],
      dd_Close_Date: ['', [Validators.required]],
      remark: ['', []],
    });
  }

  ngOnInit(): void {
    this.getCurrency();
  }

  ngOnDestory() {
    this.currencyListSub.unsubscribe();
  }

  //獲取幣別
  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  //新增按鈕出發事件
  handleOk(): void {
    console.log('Button ok clicked!');
    if (this.validateForm.valid) {
      //寫入數據並返回
      this.insertData(this.validateForm.value);
      this.isVisible = false;
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          console.log(control);
          if (control instanceof FormArray) {
            Object.values(control.controls).forEach(controlArray => {
              if (controlArray.invalid) {
                if (controlArray instanceof FormGroup) {
                  Object.values(controlArray.controls).forEach(o => {
                    if (o.invalid) {
                      o.markAsDirty();
                      o.updateValueAndValidity({ onlySelf: true });
                    }
                  })
                }
              }
            })
          }
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  insertData(data) {
    this.SecurityList.Commited_Currency = data.commited_Currency;
    this.SecurityList.Commited_Amount = data.commited_Amount;
    this.SecurityList.Effective_Date = data.effective_Date;
    this.SecurityList.DD_Close_Date = data.dd_Close_Date;
    this.SecurityList.Remark = data.remark;
    this.goBackData.emit({
      value: this.SecurityList,
      index: this.index
    });
  }

  handleCancel(event): void {
    this.isVisible = false;
    console.log('Button cancel clicked!');
  }

  //打開model觸發事件
  editVisible(data: boolean, type: FundType, userRole: any, FundName: string, SecurityData?: FundSecurity, index?: number) {
    this.isVisible = data;
    this.userRole = userRole;
    this.index = index;
    this.visibleType = type;
    switch (type) {
      case FundType.Add:
        this.handOkTxt = '加入卡片';
        this.title = '新增認購資料'
        break;
      case FundType.Update:
        this.handOkTxt = '更新卡片';
        this.title = '編輯認購資料'
        break;
      case FundType.Custom:
        this.handOkTxt = '更新卡片';
        this.title = '編輯認購資料 - 合約生效日'
        break;
    }

    //写入数据
    if (!!SecurityData) {
      this.SecurityList = SecurityData;
      this.modifyData(SecurityData);
    }
    this.closeDate(userRole, FundName);
  }

  //写入数据
  modifyData(data: FundSecurity) {
    let date = data.DD_Close_Date.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
    this.validateForm.get('dd_Close_Date').setValue(data.DD_Close_Date);
    this.validateForm.get('commited_Currency').setValue(data.Commited_Currency);
    this.validateForm.get('commited_Amount').setValue(data.Commited_Amount);
    this.validateForm.get('effective_Date').setValue(data.Effective_Date);
    this.validateForm.get('remark').setValue(data.Remark);
  }

  // 根据user的部分去调整完成日
  closeDate(userRole: any, fundName: string) {
    if (userRole.isIMDMember || userRole.isIT) {
      this.getCloseDate(fundName).subscribe(data => {

        this.closeDateList = data;
      });
    }
    if (userRole.isBoardSecretariatMember) {
      this.closeDateList = ['無']
    }
  }

  //获取尽职调查完成日
  getCloseDate(fundName: string) {
    let formData = new FormData();
    formData.set('company_Name', fundName)
    return this.httpClient.post<any>(environment.apiServerURL + "DDiligence/GetCloseDate", formData);
  }

  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  //關閉model事件
  nzAfterClose() {
    console.log('model 關閉');
    this.SecurityList = {};
    this.validateForm.reset();
  }
}
