import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundSecurityItemComponent } from './fund-security-item.component';

describe('FundSecurityItemComponent', () => {
  let component: FundSecurityItemComponent;
  let fixture: ComponentFixture<FundSecurityItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FundSecurityItemComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundSecurityItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
