import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FundSecurity, FundType } from '../fundMaintainModel';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-fund-security-item',
  templateUrl: './fund-security-item.component.html',
  styleUrls: ['./fund-security-item.component.css']
})
export class FundSecurityItemComponent implements OnInit {

  @Input() Data: FundSecurity = {};
  @Input() type: FundType;
  @Input() index: number;
  @Output() modifySecurity = new EventEmitter();
  CompanyType = FundType;
  isVisible: boolean = false;
  //紀錄卡片是否被金額紀錄使用
  isUse: boolean = false;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    if (!!this.Data.Security_No) {
      this.checkSecurity(this.Data.Security_No).subscribe((res) => {
        this.isUse = res;
      });
    }
  }

  //校驗此卡片是否被金額紀錄使用
  checkSecurity(security_No: string) {
    let formData = new FormData();
    formData.set('security_No', security_No)
    return this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/UseSecurity", formData);
  }

  DeleteData() {
    this.isVisible = true;
  }
  handleOk() {
    this.isVisible = false;
    this.modifySecurity.emit({
      value: this.Data,
      type: FundType.Delete,
      index: this.index
    });
  }
  handleCancel() {
    this.isVisible = false;
  }

  modifyData() {
    this.modifySecurity.emit({
      value: this.Data,
      type: FundType.Update,
      index: this.index
    });
  }

  modifyContactData() {
    this.modifySecurity.emit({
      value: this.Data,
      type: FundType.Custom,
      index: this.index
    });
  }
}
