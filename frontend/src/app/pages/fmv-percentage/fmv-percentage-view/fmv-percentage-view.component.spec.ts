import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FmvPercentageViewComponent } from './fmv-percentage-view.component';

describe('FmvPercentageViewComponent', () => {
  let component: FmvPercentageViewComponent;
  let fixture: ComponentFixture<FmvPercentageViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FmvPercentageViewComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FmvPercentageViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
