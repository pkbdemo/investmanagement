import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Subject } from 'rxjs';
import { FilterConditionItem } from '../../financials/financialsModel';
import { CheckDataModel, FundFinancialsListItem, FmvPercentageView } from '../fmvPercentageModel';

@Injectable({
  providedIn: 'root'
})
export class FmvPercentageService {

  private _pseudoPaging: boolean = false;
  private _dataSourse = null;
  private filterConditionValue: FilterConditionItem[] = [];
  private _TableList$ = new BehaviorSubject<any[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private _currencyList$ = new Subject();
  private _checkData$ = new BehaviorSubject<CheckDataModel>({});
  private _viewItem$ = new BehaviorSubject<FundFinancialsListItem>({});

  set filterCondition(filterCondition: FilterConditionItem[]) {
    this.filterConditionValue = filterCondition;
    if (filterCondition.length > 0) {
      this._pseudoPaging = true;
      this.search();
    }
    else this._pseudoPaging = false;
  }

  get TableList$() { return this._TableList$.asObservable(); }
  get currencyList$() { return this._currencyList$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get checkData$() { return this._checkData$.asObservable(); }
  get viewItem$() { return this._viewItem$.asObservable(); }

  listOfColumn = [
    {
      title: '基金簡稱',
      compare: (a, b) => a.nick_Name > b.nick_Name ? 1 : -1,
      priority: 1
    },
    {
      title: '財年',
      compare: (a, b) => a.financial_Year > b.financial_Year ? 1 : -1,
      priority: 1
    },
    {
      title: 'Quarter',
      compare: (a, b) => a.quarter_Name > b.quarter_Name ? 1 : -1,
      priority: 1
    },
    {
      title: '報表期初',
      compare: (a, b) => a.report_Start_Date > b.report_Start_Date ? 1 : -1,
      priority: 1
    },
    {
      title: '報表期末',
      compare: (a, b) => a.report_End_Date > b.report_End_Date ? 1 : -1,
      priority: 1
    },
    {
      title: 'Currency',
      compare: (a, b) => a.currency > b.currency ? 1 : -1,
      priority: 1
    },
    {
      title: '基金淨值',
      compare: (a, b) => a.amount - b.amount > 0 ? 1 : -1,
      priority: 1
    },
    {
      title: '關注事項',
      compare: null,
      priority: 0
    }
  ];

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService,) { }

  uploadRefresh(invest_no?: number) {
    this._dataSourse = null
    this.search(invest_no);
  }
  search(invest_No?: number) {
    let dataList: any[];
    let total: number;
    let needToGetData = this._pseudoPaging == false || this._dataSourse == null;
    if (needToGetData) {
      this.httpClient.post<any>(environment.apiServerURL + "FundFinancials/QueryTableList", invest_No || 0).subscribe({
        next: (res) => {
          console.log(res);
          this._dataSourse = res;
          if (this._pseudoPaging == false) {
            dataList = res;
            total = res.length;
            this._total$.next(total);
          } else {
            dataList = this.getData_PseudoPaging();
          }
          this._TableList$.next(dataList);
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("request failed");
        }
      });
    }
    else {
      dataList = this.getData_PseudoPaging();
      this._TableList$.next(dataList);
    }
  }

  private getData_PseudoPaging(): any[] {
    let myData: any[] = this._dataSourse;
    let total = 0;
    if (myData != null) {
      total = myData.length;
      // 1. filter
      if (this.filterConditionValue != null && this.filterConditionValue.length > 0) {
        myData = this.filterConditionValue.reduce((prev, cur) => {
          if (cur.name == 'nickName') prev = prev.filter(data => data.nick_Name.toUpperCase().replace(/\s*/g, "").indexOf(cur.value.toUpperCase().replace(/\s*/g, "")) != -1);
          if (cur.name == 'currency') prev = prev.filter(data => data.currency == cur.value);
          if (cur.name == 'Date') prev = prev.filter(data => data.financial_Year >= cur.value.startDate && data.financial_Year <= cur.value.endDate);
          if (cur.name == 'quarter') {
            this.filterConditionValue.forEach(e => {
              if (e.name == 'Date') {
                prev = prev.filter(data => {
                  if (data.financial_Year == e.value.startDate) {
                    if (data.quarter >= cur.value.startQuarter) return true;
                  } else if (data.financial_Year < e.value.endDate) {
                    return true
                  } else if (data.financial_Year = e.value.endDate) {
                    if (data.quarter <= cur.value.endQuarter) return true;
                  } return false;
                })
              }
            });
          }
          if (cur.name == 'userId') prev = prev.filter(data => data.contact_Ao.toUpperCase() == cur.value);
          total = prev.length;
          return prev;
          //
        }, myData);
        total = myData.length;
      }
    }

    this._total$.next(total);
    return myData;
  }

  //獲取幣別
  getCurrency() {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryCurrencyList").subscribe({
      next: (res) => {
        if (res != null) this._currencyList$.next([...res]);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //获取权限数据
  getCheckData(invest_No?: number) {
    this.httpClient.post<any>(environment.apiServerURL + "FundFinancials/GetCheckData", invest_No).subscribe({
      next: (res) => {
        console.log(res);
        let obj: CheckDataModel = {};
        obj.Contact_Ao = res.contact_Ao;
        obj.Nick_Name = res.nick_Name;
        obj.Full_Name = res.full_Name;
        obj.Invest_No = res.invest_No;
        obj.Fin_Year_Rule = res.fin_Year_Rule;
        this._checkData$.next(obj);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //获取View页面数据源
  async getView(financials_No?: string) {
    let formData = new FormData();
    formData.set('financials_No', financials_No)
    this.httpClient.post<any>(environment.apiServerURL + "FundFinancials/GetView", formData).subscribe({
      next: (res) => {
        if (!!res) {
          let viewObj: FmvPercentageView = {};
          viewObj.Amount = res.amount;
          viewObj.Concerns = res.concerns;
          viewObj.Contact_Ao = res.contact_Ao;
          viewObj.Currency = res.currency;
          viewObj.Fin_Year_Rule = res.fin_Year_Rule;
          viewObj.Financial_Year = res.financial_Year;
          viewObj.Financials_No = res.financials_No;
          viewObj.Full_Name = res.full_Name;
          viewObj.Invest_No = res.invest_No;
          viewObj.Nick_Name = res.nick_Name;
          viewObj.Quarter = res.quarter;
          viewObj.Quarter_Name = res.quarter_Name;
          viewObj.Report_End_Date = res.report_End_Date;
          viewObj.Report_Start_Date = res.report_Start_Date;
          this._viewItem$.next(viewObj);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

}
