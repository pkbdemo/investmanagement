import { TestBed } from '@angular/core/testing';

import { FmvPercentageService } from './fmv-percentage.service';

describe('FmvPercentageService', () => {
  let service: FmvPercentageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FmvPercentageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
