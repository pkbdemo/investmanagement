import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FmvPercentageHeaderComponent } from './fmv-percentage-header.component';

describe('FmvPercentageHeaderComponent', () => {
  let component: FmvPercentageHeaderComponent;
  let fixture: ComponentFixture<FmvPercentageHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FmvPercentageHeaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FmvPercentageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
