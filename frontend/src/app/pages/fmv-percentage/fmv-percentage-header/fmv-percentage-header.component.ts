import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../layout/services/layout.service';

@Component({
  selector: 'app-fmv-percentage-header',
  templateUrl: './fmv-percentage-header.component.html',
  styleUrls: ['./fmv-percentage-header.component.css']
})
export class FmvPercentageHeaderComponent implements OnInit {



  isShowVector: boolean = true;
  isShowUserVector: boolean = true;

  //儅選擇MyList 或 AllList 是需要傳入  否則為 ''
  listSelected: string = 'My List';

  constructor(private layoutService: LayoutService,) { }

  ngOnInit(): void {
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

}
