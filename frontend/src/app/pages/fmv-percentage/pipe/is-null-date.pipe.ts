import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isNullDate'
})
export class IsNullDatePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (!!value) return value;
    return ' - ';
  }

}
