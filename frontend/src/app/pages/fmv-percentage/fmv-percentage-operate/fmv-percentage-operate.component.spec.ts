import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FmvPercentageOperateComponent } from './fmv-percentage-operate.component';

describe('FmvPercentageOperateComponent', () => {
  let component: FmvPercentageOperateComponent;
  let fixture: ComponentFixture<FmvPercentageOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FmvPercentageOperateComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FmvPercentageOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
