import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FmvPercentageTableListComponent } from './fmv-percentage-table-list.component';

describe('FmvPercentageTableListComponent', () => {
  let component: FmvPercentageTableListComponent;
  let fixture: ComponentFixture<FmvPercentageTableListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FmvPercentageTableListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FmvPercentageTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
