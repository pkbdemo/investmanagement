import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { KeycloakService } from 'keycloak-angular';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import { ToastrService } from 'ngx-toastr';
import { Subscribable, Subscription } from 'rxjs';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { RequestService } from '../../amount-record/services/requestData.service';
import { codeNameItem } from '../../company-maintain/companyModel';
import { FinancialsService } from '../../financials/financials.service';
import { FilterConditionItem } from '../../financials/financialsModel';
import { CheckDataModel } from '../fmvPercentageModel';
import { FmvPercentageService } from '../services/fmv-percentage.service';


@Component({
  selector: 'app-fmv-percentage-table-list',
  templateUrl: './fmv-percentage-table-list.component.html',
  styleUrls: ['./fmv-percentage-table-list.component.css']
})
export class FmvPercentageTableListComponent implements OnInit {
  @Input() status: string;
  @Input() clickFromCompany: boolean;
  @Input() invest_no: number;
  @Input() set _invest_no(data: string) {
    if (!!data) {
      this.invest_no = Number(data);
      this.fundFinancialsService.getCheckData(this.invest_no);
      this.fundFinancialsService.checkData$.subscribe(data => {
        this.checkData = data;
      });
    }
  }
  @Input() set headValue(data: string) {
    this.listSelectItemClick(data);
  }
  ReqStatus = new Set<number>();
  //权限数据
  checkData: CheckDataModel = {};

  loginUserId = "";
  headerSelect = 'My List';
  accountList: string[] = [];

  searchValue: string;
  currencyValue: string;

  quarterStartValue: string;
  quarterEndValue: string;

  //Quarter觸發欄位聯動條件
  quarterStartDis = new Set<string>();
  quarterEndDis = new Set<string>();

  //數據展示源
  currencyList: string[] = [];
  currencyListSub: Subscription;
  quarterList: codeNameItem[] = [];
  quarterSub: Subscription;
  quarterListValue: string[] = [];
  @ViewChild('endDatePicker') endDatePicker!: NzDatePickerComponent;

  financialsService: FinancialsService;
  fundFinancialsService: FmvPercentageService;

  listOfColumn: any;
  constructor(private requestService: RequestService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private commonMethodService: CommonMethodService,
    private keycloakService: KeycloakService,
    private router: Router,
    private authorityControlService: AuthorityControlService) {
    this.fundFinancialsService = new FmvPercentageService(this.httpClient, this.toastr);
    this.financialsService = new FinancialsService(this.httpClient, this.toastr);
  }

  async ngOnInit() {
    if (this.keycloakService.isLoggedIn()) {
      this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }
    this.listOfColumn = this.fundFinancialsService.listOfColumn;
    this.financialsService.AccountList$.subscribe(e => this.accountList = e as string[]);
    this.authorityControlService.getUserRole().then(
      userRole => {
        console.log(userRole);
        if (!userRole.isIT && !userRole.isIMDMember) {
          // this.showUploadBtn = false;
        }
      }
    );
    this.getCurrency();
    this.getQuarterList();
    this.search();
  }

  ngOnChanges() {
    if (!this.fundFinancialsService) this.fundFinancialsService = new FmvPercentageService(this.httpClient, this.toastr);
    if (!!this._invest_no) {
      this.search();

    }
  }

  closeSelectValue() {
    this.searchValue = null;
    this.currencyValue = null;
    this.search();
  }

  //公司维护页面进入时，触发搜索方式
  //当type位002 则为财年或Quarter栏位
  tableSearch(type?: string) {
    if (type == '002') return;//&& this.ReqStatus.size !== 0
    this.search();
  }

  search() {
    let filterCondition: FilterConditionItem[] = [];
    if (this.ReqStatus.size == 0) {
      if (this.headerSelect == 'My List') filterCondition.push({ name: 'userId', value: this.loginUserId });
      if (!!this.searchValue) filterCondition.push({ name: 'nickName', value: this.searchValue });
      if (!!this.currencyValue) filterCondition.push({ name: 'currency', value: this.currencyValue });
    }
    this.fundFinancialsService.filterCondition = [...filterCondition];
    this.fundFinancialsService.search(this.invest_no);
  }

  listSelectItemClick(data: string) {
    this.headerSelect = data;
    this.search();
  }

  //Quarter觸發判斷
  checkQuarterField() {
    // if (!this.startValue) this.ReqStatus.add(0);
    if (!this.quarterStartValue) this.ReqStatus.add(1);
    // if (!this.endValue) this.ReqStatus.add(2);
    if (!this.quarterEndValue) this.ReqStatus.add(3);
  }

  //Date觸發判斷
  checkDateField() {
    // if (!this.startValue) this.ReqStatus.add(0);
    // if (!this.endValue) this.ReqStatus.add(2);
  }

  clearField() {
    // if (!this.startValue && !this.quarterStartValue && !this.endValue && !this.quarterEndValue) this.ReqStatus.clear();
  }

  //獲取幣別
  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  //獲取Quarter類別
  getQuarterList() {
    this.requestService.getCodeName('010');
    this.quarterSub = this.requestService.quarterObservable.subscribe(item => {
      this.quarterList = item as codeNameItem[];
      this.quarterList.forEach(e => this.quarterListValue = [...this.quarterListValue, e.value]);
    });
  }

  //獲取數據資料
  getFinancials(invest_no?: number) {
    this.financialsService.search(invest_no);
  }

  viewFinancials(financials_No: number, invest_No: number) {
    this.router.navigate(['fundFinancials/view'],
      {
        queryParams:
        {
          "financials_No": financials_No,
          "invest_no": invest_No,
          "fundMaintain": !!this.invest_no ? "true" : "false",
          "status": this.status
        }
      });
  }

  //跳轉到新增操作頁面
  routingOperate(data?: number) {
    this.router.navigate(['fundFinancials/operate'], { queryParams: { "financials_No": this.commonMethodService.StringTrunBase(data), "invest_no": this.commonMethodService.StringTrunBase(this.invest_no) } });
  }

  //觸發刷新table list ，將上傳內容呈現出來
  chosenBOPIC(data: any) {
    this.fundFinancialsService.uploadRefresh(this.invest_no);
  }

}
