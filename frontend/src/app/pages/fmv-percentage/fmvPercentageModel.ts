export enum OperateType { Add = 0, Update = 1 };
export enum RuleType { '一般' = '001', '二月' = '002', '四月' = '003' }
export enum QuarterType { 'Q1' = '001', 'Q2' = '002', 'Q3' = '003', 'Q4' = '004' }

export interface FundFinancialsListItem {
  Contact_Ao?: string;
  Financials_No?: string;
  Nick_Name?: string;
  Financial_Year?: number;
  Quarter?: string;
  Quarter_Name?: string;
  Report_Start_Date?: string;
  Report_End_Date?: string;
  Currency?: string;
  Amount?: number;
  Concerns?: string;
  Fin_Year_Rule?: string;
}

export interface FmvPercentageView {
  Contact_Ao?: string;
  Invest_No?: string;
  Financials_No?: string;
  Nick_Name?: string;
  Full_Name?: string;
  Financial_Year?: number;
  Quarter?: string;
  Quarter_Name?: string;
  Report_Start_Date?: string;
  Report_End_Date?: string;
  Currency?: string;
  Amount?: number;
  Concerns?: string;
  Fin_Year_Rule?: string;
}

export interface CheckDataModel {
  Contact_Ao?: string;
  Nick_Name?: string;
  Full_Name?: string;
  Invest_No?: number;
  Fin_Year_Rule?: string;
}
