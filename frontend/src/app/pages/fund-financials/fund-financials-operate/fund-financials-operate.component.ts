import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { LayoutService } from '../../layout/services/layout.service';
import { CheckDataModel, FundFinancialsListItem, FundFinancialsView, OperateType, QuarterType, RuleType } from '../fundFinaiclasModel';
import { FundFinancialsService } from '../services/fund-financials.service';
import { codeNameItem } from '../../company-maintain/companyModel';
import { filter, map, Subscription, throttleTime } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NzMessageService } from 'ng-zorro-antd/message';
import { PopupComponent } from '../../Utilities/popup/popup.component';

@Component({
  selector: 'app-fund-financials-operate',
  templateUrl: './fund-financials-operate.component.html',
  styleUrls: ['./fund-financials-operate.component.css']
})
export class FundFinancialsOperateComponent implements OnInit {

  operateType: OperateType = OperateType.Add;
  OperateType = OperateType;

  //時間軸參數
  param = {
    invest_no: '',
    financials_no: ''
  };

  //存放View页面传递的数据
  invest_No: string;
  financials_No: string;
  loginUserId: string;
  checkData: CheckDataModel;

  //编辑数据源
  dataSource: FundFinancialsView = {}

  //页面下拉选单数据源
  quarterList: codeNameItem[] = [];
  quarterSub: Subscription;
  currencyList: string[] = [];
  currencyListSub: Subscription;

  //唯读栏位
  Report_Start_Date: string = '';
  Report_End_Date: string = '';
  fin_year_rule: string = '';

  //校验结果展示内容
  checkContent: string;

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  //页面初加载对金额进行基础校验
  AmountInitValidator = (control: FormControl): { [s: string]: boolean } => {
    var r = new RegExp('^([0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)|([-][0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)$');
    if (control.value) {
      if (!r.test(control.value.toString())) {
        return { confirm: true, error: true };
      }
    }
    return {};
  };

  //表单
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '該項為必填項'
    }
  };

  validateForm: FormGroup;

  //dateObj
  dateObj: any;
  @ViewChild('popup1') popup1: PopupComponent;
  @ViewChild('popup2') popup2: PopupComponent;

  //基金净值历史资料
  fundFinancialsService: FundFinancialsService;
  listOfColumn: any;
  constructor(private route: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private commonMethodService: CommonMethodService,
    private layoutService: LayoutService,
    private location: Location) {
    this.fundFinancialsService = new FundFinancialsService(this.httpClient, this.toastr);
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      Financial_Year: ['', [Validators.required]],
      Quarter: ['', [Validators.required]],
      Currency: ['', [Validators.required]],
      Amount: ['', [Validators.required, this.AmountInitValidator]],
      Concerns: ['', []]
    });
    this.listOfColumn = this.fundFinancialsService.listOfColumn;

    this.validateForm.get('Quarter').valueChanges.subscribe(data => {
      if (!!data) this.editReportDate();
    })

    this.validateForm.get('Financial_Year').valueChanges.subscribe(data => {
      if (!!data) this.editReportDate();
    })

    this.loginUserId = sessionStorage.getItem("userID").toUpperCase();
    Promise.all([
      this.getCurrency(),
      this.getQuarterType(),
    ]).then(() => {
      this.route.queryParams.subscribe(params => {
        this.invest_No = this.commonMethodService.BaseTrunString(params['invest_no']);
        this.financials_No = this.commonMethodService.BaseTrunString(params['financials_No']);
        this.param.financials_no = this.financials_No;
        this.param.invest_no = this.invest_No;

        if (!!this.invest_No) {
          this.fundFinancialsService.search(Number(this.invest_No));
          this.fundFinancialsService.getCheckData(Number(this.invest_No));
          this.fundFinancialsService.checkData$
            .pipe(
              filter(e => !!e.Invest_No),
              throttleTime(2000)
            ).subscribe(data => {
              this.checkData = data;
              if (!!data.Invest_No) {
                if (!!this.financials_No) {
                  this.operateType = OperateType.Update;
                  this.fundFinancialsService.getView(this.financials_No);
                  this.fundFinancialsService.viewItem$
                    .pipe(
                      filter(e => !!e.Financials_No),
                      throttleTime(2000)
                    ).subscribe(data => {
                      if (!!data && !!data?.Financials_No) {
                        this.dataSource = data;
                        this.inputView(data);
                      }
                    })
                }
              }
            });
        }
      });
    })

  }
  ngAfterViewInit() {
    setTimeout(() => {
      if (!!this.dataSource && !!this.dataSource.Financials_No) {
        if (this.DateParse(this.dateObj.startDate) != this.DateParse(this.dataSource.Report_Start_Date) ||
          this.DateParse(this.dateObj.endDate) != this.DateParse(this.dataSource.Report_End_Date)) {
          this.popup2.openModel();
        }
      }
    });

  }
  //数据写入页面
  inputView(data: FundFinancialsListItem) {
    this.dateObj = this.editReportDate();
    this.validateForm.get('Financial_Year').disable();
    this.validateForm.get('Quarter').disable();
    this.validateForm.get('Currency').setValue(data.Currency);
    this.validateForm.get('Amount').setValue(data.Amount);
    this.validateForm.get('Concerns').setValue(data.Concerns);

  }

  submitForm(): void {
    if (this.validateForm.status == "INVALID") {
      Object.keys(this.validateForm.controls).forEach(key => {
        const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
        if (validationErrors != null) {
          Object.keys(validationErrors).forEach(keyError => {
            console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
          });
        }
      });
    }
    if (this.validateForm.valid) {
      this.saveCheckData();
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach(item => {
              if (item.invalid) {
                item.markAsDirty();
                item.updateValueAndValidity({ onlySelf: true });
              }
            });
          }
        }
      });
    }
  }

  saveCheckData() {
    //1.	檢核是否有invest_no、financial_year、quarter重複資料
    let dataObj = {
      Invest_No: Number(this.invest_No),
      Financial_Year: this.dataSource.Financial_Year || this.validateForm.get('Financial_Year').value.getFullYear(),
      Quarter: this.validateForm.get('Quarter').value
    }
    this.httpClient.post<boolean>(environment.apiServerURL + "FundFinancials/CheckData", dataObj).subscribe({
      next: (res) => {
        console.log(res);
        if (res) {
          let Quarter_Nm = this.quarterList.find(e => e.value == dataObj.Quarter).label;
          this.checkContent = `財年：${dataObj.Financial_Year}，Quarter：${Quarter_Nm}`;
          this.popup1.openModel();
        }
        else this.saveData();
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    })
  }

  saveData() {
    let data = this.dataJoin();
    console.log(data);
    let Url: string;
    if (this.operateType == OperateType.Add) Url = 'FundFinancials/Create';
    else Url = 'FundFinancials/Update';
    this.httpClient.post<number>(environment.apiServerURL + Url, data).subscribe({
      next: (res) => {
        if (res != 0) { this.createMessage('success', '已儲存變更'); this.routingViewSkip(res); }
        else { this.message.create('error', '儲存失敗'); }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    })
  }

  //数据拼接
  dataJoin(): FundFinancialsView {
    let data: FundFinancialsView = {};
    let validate = this.validateForm.value;
    data.Financials_No = this.financials_No;
    data.Invest_No = this.invest_No;
    data.Financial_Year = this.dataSource.Financial_Year || validate.Financial_Year.getFullYear();
    data.Quarter = this.dataSource.Quarter || validate.Quarter;
    data.Report_Start_Date = this.Report_Start_Date;
    data.Report_End_Date = this.Report_End_Date;
    data.Currency = validate.Currency;
    data.Amount = Number(validate.Amount);
    data.Concerns = validate.Concerns;
    return data;
  }

  routingViewSkip(data: any) {
    this.router.navigate(['/fundFinancials/view'], { queryParams: { "financials_No": data, "invest_no": this.invest_No, "fundMaintain": "true" } });
  }

  //报表期末期初写入逻辑
  editReportDate() {
    let Quarter = this.dataSource.Quarter || this.validateForm.get('Quarter').value;
    let FinancialYearDate = !!this.dataSource.Financial_Year ? new Date(this.dataSource.Financial_Year + '/01/01') : null;
    let Financial_Year = FinancialYearDate || this.validateForm.get('Financial_Year').value;
    let dateObj = {
      startDate: '',
      endDate: ''
    }
    if (!!Quarter && !!Financial_Year && !!this.checkData?.Fin_Year_Rule) {
      let inintMonth: number = Number(this.checkData?.Fin_Year_Rule);
      if (this.checkData?.Fin_Year_Rule == RuleType['四月']) inintMonth = Number(this.checkData?.Fin_Year_Rule) + 1;
      dateObj = this.getQuarter(inintMonth, this.checkData?.Fin_Year_Rule, Financial_Year, Quarter);
      this.Report_Start_Date = dateObj.startDate;
      this.Report_End_Date = dateObj.endDate;
    }
    return dateObj;
  }

  //根据Quarter拿到不同的报表起初
  getQuarter(inintMonth: number, Fin_Year_Rule: string, Financial_Year: Date, Quarter: QuarterType) {
    let dateObj = {
      startDate: '',
      endDate: ''
    }
    let startMonth = 0;
    switch (Quarter) {
      case QuarterType['Q1']:
        startMonth = inintMonth;
        break;
      case QuarterType['Q2']:
        startMonth = inintMonth + Number(QuarterType['Q1']) * 3;
        break;
      case QuarterType['Q3']:
        startMonth = inintMonth + Number(QuarterType['Q2']) * 3;
        break;
      case QuarterType['Q4']:
        startMonth = inintMonth + Number(QuarterType['Q3']) * 3;
        break;
    }
    dateObj = this.getDate(Fin_Year_Rule, Financial_Year, startMonth > 12 ? 1 : startMonth, Quarter);
    return dateObj;
  }

  //根据不同报表起初 返回不同报表期末
  getDate(Rule: string, Financial_Year: Date, startMonth: number, Quarter: QuarterType) {
    let dateObj = {
      startDate: '',
      endDate: ''
    }
    let Year = Financial_Year.getFullYear();
    dateObj.startDate = Year + '-' + startMonth + '-1';
    if (startMonth + 2 <= 12) dateObj.endDate = Year + '-' + (startMonth + 2) + '-' + new Date(Year, startMonth + 2, 0).getDate();
    if (Rule == RuleType['二月'] && Quarter == QuarterType['Q4']) {
      dateObj.endDate = Year + 1 + '-' + 1 + '-' + new Date(Year, 1, 0).getDate();
    }
    if (Rule == RuleType['四月'] && Quarter == QuarterType['Q4']) {
      dateObj.startDate = Year + 1 + '-' + startMonth + '-1';
      dateObj.endDate = Year + 1 + '-' + 3 + '-' + new Date(Year + 1, 3, 0).getDate();
    }
    return dateObj;
  }

  //路由跳转
  goback() {
    this.location.back();
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //獲取Quarter
  getQuarterType() {
    this.commonMethodService.getCodeName('010');
    this.quarterSub = this.commonMethodService.quarterListObservable.subscribe(item => {
      this.quarterList = item as codeNameItem[]
    });
  }

  //獲取幣別
  getCurrency() {
    this.commonMethodService.RequestCurrencyList();
    this.currencyListSub = this.commonMethodService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  updateActive() { }
  //提示框
  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  //字符串轉時間戳
  DateParse(data: string) {
    if (!data) return null;
    return new Date(data).getTime();
  }
}
