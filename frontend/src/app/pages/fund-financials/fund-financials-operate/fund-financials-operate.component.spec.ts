import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundFinancialsOperateComponent } from './fund-financials-operate.component';

describe('FundFinancialsOperateComponent', () => {
  let component: FundFinancialsOperateComponent;
  let fixture: ComponentFixture<FundFinancialsOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundFinancialsOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundFinancialsOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
