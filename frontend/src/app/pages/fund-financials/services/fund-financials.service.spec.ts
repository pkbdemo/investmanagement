import { TestBed } from '@angular/core/testing';

import { FundFinancialsService } from './fund-financials.service';

describe('FundFinancialsService', () => {
  let service: FundFinancialsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FundFinancialsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
