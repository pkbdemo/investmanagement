import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundFinancialsTableListComponent } from './fund-financials-table-list.component';

describe('FundFinancialsTableListComponent', () => {
  let component: FundFinancialsTableListComponent;
  let fixture: ComponentFixture<FundFinancialsTableListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundFinancialsTableListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundFinancialsTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
