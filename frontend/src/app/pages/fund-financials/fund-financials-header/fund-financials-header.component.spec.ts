import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundFinancialsHeaderComponent } from './fund-financials-header.component';

describe('FundFinancialsHeaderComponent', () => {
  let component: FundFinancialsHeaderComponent;
  let fixture: ComponentFixture<FundFinancialsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundFinancialsHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundFinancialsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
