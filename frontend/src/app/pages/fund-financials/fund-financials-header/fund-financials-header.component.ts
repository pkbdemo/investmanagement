import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../layout/services/layout.service';

@Component({
  selector: 'app-fund-financials-header',
  templateUrl: './fund-financials-header.component.html',
  styleUrls: ['./fund-financials-header.component.css']
})
export class FundFinancialsHeaderComponent implements OnInit {



  isShowVector: boolean = true;
  isShowUserVector: boolean = true;

  //儅選擇MyList 或 AllList 是需要傳入  否則為 '' 
  listSelected: string = 'My List';

  constructor(private layoutService: LayoutService,) { }

  ngOnInit(): void {
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

}
