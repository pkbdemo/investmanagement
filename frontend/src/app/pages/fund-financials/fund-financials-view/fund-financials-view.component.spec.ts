import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundFinancialsViewComponent } from './fund-financials-view.component';

describe('FundFinancialsViewComponent', () => {
  let component: FundFinancialsViewComponent;
  let fixture: ComponentFixture<FundFinancialsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundFinancialsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundFinancialsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
