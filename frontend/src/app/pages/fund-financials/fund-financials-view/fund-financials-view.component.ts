import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from '../../layout/services/layout.service';
import { Location } from '@angular/common';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { NzMessageService } from 'ng-zorro-antd/message';
import { FundFinancialsService } from '../services/fund-financials.service';
import { FundFinancialsView } from '../fundFinaiclasModel';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { debounceTime, filter, Subscription, throttleTime } from 'rxjs';

@Component({
  selector: 'app-fund-financials-view',
  templateUrl: './fund-financials-view.component.html',
  styleUrls: ['./fund-financials-view.component.css']
})
export class FundFinancialsViewComponent implements OnInit {


  //時間軸參數
  param = {
    invest_no: '',
    financials_no: ''
  };
  loginUserId: string;
  invest_no: string;
  financials_no: string;
  fundMatain: boolean;
  vieScription: Subscription;

  isShowBtn: boolean = false;

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  //页面展示数据源
  tableListData: FundFinancialsView = {};

  constructor(public router: Router,
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private location: Location,
    private httpClient: HttpClient,
    private commonMethodService: CommonMethodService,
    private fundFinancialsService: FundFinancialsService,
    private message: NzMessageService) { }

  ngOnInit(): void {
    this.loginUserId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      let sFinancialsNo = params['financials_No'];
      let invest_No = params['invest_no']
      let fundMaintain = params['fundMaintain'] == 'true';
      this.param.invest_no = invest_No;
      this.invest_no = invest_No;
      if (!!sFinancialsNo) {
        this.param.financials_no = sFinancialsNo.toString();
        this.financials_no = sFinancialsNo.toString();
        this.fundFinancialsService.getView(sFinancialsNo);
        this.vieScription = this.fundFinancialsService.viewItem$
          .pipe(
            filter(e => !!e.Financials_No),
            debounceTime(200)
          ).subscribe(data => {
            this.tableListData = data;
            if (fundMaintain) {
              if (!!this.tableListData?.Fin_Year_Rule && this.loginUserId == data?.Contact_Ao && params['status'] != '003') {
                this.isShowBtn = true;
              }
            }
            console.log(this.tableListData);
          });
      }
      this.financials_no = params['financials_No'];
      this.fundMatain = params['fundMaintain'];
    });
  }

  handleOk(): void {
    let formData = new FormData();
    formData.set("financials_No", this.financials_no);
    this.httpClient.post<any>(environment.apiServerURL + "FundFinancials/DeleteFinancials", formData).subscribe({
      next: (res) => {
        if (res == true) {
          this.message.create('success', "刪除成功!");
          this.router.navigate(['fundMaintain/view'], { queryParams: { invest_no: this.invest_no, isShowBtn: true } })
        }
        else {
          this.message.create('error', res.toString());
        }
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //路由跳转
  goBack() {
    this.location.back();
  }

  //面包屑跳转
  breadRouting() {
    this.commonMethodService.leftSider('fundFinancials');
  }

  //路由跳轉到操作頁面
  routingOperate() {
    this.router.navigate(['fundFinancials/operate'], {
      queryParams: {
        "financials_No": this.commonMethodService.StringTrunBase(this.financials_no),
        "invest_no": this.commonMethodService.StringTrunBase(this.invest_no)
      }
    });
  }

  updateActive() { }
}
