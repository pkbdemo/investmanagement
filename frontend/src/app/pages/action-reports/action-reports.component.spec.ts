import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionReportsComponent } from './action-reports.component';

describe('ActionReportsComponent', () => {
  let component: ActionReportsComponent;
  let fixture: ComponentFixture<ActionReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
