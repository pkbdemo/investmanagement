import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { RequestService } from '../amount-record/services/requestData.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { codeNameItem } from './actionReportsModel';
import { LayoutService } from '../layout/services/layout.service';

@Component({
  selector: 'app-action-reports',
  templateUrl: './action-reports.component.html',
  styleUrls: ['./action-reports.component.css']
})
export class ActionReportsComponent implements OnInit {
  // param = {
  //   invest_no: '35',
  //   financials_no: ''
  // };

  validateForm!: FormGroup;
  date = null;
  currencySub: Subscription;
  reportSub: Subscription;
  resultsSub: Subscription;
  quarterSub: Subscription;

  listOfSecurityValue: string[] = [];
  SecurityValueId: string[] = [];

  currencyList: string[] = [];
  reportTypeLsit: codeNameItem[] = [];
  resultsTypeList: codeNameItem[] = [];
  quarterList: codeNameItem[] = [];

  isShowHidle: boolean = false;
  isError: boolean = false;

  //證券種類
  SecurityValue: string[] = [];

  //時間提示格式
  dateFormat = IMSConstants.dateFormat4Tip;

  constructor(private layoutService: LayoutService,
    private requestService: RequestService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      dateValue: [{ value: [], disabled: false }, [Validators.required]],
      currency: [null, [Validators.required]],
      results: [null, []],
      year: [null, []],
      quarter: ['', []]
    });
    this.getCurrency();
    this.getReportType();
    this.getResultsType();
    this.getQuarterList();

  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  onChange(result: Date[]): void {
    console.log('onChange: ', result);
  }

  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencySub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  getReportType() {
    this.requestService.getCodeName('017');
    this.reportSub = this.requestService.reportTypeObservable.subscribe(item =>
      this.reportTypeLsit = item as codeNameItem[]
    );
  }

  getResultsType() {
    this.requestService.getCodeName('016');
    this.resultsSub = this.requestService.resultsTypeObservable.subscribe(item =>
      this.resultsTypeList = item as codeNameItem[]
    );
  }

  getQuarterList() {
    this.requestService.getCodeName('010');
    this.quarterSub = this.requestService.quarterObservable.subscribe(item =>
      this.quarterList = item as codeNameItem[]
    );
  }

  ngOnDestroy() {
    this.currencySub.unsubscribe();
    this.reportSub.unsubscribe();
    this.resultsSub.unsubscribe();
    this.quarterSub.unsubscribe();
  }

  closeSelectValue(type: string) {
    if (type == 'Security' && this.listOfSecurityValue.length > 0) {
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d == '-1');
      for (let item of this.reportTypeLsit) {
        item.checked = false;
      }
      this.SecurityValueId = [];
      this.isShowHidle = false;
      this.validateForm.get('results').clearValidators();
      this.validateForm.get('year').clearValidators();
      this.validateForm.get('quarter').clearValidators();
      this.validateForm.get('dateValue').setValidators([Validators.required]);
      this.validateForm.get('dateValue').enable();
      this.validateForm.updateValueAndValidity();
      this.validateForm.enable();
    }
  }

  submitForm(): void {

    if (this.validateForm.status == "INVALID") {
      Object.keys(this.validateForm.controls).forEach(key => {
        const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
        if (validationErrors != null) {
          Object.keys(validationErrors).forEach(keyError => {
            console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
          });
        }
      });
    }
    console.log(this.SecurityValueId);

    if (this.SecurityValueId.length <= 0) this.isError = true;
    else this.isError = false;
    if (this.validateForm.valid) {
      if (!this.isError) return;
      console.log('submit', this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  CompanyTransaction(data: codeNameItem) {
    console.log(this.validateForm.controls['dateValue']);
    if (data.checked) {
      this.listOfSecurityValue = [...this.listOfSecurityValue, data.label];
      this.SecurityValueId = [...this.SecurityValueId, data.value]

    } else {
      this.isShowHidle = false;
      this.listOfSecurityValue = this.listOfSecurityValue.filter(d => d !== data.label);
      this.SecurityValueId = this.SecurityValueId.filter(d => d !== data.value);
    }

    if (this.SecurityValueId.includes('004')) {
      this.isShowHidle = true;
      this.validateForm.get('results').setValidators([Validators.required]);
      this.validateForm.get('year').setValidators([Validators.required]);
      this.validateForm.get('quarter').setValidators([Validators.required]);
    } else {
      this.isShowHidle = false;
      this.validateForm.get('results').clearValidators();
      this.validateForm.get('year').clearValidators();
      this.validateForm.get('quarter').clearValidators();
    }

    if (this.SecurityValueId.length == 1) {
      if (this.SecurityValueId.includes('004')) {
        this.validateForm.get('dateValue').clearValidators();
        this.validateForm.get('dateValue').setValue([]);
        this.validateForm.get('dateValue').disable();

      } else {
        this.validateForm.get('dateValue').setValidators([Validators.required]);
        this.validateForm.get('dateValue').enable();
      }
    } else {
      this.validateForm.get('dateValue').setValidators([Validators.required]);
      this.validateForm.get('dateValue').enable();
    }
    this.validateForm.updateValueAndValidity();
    this.validateForm.get('results').enable();
    this.validateForm.get('year').enable();
    this.validateForm.get('quarter').enable();
  }

}
