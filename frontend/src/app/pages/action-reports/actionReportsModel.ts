export interface codeNameItem {
    label: string;
    value: string;
    code_Extend_A?: string;
    code_Extend_B?: string;
    code_Extend_C?: string;
    checked?: boolean;
    disabled?: boolean;
}