import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './layout/components/admin/admin.component';

import { BasicformComponent } from './examples/basicform/basicform.component';
import { AdvancedformComponent } from './examples/advancedform/advancedform.component';
import { FormexamplesComponent } from './examples/formexamples/formexamples.component';
import { FormvalidationComponent } from './examples/formvalidation/formvalidation.component';
import { SummernoteComponent } from './examples/summernote/summernote.component';
import { CallapiComponent } from "./examples/callapi/callapi.component";
import { GridviewComponent } from './examples/gridview/gridview.component';
import { VendorCodeFundUtilities } from './examples/vendorcodefundutilities/vendor-code-fund-utilities.component';
import { IndexComponent } from './index/index.component';
import { DueDiligenceComponent } from './investigation/duediligence/due-diligence/due-diligence.component';
import { DdOperateComponent } from './investigation/duediligence/dd-operate/dd-operate.component';
import { ActionReportsComponent } from './action-reports/action-reports.component'
import { PicMaintainComponent } from './pic-maintain/pic-maintain.component'
import { AmountHeaderComponent } from './amount-record/amount-header/amount-header.component'
import { AmountTableViewComponent } from './amount-record/amount-table-view/amount-table-view.component';
import { AmountOperateComponent } from './amount-record/amount-operate/amount-operate.component'
import { CurrencyRateComponent } from './currency-rate/currency-rate.component'
import { CompanyListComponent } from './company-maintain/company-list/company-list.component';
import { CompanyOperateComponent } from './company-maintain/company-operate/company-operate.component'
import { FundOperateComponent } from './fund-maintain/fund-operate/fund-operate.component';
import { FundUpdateComponent } from './fund-maintain/fund-update/fund-update.component';
import { FundListComponent } from './fund-maintain/fund-list/fund-list.component';
import { FundViewComponent } from './fund-maintain/fund-view/fund-view.component';
import { FmvPercentageHeaderComponent } from './fmv-percentage/fmv-percentage-header/fmv-percentage-header.component';
import { FmvPercentageOperateComponent } from './fmv-percentage/fmv-percentage-operate/fmv-percentage-operate.component';
import { FmvPercentageTableListComponent } from './fmv-percentage/fmv-percentage-table-list/fmv-percentage-table-list.component';
import { FmvPercentageViewComponent } from './fmv-percentage/fmv-percentage-view/fmv-percentage-view.component';
import { ViewCompanyComponent } from './company-maintain/view-company/view-company.component';
import { FinancialsHeaderComponent } from './financials/financials-header/financials-header.component';
import { FinancialsOperateComponent } from './financials/financials-operate/financials-operate.component';
import { DashboardComponent } from './Dashboards/dashboard/dashboard.component';
import { FinancialsViewComponent } from './financials/financials-view/financials-view.component';
import { DdViewComponent } from './investigation/duediligence/dd-view/dd-view.component';
import { CompanyMappingComponent } from './company-maintain/company-mapping/company-mapping.component'
import { FundFinancialsHeaderComponent } from './fund-financials/fund-financials-header/fund-financials-header.component'
import { FundFinancialsTableListComponent } from './fund-financials/fund-financials-table-list/fund-financials-table-list.component'
import { FundFinancialsOperateComponent } from './fund-financials/fund-financials-operate/fund-financials-operate.component'
import { FundFinancialsViewComponent } from './fund-financials/fund-financials-view/fund-financials-view.component'
import { ReferencePriceComponent } from './reference-price/reference-price/reference-price.component'
import { ViewStockPriceComponent } from './reference-price/view-stock-price/view-stock-price.component';
import { StockPriceListComponent } from './reference-price/stock-price-list/stock-price-list.component';
import { StockPriceOperateComponent } from './reference-price/stock-price-operate/stock-price-operate.component';
import { CompanyFinMapViewComponent } from './company-maintain/company-mapping/company-fin-map-view/company-fin-map-view.component'
import { RealTimeTextViewComponent } from './real-time-text/real-time-text.component'
import { RealTimeTextYesterdayViewComponent } from './real-time-text-yesterday/real-time-text-yesterday.component'
import { RealTimeTextWeekViewComponent } from './real-time-text-week/real-time-text-week.component'
import { RealTimeTextMonthViewComponent } from './real-time-text-month/real-time-text-month.component'
import { ParameterSettingViewComponent } from './parameter-setting/parameter-setting.component'
import { OffsetSettingViewComponent } from './offset-setting/offset-setting.component'
import { SummaryTableMonthViewComponent } from './summary-table-month/summary-table-month.component'
import { SummaryTableDayViewComponent } from './summary-table-day/summary-table-day.component'
import { SummaryTableHourViewComponent } from './summary-table-hour/summary-table-hour.component'
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'index', pathMatch: 'full' },

      // Home
      { path: 'index', component: IndexComponent },

      // Examples
      { path: 'basicform', component: BasicformComponent },
      { path: 'advancedform', component: AdvancedformComponent },
      { path: 'formexamples', component: FormexamplesComponent },
      { path: 'formvalidation', component: FormvalidationComponent },
      { path: 'formsummernote', component: SummernoteComponent },
      { path: 'callapi', component: CallapiComponent },
      { path: 'gridview', component: GridviewComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'duediligence', component: DueDiligenceComponent },
      { path: 'duediligence/ddView', component: DdViewComponent },
      { path: 'duediligence/ddOperate', component: DdOperateComponent },
      { path: "actionReport", component: ActionReportsComponent },
      { path: "picMaintain", component: PicMaintainComponent },
      { path: "amountMaintain", component: AmountHeaderComponent },
      { path: "amountMaintain/tableView", component: AmountTableViewComponent },
      { path: "amountMaintain/operate", component: AmountOperateComponent },
      { path: "currencyRate", component: CurrencyRateComponent },
      { path: "companyMaintain/list", component: CompanyListComponent },
      { path: "companyMaintain/operate", component: CompanyOperateComponent },
      { path: "companyMaintain/view", component: ViewCompanyComponent },
      { path: "fundMaintain/list", component: FundListComponent },
      { path: "fundMaintain/operate", component: FundOperateComponent },
      { path: "fundMaintain/update", component: FundUpdateComponent },
      { path: "fundMaintain/view", component: FundViewComponent },
      { path: "fmvPercentage", component: FmvPercentageHeaderComponent },
      { path: "fmvPercentage/operate", component: FmvPercentageOperateComponent },
      { path: "fmvPercentage/view", component: FmvPercentageViewComponent },
      { path: "fundFinancials", component: FundFinancialsHeaderComponent },
      { path: "fundFinancials/operate", component: FundFinancialsOperateComponent },
      { path: "fundFinancials/view", component: FundFinancialsViewComponent },
      { path: "vendorcodefundutilities", component: VendorCodeFundUtilities },
      { path: "financials", component: FinancialsHeaderComponent },
      { path: "financials/operate", component: FinancialsOperateComponent },
      { path: "financials/view", component: FinancialsViewComponent },
      { path: "companyMapping", component: CompanyMappingComponent },
      { path: "referencePrice", component: ReferencePriceComponent },
      { path: "referencePrice/view", component: ViewStockPriceComponent },
      { path: "referencePrice/list", component: StockPriceListComponent },
      { path: "referencePrice/operate", component: StockPriceOperateComponent },
      { path: "companyMapping/view", component: CompanyFinMapViewComponent },
      { path: 'parametersetting', component: ParameterSettingViewComponent },
      { path: 'realtimetext', component: RealTimeTextViewComponent },
      {
        path: 'realtimetextyesterday',
        component: RealTimeTextYesterdayViewComponent,
      },
      {
        path: 'realtimetextweek',
        component: RealTimeTextWeekViewComponent,
      },
      {
        path: 'realtimetextmonth',
        component: RealTimeTextMonthViewComponent,
      },
      {
        path: 'summarytablemonth',
        component: SummaryTableMonthViewComponent,
      },
      {
        path: 'summarytableday',
        component: SummaryTableDayViewComponent,
      },
      {
        path: 'summarytablehour',
        component: SummaryTableHourViewComponent,
      },
      {
        path: 'offsetsetting',
        component: OffsetSettingViewComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

