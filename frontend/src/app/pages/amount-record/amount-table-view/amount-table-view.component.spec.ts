import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountTableViewComponent } from './amount-table-view.component';

describe('AmountTableViewComponent', () => {
  let component: AmountTableViewComponent;
  let fixture: ComponentFixture<AmountTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmountTableViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
