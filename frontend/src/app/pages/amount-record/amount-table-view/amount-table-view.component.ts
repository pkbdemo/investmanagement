import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '@environments/environment';

import { AmountRecordItem, ShowBtn } from '../amountMaintainModel';
import { LayoutService } from '../../layout/services/layout.service';

@Component({
  selector: 'app-amount-table-view',
  templateUrl: './amount-table-view.component.html',
  styleUrls: ['./amount-table-view.component.css']
})
export class AmountTableViewComponent implements OnInit {

  //刪除請求内容
  request: number[] = [];
  userId: string = '';
  isVisible = false;
  tableListData: AmountRecordItem = {};

  //用於編輯頁面展示Table List展示
  invest_no: string;
  ShowBtn = ShowBtn;
  showBtnSet = new Set<ShowBtn>();

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  constructor(
    private layoutService: LayoutService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    public router: Router,
    private location: Location) { }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.route.queryParams.subscribe(async params => {
      console.log(params['amount_No']);
      this.request.push(params['amount_No']);
      if (params['amount_No'] != null) this.getAmountTableList(params['amount_No']);
    });
  }

  getAmountTableList(amount_no: string) {
    let formData = new FormData();
    formData.set("amount_no", amount_no);
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/QueryTabeList", formData).subscribe({
      next: (res) => {
        if (res != null) {
          console.log(res);
          this.tableListData = res[0];
          if (res[0].create_User.toUpperCase() == this.userId) {
            if (res[0].data_Source == '001') {
              this.showBtnSet.add(ShowBtn.Update)
              this.showBtnSet.add(ShowBtn.Delete)
            } else {
              if (this.showBtnSet.has(ShowBtn.Delete)) this.showBtnSet.delete(ShowBtn.Delete)
              if (!res[0].security_No) this.showBtnSet.add(ShowBtn.Update)
              else {
                if (this.showBtnSet.has(ShowBtn.Update)) this.showBtnSet.delete(ShowBtn.Delete)
              }
            }
          }
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  updateActive() { }

  //刪除確認框
  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/Delete", this.request).subscribe({
      next: (res) => {
        if (res) this.goback();
        console.log(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //刪除取消框
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  //路由跳转
  goback() {
    this.location.back();
  }

  //路由跳轉到操作頁面
  routingOperate() {
    this.router.navigate(['amountMaintain/operate'], { queryParams: { amount_no: this.tableListData.amount_No, dataName: null } });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }
}
