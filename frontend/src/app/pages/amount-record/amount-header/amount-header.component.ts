import { Component, OnInit } from '@angular/core';

import { LayoutService } from '../../layout/services/layout.service';
import { RequestService } from '../services/requestData.service';

@Component({
  selector: 'app-amount-header',
  templateUrl: './amount-header.component.html',
  styleUrls: ['./amount-header.component.css']
})
export class AmountHeaderComponent implements OnInit {
  isShowVector: boolean = true;
  isShowUserVector: boolean = true;

  //儅選擇MyList 或 AllList 是需要傳入  否則為 '' 
  listSelected: string = 'My List';
  //儅為公司或基金維護進入是需要傳入值 否則為 ''
  invest_no: string = '';

  constructor(private layoutService: LayoutService,
    private requestService: RequestService) { }

  ngOnInit(): void {
    this.listSelected = this.requestService.listSelectedTxt;
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  Visible(data: any, Type: string) {
    if (Type == 'List') this.isShowVector = !data;
    else this.isShowUserVector = !data;
  }

  sendListSelected(data: string) {
    this.requestService.sendListSelectedTxt(data);
  }
}
