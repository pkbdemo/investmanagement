import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountHeaderComponent } from './amount-header.component';

describe('AmountHeaderComponent', () => {
  let component: AmountHeaderComponent;
  let fixture: ComponentFixture<AmountHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmountHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
