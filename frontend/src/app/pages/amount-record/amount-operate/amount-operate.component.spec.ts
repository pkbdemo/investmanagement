import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountOperateComponent } from './amount-operate.component';

describe('AmountOperateComponent', () => {
  let component: AmountOperateComponent;
  let fixture: ComponentFixture<AmountOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmountOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
