import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzMessageService } from 'ng-zorro-antd/message';
import { environment } from '@environments/environment';

import { RequestService } from '../services/requestData.service';
import { AmountRecordItem, RequestAmountRecord, codeNameItem, REQUIREDCOLUMN } from '../amountMaintainModel';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { LayoutService } from '../../layout/services/layout.service';
import { CommonMethodService } from 'src/app/service/common-method.service';

@Component({
  selector: 'app-amount-operate',
  templateUrl: './amount-operate.component.html',
  styleUrls: ['./amount-operate.component.css']
})
export class AmountOperateComponent implements OnInit {
  dataName: string = '*未命名公司';

  name: string = '';
  typeName: string = '';
  amount_no: number = -1;

  //页面唯读数据
  effective_Date: string;
  dd_Close_Date: string;
  vendor_Name: string;




  validateForm: FormGroup;
  userId: string = '';
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '該項為必填項'
    }
  };
  dateFormat = IMSConstants.dateFormat4Tip;
  tableListData: AmountRecordItem = {};

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  //捨棄窗口
  isVisible = false;
  //Select選項邏輯
  code_Extend_A: string = '';
  code_Extend_B: string = '';

  REQUIREDCOLUMN = REQUIREDCOLUMN;
  requiredSet = new Set<REQUIREDCOLUMN>();

  // detail可選數據源
  nameList: string[] = [];
  securityList: codeNameItem[] = [];
  securityDataList: codeNameItem[] = [];
  subjectList: codeNameItem[] = [];
  typeList: codeNameItem[] = [];
  typeItemList: codeNameItem[] = [];
  currencyList: string[] = [];
  boTypeList: codeNameItem[] = [];
  paymentList: codeNameItem[] = [];
  //數據源Subscription
  subjectListSub: Subscription;
  typeListSub: Subscription;
  currencyListSub: Subscription;
  kindListSub: Subscription;
  boTypeListSub: Subscription;
  paymentListSub: Subscription;

  isInvestAmountUpdate: boolean = false;

  //判斷是新增還是修改  為true則為 新增 false則爲修改
  isAddOrUpdate: boolean = false;

  //展示是否必選
  isCurrency: boolean = false; //幣別必選
  isAmount: boolean = false; //金額必選
  isShare: boolean = false; //股數必選
  isCost: boolean = false; //處分成本必選
  isPrice: boolean = false; //股價必選
  isRatio: boolean = false;//持股比例必選

  //歷史金額記錄資料
  historyTableList: AmountRecordItem[] = [];
  listOfColumn = [
    {
      title: '公司全稱',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.use_Name > b.use_Name ? -1 : 1,
      priority: 1
    },
    {
      title: '證券/基金種類',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.use_Type > b.use_Type ? -1 : 1,
      priority: 2
    },
    {
      title: '類型',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.record_Type > b.record_Type ? -1 : 1,
      priority: 3
    },
    {
      title: '日期',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.investment_Date > b.investment_Date ? -1 : 1,
      priority: 4
    },
    {
      title: '主體',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.investment_Subject > b.investment_Subject ? -1 : 1,
      priority: 5
    },
    {
      title: '幣別',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.ori_Currency > b.ori_Currency ? -1 : 1,
      priority: 6
    },
    {
      title: '金額(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '處分成本(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '股數(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '股價(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '持股比例(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '備註',
      compare: null,
      priority: false
    },
    {
      title: '換算幣別',
      compare: null,
      priority: false
    },
    {
      title: '金額(美金)',
      compare: null,
      priority: false
    },
    {
      title: '處分成本(美金)',
      compare: null,
      priority: false
    },
    {
      title: '股價(美金)',
      compare: null,
      priority: false
    }
  ];
  invest_no: string = '';

  constructor(private layoutService: LayoutService,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private requestService: RequestService,
    private commonMethodService: CommonMethodService,
    public router: Router,
    private message: NzMessageService) {
    this.validateForm = this.fb.group({
      [REQUIREDCOLUMN.Name]: ['', [Validators.required]],
      [REQUIREDCOLUMN.Use_Type]: ['', [Validators.required]],
      [REQUIREDCOLUMN.INVESTMENT_SUBJECT]: ['', [Validators.required]],
      [REQUIREDCOLUMN.BO]: ['', [Validators.required]],
      [REQUIREDCOLUMN.RECORD_TYPE]: ['', [Validators.required]],
      [REQUIREDCOLUMN.INVESTMENT_DATE]: [null, [Validators.required]],
      [REQUIREDCOLUMN.ORI_CURRENCY]: ['', []],
      ori_Amount: ['', [this.AmountValidator]],
      ori_Cost: ['', [this.AmountInitValidator]],
      ori_Stock_Share: ['', [this.AmountInitValidator]],
      ori_Stock_Price: [{ value: '', disabled: true }, [this.AmountInitValidator]],
      ori_Stock_Ratio: ['', [this.AmountRatioValidator]],
      vendor_Code: ['', [Validators.required]],
      payment: [null, [Validators.required]],
      board_Meeting_Date: [null, []],
      outstanding_Shares: [null, []],
      remark: ['', []]
    });

    this.getBoTypeList();
  }


  rCheck = new RegExp('^([0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)|([-][0-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?)$');
  AmountInitValidator = (control: FormControl): { [s: string]: boolean } => {

    if (control.value) {
      if (!this.rCheck.test(control.value.toString())) {
        return { confirm: true, error: true };
      }
    }
    return {};
  };

  // 对金额增加必选校验
  AmountValidator = (control: FormControl): { [s: string]: boolean } => {
    // var r = new RegExp('^[+-]?[1-9]\\d{0,11}(\\.\\d{0,6})?$|^0(\\.\\d{0,6})?$');
    if (!control.value) {
      if (this.isInvestAmountUpdate) return { error: true, required: true };
      return {};
    } else if (!this.rCheck.test(control.value.toString())) {
      return { confirm: true, error: true };
    }
    return {};
  };

  //持股比例的校驗
  AmountRatioValidator = (control: FormControl): { [s: string]: boolean } => {
    var r = new RegExp('^(100|(([1-9]){1}[0-9]?|0{1})((\.)([0-9]){0,4})?)$');
    if (control.value) {
      if (!r.test(control.value.toString())) {
        return { confirm: true, error: true };
      }
    }
    return {};
  };

  createMessage(type: string): void {
    this.message.create(type, `已儲存變更`);
  }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem("userID").toUpperCase();

    // this.QueryBasicContent();
    this.QueryNameList();
    this.getSubjectList();
    this.getCurrency();
    this.getPaymentList();

    this.route.queryParams.subscribe(async params => {
      if (!!params['dataName']) {
        this.dataName = params['dataName'];
        this.name = params['dataName'];
        this.validateForm.get('Name').setValue(this.name);
      }
      if (!!params['amount_no']) { this.getAmountTableList(params['amount_no']); this.isAddOrUpdate = false; }
      else this.isAddOrUpdate = true;

      this.invest_no = params['invest_no'];
      if (!!params['dataName'] && !!params['invest_no']) {
        this.getHistoryTableList(params['invest_no'], params['dataName']);
      }
    });
  }

  ngOnDestory() {
    this.typeListSub.unsubscribe();
    this.kindListSub.unsubscribe();
    this.subjectListSub.unsubscribe();
    this.currencyListSub.unsubscribe();
  }






  submitForm(): void {
    if (this.validateForm.status == "INVALID") {
      Object.keys(this.validateForm.controls).forEach(key => {
        const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
        if (validationErrors != null) {
          Object.keys(validationErrors).forEach(keyError => {
            console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
          });
        }
      });
    }
    console.log(this.validateForm.value);


    if (this.validateForm.valid) {
      let data: RequestAmountRecord = {
        Amount_No: this.amount_no,
        Name: this.validateForm.get('Name').value,
        Type: this.validateForm.get('type').value,
        Type_Name: '',
        Record_Type: this.validateForm.get('record_type').value,
        Investment_Date: this.commonMethodService.timestampToTime(this.validateForm.get('investment_Date').value),
        Investment_Subject: this.validateForm.get('investment_Subject').value,
        Ori_Currency: this.validateForm.get('ori_Currency').value,
        Ori_Amount: this.validateForm.get('ori_Amount').value == '' ? null : this.validateForm.get('ori_Amount').value,
        Ori_Cost: this.validateForm.get('ori_Cost').value == '' ? null : this.validateForm.get('ori_Cost').value,
        Ori_Stock_Share: this.validateForm.get('ori_Stock_Share').value == '' ? null : this.validateForm.get('ori_Stock_Share').value,
        Ori_Stock_Price: this.validateForm.get('ori_Stock_Price').value == '' ? null : this.validateForm.get('ori_Stock_Price').value,
        Ori_Stock_Ratio: this.validateForm.get('ori_Stock_Ratio').value == '' ? null : this.validateForm.get('ori_Stock_Ratio').value,
        Remark: this.validateForm.get('remark').value
      }
      if (this.isAddOrUpdate) {
        this.CreateAmount(data);
      } else {
        data.Name = this.name;
        data.Type = this.tableListData.use_Type;
        this.UpdateAmount(data);
      }
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //輸入事件出發計算股價
  inputTxt() {
    let type = this.validateForm.get('record_type').value;
    let Amount = this.validateForm.get('ori_Amount').value;
    let Share = this.validateForm.get('ori_Stock_Share').value;
    if (type != '' && type != null) {
      if (type == '001' || type == '002') {
        if (Amount != '' && Amount != null && Share != '' && Share != null && Share != 0) {
          this.validateForm.get('ori_Stock_Price').setValue(Math.round(Amount / Share * 1000000) / 1000000);
        } else {
          this.validateForm.get('ori_Stock_Price').setValue('');
        }
      }
    }
  }

  //操作邏輯
  typeChange(data: string, isCleaer: boolean = true) {
    this.securityList.forEach((x) => {
      if (x.value == data) {
        this.code_Extend_A = x.code_Extend_A;
        if (x.code_Extend_A == 'Fund') {
          this.typeItemList = this.typeList.filter((e) => e.value !== '005');
        } else if (x.code_Extend_A == 'Security') {
          this.typeItemList = this.typeList.filter((e) => (e.value !== '007' && e.value !== '004'));
          if (x.code_Extend_B == 'S') {
            this.typeItemList = this.typeList.filter((e) => e.value !== '007');
          }
        }
        this.clearTypeValue();
        this.LinkLogic(isCleaer);
      }
    });
  }

  //修改证券种类 判定是否将类型清空
  clearTypeValue() {
    if (this.validateForm.get('record_type').value) {
      if (!(this.typeItemList.find((x) => x.value == this.validateForm.get('record_type').value))) {
        this.validateForm.get('record_type').setValue('');
      }
    }
  }

  //聯動邏輯
  LinkLogic(init: boolean) {
    let str = ['ori_Currency', 'ori_Amount', 'ori_Cost', 'ori_Stock_Share', 'ori_Stock_Price', 'ori_Stock_Ratio'];
    const recordType = this.validateForm.get('record_type').value;
    let data: string[] = [];
    if (this.code_Extend_A != null && this.code_Extend_A != '' && recordType != null && recordType != '') {
      if (this.code_Extend_A == 'Security' || this.code_Extend_A == 'Fund') {
        switch (recordType) {
          case '001':
            data = ['ori_Currency', 'ori_Amount', 'ori_Stock_Share'];
            break;
          case '002':
            data = ['ori_Currency', 'ori_Amount', 'ori_Cost', 'ori_Stock_Share'];
            break;
          case '003':
            data = ['ori_Currency', 'ori_Amount'];
            break;
          case '004':
            data = ['ori_Stock_Share'];
            break;
          case '005':
            data = ['ori_Currency', 'ori_Stock_Price'];
            break;
          case '006':
            data = ['ori_Currency', 'ori_Amount'];
            break;
          case '008':
            data = ['ori_Stock_Ratio'];
            break;
          case '009':
            data = ['ori_Currency', 'ori_Amount'];
            break;
          case '010':
            data = ['ori_Currency', 'ori_Amount'];
            break;
        }
      }
      if (this.code_Extend_A == 'Fund') {
        if (recordType == '007') data = ['ori_Currency', 'ori_Amount'];
      }
      if (this.code_Extend_A == 'Security') {
        if (recordType == '005') data = ['ori_Currency', 'ori_Stock_Price'];
      }
      this.logicOperation(str, data, init);
    }
  }

  //邏輯操作
  logicOperation(str: string[], data: string[], init: boolean) {
    this.MandatoryLogic(data, init);
    data.forEach((e) => {
      let index = str.indexOf(e);
      if (index > -1) {
        str.splice(index, 1);
      }
    });
    this.isShowRequired(str, data);
    this.ReadOnlyLogic(str);
  }

  //添加欄位邏輯
  MandatoryLogic(data: string[], init: boolean) {
    const { max, min } = MyValidators;
    if (data != null && data.length > 0) {
      for (let item of data) {
        //type 用于判断001、002选填栏位
        const recordType = this.validateForm.get('record_type').value;
        const type = this.validateForm.get('type').value;

        if (init) this.validateForm.get(item).setValue('');
        this.validateForm.get(item).clearValidators();
        if (item == 'ori_Stock_Ratio') this.validateForm.get(item).setValidators([Validators.required, max(100), min(0), this.AmountRatioValidator]);
        else if (item == 'ori_Currency') this.validateForm.get(item).setValidators([Validators.required]);
        else this.validateForm.get(item).setValidators([Validators.required, this.AmountValidator]);

        if (item == 'ori_Stock_Share') {
          this.validateForm.get(item).updateValueAndValidity();
          this.validateForm.get(item).enable();
          if (recordType == '001' && (type != '001' && type != '002' && (type == '003' || '004' || '005'))) {
            this.validateForm.get(item).clearValidators();
            this.validateForm.get(item).setValidators([this.AmountInitValidator]);
          }
          if (recordType == '002' && (type != '001' && type != '002' && (type == '003' || '004' || '005'))) {
            this.validateForm.get(item).clearValidators();
            this.validateForm.get(item).setValidators([this.AmountInitValidator]);
          }
        }
        if (item == 'ori_Cost') {
          this.validateForm.get(item).updateValueAndValidity();
          this.validateForm.get(item).enable();
          if (recordType == '002' && item == 'ori_Cost') {
            this.validateForm.get(item).clearValidators();
            this.validateForm.get(item).setValidators([this.AmountInitValidator]);
          }
        }
        this.validateForm.get(item).updateValueAndValidity();
        this.validateForm.get(item).enable();
      }
    }
  }

  //添加操作
  CreateAmount(data: RequestAmountRecord) {
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/Create", data).subscribe({
      next: (res) => {
        if (res) this.createMessage('success'); this.routingViewSkip(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //修改操作
  UpdateAmount(data: RequestAmountRecord) {
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/Update", data).subscribe({
      next: (res) => {
        if (res) this.createMessage('success'); this.routingViewSkip(res);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  QueryNameList() {
    this.httpClient.get<any>(environment.apiServerURL + "AmountRecord/QueryAmountRecordNameList").subscribe({
      next: (res) => {
        this.nameList = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    })
  }

  //獲取TableList展示内容
  getHistoryTableList(invest_no: string, NameTxt: string) {
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/QueryTabeList", null).subscribe({
      next: (res) => {
        if (invest_no != null && invest_no != '') {
          res = res.filter((e) => e.invest_No == invest_no);
        }
        if (NameTxt != null && NameTxt != '') {
          res = res.filter((e) => e.name == NameTxt);
        } else {
          res = res.filter(() => false);
        }
        this.historyTableList = [...res];
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getAmountTableList(amount_no: string) {
    let formData = new FormData();
    formData.set("amount_no", amount_no);
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/QueryTabeList", formData).subscribe({
      next: (res) => {
        if (res != null) {
          console.log(res);

          this.tableListData = res[0];
          this.dataName = res[0].name;
          this.name = res[0].name;
          this.typeName = res[0].type_Name;
          this.amount_no = res[0].amount_No;

          this.validateForm.get('Name').setValue(res[0].name);
          this.validateForm.get('Name').disable();
          this.validateForm.get('type').setValue(res[0].type);
          this.validateForm.get('type').disable();
          this.validateForm.get('investment_Subject').setValue(res[0].investment_Subject);
          this.validateForm.get('record_type').setValue(res[0].record_Type);
          this.validateForm.get('investment_Date').setValue(res[0].investment_Date);
          this.validateForm.get('ori_Currency').setValue(res[0].ori_Currency);
          this.validateForm.get('ori_Amount').setValue(res[0].ori_Amount);
          this.validateForm.get('ori_Cost').setValue(res[0].ori_Cost);
          this.validateForm.get('ori_Stock_Share').setValue(res[0].ori_Stock_Share);
          this.validateForm.get('ori_Stock_Price').setValue(res[0].ori_Stock_Price);
          if (res[0].ori_Stock_Ratio == '' || res[0].ori_Stock_Ratio == null) this.validateForm.get('ori_Stock_Ratio').setValue('');
          else this.validateForm.get('ori_Stock_Ratio').setValue(res[0].ori_Stock_Ratio * 1000000 / 10000);
          this.validateForm.get('remark').setValue(res[0].remark);
          this.requestService.getCodeName('001');
          this.kindListSub = this.requestService.kindListObservable.subscribe(item =>
            (item as codeNameItem[]).forEach((e) => {
              if (e.value == res[0].type) {
                this.code_Extend_A = e.code_Extend_A;
              }
            })
          );
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //獲取主體
  getSubjectList() {
    this.commonMethodService.getCodeName('009');
    this.subjectListSub = this.commonMethodService.subjectListObservable.subscribe(item => this.subjectList = item as codeNameItem[]
    );
  }

  //获取boType
  getBoTypeList() {
    this.commonMethodService.getCodeName('004');
    this.boTypeListSub = this.commonMethodService.boListObservable.subscribe(item => this.boTypeList = item as codeNameItem[]
    );
  }
  //取消欄位邏輯
  ReadOnlyLogic(data: string[]) {
    if (data != null && data.length > 0) {
      for (let item of data) {
        this.validateForm.get(item).setValue('');
        this.validateForm.get(item).clearValidators();
        this.validateForm.get(item).disable();
        this.validateForm.get(item).updateValueAndValidity();
      }
      this.inputTxt();
    }
  }

  //是否展示必選圖標
  isShowRequired(str: string[], data: string[]) {
    data.forEach((e) => {
      switch (e) {
        case 'ori_Currency':
          this.isCurrency = true;
          break;
        case 'ori_Amount':
          this.isAmount = true;
          break;
        case 'ori_Cost':
          this.isCost = true;
          break;
        case 'ori_Stock_Share':
          this.isShare = true;
          break;
        case 'ori_Stock_Price':
          this.isPrice = true;
          break;
        case 'ori_Stock_Ratio':
          this.isRatio = true;
          break;
      }
    })
    str.forEach((e) => {
      switch (e) {
        case 'ori_Currency':
          this.isCurrency = false;
          break;
        case 'ori_Amount':
          this.isAmount = false;
          break;
        case 'ori_Cost':
          this.isCost = false;
          break;
        case 'ori_Stock_Share':
          this.isShare = false;
          break;
        case 'ori_Stock_Price':
          this.isPrice = false;
          break;
        case 'ori_Stock_Ratio':
          this.isRatio = false;
          break;
      }
    });
    //type 用于判断001、002选填栏位
    const recordType = this.validateForm.get('record_type').value;
    const type = this.validateForm.get('type').value;

    if (recordType == '001' && (type != '001' && type != '002' && (type == '003' || '004' || '005'))) {
      this.isShare = false;
    }
    if (recordType == '002' && data.includes('ori_Cost')) {
      this.isCost = false;
    }
    if (recordType == '002' && (type != '001' && type != '002' && (type == '003' || '004' || '005'))) {
      this.isShare = false;
      this.isCost = false;
    }
  }

  //获取交付或付款条件
  getPaymentList() {
    this.commonMethodService.getCodeName('023');
    this.paymentListSub = this.commonMethodService.paymentListObservable.subscribe(item => this.paymentList = item as codeNameItem[])
  }

  //獲取幣別
  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  //捨棄事件
  giveUp(event) {
    event.preventDefault();
    this.isVisible = true;
  }

  //刪除確認框
  handleOk(): void {
    this.isVisible = false;
    this.routingSkip();
  }

  //刪除取消框
  handleCancel(): void {
    this.isVisible = false;
  }

  //路由跳转
  routingSkip() {
    this.router.navigate(['amountMaintain']);
  }

  //路由跳转
  routingViewSkip(data: number) {
    /**
     * 路由跳轉時需要兩個參數，amount_no  以及 Table_Name
     * 新增時，如果table_Name有值，需要傳遞，amount_no 為 '' ，衹有進入新增頁面時需要
     * 修改時，amount_no 需要有值，table_Name 可爲空或非空  進入檢視區域是不需要傳遞table_Name
    */
    this.router.navigate(['amountMaintain/tableView'], { queryParams: { amount_No: data, table_Name: this.validateForm.get('Name').value } });
  }

  updateActive() { }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

}

export type MyErrorsOptions = { 'zh-cn': string; en: string } & Record<string, NzSafeAny>;
export type MyValidationErrors = Record<string, MyErrorsOptions>;
export class MyValidators extends Validators {
  static override min(min: number): ValidatorFn {
    return (control: AbstractControl): MyValidationErrors | null => {
      if (Validators.min(min)(control) === null) {
        return null;
      }
      return { min: { 'zh-cn': `最小值为 ${min}`, en: `Min is ${min}` } };
    };
  }

  static override max(max: number): ValidatorFn {
    return (control: AbstractControl): MyValidationErrors | null => {
      if (Validators.max(max)(control) === null) {
        return null;
      }
      return { max: { 'zh-cn': `最值为 ${max}`, en: `Max is ${max}` } };
    };
  }
}