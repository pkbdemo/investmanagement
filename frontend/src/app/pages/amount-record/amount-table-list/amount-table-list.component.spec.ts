import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountTableListComponent } from './amount-table-list.component';

describe('AmountTableListComponent', () => {
  let component: AmountTableListComponent;
  let fixture: ComponentFixture<AmountTableListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmountTableListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
