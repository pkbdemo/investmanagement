import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';

import { codeNameItem, AmountRecordItem } from '../amountMaintainModel';
import { RequestService } from '../services/requestData.service';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { AuthorityControlService } from 'src/app/service/authority-control.service';

@Component({
  selector: 'app-amount-table-list',
  templateUrl: './amount-table-list.component.html',
  styleUrls: ['./amount-table-list.component.css']
})
export class AmountTableListComponent implements OnInit {

  @Input() invest_no: string;

  @Input() set Name(data: string) {
    console.log(data);

    if (!!data) this.table_Name = data;
  }

  //控制從基金或公司維護進入时需要隐藏的内容

  isShowView: boolean = true;

  //儅invest_no 不爲空時代表，從基金或公司維護進入，需要保存name值新增時傳入新增頁面
  table_Name: string = '';

  checked = false;
  indeterminate = false;



  userId: string;

  isShowSelect: boolean = false;
  isShowSelectBtn: boolean = true;
  isShowInsert: boolean = true;
  //刪除確認框
  isVisible = false;
  deleteStr: string[] = [];
  //刪除請求内容
  request: number[] = [];
  //搜索條件
  searchValue: string;
  typeValue: string[] = [];
  subjectValue: string;
  currencyValue: string;
  listOfSecurityValue: string[] = [];
  SecurityValueId: string[] = [];
  boTypeValue: string[] = [];



  set typeValueData(data) {
    this.typeValue = data;
    this.search();
  }

  set subjectValueData(data) {
    this.subjectValue = data;
    this.search();
  }

  set currencyValueData(data) {
    this.currencyValue = data;
    this.search();
  }

  //下拉選礦展示數據
  typeList: codeNameItem[] = [];
  kindList: codeNameItem[] = [];
  subjectList: codeNameItem[] = [];
  boTypeList: codeNameItem[] = [];
  currencyList: string[] = [];

  typeLsitSub: Subscription;
  currencySub: Subscription;
  kindListSub: Subscription;
  boTypeSub: Subscription;
  subjectListSub: Subscription;
  currencyListSub: Subscription;

  //checkBox選取
  setOfCheckedId = new Set<number>();

  //原數據存放位置
  listOfData: AmountRecordItem[] = [];
  //用於頭部My 或All篩選資料
  tableListHeaderData: AmountRecordItem[] = [];
  //用於detail内部篩選
  tableListDetailData: AmountRecordItem[] = [];
  //頁面展示TableList
  tableListData: AmountRecordItem[] = [];

  listOfColumn = [
    {
      title: '公司全稱',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.use_Name > b.use_Name ? 1 : -1,
      priority: 1
    },
    {
      title: '證券/基金種類',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.use_Type_Name > b.use_Type_Name ? 1 : -1,
      priority: 2
    },
    {
      title: '合約日期',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.effective_Date.getTime() > b.effective_Date.getTime() ? 1 : -1,
      priority: 3
    },
    {
      title: '盡職調查日期',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => new Date(a.dD_Close_Date).getTime() > new Date(b.dD_Close_Date).getTime() ? 1 : -1,
      priority: 4
    },
    {
      title: '投資主體',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.investment_Subject > b.investment_Subject ? -1 : 1,
      priority: 5
    },
    {
      title: 'BO TYPE',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.bo > b.bo ? -1 : 1,
      priority: 6
    },
    {
      title: '類型',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.bo > b.bo ? -1 : 1,
      priority: 6
    },
    {
      title: '日期',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.investment_Date.getTime() > b.investment_Date.getTime() ? -1 : 1,
      priority: 6
    },
    {
      title: '幣別',
      compare: (a: AmountRecordItem, b: AmountRecordItem) => a.ori_Currency > b.ori_Currency ? -1 : 1,
      priority: 6
    },
    {
      title: '金額(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '處分成本(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '股價(原幣別)',
      compare: null,
      priority: false
    },
    {
      title: '股數',
      compare: null,
      priority: false
    },
    {
      title: '持股比例',
      compare: null,
      priority: false
    },
    {
      title: 'Vendor code',
      compare: null,
      priority: false
    },
    {
      title: 'Vendor Name',
      compare: null,
      priority: false
    },
    {
      title: '交付或付款條件',
      compare: null,
      priority: false
    },
    {
      title: '董事會日期',
      compare: null,
      priority: false
    },
    {
      title: '該公司發行股份總數',
      compare: null,
      priority: false
    },
    {
      title: '備註',
      compare: null,
      priority: false
    },
    {
      title: '請繳款單號',
      compare: null,
      priority: false
    },
    {
      title: '換算幣別',
      compare: null,
      priority: false
    },
    {
      title: '金額(美金)',
      compare: null,
      priority: false
    },
    {
      title: '處分成本(美金)',
      compare: null,
      priority: false
    },
    {
      title: '股價(美金)',
      compare: null,
      priority: false
    }
  ];
  listFilter = {
    listOfFilter: [
      { text: 'Joe', value: 'Joe' },
      { text: 'Jim', value: 'Jim' }
    ],
    filterFn: (list: string[], item: AmountRecordItem) => list.some(name => item.investment_Subject.indexOf(name) !== -1)
  }
  constructor(private requestService: RequestService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    public router: Router,
    private commonMethodService: CommonMethodService,
    private authorityControlService: AuthorityControlService
  ) { }

  ngOnInit(): void {
    this.getKindList();
    this.getTypeList();
    this.getSubjectList();
    this.getCurrency();
    this.getboTypeList();
    this.getAmountTableList();
    this.userId = sessionStorage.getItem("userID").toUpperCase();
    this.authorityControlService.getUserRole().then(
      userRole => {
        if (!userRole.isIT && !userRole.isIMDMember && !userRole.isBoardSecretariatMember) {
          this.isShowSelectBtn = false;
        } else {
          this.isShowSelectBtn = true;
          this.isShowInsert = true;
        }
      }
    );
  }

  ngOnChanges() {
    if (!!this.invest_no) {
      this.isShowView = false;
      this.getContact(this.invest_no);
    }
  }

  ngOnDestory() {
    this.typeLsitSub.unsubscribe();
    this.currencySub.unsubscribe();
    this.kindListSub.unsubscribe();
    this.subjectListSub.unsubscribe();
    this.currencyListSub.unsubscribe();
  }

  boTypeCheckBox() {
    setTimeout(() => {
      this.boTypeValue = this.boTypeList.filter(e => e.checked).map(e => e.value);
      this.search();
    });
  }

  typeCheckBox() {
    setTimeout(() => {
      this.typeValue = this.typeList.filter(e => e.checked).map(e => e.value);
      this.search();
    });
  }

  // 搜索
  search() {
    this.searchValue = this.searchValue == null ? '' : this.searchValue;
    this.tableListData = this.tableListDetailData.filter((item: AmountRecordItem) => {
      return (item.use_Name.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1)
    });

    //Type搜索
    if (!!this.typeValue && this.typeValue.length > 0) {
      this.tableListData = this.tableListData.filter((item: AmountRecordItem) => {
        return this.typeValue.some(e => item.record_Type == e)
      });
    }


    //Bo Type搜索
    if (!!this.boTypeValue && this.boTypeValue.length > 0) {
      this.tableListData = this.tableListData.filter((item: AmountRecordItem) => {
        return this.boTypeValue.some(e => item.bo == e)
      });
    }

    //幣別搜索
    if (this.currencyValue != null && this.currencyValue != '') {
      this.tableListData = this.tableListData.filter((item: AmountRecordItem) => {
        return (item.ori_Currency == this.currencyValue)
      });
    }

    //基金/公司類別搜索
    if (this.SecurityValueId != null && this.SecurityValueId.length > 0) {
      this.tableListData = this.tableListData.filter((item: AmountRecordItem) => {
        return this.SecurityValueId.some((x) => {
          return item.use_Type.includes(x);
        })
      });
    }
  }

  //Delete
  DeleteAmount() {
    this.deleteStr = [];
    this.request = [];
    let idList = [...this.setOfCheckedId];
    if (idList != null && idList.length > 0) {
      this.tableListData.forEach((e) => {
        if (idList.includes(e.id)) {
          this.request.push(e.amount_No);
          this.deleteStr.push(e.use_Name + '-' + e.use_Type_Name)
        }
      });
    }
  }

  //刪除確認框
  handleOk(): void {
    this.isVisible = false;
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/Delete", this.request).subscribe({
      next: (res) => {
        this.setOfCheckedId.clear();
        this.getAmountTableList();
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //刪除取消框
  handleCancel(): void {
    this.isVisible = false;
  }

  //My 或 All 觸發方法
  UpdateHeaderDate(data?: string) {
    if (data == 'My List') data = this.userId;
    else data = '';
    this.tableListHeaderData = this.listOfData.filter((item: AmountRecordItem) => {
      return item.contact_Ao.toUpperCase().indexOf(data.toUpperCase()) !== -1
    });
    this.tableListDetailData = [...this.tableListHeaderData];
    this.tableListData = [...this.tableListDetailData];
  }

  //清空類別搜索框
  closeSelectValue() {
    this.SecurityValueId = this.SecurityValueId.filter(d => d == '-1');
    for (let item of this.kindList) {
      item.checked = false;
    }
    this.typeList.forEach(e => e.checked = false);
    this.boTypeList.forEach(e => e.checked = false)
    this.typeValue = [];
    this.subjectValue = '';
    this.currencyValue = '';
    this.boTypeValue = [];
    this.search();
  }

  CompanyTransaction(data: codeNameItem) {
    if (data.checked) {
      this.SecurityValueId = [...this.SecurityValueId, data.value];
    } else {
      this.SecurityValueId = this.SecurityValueId.filter(d => d !== data.value);
    }
    this.search();
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.tableListData.filter(({ contact_Ao }) => contact_Ao == this.userId);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onAllChecked(checked: boolean): void {
    this.tableListData
      .filter(({ contact_Ao }) => contact_Ao == this.userId)
      .forEach(item => {
        if (item.create_User == this.userId && item.data_Source == '001') this.updateCheckedSet(item.id, checked)
      });
    this.refreshCheckedStatus();
  }

  //獲取公司負責人
  getContact(name: string) {
    let formDate = new FormData();
    formDate.set('request', '');
    formDate.set('invest_no', name);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/FindContactAo", formDate).subscribe({
      next: (res) => {
        if (!!res) {
          if (this.userId == res) this.isShowInsert = true;
          else this.isShowInsert = false;
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getAmountTableList() {
    this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/QueryTabeList", null).subscribe({
      next: (res) => {

        if (!!this.invest_no) {
          res = res.filter(e => e.invest_No == this.invest_no);
          if (res.length > 0) this.table_Name = res[0].name;
        }
        this.listOfData = [...res];
        let filterMap = new Map<string, any>();

        this.listOfData.forEach(e => {
          if (!!e.investment_Subject) {
            filterMap.set(e.investment_Subject, e.investment_Subject_Name)
          }
        })
        this.listFilter.listOfFilter = [...filterMap.entries()].map(e => {
          return { text: e[1], value: e[0] }
        })

        this.tableListHeaderData = [...this.listOfData];
        this.tableListDetailData = [...this.tableListHeaderData];
        this.tableListData = [...this.tableListDetailData];

        this.UpdateHeaderDate(this.requestService.listSelectedTxt);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getboTypeList() {
    this.commonMethodService.getCodeName('004');
    this.boTypeSub = this.commonMethodService.boListObservable.subscribe(item => this.boTypeList = item as codeNameItem[]
    );
  }
  getKindList() {
    this.commonMethodService.getCodeName('001');
    this.kindListSub = this.commonMethodService.securityListObservable.subscribe(item => this.kindList = item as codeNameItem[]
    );
  }

  getTypeList() {
    this.requestService.getCodeName('008');
    this.typeLsitSub = this.requestService.typeListObservable.subscribe(item => this.typeList = item as codeNameItem[]
    );
  }

  getSubjectList() {
    this.requestService.getCodeName('009');
    this.subjectListSub = this.requestService.subjectListObservable.subscribe(item => this.subjectList = item as codeNameItem[]
    );
  }

  getCurrency() {
    this.requestService.RequestCurrencyList();
    this.currencyListSub = this.requestService.currencyObservable.subscribe(item => this.currencyList = item as string[]
    );
  }

  //路由跳转
  routingSkip(data: number) {
    /**
     * 路由跳轉時需要兩個參數，amount_no  以及 dataName
     * 新增時，如果dataName有值，需要傳遞，amount_no 為 '' ，衹有進入新增頁面時需要
     * 修改時，amount_no 需要有值，dataName 可爲空或非空  進入檢視區域是不需要傳遞dataName
    */
    this.router.navigate(['amountMaintain/tableView'], { queryParams: { amount_No: data, dataName: this.table_Name } });
  }

  //路由跳轉到操作頁面
  routingOperate() {
    this.router.navigate(['amountMaintain/operate'], { queryParams: { amount_no: null, dataName: this.table_Name, invest_no: this.invest_no } });
  }

}
