
export enum ShowBtn { Update, Delete, insert }
//新增修改页面必填栏位
export enum REQUIREDCOLUMN {
    Name = 'Name', Use_Type = 'Use_Type', INVESTMENT_SUBJECT = 'Investment_Subject', BO = 'Bo',
    RECORD_TYPE = 'Record_Type', INVESTMENT_DATE = 'Investment_Date', ORI_CURRENCY = 'Ori_Currency',
    ORI_AMOUNT = 'Ori_Amount', ORI_COST = 'Ori_Cost', ORI_STOCK_SHARE = 'Ori_Stock_Share',
    ORI_STOCK_RATIO = 'Ori_Stock_Ratio', VENDOR_CODE = 'Vendor_Code', VENDOR_NAME = 'Vendor_Name',
    PAYMENT = 'Payment', REMARK = 'Remark'
}

export interface codeNameItem {
    name?: string;
    label: string;
    value: string;
    code_Extend_A?: string;
    code_Extend_B?: string;
    code_Extend_C?: string;
    checked?: boolean;
    disabled?: boolean;
}

export interface AmountRecordItem {

    id?: number;
    amount_No?: number;
    invest_No?: string;
    category?: string;
    contact_Ao?: string;
    use_Name?: string;
    security_No?: string;
    use_Type?: string;
    use_Type_Name?: string;
    effective_Date?: Date;
    dD_Close_Date?: string;
    investment_Subject?: string;
    investment_Subject_Name?: string;
    bo?: string;
    bo_Name?: string;
    record_Type?: string;
    record_Type_Name?: string;
    investment_Date?: Date;
    ori_Currency?: string;
    ori_Amount?: number;
    ori_Cost?: number;
    ori_Stock_Ratio?: number;
    ori_Stock_Share?: number;
    ori_Stock_Price?: number;
    vendor_Code?: string;
    vendor_Name?: string;
    payment?: string;
    payment_Name?: string;
    board_Meeting_Date?: Date;
    outstanding_Shares?: number;
    remark?: string;
    external_Pid?: string;
    trans_Currency?: string;
    trans_Amount?: number;
    trans_Cost?: number;
    trans_Stock_Price?: number;
    data_Source?: string;
    create_User?: string;
}
export interface BasicContentModel {
    Name: string;
    Use_Type: string;
    Category: string;
    Contact_Ao: string;
    Security_No: string;
    Code_Extend_A: string;
    Code_Extend_B: string;
}

export interface RequestAmountRecord {
    Amount_No: number;
    Name: string;
    Type: string;
    Type_Name: string;
    Record_Type: string;
    Investment_Date: string;
    Investment_Subject: string;
    Ori_Currency: string;
    Ori_Amount: number;
    Ori_Cost: number;
    Ori_Stock_Share: number;
    Ori_Stock_Price: number;
    Ori_Stock_Ratio: number;
    Remark: string;
}