import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { environment } from '@environments/environment';

import { codeNameItem } from '../amountMaintainModel';

@Injectable({
  providedIn: 'root'
})
export class RequestService {


  //離開TableList的頁面内容
  listSelectedTxt: string = 'My List';
  currencyList = new Subject();
  reportTypeList = new Subject();
  resultsTypeList = new Subject();
  quarterList = new Subject();

  // 金額記錄
  typeList = new Subject();
  kindList = new Subject();
  subjectList = new Subject();

  currencyObservable = this.currencyList.asObservable();
  reportTypeObservable = this.reportTypeList.asObservable();
  resultsTypeObservable = this.resultsTypeList.asObservable();
  quarterObservable = this.quarterList.asObservable();

  //金額記錄
  typeListObservable = this.typeList.asObservable();
  kindListObservable = this.kindList.asObservable();
  subjectListObservable = this.subjectList.asObservable();

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) {
  }

  RequestCurrencyList() {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryCurrencyList").subscribe({
      next: (res) => {
        if (res != null) this.currencyList.next([...res]);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getCodeName(Kind_id: string) {
    let result: codeNameItem[] = [];
    let formData = new FormData();
    formData.set("Kind_id", Kind_id);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData).subscribe({
      next: (res) => {
        if (res != null) {
          for (let item of res) {
            let code: codeNameItem = {
              label: '',
              value: '',
              code_Extend_A: '',
              code_Extend_B: '',
              code_Extend_C: '',
              checked: false,
              disabled: true
            };
            code.label = item.code_Name;
            code.value = item.code_id;
            code.code_Extend_A = item.code_Extend_A;
            code.code_Extend_B = item.code_Extend_B;
            code.code_Extend_C = item.code_Extend_C;
            code.checked = false;
            code.disabled = true;
            result = [...result, code];
          }
          if (Kind_id == '017') this.reportTypeList.next([...result]);
          else if (Kind_id == '016') this.resultsTypeList.next([...result]);
          else if (Kind_id == '010') this.quarterList.next([...result].filter((e) => e.code_Extend_A == 'Quarter'));
          else if (Kind_id == '008') this.typeList.next([...result]);
          else if (Kind_id == '001') this.kindList.next([...result]);
          else if (Kind_id == '009') this.subjectList.next([...result]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  sendListSelectedTxt(data: string) {
    this.listSelectedTxt = data;
  }


}
