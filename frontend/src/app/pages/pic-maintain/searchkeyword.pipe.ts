import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'searchkeyword'
})
@Injectable()
export class SearchkeywordPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(val: string, searchkeyword: string): any {
    // searchkeyword
    val = val.toUpperCase();
    searchkeyword = searchkeyword.toUpperCase();
    const Reg = new RegExp(searchkeyword, 'g'); // important
    if (val) {
      const objToStr = JSON.stringify(val).replace('\"', '').replace('\"', '');

      const res = objToStr.replace(Reg, `<a style="color:#3074B8;font-weight: bold;">${searchkeyword}</a>`);
      return this.sanitizer.bypassSecurityTrustHtml(res);
    }
  }
}
