import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicMaintainComponent } from './pic-maintain.component';

describe('PicMaintainComponent', () => {
  let component: PicMaintainComponent;
  let fixture: ComponentFixture<PicMaintainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicMaintainComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PicMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
