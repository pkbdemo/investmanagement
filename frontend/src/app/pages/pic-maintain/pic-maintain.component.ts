import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { environment } from '@environments/environment';

import { investMainModel } from './investMainModel'
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { LayoutService } from '../layout/services/layout.service';

@Component({
  selector: 'app-pic-maintain',
  templateUrl: './pic-maintain.component.html',
  styleUrls: ['./pic-maintain.component.css']
})

export class PicMaintainComponent implements OnInit {

  expandSet = new Set<number>();
  listOfData: any[];
  listOfPicCompany: any[];
  listOfPic: any[];
  date = null;
  listOfSecurityValue: string[] = [];
  searchValue: string = '';
  ddlValue: string = '';
  userName: string = '';
  userId: string = '';
  transferAreaIsVisible = false;
  investList: Array<investMainModel> = [];

  constructor(private httpClient: HttpClient,
    private layoutService: LayoutService,
    private toastr: ToastrService,
    private router: Router,
    private authorityControlService: AuthorityControlService) { }

  ngOnInit(): void {
    this.authorityControlService.getUserRole().then(
      userRole => {
        if (!userRole.isIT && !userRole.isManagerOfIMD) {
          Swal.fire(IMSConstants.swalTitle.error, "您無權限訪問該頁面，點擊OK後會返回首頁", "error").then((result) => {
            this.router.navigateByUrl("");
          });
        }
      }
    );

    this.Query();
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  Query(): void {
    let formData1 = new FormData();
    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryPic", formData1).subscribe({
      next: (res) => {
        this.listOfData = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  search(arg: number): void {
    let formData1 = new FormData();
    formData1.set("condition", this.searchValue);
    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryPic", formData1).subscribe({
      next: (res) => {
        this.listOfData = res;
        if (arg != -1 && this.searchValue.length > 0) {//一般查詢，查詢後expandSet Add 值
          this.expandSet.clear();
          this.listOfData.forEach((value, index) => {
            if (value.name_list.toUpperCase().indexOf(this.searchValue.toUpperCase()) !== -1) {
              this.expandSet.add(value.contact_ao);
            }
          });
        }
        else {//清除查詢內容後查詢，不用再把expandSet Add 值
          this.expandSet.clear();
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  clearValue(): void {
    this.searchValue = '';
    this.search(-1);
  }

  transfer1(key: number, name: string) {
    this.listOfPicCompany.forEach((value, index) => {
      if (value.invest_no == key) {
        let Obj = new investMainModel();
        Obj.name = name;
        Obj.invest_no = key;
        this.investList.push(Obj);
        this.listOfPicCompany.splice(index, 1);
      }
    });
    if (this.investList.length > 0)
      $('#idBottomdiv').removeClass('bottomdivDashed').addClass('bottomdivSolid');//灰色虛線與實線切換
    else
      $('#idBottomdiv').removeClass('bottomdivSolid').addClass('bottomdivDashed');
  }

  transfer2(key: number, name: string) {
    this.investList.forEach((value, index) => {
      if (value.invest_no == key) {
        let Obj = new investMainModel();
        Obj.name = name;
        Obj.invest_no = key;
        this.listOfPicCompany.push(Obj);
        this.investList.splice(index, 1);
      }
    });
    if (this.investList.length > 0)
      $('#idBottomdiv').removeClass('bottomdivDashed').addClass('bottomdivSolid');
    else
      $('#idBottomdiv').removeClass('bottomdivSolid').addClass('bottomdivDashed');
  }

  onChange(result: Date[]): void {
    console.log('onChange: ', result);
  }

  onExpandChange(contact_ao: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(contact_ao);
    } else {
      this.expandSet.delete(contact_ao);
    }
  }

  ExpandClick(contact_ao: number): void {
    if (this.expandSet.has(contact_ao)) {
      this.expandSet.delete(contact_ao);
    }
    else
      this.expandSet.add(contact_ao);
  }

  showTransferAreaModal(userid, name_a) {
    this.userId = userid;
    this.userName = name_a;
    let formData1 = new FormData();
    formData1.set("userid", userid);
    let formData2 = new FormData();
    formData2.set("userid", userid);
    formData2.set("deptid", 'WS1100');

    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryNameByContactAo", formData1).subscribe({
      next: (res) => {
        this.listOfPicCompany = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
    this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/QueryFindwHcmEmpByDeptExcludeSearchId", formData2).subscribe({
      next: (res) => {
        this.listOfPic = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
    this.transferAreaIsVisible = true;
  }

  TransferCancel(): void {
    this.ddlValue = '';
    this.investList = [];
    this.transferAreaIsVisible = false;
  }

  TransferSubmit(): void {
    //check required data
    if (!this.ddlValue)
      this.toastr.warning("對象不能為空");
    else if (this.investList.length <= 0) {
      this.toastr.warning("公司/基金名稱標籤不能為空");
    }
    else {//start transfer
      var JsonString = JSON.stringify(this.investList);
      let formData = new FormData();
      formData.append('investList', JsonString);
      formData.append('userid', this.ddlValue.toString());
      this.httpClient.post<any>(environment.apiServerURL + "PicMaintain/UpdateInvestMain", formData).subscribe({
        next: async (res) => {
          this.toastr.success("轉移任務成功");
          await this.Query();
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("轉移任務失敗");
        }
      });
      this.ddlValue = '';
      this.investList = [];
      this.transferAreaIsVisible = false;
    }
  }
}
