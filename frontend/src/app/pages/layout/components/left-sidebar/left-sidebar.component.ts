import { Component, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

import { environment } from 'src/environments/environment';
import { LayoutService } from '../../services/layout.service';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { CommonMethodService } from 'src/app/service/common-method.service';

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSidebarComponent implements OnInit {
  public activeMenu: string = "";

  userDetails: KeycloakProfile;
  accessToken: string;
  responseResult: string;
  userName: string;
  isLocalhost = environment.isLocalhost;
  isManagerOfIMDOrIT = false;
  isBoardSecretariatMember = false;

  subscription: Subscription;

  InProfressCount: any;

  public MyFavorite: any;

  constructor(private layoutService: LayoutService,
    private keycloakService: KeycloakService,
    private authorityControlService: AuthorityControlService,
    private toastr: ToastrService,
    private commonMethod: CommonMethodService,
    private httpClient: HttpClient) {
    this.subscription = this.layoutService.titleObservable.subscribe(src => this.InProfressCount = src);
  }

  async ngOnInit() {
    if (await this.keycloakService.isLoggedIn()) {
      this.userDetails = await this.keycloakService.loadUserProfile();
      this.checkValidUser();
    }
    let userID = sessionStorage.getItem("userID");

    if (userID == null) {
      sessionStorage.setItem("userID", this.userDetails.username);
      sessionStorage.setItem("userName", this.userDetails.firstName);
    } else {
      if (userID != this.userDetails.username) {
        sessionStorage.setItem("userID", this.userDetails.username);
        sessionStorage.setItem("userName", this.userDetails.firstName);
      }
    }

    $(".sidebar .menu .list li").on("click", function () {
      let hasSubMenu = $(this).has("ul").length > 0;

      if (!hasSubMenu) {
        $(".sidebar .menu .list li").removeClass("active");
        $(this).addClass("active");
      }

      // Clear the selected tab index in the session cache
      sessionStorage.removeItem(IMSConstants.sessionStorageKey.selectedTabIndex.viewCompany);
      sessionStorage.removeItem(IMSConstants.sessionStorageKey.selectedTabIndex.viewFund);
    });

    this.authorityControlService.getUserRole().then(
      userRole => {
        if (userRole.isIT || userRole.isManagerOfIMD) {
          this.isManagerOfIMDOrIT = true;
        }

        if (userRole.isBoardSecretariatMember) {
          this.isBoardSecretariatMember = true;
        }
      }
    );

    this.commonMethod.leftSiderEditObservable.subscribe(data => {
      if (!!data) {
        $(".sidebar .menu .list li").removeClass("active");
        $('.' + data).addClass('active');
      }
    })

  }

  openItem(item: string) {
    if (this.activeMenu == item) {
      this.activeMenu = "";
    } else {
      this.activeMenu = item;
    }
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  clickLogo() {
    $(".sidebar .menu .list li").removeClass("active");
  }

  checkValidUser() {
    this.httpClient.get<any>(environment.apiServerURL + "SettingUser/QueryIsValidUser").subscribe({
      next: (res) => {
        if (res == false) {
          let Message = "您所登入的使用者帳號沒有訪問權限, <br> 點擊OK後會自動登出系統";

          sessionStorage.clear();

          Swal.fire(IMSConstants.swalTitle.unauth, Message, "error").then((result) => {
            this.keycloakService.logout(window.location.protocol + "//" + window.location.host);
          });
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }
}