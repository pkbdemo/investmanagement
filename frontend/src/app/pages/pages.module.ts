import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { A11yModule } from '@angular/cdk/a11y';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzElementPatchModule } from 'ng-zorro-antd/core/element-patch'
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzTreeViewModule } from 'ng-zorro-antd/tree-view';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzTransferModule } from 'ng-zorro-antd/transfer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { FileUploadComponent } from './Utilities/file-upload/file-upload.component';
import { PagesRoutingModule } from './pages-routing.module';
import { AdminComponent } from './layout/components/admin/admin.component';
import { OverlayMenuComponent } from './layout/components/overlay-menu/overlay-menu.component';
import { RightSidebarComponent } from './layout/components/right-sidebar/right-sidebar.component';

import { LeftSidebarComponent } from './layout/components/left-sidebar/left-sidebar.component';
import { SettingsComponent } from './layout/components/settings/settings.component';

import { NgSortableHeader } from './directive/sortable.directive';

import { BasicformComponent } from './examples/basicform/basicform.component';
import { AdvancedformComponent } from './examples/advancedform/advancedform.component';
import { FormexamplesComponent } from './examples/formexamples/formexamples.component';
import { FormvalidationComponent } from './examples/formvalidation/formvalidation.component';
import { SummernoteComponent } from './examples/summernote/summernote.component';
import { CallapiComponent } from './examples/callapi/callapi.component';
import { GridviewComponent } from './examples/gridview/gridview.component';
import { VendorCodeFundUtilities } from './examples/vendorcodefundutilities/vendor-code-fund-utilities.component';
import { VendorCodeSearchComponent } from './Utilities/vendor-code-search/vendor-code-search.component';
import { BoPicSearchComponent } from './Utilities/bo-pic-search/bo-pic-search.component';
import { IndexComponent } from './index/index.component';
import { DueDiligenceComponent } from './investigation/duediligence/due-diligence/due-diligence.component';
import { TableListComponent } from './investigation/duediligence/table-list/table-list.component';
import { ActionReportsComponent } from './action-reports/action-reports.component';
import { PicMaintainComponent } from './pic-maintain/pic-maintain.component';
import { SearchkeywordPipe } from './pic-maintain/searchkeyword.pipe';
import { ReplaceCommaPipe } from './fund-maintain/replaceComma.pipe';
import { AmountTableListComponent } from './amount-record/amount-table-list/amount-table-list.component';
import { AmountHeaderComponent } from './amount-record/amount-header/amount-header.component';
import { AmountTableViewComponent } from './amount-record/amount-table-view/amount-table-view.component';
import { AmountOperateComponent } from './amount-record/amount-operate/amount-operate.component';
import { CurrencyRateComponent } from './currency-rate/currency-rate.component';
import { CompanyOperateComponent } from './company-maintain/company-operate/company-operate.component';
import { CompanyListComponent } from './company-maintain/company-list/company-list.component';
import { FundOperateComponent } from './fund-maintain/fund-operate/fund-operate.component';
import { FundUpdateComponent } from './fund-maintain/fund-update/fund-update.component';
import { FundListComponent } from './fund-maintain/fund-list/fund-list.component';
import { FundViewComponent } from './fund-maintain/fund-view/fund-view.component';
import { FmvPercentageHeaderComponent } from './fmv-percentage/fmv-percentage-header/fmv-percentage-header.component';
import { FmvPercentageOperateComponent } from './fmv-percentage/fmv-percentage-operate/fmv-percentage-operate.component';
import { FmvPercentageTableListComponent } from './fmv-percentage/fmv-percentage-table-list/fmv-percentage-table-list.component';
import { FmvPercentageViewComponent } from './fmv-percentage/fmv-percentage-view/fmv-percentage-view.component';
import { InvestMainRenameComponent } from './Utilities/invest-main-rename/invest-main-rename.component';
import { ViewCompanyComponent } from './company-maintain/view-company/view-company.component';
import { FinancialsHeaderComponent } from './financials/financials-header/financials-header.component';
import { FinancialsTableListComponent } from './financials/financials-table-list/financials-table-list.component';
import { FinancialsOperateComponent } from './financials/financials-operate/financials-operate.component';
import { DashboardComponent } from './Dashboards/dashboard/dashboard.component';
import { FinancialsViewComponent } from './financials/financials-view/financials-view.component';
import { GanttChartControlComponent } from './gantt-chart/gantt-chart-control/gantt-chart-control.component';
import { DatePipPipe } from './financials/pip/date-pip.pipe';
import { DdViewComponent } from './investigation/duediligence/dd-view/dd-view.component';
import { HeaderRightComponent } from './Utilities/header-right/header-right.component';
import { DdOperateComponent } from './investigation/duediligence/dd-operate/dd-operate.component';
import { InsertSurveyComponent } from './investigation/duediligence/insert-survey/insert-survey.component';
import { CompanyMappingComponent } from './company-maintain/company-mapping/company-mapping.component';
import { SecurityItemComponent } from './company-maintain/security-item/security-item.component';
import { FundSecurityItemComponent } from './fund-maintain/fund-security-item/fund-security-item.component';
import { SpanDataPipe } from './Utilities/Pipe/span-data.pipe';
import { BackTopComponent } from './Utilities/back-top/back-top.component';
import { CompanySecurityComponent } from './company-maintain/company-security/company-security.component';
import { FundFinancialsTableListComponent } from './fund-financials/fund-financials-table-list/fund-financials-table-list.component';
import { FundFinancialsViewComponent } from './fund-financials/fund-financials-view/fund-financials-view.component';
import { FundFinancialsOperateComponent } from './fund-financials/fund-financials-operate/fund-financials-operate.component';
import { FundFinancialsHeaderComponent } from './fund-financials/fund-financials-header/fund-financials-header.component';
import { ReferencePriceComponent } from './reference-price/reference-price/reference-price.component';
import { IsNullDatePipe } from './fund-financials/pipe/is-null-date.pipe';
import { PopupComponent } from './Utilities/popup/popup.component';
import { ComPicSearchComponent } from './reference-price/com-pic-search/com-pic-search.component';
import { ViewStockPriceComponent } from './reference-price/view-stock-price/view-stock-price.component';
import { StockPriceListComponent } from './reference-price/stock-price-list/stock-price-list.component';
import { StockPriceOperateComponent } from './reference-price/stock-price-operate/stock-price-operate.component';
import { FundSecurityComponent } from './fund-maintain/fund-security/fund-security.component';
import { CompanyFinMapViewComponent } from './company-maintain/company-mapping/company-fin-map-view/company-fin-map-view.component';
import { NzSliderModule } from 'ng-zorro-antd/slider'
import { RealTimeTextViewComponent } from './real-time-text/real-time-text.component'
import { RealTimeTextYesterdayViewComponent } from './real-time-text-yesterday/real-time-text-yesterday.component'
import { RealTimeTextWeekViewComponent } from './real-time-text-week/real-time-text-week.component'
import { RealTimeTextMonthViewComponent } from './real-time-text-month/real-time-text-month.component'
import { ParameterSettingViewComponent } from './parameter-setting/parameter-setting.component'
import { SummaryTableMonthViewComponent } from './summary-table-month/summary-table-month.component'
import { SummaryTableDayViewComponent } from './summary-table-day/summary-table-day.component'
import { SummaryTableHourViewComponent } from './summary-table-hour/summary-table-hour.component'
import { OffsetSettingViewComponent } from './offset-setting/offset-setting.component'
const MatDragDropModules = [
  ClipboardModule,
  DragDropModule,
  PortalModule,
  ScrollingModule,
  CdkStepperModule,
  CdkTableModule,
  CdkTreeModule
]

@NgModule({
  declarations: [AdminComponent, OverlayMenuComponent, RightSidebarComponent, LeftSidebarComponent,
    SettingsComponent, BasicformComponent, AdvancedformComponent,
    FormexamplesComponent, FormvalidationComponent, SummernoteComponent, CallapiComponent,
    GridviewComponent,
    NgSortableHeader,
    IndexComponent,
    DueDiligenceComponent,
    TableListComponent,
    ActionReportsComponent,
    PicMaintainComponent,
    SearchkeywordPipe,
    ReplaceCommaPipe,
    AmountTableListComponent,
    AmountHeaderComponent,
    AmountTableViewComponent,
    AmountOperateComponent,
    CurrencyRateComponent,
    CompanyOperateComponent,
    CompanyListComponent,
    FundOperateComponent,
    FundUpdateComponent,
    FundListComponent,
    FundViewComponent,
    FmvPercentageHeaderComponent,
    FmvPercentageOperateComponent,
    FmvPercentageTableListComponent,
    FmvPercentageViewComponent,
    VendorCodeFundUtilities,
    VendorCodeSearchComponent,
    BoPicSearchComponent,
    InvestMainRenameComponent,
    ViewCompanyComponent,
    FinancialsHeaderComponent,
    FinancialsTableListComponent,
    FinancialsOperateComponent,
    DashboardComponent,
    FinancialsViewComponent,
    GanttChartControlComponent,
    DatePipPipe,
    DdViewComponent,
    HeaderRightComponent,
    DdOperateComponent,
    InsertSurveyComponent,
    FileUploadComponent,
    CompanyMappingComponent,
    SecurityItemComponent,
    FundSecurityItemComponent,
    SpanDataPipe,
    BackTopComponent,
    CompanySecurityComponent,
    FundFinancialsTableListComponent,
    FundFinancialsViewComponent,
    FundFinancialsOperateComponent,
    FundFinancialsHeaderComponent,
    ReferencePriceComponent,
    IsNullDatePipe,
    PopupComponent,
    ComPicSearchComponent,
    ViewStockPriceComponent,
    StockPriceListComponent,
    StockPriceOperateComponent,
    FundSecurityComponent,
    CompanyFinMapViewComponent,
    RealTimeTextViewComponent,
    RealTimeTextYesterdayViewComponent,
    RealTimeTextWeekViewComponent,
    RealTimeTextMonthViewComponent,
    ParameterSettingViewComponent,
    SummaryTableMonthViewComponent,
    SummaryTableDayViewComponent,
    SummaryTableHourViewComponent,
    OffsetSettingViewComponent
  ],

  imports: [
    NzIconModule,
    CommonModule,
    PagesRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatDragDropModules,
    NzDatePickerModule,
    NzPopoverModule,
    NzStepsModule,
    NzSelectModule,
    NzModalModule,
    NzTabsModule,
    NzRadioModule,
    NzSwitchModule,
    NzTimePickerModule,
    NzInputNumberModule,
    NzButtonModule,
    NzPopconfirmModule,
    NzInputModule,
    NzDropDownModule,
    NzToolTipModule,
    NzElementPatchModule,
    NzTableModule,
    NzPageHeaderModule,
    NzFormModule,
    NzTransferModule,
    NzCheckboxModule,
    NzBadgeModule,
    NzMenuModule,
    NzTreeViewModule,
    NzUploadModule,
    NzTreeModule,
    NzCollapseModule,
    NzEmptyModule,
    NzBackTopModule,
    NzSpinModule,
    NzSliderModule
  ],

  exports: [
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    GanttChartControlComponent
  ],

  providers: [
    // 在应用根模块中设置NZ_DATE_LOCALE的值，将激活date-fns方式的日期格式化展示
    DatePipe,
    { provide: NzMessageService }
  ]

})
export class AdminModule { }
