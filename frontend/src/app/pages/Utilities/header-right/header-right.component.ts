import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import Swal from 'sweetalert2';

import { IMSConstants } from 'src/app/utils/IMSConstants';

@Component({
  selector: 'app-header-right',
  templateUrl: './header-right.component.html',
  styleUrls: ['./header-right.component.css']
})
export class HeaderRightComponent implements OnInit {
  isShowList = false;
  userDetails: KeycloakProfile;

  constructor(private keycloakService: KeycloakService) { }

  async ngOnInit(): Promise<void> {
    if (await this.keycloakService.isLoggedIn()) {
      this.userDetails = await this.keycloakService.loadUserProfile();
    }
  }

  visibleChange(data: any) {
    this.isShowList = data;
  }

  showHelpDesk() {
    Swal.fire("Contact Us", IMSConstants.contactEmail);
  }

  async doLogout() {
    let signOutWarning = "Are you sure to logout?";

    if (confirm(signOutWarning)) {
      sessionStorage.clear();
      await this.keycloakService.logout(window.location.protocol + "//" + window.location.host);
    }
  }
}
