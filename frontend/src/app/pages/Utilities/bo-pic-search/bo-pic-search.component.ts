import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2';
import { environment } from '@environments/environment';

import { BOPICItem } from '../../../utils/utility-component-model';
import { IMSConstants } from 'src/app/utils/IMSConstants';

@Component({
  selector: 'app-bo-pic-search',
  templateUrl: './bo-pic-search.component.html',
  styleUrls: ['./bo-pic-search.component.css']
})
export class BoPicSearchComponent implements OnInit {
  @Output() chosenUnitPICItem = new EventEmitter<BOPICItem[]>();

  selectVisible = false;
  alertVisible = false;
  boPICListVisible = false;
  boPICList: BOPICItem[] = [];
  set checkListValue(data: BOPICItem[]) {
    console.log(data);
    this.chosenBOPIC = data;
    this.setOfCheckedEmplid.clear();
    data.forEach(e => {
      this.setOfCheckedEmplid.add(e.emplid);
    })
  }
  get checkListValue() {
    return this.chosenBOPIC;
  }
  chosenBOPIC: BOPICItem[] = [];

  searchDeptid: string = '';
  searchEmplidorEnName: string = '';
  setOfCheckedEmplid = new Set<string>();

  searchValue: string = '';

  searchSimple: boolean = false;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {

  }

  AfterOpen = () => {
    this.checkListValue = this.checkListValue;
  }

  initBOPIC(data?: BOPICItem[]) {
    this.selectVisible = true;
    this.checkListValue = data;
  }

  handleOk(): void {
    if (this.checkListValue.length <= 0) {
      this.selectVisible = false;
      Swal.fire(IMSConstants.swalTitle.error, "你沒選擇任何事業體窗口", "error").then((result) => { this.selectVisible = true });
    }

    this.chosenUnitPICItem.emit(this.checkListValue);
    this.selectVisible = false;
  }

  handleCancel(): void {
    this.selectVisible = false;
  }

  onSearch(): void {
    this.searchSimple = true;
    this.getVendorList(this.searchValue);
  }

  onBOPICChecked(boPICItem: BOPICItem, checked: boolean): void {
    console.log(boPICItem, checked);
    if (checked && !this.checkListValue.some(e => e.emplid == boPICItem.emplid)) {
      this.setOfCheckedEmplid.add(boPICItem.emplid);
      this.checkListValue.push(boPICItem)
    }
    else {
      this.setOfCheckedEmplid.delete(boPICItem.emplid);
      this.checkListValue = this.checkListValue.filter(e => e.emplid != boPICItem.emplid);
    }
  }

  getVendorList(searchValue: string) {
    let formData1 = new FormData();
    formData1.set("searchValue", searchValue);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/GetBOPICList", formData1).subscribe({
      next: (res) => {
        this.boPICList = [...res];
        console.log(this.boPICList);
        if (this.boPICList.length == 0)
          this.boPICListVisible = false;
        else
          this.boPICListVisible = true;
      },
      error: (err) => {
        console.error(err);
      }
    });
  }
  //刪除事業單位
  deleteUnit(emplid: string) {
    if (emplid != null) {
      this.checkListValue = this.checkListValue.filter(e => e.emplid != emplid);
    }
  }
}
