import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoPicSearchComponent } from './bo-pic-search.component';

describe('BoPicSearchComponent', () => {
  let component: BoPicSearchComponent;
  let fixture: ComponentFixture<BoPicSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoPicSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoPicSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
