import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestMainRenameComponent } from './invest-main-rename.component';

describe('InvestMainRenameComponent', () => {
  let component: InvestMainRenameComponent;
  let fixture: ComponentFixture<InvestMainRenameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestMainRenameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestMainRenameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
