import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-invest-main-rename',
  templateUrl: './invest-main-rename.component.html',
  styleUrls: ['./invest-main-rename.component.css']
})
export class InvestMainRenameComponent implements OnInit {
  @Input() modeName: string;
  @Output() updateSuccessName = new EventEmitter<string>();

  renameVisible = false;
  inputRenameLabel: string = '';
  newName: string = '';
  investNo: string = '';

  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  initRename(investNo: string, originName: string) {
    this.inputRenameLabel = '請輸入新的' + this.modeName + '全稱';
    this.newName = originName;
    this.investNo = investNo;
    this.renameVisible = true;
  }

  handleOk(): void {
    let request = {
      name: this.newName,
      invest_no: this.investNo
    }

    this.httpClient.post<any>(environment.apiServerURL + "Utilities/RenameInvestMain", request).subscribe({
      next: (res) => {
        this.updateSuccessName.emit(this.newName);
        this.renameVisible = false;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(err.error.title);
      }
    });
  }

  handleCancel(): void {
    this.renameVisible = false;
  }
}
