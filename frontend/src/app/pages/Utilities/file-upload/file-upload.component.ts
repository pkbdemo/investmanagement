import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2';
import { environment } from '@environments/environment';
import { NzShowUploadList, NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { BOPICItem, UploadFileItem, MsgList, PreVerifyItem, PreVerifyMsg } from '../../../utils/utility-component-model';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import * as XLSX from "xlsx";
import { Router } from '@angular/router';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  @Output() chosenBOPICItem = new EventEmitter<BOPICItem>();
  @Input() logicAddr: string;
  @Input() preVerifyAddr: string;
  @Input() titleName: string;
  @Input() fileName: string;
  @Input() fileDescription: string;
  @Input() templateName: string;
  @Input() downloadUrl: string;
  uploadVisible = false;
  uploadSuccessVisible = false;
  uploadErrorVisible = false;
  alertVisible = false;
  boPICListVisible = false;
  preVerifyVisible = false;
  isSpinning = false;
  containsCompanyFinancialsYearVisible = false;
  filename: string;
  fileSize: string;
  templateFormData: FormData = new FormData();
  url = environment.apiServerURL
  listOfColumn = [
    {
      title: 'Type',
      compare: (a: MsgList, b: MsgList) => a.msgType > b.msgType ? -1 : 1,
      priority: 1,
      width: '72px'
    },
    {
      title: 'Cell',
      compare: (a: MsgList, b: MsgList) => a.cell > b.cell ? -1 : 1,
      priority: 2,
      width: '68px'
    },
    {
      title: '驗證訊息',
      compare: (a: MsgList, b: MsgList) => a.verifyMsg > b.verifyMsg ? -1 : 1,
      priority: 3
      ,
      width: '420px'
    }
  ];

  //上傳excel，驗證資料，並寫入DB
  uploadFileList: UploadFileItem = {
    success: null,
    successCount: 0,
    infoCount: 0,
    warningCount: 0,
    errorCount: 0,
    MsgList: null
  };

  msgList: MsgList = {
    msgType: "",
    cell: "",
    verifyMsg: ""
  };

  //上傳excel，進行事前驗證
  preVerifyItem: PreVerifyItem = {
    success: null,
    duplicateData: null,
    containsCompanyFinancialsYearChange: null
  };

  preVerifyMsg: PreVerifyMsg = {
    company: "",
    financialsYear: 0,
    quarter: ""
  };

  oriMsgList: MsgList[] = [];
  infoWarningMsgList: MsgList[] = [];
  errorMsgList: MsgList[] = [];

  chosenBOPIC: BOPICItem = null;
  fileType: string = ".xlsx"; //文件類型
  importExcelList: any[];
  importUserHeader: string;
  showUploadList: NzShowUploadList = {
    showDownloadIcon: false,
    showPreviewIcon: false,
    showRemoveIcon: false
  }

  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }

  beforeUpload = (file): boolean => {
    if (file && file.name.toString().toUpperCase().includes('.XLSX')) {
      this.oriMsgList = [];
      this.infoWarningMsgList = [];
      this.errorMsgList = [];
      this.templateFormData = new FormData();
      this.filename = file.name; //獲取文件名稱
      const reader: FileReader = new FileReader(); // FileReader 允許web應用程式非同步讀取儲存在本地電腦的文件
      //讀取操作成功完成時使用FileReader.onload
      reader.onload = (e: any) => {

        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary", cellDates: true, dateNF: 'mm/dd/yyyy;@' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        const sheet2JSONOpts = { defval: '', raw: false };
        this.importExcelList = XLSX.utils.sheet_to_json(ws, sheet2JSONOpts); //解析出文件content，可以進行後續操作

        let formData = new FormData();
        formData.set("file", JSON.stringify(this.importExcelList));
        this.templateFormData = formData;
        //step1
        //先呼叫事前檢核的API位置，判斷是否有事前驗證的訊息，如果有的話，先顯示彈窗，再根據彈窗的選擇，判斷是否繼續上傳
        //如果兩個事前檢核都有報錯，則關閉一個後再接著彈出第二的報錯
        if (this.preVerifyAddr != null && this.preVerifyAddr != "") {
          this.httpClient.post<any>(environment.apiServerURL + this.preVerifyAddr, formData).subscribe({
            next: (res) => {
              this.preVerifyItem = res;
              if (res.success == false) {
                //判斷是否有重複資料
                if (this.preVerifyItem.duplicateData.length > 0) {
                  this.uploadVisible = false;
                  this.preVerifyVisible = true;
                }
                //判斷是否有此次上傳涵蓋公司財年制度變更內容
                else if (this.preVerifyItem.containsCompanyFinancialsYearChange.length > 0) {
                  this.uploadVisible = false;
                  this.containsCompanyFinancialsYearVisible = true;
                }
              }
              else {
                //step2
                //如果沒有報錯的話，繼續上傳的動作
                this.keepUpload();
              }
            },
            error: (err) => {
              console.error(err);
            }
          });
        }
        else//preVerifyAddr API地址為空=>不需要事前檢核
        {
          this.keepUpload();
        }
      };
      reader.readAsBinaryString(file);
    }
    else {
      this.uploadVisible = false;
      Swal.fire(IMSConstants.swalTitle.error, "檔案類型必須為.Xlsx", "error").then((result) => { this.uploadVisible = true });
    }
    return false;
  };

  //繼續上傳
  keepUpload(): void {
    this.uploadVisible = true;
    this.isSpinning = true;
    this.httpClient.post<any>(environment.apiServerURL + this.logicAddr, this.templateFormData).subscribe({
      next: (res) => {
        this.uploadFileList = res;

        if (res.success == true) {
          this.isSpinning = false;
          this.uploadVisible = false;
          this.uploadSuccessVisible = true;
          if (res.infoCount > 0 || res.warningCount > 0) {
            res.msgList.forEach(item => {
              this.oriMsgList.push(item);
            });
            this.infoWarningMsgList = this.oriMsgList.filter(d => d.msgType == 'info' || d.msgType == 'warn');
          }
        }
        else if (res.success == false) {
          this.isSpinning = false;
          this.uploadVisible = false;
          this.uploadErrorVisible = true;
          if (res.errorCount > 0) {
            res.msgList.forEach(item => {
              this.oriMsgList.push(item);
            });
            this.errorMsgList = this.oriMsgList.filter(d => d.msgType == 'error');
          }
        }
      },
      error: (err) => {
        console.error(err);
      }
    });
  }

  handleDuplicateKeepUpload(): void {
    this.preVerifyVisible = false;
    if (this.preVerifyItem.containsCompanyFinancialsYearChange.length > 0) {
      this.containsCompanyFinancialsYearVisible = true;
    }
    else {
      this.keepUpload();
    }
  }
  handleDuplicateCancel(): void {
    this.preVerifyVisible = false;
  }

  handleContainsCompanyFinancialsYearKeepUpload(): void {
    this.containsCompanyFinancialsYearVisible = false;
    this.keepUpload();
  }
  handleContainsCompanyFinancialsYearCancel(): void {
    this.containsCompanyFinancialsYearVisible = false;
  }

  handleOk(): void {
    if (this.chosenBOPIC == null) {
    }
    //emit=>將()裡面的值回拋給父組件，父組件則使用$event承接，觸發父組件Refresh
    this.chosenBOPICItem.emit(this.chosenBOPIC);
    this.uploadVisible = false;
    this.uploadSuccessVisible = false;
    this.uploadErrorVisible = false;
  }

  handleCancel(): void {
    this.uploadVisible = false;
    this.chosenBOPIC = null;
  }

  handleSuccessCancel(): void {
    this.uploadSuccessVisible = false;
    this.uploadErrorVisible = false;
  }

  handleErrorCancel(): void {
    this.uploadErrorVisible = false;
  }
  downloadExcel(): void {
    if (!!this.downloadUrl) {
      this.httpClient.get<any>(environment.apiServerURL + this.downloadUrl).subscribe({
        next: (res) => {
          if (res != null) {
            console.log(res);
            const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res);
            let obj: any = {};
            Object.keys(worksheet).forEach(key => {
              if (key.indexOf('!') < 0) {
                if (key.length == 2 && key.indexOf('1') > 0) {
                  worksheet[key].v = worksheet[key].v.toUpperCase();
                }
                if (!!worksheet[key].v) {
                  if (!obj[key.charAt(0)]) obj[key.charAt(0)] = worksheet[key].v.length * 2;
                  if (obj[key.charAt(0)] < worksheet[key].v.length * 2) {
                    obj[key.charAt(0)] = worksheet[key].v.toString().length * 2;
                  }
                }
              }
            })
            let width = [];
            for (let item in obj) {
              width.push({ wch: obj[item] })
            }
            worksheet['!cols'] = width;
            const workbook: XLSX.WorkBook = {
              Sheets: { [this.templateName]: worksheet },
              SheetNames: [this.templateName]
            };
            const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
            //这里类型如果不正确，下载出来的可能是类似xml文件的东西或者是类似二进制的东西等
            this.saveAsExcelFile(excelBuffer, this.templateName);
          }
        },
        error: (err) => {
          console.error(err);
        }
      });
    } else {
      window.location.href = this.router['location']._platformLocation.location.origin + "/assets/file/" + this.templateName;
    }
  }
  private saveAsExcelFile(buffer: any, fileName: string) {
    const data: Blob = new Blob([buffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
    });
    FileSaver.saveAs(data, fileName + '.xlsx');
  }
  initUploadDialog(): void {
    this.uploadVisible = true;
  }


}
