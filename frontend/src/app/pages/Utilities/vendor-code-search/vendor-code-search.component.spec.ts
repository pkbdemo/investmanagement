import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorCodeSearchComponent } from './vendor-code-search.component';

describe('VendorCodeSearchComponent', () => {
  let component: VendorCodeSearchComponent;
  let fixture: ComponentFixture<VendorCodeSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorCodeSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorCodeSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
