import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2';
import { environment } from '@environments/environment';

import { VendorCodeItem } from '../../../utils/utility-component-model';
import { IMSConstants } from 'src/app/utils/IMSConstants';

@Component({
  selector: 'app-vendor-code-search',
  templateUrl: './vendor-code-search.component.html',
  styleUrls: ['./vendor-code-search.component.css']
})
export class VendorCodeSearchComponent implements OnInit {

  @Input() modeName: string;
  @Output() chosenVendorCode = new EventEmitter<string>();

  selectVisible = false;
  alertVisible = false;
  vendorCodeListVisible = false;
  vendorCodeList: VendorCodeItem[] = [];
  searchVendorName: string = '';
  originalVendorName: string = '';
  setOfCheckedVendorCode: string = '';
  setOfCheckedVendorName: string = '';

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

  initVendorName(inputVendorName: string) {
    console.log(inputVendorName);

    if (inputVendorName) {
      this.searchVendorName = inputVendorName;
      this.originalVendorName = inputVendorName;
      this.getVendorList(inputVendorName);
      this.selectVisible = true;
    }
  }

  onSearch(): void {
    this.getVendorList(this.searchVendorName);
  }

  handleOk(): void {
    if (this.setOfCheckedVendorCode == '') {
      this.selectVisible = false;
      Swal.fire(IMSConstants.swalTitle.error, "你沒選擇任何Vendor Code", "error").then((result) => { this.selectVisible = true });
    }

    if (this.searchVendorName == this.originalVendorName) {
      this.chosenVendorCode.emit(this.setOfCheckedVendorCode);
      this.selectVisible = false;
    }
    else {
      this.selectVisible = false;
      Swal.fire("Vendor Name與" + this.modeName + "全稱不同", "請選擇與" + this.modeName + "全稱相同的Vendor Name, <br>否則無法新增該筆資料 ", "error").then((result) => { this.selectVisible = true });
    }
  }

  handleCancel(): void {
    this.selectVisible = false;
    this.setOfCheckedVendorCode = '';
  }

  onVendorCodeChecked(vendorCodeItem: VendorCodeItem, checked: boolean): void {
    if (checked) {
      this.setOfCheckedVendorCode = vendorCodeItem.vendorCode;
      this.setOfCheckedVendorName = vendorCodeItem.vendorName;
    }
    else {
      this.setOfCheckedVendorCode = '';
      this.setOfCheckedVendorName = '';
    }

  }

  getVendorList(vendorName: string) {
    let formData1 = new FormData();
    formData1.set("vendorName", vendorName);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/GetVendorList", formData1).subscribe({
      next: (res) => {
        this.vendorCodeList = [...res];
        console.log(this.vendorCodeList);
        if(this.vendorCodeList.length == 0)
          this.vendorCodeListVisible = false;
        else
          this.vendorCodeListVisible = true;
      },
      error: (err) => {
        console.error(err);
      }
    });
  }
}
