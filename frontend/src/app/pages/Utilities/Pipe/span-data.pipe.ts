import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'spanData'
})
export class SpanDataPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    if (!!value) {
      return value.replace(/,/g, `,\n`);
    }
    return null;
  }

}
