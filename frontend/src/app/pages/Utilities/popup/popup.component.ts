import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  isVisible: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  openModel() {
    this.isVisible = true;
  }

  closeModel() {
    this.isVisible = false;

  }
  handleCancel(data: string) {
    console.log(data);
  }

  handleOk(data: string) {
    console.log(data);
  }
}
