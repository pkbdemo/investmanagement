import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import moment from 'moment';

import { IGanttChartEvent } from '../_models/gantt-chart-event.model';
import { IGanttCharRow } from '../_models/gantt-chart-row.model';
import { MonthAxis } from '../_models/month-axis.model';
import { QuarterAxis } from '../_models/quarter-axis.model';
import { DateHelperService } from '../_services/date-helper.service';
import { environment } from 'src/environments/environment';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-gantt-chart-control',
  templateUrl: './gantt-chart-control.component.html',
  styleUrls: ['./gantt-chart-control.component.scss']
})
export class GanttChartControlComponent implements OnInit {

  @Input() param;
  @Input() isFundFinancials = false;
  timelineEmpty: boolean = false;
  invest_no: string;
  financials_no: string;
  rows: IGanttCharRow[];
  startDate: Date = new Date('2021-01-01');
  endDate: Date = new Date('2023-3-31');
  test: Date[] = [];
  lastQuarterEndDate: moment.Moment
  lastQuarterStartDate: moment.Moment
  chartPeriodDays: number;
  monthAxis: MonthAxis[];
  quarterAxis: QuarterAxis[];
  listOfData: any[] = [];
  monthToQuarterPercentage: number[];
  quarterBackgroundColor: any[] = [];
  colourPallete = ['#7C4DFF',
    '#81c784',
    '#e53935',
    '#FF8A80',
    '#303F9F',
    '#40C4FF',
    '#006064',
    '#FF8A65']

  constructor(private httpClient: HttpClient, private message: NzMessageService) {

  }

  ngOnInit(): void {
  }

  ngOnChanges() {
    console.log(this.param);
    if (!!this.param.invest_no || !!this.param.financials_no) {
      this.invest_no = this.param.invest_no;
      this.financials_no = this.param.financials_no;
      this.queryLatestFinancialsYearQuarter();
    }
  }

  queryGanttTimeLineDateList(): string[] {
    var tt: any[];
    let formData = new FormData();
    formData.set("invest_no", this.invest_no);
    formData.set("financials_no", this.financials_no);
    if (this.isFundFinancials) formData.set('isFundFinancials', 'true');
    else formData.set('isFundFinancials', 'false');
    this.httpClient.post<any>(environment.apiServerURL + "Financials/CalculateGanttTimeLineDateList", formData).subscribe({
      next: (res) => {
        Object.entries(res).forEach(
          ([key, value]) => {
            //取得Dictionary組值，用來判斷要放入的GanttChart位置
            //key:流水號
            //value:圖表資料(ex:2020|001|005)(financial_year + report_type + quarter)
            //rows[0]:YTDQ4 ; rows[1]:YTDQ3 ; rows[2]:YTDQ2 ; rows[3]:YTDQ1 ;
            //quarter-001:Q1 ; quarter-002:Q2 ; quarter-003:Q3 ; quarter-004:Q4  ;
            //report_type-001:YTD ; report_type-002:Quarter
            var financial_year = value.toString().split("|")[0];
            var quarter = value.toString().split("|")[1];
            switch (quarter) {
              case IMSConstants.Quarter_Type.Q1:
                var startDate = moment(financial_year, "YYYY").quarter(1).startOf('quarter').toDate();
                var endDate = moment(financial_year, "YYYY").quarter(1).endOf('quarter').toDate();
                //不要讓繪製的Bar條日期區間超出開始日期或是結束日期
                if (startDate < this.startDate)
                  startDate = this.startDate;
                if (endDate > this.endDate)
                  endDate = this.endDate;
                this.rows[3].events.push({ name: 'YTD Q1', startDate: startDate, endDate: endDate } as IGanttChartEvent);
                break;
              case IMSConstants.Quarter_Type.Q2:
                var startDate = moment(financial_year, "YYYY").quarter(1).startOf('quarter').toDate();
                var endDate = moment(financial_year, "YYYY").quarter(2).endOf('quarter').toDate();
                if (startDate < this.startDate)
                  startDate = this.startDate;
                if (endDate > this.endDate)
                  endDate = this.endDate;
                this.rows[2].events.push({ name: 'YTD Q2', startDate: startDate, endDate: endDate } as IGanttChartEvent);
                break;
              case IMSConstants.Quarter_Type.Q3:
                var startDate = moment(financial_year, "YYYY").quarter(1).startOf('quarter').toDate();
                var endDate = moment(financial_year, "YYYY").quarter(3).endOf('quarter').toDate();
                if (startDate < this.startDate)
                  startDate = this.startDate;
                if (endDate > this.endDate)
                  endDate = this.endDate;
                this.rows[1].events.push({ name: 'YTD Q3', startDate: startDate, endDate: endDate } as IGanttChartEvent);
                break;
              case IMSConstants.Quarter_Type.Q4:
                var startDate = moment(financial_year, "YYYY").quarter(1).startOf('quarter').toDate();
                var endDate = moment(financial_year, "YYYY").quarter(4).endOf('quarter').toDate();

                if (startDate < this.startDate)
                  startDate = this.startDate;
                if (endDate > this.endDate)
                  endDate = this.endDate;
                this.rows[0].events.push({ name: 'YTD Q4', startDate: startDate, endDate: endDate } as IGanttChartEvent);
                break;
            }
          }
        );
        return tt;
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
    return null;
  }

  getEventDurationPercentage(event: IGanttChartEvent): number {
    const eventDays = DateHelperService.dateDifference(event.endDate, event.startDate);
    return (eventDays / this.chartPeriodDays) * 100;
  }

  getEventOffsetPercentage(eventStartDate: Date): number {
    const daysPriorToEventStart = DateHelperService.dateDifference(eventStartDate, this.startDate);
    return ((daysPriorToEventStart - 1) / this.chartPeriodDays) * 100;
  }

  getMonths(startDate: Date, endDate: Date): MonthAxis[] {
    var dateRange = moment(endDate).diff(startDate, 'month');
    const totalDurationDays = DateHelperService.dateDifference(startDate, endDate, true);
    let months: MonthAxis[] = new Array();
    for (var i = 0; i <= dateRange; i++) {
      const adjustedStartDate = DateHelperService.addMonths(startDate, i);

      const daysInMonth = DateHelperService.daysInMonth(adjustedStartDate);
      var monthDurationPercentage = daysInMonth / totalDurationDays * 100;
      months.push({ monthName: '', monthDurationPercentage: monthDurationPercentage, monthNum: (i + 1) });
    }
    return months;
  }

  getQuarters(startDate: Date, endDate: Date): QuarterAxis[] {
    let tmpNum: number = 0;
    let tmpQuarterNum: number[] = new Array();
    this.monthAxis.forEach(value => {
      if (value.monthNum % 3 > 0) {
        tmpNum = tmpNum + value.monthDurationPercentage;
      }
      else {
        tmpNum = tmpNum + value.monthDurationPercentage;
        tmpQuarterNum.push(tmpNum);
        tmpNum = 0;
      }

    });

    let quarters: QuarterAxis[] = new Array();
    let j: number = 0;
    tmpQuarterNum.forEach(value => {
      var adjustEndDate = moment(endDate).subtract(j, 'quarter')
      var quarterStartDate = moment(adjustEndDate, "MM-DD-YYYY").startOf('quarter').toDate();
      var quarterYear = adjustEndDate.year().toString();
      var quarterNum = adjustEndDate.quarter().toString();
      quarters.push({ quarterName: quarterYear + ' Q' + quarterNum, quarterDurationPercentage: value, quarterStartDate: quarterStartDate });
      j++;
    });
    return quarters.sort((a, b) => a.quarterName.localeCompare(b.quarterName));
  }

  getColour(rowIndex: number): string {
    // return '#7a85ff';
    return '#8992FF';
  }

  getBackgroundColor(rowIndex: number): string {
    var resultColor: string;
    if (this.quarterBackgroundColor.length > 0) {
      if (rowIndex >= 0 && rowIndex <= 2) {
        if (this.quarterBackgroundColor[8].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 3 && rowIndex <= 5) {
        if (this.quarterBackgroundColor[7].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 6 && rowIndex <= 8) {
        if (this.quarterBackgroundColor[6].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 9 && rowIndex <= 11) {
        if (this.quarterBackgroundColor[5].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 12 && rowIndex <= 14) {
        if (this.quarterBackgroundColor[4].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 15 && rowIndex <= 17) {
        if (this.quarterBackgroundColor[3].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 18 && rowIndex <= 20) {
        if (this.quarterBackgroundColor[2].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 21 && rowIndex <= 23) {
        if (this.quarterBackgroundColor[1].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }

      if (rowIndex >= 24 && rowIndex <= 26) {
        if (this.quarterBackgroundColor[0].split("|")[1] == "true")
          return '#F6F6F6';
        else
          return '#ffffff';
      }
    }

    return resultColor;
  }

  getBorderRadius(eventStartDate: Date): string {
    const daysPriorToEventStart = DateHelperService.dateDifference(eventStartDate, this.startDate);
    if (((daysPriorToEventStart - 1) / this.chartPeriodDays) * 100 == 0 && eventStartDate == this.startDate)
      return "0px 16px 16px 0px";
    else
      return "16px 16px 16px 16px";
  }

  queryLatestFinancialsYearQuarter(): Date {
    var tyt: Date;
    let formData = new FormData();
    formData.set("invest_no", this.invest_no);
    formData.set("financials_no", this.financials_no);

    if (this.isFundFinancials) formData.set('isFundFinancials', 'true');
    else formData.set('isFundFinancials', 'false');
    this.httpClient.post<any>(environment.apiServerURL + "Financials/QueryLatestFinancialsYearQuarter", formData).subscribe({
      next: (res) => {
        if (res != "") {
          this.timelineEmpty = false;
          var startYear = res.toString().split("|")[0];
          var startQuarter = res.toString().split("|")[1];
          var endYear = res.toString().split("|")[2];
          var endQuarter = res.toString().split("|")[3];

          this.startDate = moment(startYear, "YYYY").quarter(Number(startQuarter)).startOf('quarter').toDate();
          this.endDate = moment(endYear, "YYYY").quarter(Number(endQuarter)).endOf('quarter').toDate();

          this.chartPeriodDays = DateHelperService.dateDifference(this.endDate, this.startDate, true);
          this.monthAxis = this.getMonths(this.startDate, this.endDate);
          this.quarterAxis = this.getQuarters(this.startDate, this.endDate);
          this.queryGanttTimeLineDateList();
          this.queryGanttQuarterBackgroundColor();
          //繪製橫軸
          this.rows = [
            {
              name: 'YTD Q4', events: [
                // { name: 'YTD Q4', startDate: new Date('2021-01-01'), endDate: new Date('2021-03-31') } as IGanttChartEvent
              ]

            } as IGanttCharRow,
            {
              name: 'YTD Q3', events: [
                // { name: 'YTD Q3', startDate: new Date('2021-04-01'), endDate: new Date('2021-06-30') } as IGanttChartEvent
              ]
            } as IGanttCharRow,
            {
              name: 'YTD Q2', events: [
                // { name: 'YTD Q2', startDate: new Date('2021-07-01'), endDate: new Date('2021-09-30') } as IGanttChartEvent
              ]
            } as IGanttCharRow,
            {
              name: 'YTD Q1', events: [
                // { name: 'Quarter', startDate: new Date('2021-10-01'), endDate: new Date('2021-12-31') } as IGanttChartEvent
              ]
            } as IGanttCharRow
          ]
        }
        else
          this.timelineEmpty = true;
      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
    return tyt;
  }

  queryGanttQuarterBackgroundColor(): void {

    let formData = new FormData();
    formData.set("invest_no", this.invest_no);
    formData.set("financials_no", this.financials_no);
    if (this.isFundFinancials) formData.set('isFundFinancials', 'true');
    else formData.set('isFundFinancials', 'false');
    this.httpClient.post<any>(environment.apiServerURL + "Financials/QueryGanttQuarterBackgroundColor", formData).subscribe({
      next: (res) => {
        this.quarterBackgroundColor = res;

      },
      error: (err) => {
        console.error(err);
        this.message.create('error', "request failed");
      }
    });
  }

}

