export interface TimelineDateList {
  num: number;
  timelineDate: string;
}
