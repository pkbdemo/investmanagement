export interface MonthAxis {
  monthNum: number;
  monthName: string;
  monthDurationPercentage: number;
}
