export interface QuarterAxis {
  quarterStartDate: Date;
  quarterName: string;
  quarterDurationPercentage: number;
}
