import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyOperateComponent } from './company-operate.component';

describe('CompanyOperateComponent', () => {
  let component: CompanyOperateComponent;
  let fixture: ComponentFixture<CompanyOperateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyOperateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyOperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
