import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpClient, HttpParams } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { NzMessageService } from 'ng-zorro-antd/message';
import { environment } from '@environments/environment';

import { CompanyService } from '../services/company.service';
import { LayoutService } from '../../layout/services/layout.service';
import { tableListItem } from '../../investigation/duediligence/duediligenceModel';
import { codeNameItem, RequestCompanyModify, Security, SecurityCodeItem, SecurityItem, SecurityStrike, SecurityType, Type } from '../companyModel';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { BOPICItem } from 'src/app/utils/utility-component-model';
import { CompanySecurityComponent } from '../company-security/company-security.component';
import { IMSConstants } from 'src/app/utils/IMSConstants';

@Component({
  selector: 'app-company-operate',
  templateUrl: './company-operate.component.html',
  styleUrls: ['./company-operate.component.css']
})
export class CompanyOperateComponent implements OnInit {
  getNeedToDeleteSecurity = 0;
  dataName: string = '*未命名公司';
  modeName: string = '公司';

  user_Name: string;
  user_Id: string;
  userRole: any = {};


  //捨棄窗口
  isVisible = false;
  isCheckVisible = false;
  CheckDataVisible = false;

  //校驗彈窗内容
  checkDataType = new Set<number>();  //用來存放校驗步驟：1，2，3
  checkDataTitle: string;
  checkDataContent: string;
  checkDataContentList: string[] = [];

  //刪除卡片窗口
  isDelete = false;

  //表单
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '不是合法Email格式'
    }
  };
  validateForm: FormGroup;

  //修改状态下展示的内容
  name: string = '';
  name_a: string = '無';
  nick_Name: string = '';

  //建議Status
  suggest_status: string;
  set _suggest_status(data: string) {
    if (!!data) {
      this.suggest_status = data
      this.validateForm.get('status').setValidators(Validators.required);
      this.validateForm.get('status').setValue(data);
    }
  }
  invest_no: number = 0;

  //折叠面板
  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }

  //表单下拉选单元數據
  nameList: tableListItem[] = [];
  boList: codeNameItem[] = [];
  statusList: codeNameItem[] = [];
  catagoryList: codeNameItem[] = [];
  stageList: codeNameItem[] = [];
  headquarterList: string[] = [];
  accountList: codeNameItem[] = [];

  stockMarketList: codeNameItem[] = [];
  finYearList: codeNameItem[] = [];

  nameListSub: Subscription;
  boListSub: Subscription;
  statusListSub: Subscription;
  catagoryListSub: Subscription;
  stageListSub: Subscription;
  headquarterListSub: Subscription;
  accountListSub: Subscription;

  marketListSub: Subscription;
  finYearListSub: Subscription;

  //Security 数组存放
  SecurityData: SecurityItem[] = [];

  //Email展示數據
  emailList: string[] = [];

  //Security
  //Security 下拉選單樣式
  securityList: codeNameItem[] = [];
  securityListSub: Subscription;


  //存放選擇的新增的Security
  selectSecurityValue: SecurityCodeItem[] = [];


  //控制欄位是否必選
  isStatusReq: boolean = false;
  isReasonReq: boolean = false;
  isMarketReq: boolean = false;
  setOfCheckedId = new Set<string>();
  //判斷修改Or新增 默認為新增
  operateType: Type;
  CompanyType = Type;

  hasQuitDate: boolean = false;
  hasQuiteDateAndAddSecurityValue: string = "";
  //儲存數據
  unitPicList: BOPICItem[] = [];
  securityDataList: Security[] = [];
  set _securityDataList(data: Security[]) {
    this.securityDataList = data.sort((a, b) => this.companyService.SortData(a, b));
  }

  get _securityDataList() { return this.securityDataList; }
  SecurityType: SecurityType;

  @ViewChild('companySecurity') companySecurity!: CompanySecurityComponent;
  constructor(private layoutService: LayoutService,
    public router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private companyService: CompanyService,
    private authorityControlService: AuthorityControlService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService) {

  }

  ngOnInit(): void {
    this.name_a = sessionStorage.getItem("userName").toUpperCase();
    this.user_Id = sessionStorage.getItem("userID").toUpperCase();
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      nickName: ['', [Validators.required]],
      subject: ['', []],
      status: ['', []],
      unit_Pic: ['', []],
      email: ['', [Validators.email]],
      stock_Market: ['', []],
      stock_Symbol: ['', []],
      fin_Year_Rule: ['', [Validators.required]],
      industry_Catagory: ['', [Validators.required]],
      industry_Detail: ['', []],
      company_Stage: ['', []],
      main_Business: ['', []],
      headquarter: ['', []],
    });
    this.getNameList();
    this.getBoList();
    this.getStatusList();
    this.getCatagoryList();
    this.getStageList();
    this.getheadquarter();
    this.getSecurity();
    this.getAccount();
    this.getMarket();
    this.getFinYear();

    this.validateForm.get('stock_Symbol').valueChanges.subscribe((data: string) => {
      if (!!data) {
        if (data != this.validateForm.value['stock_Symbol']) {
          this.isMarketReq = true;
          this.validateForm.get('stock_Market').setValidators(Validators.required);
        }
      } else {
        this.isMarketReq = false;
        this.validateForm.get('stock_Market').clearValidators();
      }
    });
    this.validateForm.get('name').valueChanges.subscribe(data => {
      this.name = data;
    });

    this.authorityControlService.getUserRole().then(
      userRole => {
        console.log(userRole);
        this.userRole = userRole;
        this.getNameList();
        this.route.queryParams.subscribe(async params => {
          if (params['invest_no'] != null && params['invest_no'] != '') {
            this.operateType = Type.Update
            this.invest_no = params['invest_no'];
            this.getCompanyData(params['invest_no']);
          }
          else {
            this.operateType = Type.Add
          }
        });
      }
    );
  }

  ngOnDestory() {
    this.nameListSub.unsubscribe();
    this.boListSub.unsubscribe();
    this.statusListSub.unsubscribe();
    this.catagoryListSub.unsubscribe();
    this.stageListSub.unsubscribe();
    this.headquarterListSub.unsubscribe();
    this.headquarterListSub.unsubscribe();
    this.accountListSub.unsubscribe();
  }

  //選取公司全稱
  nameChange() {
    let NameData = this.validateForm.get('name').value;
    this.name = NameData;
    if (!!NameData) {
      let formData = new FormData();
      formData.set('name', NameData);
      this.httpClient.post<any>(environment.apiServerURL + "DDiligence/VertifyName", formData).subscribe({
        next: (res) => {
          if (!res) {
            this.isCheckVisible = true;
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error("request failed");
        }
      });
    }
  }

  //security的返回信息
  goBackData(data: any) {
    console.log(data, '新增返回');
    if (data.index == 0 || !!data.index) this.securityDataList.splice(data.index, 1);
    this._securityDataList.push(data.value);
  }

  //添加Emali
  addEmail() {
    if (this.validateForm.get('email').valid) {
      if (this.emailList.length == 3) return;
      let email = this.validateForm.get('email').value;
      if (email != '' && email != null) {
        if (this.emailList.some(e => e.trim() == email.trim())) this.createMessage('warning', 'Eamil不可重複');
        else {
          this.emailList.push(email.trim());
          this.validateForm.get('email').setValue('');
        }
      }
    }

  }

  //刪除Email
  deleteEmail(index: number) {
    if (index != null) {
      console.log(index);
      this.emailList.splice(index, 1);
    }
  }

  //刪除事業單位
  deleteUnit(index: number) {
    if (index != null) {
      console.log(index);
      this.unitPicList.splice(index, 1);
    }
  }

  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  //更改Status欄位為必選
  modifyStatus() {
    this.isStatusReq = true;
    this.validateForm.get('status').setValidators([Validators.required]);
  }

  //修改時獲取信息
  getCompanyData(invest_no: number) {
    let formData = new FormData();
    formData.set("invest_no", invest_no.toString());
    this.httpClient.post<any>(environment.apiServerURL + "Company/GetCompanyModifyModel", formData).subscribe({
      next: (res) => {
        console.log(res);
        if (res != null) {
          this.dataName = res.name;
          this.name = res.name;
          this.invest_no = res.invest_No;
          this.validateForm.get('name').setValue(res.name);
          this.validateForm.get('name').disable();   //唯读
          this.nick_Name = res.nickname;
          this.validateForm.get('nickName').setValue(res.nickname);
          this.validateForm.get('nickName').disable();   //唯读
          this.name_a = res.contact_Ao_Nm;
          this.validateForm.get('subject').setValue(res.subject);
          this.suggest_status = res.suggest_Status_Nm;
          this.validateForm.get('status').setValue(res.status);
          if (!!res.unitPicList && res.unitPicList.length > 0) {
            this.unitPicList = res.unitPicList.map(e => {
              let obj: BOPICItem = {}
              obj.emplid = e.item_Value;
              obj.name_A = e.name_A;
              return obj;
            });
          }
          this.emailList = res.email || [];
          this.validateForm.get('stock_Market').setValue(res.stock_Market);
          this.validateForm.get('stock_Symbol').setValue(res.stock_Symbol);
          this.validateForm.get('fin_Year_Rule').setValue(res.fin_Year_Rule);
          this.validateForm.get('industry_Catagory').setValue(res.industry_Catagory);
          this.validateForm.get('industry_Detail').setValue(res.industry_Detail);
          this.validateForm.get('company_Stage').setValue(res.company_Stage);
          this.validateForm.get('main_Business').setValue(res.main_Business);
          this.validateForm.get('headquarter').setValue(res.headQuarter);
          this.securityDataList = res.securityList.map(e => {
            let obj: Security = {};
            obj.Security_No = e.security_No;
            obj.Type = e.type;
            obj.TypeName = e.type_Name;
            obj.Stock_Type = e.stock_Type;
            obj.Stock_Type_Name = e.stock_Type_Name;
            obj.Quit_Date = e.quit_Date;
            obj.Quit_Reason = e.quit_Reason;
            obj.DD_Close_Date = e.dD_Close_Date.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
            obj.First_Trans_Date = e.first_Trans_Date;
            obj.Remark = e.remark;
            if (!!e.securityStrikeList && e.securityStrikeList.length > 0)
              obj.SecurityStrikeList = e.securityStrikeList.map(item => {
                let securityStrike: SecurityStrike = {}
                securityStrike.Due_Date = item.due_Date;
                securityStrike.Strike_Price = item.strike_Price;
                securityStrike.Strike_Stock_Share = item.strike_Stock_Share;
                return securityStrike;
              });
            return obj;
          });
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //显示组件回传信息
  modifySecurity(data) {
    console.log(data);
    switch (data.type) {
      case Type.Update:
        this.companySecurity.editVisible(true, data.value.Type, Type.Update, this.userRole, this.name, data.value, data.index);
        break;
      case Type.Delete:
        this._securityDataList.splice(data.index, 1);
        break;
    }
  }

  //獲取公司全稱
  getNameList() {
    if (this.userRole.isIMDMember || this.userRole.isIT) {
      this.companyService.getNameList();
      this.nameListSub = this.companyService.nameListObservable.subscribe(item => this.nameList = item as tableListItem[]);
    }
  }

  //獲取事業躰
  getBoList() {
    this.companyService.getCodeName('004');
    this.boListSub = this.companyService.boListObservable.subscribe(item => this.boList = item as codeNameItem[]);
  }

  //獲取Status
  getStatusList() {
    this.companyService.getCodeName('002');
    this.statusListSub = this.companyService.statusListObservable.subscribe(item => this.statusList = item as codeNameItem[]);
  }

  //獲取行業別大類
  getCatagoryList() {
    this.companyService.getCodeName('003');
    this.catagoryListSub = this.companyService.catagoryListObservable.subscribe(item => this.catagoryList = item as codeNameItem[]);
  }

  //獲取公司所處階段
  getStageList() {
    this.companyService.getCodeName('012');
    this.stageListSub = this.companyService.stageListObservable.subscribe(item => this.stageList = item as codeNameItem[]);
  }

  //獲取總部
  getheadquarter() {
    this.companyService.getheadquarter();
    this.headquarterListSub = this.companyService.headquarterListObservable.subscribe(item => this.headquarterList = item as string[]
    );
  }

  //獲取SecurityList
  getSecurity() {
    this.companyService.getCodeName('001');
    this.securityListSub = this.companyService.securityListObservable.subscribe(item => this.securityList = (item as codeNameItem[]).filter(e => e.code_Extend_A == '001')
    );
  }

  //獲取會計科目
  getAccount() {
    this.companyService.getCodeName('005');
    this.accountListSub = this.companyService.accountListObservable.subscribe(item => this.accountList = item as codeNameItem[]
    );
  }

  //獲取股票市場種類
  getMarket() {
    this.companyService.getCodeName('019');
    this.marketListSub = this.companyService.MarketListObservable.subscribe(item => this.stockMarketList = item as codeNameItem[]
    );
  }

  //獲取公司財年制度
  getFinYear() {
    this.companyService.getCodeName('020');
    this.finYearListSub = this.companyService.FinYearListObservable.subscribe(item => this.finYearList = item as codeNameItem[]
    );
  }

  submitForm(): void {
    // if (this.validateForm.status == "INVALID") {
    //   Object.keys(this.validateForm.controls).forEach(key => {
    //     const validationErrors: ValidationErrors = this.validateForm.get(key).errors;
    //     if (validationErrors != null) {
    //       Object.keys(validationErrors).forEach(keyError => {
    //         console.warn("Validation => Control: " + key + ", error: " + keyError + ", error value: " + validationErrors[keyError]);
    //       });
    //     }
    //   });
    // }

    if (this.validateForm.valid) {
      this.checkData1();
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          console.log(control);
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach(controlSon => {
              if (controlSon.invalid) {
                controlSon.markAsDirty();
                controlSon.updateValueAndValidity({ onlySelf: true });
                if (controlSon instanceof FormArray) {
                  Object.values(controlSon.controls).forEach(controlArray => {
                    if (controlArray.invalid) {
                      if (controlArray instanceof FormGroup) {
                        Object.values(controlArray.controls).forEach(o => {
                          if (o.invalid) {
                            o.markAsDirty();
                            o.updateValueAndValidity({ onlySelf: true });
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          }
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  //1.校驗Security卡片是否有資料
  checkData1() {
    if (this.securityDataList.length <= 0) {
      this.checkDataTitle = '無Security資料，無法儲存'
      this.checkDataContent = 'Security需新增至少一筆資料';
      this.checkDataContentList = [];
      this.checkDataType.clear();
      this.checkDataType.add(1);
      this.CheckDataVisible = true;
    } else {
      this.checkData2();
    }
  }

  //2.檢核是否此公司有重複的證券種類及盡職調查完成日資料
  checkData2() {
    this.checkDataContentList = [];
    this.checkDataType.clear()
    let bool: boolean = false;
    this.checkDataTitle = '證券種類及盡職調查完成日欄位，資料重複'

    this.checkDataContent = '以下項目資料重複：'
    let contentList = [];
    this._securityDataList.forEach(item => {
      let newLength = this._securityDataList.filter(e => e.Type == item.Type && e.DD_Close_Date == item.DD_Close_Date).length;
      if (newLength > 1) {
        bool = true;
        if (!contentList.some(e => e.Type == item.Type && e.DD_Close_Date == item.DD_Close_Date)) {
          contentList.push({
            Type: item.Type,
            TypeName: item.TypeName,
            DD_Close_Date: item.DD_Close_Date
          })
        }
      }
    });
    if (contentList.length > 0) {
      contentList.forEach(item => {
        this.checkDataContentList.push(`證券種類：${item.TypeName}，盡職調查完成日：${this.companyService.timestampToTime(item.DD_Close_Date)}`);
      });
    }
    this.CheckDataVisible = bool;
    if (bool) this.checkDataType.add(2);
    else this.checkData31();
  }

  //3.1校驗上市挂牌代碼是否重複
  checkData31() {
    let stockSymbol = this.validateForm.value.stock_Symbol;
    let name = this.name || this.validateForm.value.name;
    if (!!stockSymbol) {
      let formData = new FormData();
      formData.set('name', name);
      formData.set('stock_Symbol', stockSymbol.trim());
      this.httpClient.post<boolean>(environment.apiServerURL + "Company/Check3StockSymbol", formData).subscribe({
        next: (res) => {
          if (res) {
            this.checkDataContentList = [];
            this.checkDataType.clear()
            this.checkDataTitle = '已存在此筆上市掛牌代碼，無法儲存'
            this.checkDataContent = '以下項目資料重複：';
            this.checkDataContentList.push(`上市掛牌代碼：${stockSymbol}`);
            this.CheckDataVisible = true;
            this.checkDataType.add(2);
          } else {
            this.checkData32();
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error(IMSConstants.serverErrorMsg);
        }
      })
    } else {
      this.checkData32();
    }
  }

  //3.2檢核是否有此上市掛牌代碼
  checkData32() {
    this.checkDataType.clear();
    this.checkDataContentList = [];

    let stockSymbol = this.validateForm.value.stock_Symbol;
    if (stockSymbol == null || stockSymbol == "") {
      this.saveCompanyData();
    } else {
      let httpParams = new HttpParams().set("stockSymbol", stockSymbol);
      //Attention!!! If have multi parameters, it should be
      //let httpParams = new HttpParams().set("param1", "value1").set("param2", "value2").set("param3", "value3");

      let options = { params: httpParams };

      this.httpClient.get<boolean>(environment.apiServerURL + "Company/CheckStockSymbol", options).subscribe({
        next: (res) => {
          if (res) {
            this.saveCompanyData();
          } else {
            this.checkDataTitle = '無此上市掛牌代碼，是否儲存？';
            this.checkDataContent = `掛牌代碼：${stockSymbol}`;
            this.checkDataType.add(3);
            this.CheckDataVisible = true;
          }
        },
        error: (err) => {
          console.error(err);
          this.toastr.error(IMSConstants.serverErrorMsg);

          this.saveCompanyData();
        }
      });
    }
  }

  handCheckCancel() {
    this.CheckDataVisible = false;
  }

  handCheckOk() {
    this.CheckDataVisible = false;
    if (this.checkDataType.has(3)) this.saveCompanyData();
  }

  //儲存事件
  saveCompanyData() {
    let obj = this.saveDataJoin(this.validateForm.value);
    let url: string;
    switch (this.operateType) {
      case Type.Add:
        url = 'Company/Create';
        break
      case Type.Update:
        url = 'Company/Update';
        break;
    }
    this.httpClient.post(environment.apiServerURL + url, obj).subscribe({
      next: (res) => {
        if (res) this.createMessage('success', '已儲存變更'); this.routingViewSkip(res || this.invest_no);
      },
      error: (err) => {
        this.toastr.error("request failed");
      }
    })
  }

  saveDataJoin(Formdata): RequestCompanyModify {
    let data: RequestCompanyModify = {};
    data.Invest_No = this.invest_no;
    data.Name = Formdata.name || this.name;
    data.Nickname = Formdata.nickName || this.nick_Name;
    data.Subject = Formdata.subject;
    data.Status = Formdata.status;
    data.Unit_Pic = this.unitPicList.map(e => e.emplid);
    data.Email = this.emailList;
    data.Stock_Market = Formdata.stock_Market;
    data.Stock_Symbol = Formdata.stock_Symbol;
    data.Fin_Year_Rule = Formdata.fin_Year_Rule;
    data.Industry_Catagory = Formdata.industry_Catagory;
    data.Industry_Detail = Formdata.industry_Detail;
    data.Company_Stage = Formdata.company_Stage;
    data.Headquarter = Formdata.headquarter;
    data.Main_Business = Formdata.main_Business;
    data.SecurityList = this._securityDataList;
    console.log(data);
    return data;
  }


  routingViewSkip(data: any) {
    this.router.navigate(['companyMaintain/view'], { queryParams: { "investNo": data } });
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  //捨棄事件
  giveUp(event) {
    event.preventDefault();
    this.isVisible = true;
  }

  //捨棄取消框
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  //捨棄確認框
  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    this.routingSkip();
  }

  handleCheckOk() {
    this.isCheckVisible = false;
  }

  //刪除卡片事件
  giveDelete(event, index: number) {
    this.getNeedToDeleteSecurity = index
    event.preventDefault();
    this.isDelete = true;
  }


  //路由跳转
  routingSkip() {
    this.router.navigate(['companyMaintain/list']);
  }

  //折叠面板点击事件
  updateActive() { }

  //bo_pic
  chosenUnitPIC(data: any) {
    console.log(data);
    if (!!data) {
      this.unitPicList = data;
    }
  }
}
