import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '@environments/environment';

import { FilterCondition, GridviewService } from 'src/app/service/gridview.service';
import { LayoutService } from '../../layout/services/layout.service';
import { codeNameItem } from '../companyModel';
import { CompanyService } from '../services/company.service';
import { AuthorityControlService } from 'src/app/service/authority-control.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  loginUserId = "";
  listSelected = 'My List';
  isListVisible = false;
  securityTypeList: codeNameItem[] = [];
  searchValue = '';
  arrSecurityTypeId: string[] = [];
  boList = [];
  selectedBO = "";
  selectedBOLabel = "";
  statusList = [];
  selectedStatus = "";
  selectedStatusLabel = "";
  canCreateCompany = false;
  canUploadCompany = false;

  //File-Upload-Params
  logicAddr: string = "Company/UploadExcel";
  titleName: string = "公司營運狀況表";
  fileName: string = "公司營運狀況表";
  fileDescription: string = "提供Status上傳、修改用";
  templateName: string = "公司營運狀況表";
  downloadUrl: string = "Company/DownloadExcel";

  gridviewService: GridviewService;

  listOfColumn = [
    {
      title: ''
    },
    {
      title: '公司全稱',
      compare: (a, b) => a.name > b.name ? 1 : -1,
      priority: 1
    },
    {
      title: '公司簡稱',
      compare: (a, b) => a.nickname > b.nickname ? 1 : -1,
      priority: 2
    },
    {
      title: '投資部PIC',
      compare: (a, b) => a.emp_name > b.emp_name ? 1 : -1,
      priority: 3
    },
    {
      title: 'Status',
      compare: (a, b) => a.status_nm > b.status_nm ? 1 : -1,
      priority: 4
    },
    {
      title: '證券種類',
      compare: (a, b) => a.type_list_nm > b.type_list_nm ? 1 : -1,
      priority: 6
    }
  ];

  constructor(private layoutService: LayoutService,
    private authorityControlService: AuthorityControlService,
    private httpClient: HttpClient,
    private keycloakService: KeycloakService,
    private router: Router,
    private companyService: CompanyService) { }

  async ngOnInit(): Promise<void> {
    if (await this.keycloakService.isLoggedIn()) {
      await this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }

    if (!sessionStorage.getItem("companyMaintainList")) {
      this.listSelected = 'My List'
    } else {
      this.listSelected = sessionStorage.getItem('companyMaintainList')
    }
    this.getBOList();
    this.getKindList();
    this.getStatusList();

    this.gridviewService = new GridviewService(this.httpClient);
    this.gridviewService.pseudoPaging = true;
    this.gridviewService.url = environment.apiServerURL + "Company/GetAllCompany";


    this.authorityControlService.getUserRole().then(
      userRole => {
        console.log(userRole);
        if (userRole.isIT || userRole.isIMDMember || userRole.isBoardSecretariatMember) {
          this.canCreateCompany = true;
        }

        if (userRole.isIT || userRole.isIMDMember) {
          this.canUploadCompany = true;
        }
        this.search();
      }
    );
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  listVisibleChange(data) {
    this.isListVisible = data;
  }

  listSelectItemClick(data) {
    if (this.listSelected != data) {
      this.listSelected = data;
      this.search();
    }
  }

  updateListSelected(listSelected) {
    sessionStorage.setItem('companyMaintainList', listSelected)
  }

  search() {
    let filterCondition: FilterCondition[] = [];

    if (this.listSelected == 'My List') {
      let condition: FilterCondition = { columns: ["contact_ao"], operator: "=", term: [this.loginUserId] };
      filterCondition.push(condition);
    }

    if (this.searchValue != null && this.searchValue.trim() != "") {
      let condition: FilterCondition = { columns: ["name", "nickname", "emp_name"], operator: "like", term: [this.searchValue.trim()] };
      filterCondition.push(condition);
    }

    if (this.selectedStatus != null && this.selectedStatus.trim() != "") {
      let condition: FilterCondition = { columns: ["status"], operator: "=", term: [this.selectedStatus] };
      filterCondition.push(condition);
    }

    if (this.arrSecurityTypeId != null && this.arrSecurityTypeId.length > 0) {
      let condition: FilterCondition = { columns: ["type_list"], operator: "like", term: this.arrSecurityTypeId };
      filterCondition.push(condition);
    }

    this.gridviewService.filterCondition = filterCondition;
  }

  getKindList() {
    this.companyService.getCodeName('001');
    this.companyService.securityList.subscribe(item => this.securityTypeList = item as codeNameItem[]);
  }

  getBOList() {
    this.companyService.getCodeName('004');
    this.companyService.boList.subscribe(item => this.boList = item as codeNameItem[]);
  }

  getStatusList() {
    this.companyService.getCodeName('002');
    this.companyService.statusList.subscribe(item => this.statusList = item as codeNameItem[]);
  }

  securityTypeSelect(data: codeNameItem) {
    if (data.checked) {
      this.arrSecurityTypeId = [...this.arrSecurityTypeId, data.value];
    } else {
      this.arrSecurityTypeId = this.arrSecurityTypeId.filter(d => d !== data.value);
    }
  }

  clearSearchCondition() {
    this.searchValue = "";
    this.selectedStatus = "";
    this.selectedBO = "";
    this.arrSecurityTypeId = [];
    this.securityTypeList.forEach(x => {
      x.checked = false;
    });

    this.search();
  }

  viewCompany(investNo: number) {
    this.router.navigate(['companyMaintain/view'], { queryParams: { "investNo": investNo } });
  }

  clearSelectedStatus() {
    this.selectedStatus = "";
    this.selectedStatusLabel = "";
  }

  clearSelectedBO() {
    this.selectedBO = "";
    this.selectedBOLabel = "";
  }

  clearSelectedSecurityType() {
    this.arrSecurityTypeId = [];
    this.securityTypeList.forEach(x => {
      x.checked = false;
    });
  }
  //觸發刷新table list ，將上傳內容呈現出來
  chosenBOPIC(data: any) {
    console.log(data);
    this.gridviewService.refreshData();
    // this.chosenBOPICEmplid = data.emplid;
    // this.validateForm.get('bo_pic').setValue(data.name_A);
  }

}
