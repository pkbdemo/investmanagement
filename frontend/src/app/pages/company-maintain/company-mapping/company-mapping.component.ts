import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { ToastrService } from 'ngx-toastr';

import { LayoutService } from '../../layout/services/layout.service';
import { codeNameItem } from '../companyModel';
import { FilterCondition, GridviewService } from 'src/app/service/gridview.service';
import { CompanyService } from '../services/company.service';
import { AuthorityControlService } from 'src/app/service/authority-control.service';

@Component({
  selector: 'app-company-mapping',
  templateUrl: './company-mapping.component.html',
  styleUrls: ['./company-mapping.component.css']
})
export class CompanyMappingComponent implements OnInit {
  @Input() comeFrom: string;
  invest_No: string;
  @Input() set _investNo(data: any) {
    this.invest_No = data
  };


  constructor(private layoutService: LayoutService,
    private authorityControlService: AuthorityControlService,
    private httpClient: HttpClient,
    private router: Router,
    private companyService: CompanyService,
    private keycloakService: KeycloakService,
    private toastr: ToastrService
  ) { }

  loginUserId = "";
  isListVisible = false;
  canShowWholeView = true;
  listSelected = '';
  searchValue = '';
  selectedStatusLabel = "";
  statusList = [];
  arrSecurityTitleId: string[] = [];
  securityList: codeNameItem[] = [];
  searchPlaceholder = "搜尋公司/基金名稱、投資部PIC、財務系統使用名稱"
  comeFromOther = false;

  listOfColumn = [
    {
      title: '公司/基金名稱',
      compare: (a, b) => a.name > b.name ? 1 : -1,
      priority: 1
    },
    {
      title: '投資部PIC',
      compare: (a, b) => a.pic > b.pic ? 1 : -1,
      priority: 2
    },
    {
      title: '財務系統使用名稱',
      compare: (a, b) => a.fin_Sys_Name > b.fin_Sys_Name ? 1 : -1,
      priority: 3
    },
    {
      title: '證券/基金種類',
      compare: (a, b) => a.fund_Type_Code > b.fund_Type_Code ? 1 : -1,
      priority: 4
    },
    {
      title: '是否為權益法',
      compare: (a, b) => a.equity_Method > b.equity_Method ? 1 : -1,
      priority: 5
    }
  ];

  gridviewService: GridviewService;

  ngOnInit(): void {
    if (this.keycloakService.isLoggedIn()) {
      this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }

    this.authorityControlService.getUserRole().then(
      userRole => {
        console.log(userRole);
        if (userRole.isBoardSecretariatMember) {
          this.canShowWholeView = false;
          this.toastr.info("This account has no access rights")
        }
      }
    );

    if (!sessionStorage.getItem("companyFinanceList")) {
      this.listSelected = 'My List'
    } else {
      this.listSelected = sessionStorage.getItem('companyFinanceList')
    }

    this.getKindList();

    this.gridviewService = new GridviewService(this.httpClient);
    this.gridviewService.pseudoPaging = true;
    this.gridviewService.url = environment.apiServerURL + "CompanyFinanceMapping/GetAllCompanyFinanceMapping";

    if (!!this.comeFrom) {
      this.searchPlaceholder = "搜尋財務系統使用名稱"
      this.searchValue = "";
      this.listSelected = "";
      this.arrSecurityTitleId = [];
      this.securityList.forEach(x => {
        x.checked = false;
      });
      this.comeFromOther = true;
    }

    this.search();//防止刷新页面时数据闪现

  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  listVisibleChange(data) {
    this.isListVisible = data;
  }
  updateListSelected(listSelected) {
    sessionStorage.setItem('companyFinanceList', listSelected)
  }

  listSelectItemClick(data) {
    if (this.listSelected != data) {
      this.listSelected = data;
      this.search();
    }
  }

  search() {
    let filterCondition: FilterCondition[] = [];

    if (this.listSelected == 'My List') {
      console.log(this.loginUserId);
      let condition: FilterCondition = { columns: ["contact_Ao"], operator: "=", term: [this.loginUserId] };
      filterCondition.push(condition);
    }

    if (this.invest_No != null) {
      let condition: FilterCondition = { columns: ["invest_No"], operator: "=", term: [this.invest_No] };
      filterCondition.push(condition);
    }

    if (this.searchValue != null && this.searchValue.trim() != "") {
      let condition: FilterCondition;
      let searchField: string[] = ["name", "fin_Sys_Name", "pic"];
      if (this.comeFrom == "companyMaintain" || this.comeFrom == "fundMaintain") {
        searchField = ["fin_Sys_Name"];
      }
      condition = { columns: searchField, operator: "like", term: [this.searchValue.trim()] };
      filterCondition.push(condition);
    }

    if (this.arrSecurityTitleId != null && this.arrSecurityTitleId.length > 0) {
      let condition: FilterCondition = { columns: ["fund_Type_Name"], operator: "=", term: this.arrSecurityTitleId };
      filterCondition.push(condition);
    }

    this.gridviewService.filterCondition = filterCondition;

  }

  clearSelectedSecurityType() {
    this.arrSecurityTitleId = [];
    this.securityList.forEach(x => {
      x.checked = false;
    });
    this.search();
  }

  AccountTitleSelect(data: codeNameItem) {
    if (data.checked) {
      this.arrSecurityTitleId = [...this.arrSecurityTitleId, data.label];
    } else {
      this.arrSecurityTitleId = this.arrSecurityTitleId.filter(d => d !== data.label);
    }
    this.search();
  }

  clearSearchCondition() {
    this.searchValue = "";
    this.arrSecurityTitleId = [];
    this.securityList.forEach(x => {
      x.checked = false;
    });

    this.search();
  }

  getKindList() {
    this.companyService.getCodeName('001');
    this.companyService.securityList.subscribe(item => this.securityList = item as codeNameItem[]);
  }

  toDetail(_mapping_No: number) {
    this.router.navigate(['companyMapping/view'], { queryParams: { mapping_No: _mapping_No } });
  }
}

