import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { LayoutService } from 'src/app/pages/layout/services/layout.service';
import { companyFinanceView } from '../companyFinancialMappingModel';

@Component({
  selector: 'app-company-fin-map-view',
  templateUrl: './company-fin-map-view.component.html',
  styleUrls: ['./company-fin-map-view.component.css']
})
export class CompanyFinMapViewComponent implements OnInit {

  listOfData: companyFinanceView;
  mapping_No: number;

  panel =
    {
      active: true,
      disabled: false,
      name: '基本資料',
      customStyle: {
        background: '#ffffff',
        'border-radius': '4px',
        'margin-bottom': '24px',
        border: '0px'
      }
    }
  constructor(private layoutService: LayoutService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.route.queryParams.subscribe(params => {
      this.mapping_No = params["mapping_No"];
    });


  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  goBack() {
    this.location.back();
    // this.router.navigate(['../duediligence/'])
  }

  //必须方法，不然下拉选单不能展开
  updateActive(data?: any) {
  }
}
