import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyFinMapViewComponent } from './company-fin-map-view.component';

describe('CompanyFinMapViewComponent', () => {
  let component: CompanyFinMapViewComponent;
  let fixture: ComponentFixture<CompanyFinMapViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyFinMapViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyFinMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
