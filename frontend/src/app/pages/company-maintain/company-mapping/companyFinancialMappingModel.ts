export interface companyFinanceView {
    invest_No?: number;
    mapping_No?: number;
    fin_Sys_Name?: string;
    fund_Type_Code?: string;
    equity_Method?: boolean;
    name?: string;
    contact_ao?: string;
    pic?: string;
    fund_Type_Name?: string;
}