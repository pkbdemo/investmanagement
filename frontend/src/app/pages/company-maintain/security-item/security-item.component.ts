import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Security, Type } from '../companyModel';

@Component({
  selector: 'app-security-item',
  templateUrl: './security-item.component.html',
  styleUrls: ['./security-item.component.css']
})
export class SecurityItemComponent implements OnInit {

  @Input() Data: Security = {};
  @Input() type: Type;
  @Input() index: number;
  @Output() modifySecurity = new EventEmitter();
  CompanyType = Type;
  isVisible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  DeleteData() {
    this.isVisible = true;
  }
  handleOk() {
    this.isVisible = false;
    this.modifySecurity.emit({
      value: this.Data,
      type: Type.Delete,
      index: this.index
    });
  }
  handleCancel() {
    this.isVisible = false;
  }

  modifyData() {
    this.modifySecurity.emit({
      value: this.Data,
      type: Type.Update,
      index: this.index
    });
  }
}
