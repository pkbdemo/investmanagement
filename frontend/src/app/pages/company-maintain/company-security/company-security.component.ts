import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subscription } from 'rxjs';
import { CommonMethodService } from 'src/app/service/common-method.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { codeNameItem, Security, SecurityRequired, SecurityType, Type } from '../companyModel';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-company-security',
  templateUrl: './company-security.component.html',
  styleUrls: ['./company-security.component.css']
})
export class CompanySecurityComponent implements OnInit {
  @Output() goBackData = new EventEmitter();

  //用來存放編輯是編輯的那一筆
  index: number;

  title: string = '';
  type: string;
  type_Name: string;
  security_No: string;
  //记录卡片是否被金额记录使用
  isUse: boolean = false;
  //区分user
  userRole: any = {};

  dateFormat = IMSConstants.dateFormat4Tip;

  isVisible: boolean = false;
  handOkTxt: string = '加入卡片';
  stockList: codeNameItem[] = [];
  stockListOf: codeNameItem[] = [];
  closeDateList: string[] = [];
  stockListSub: Subscription;


  isShowStrike: boolean = false;

  securityRequired = SecurityRequired;
  requeiredSet = new Set<SecurityRequired>();
  //股票形式不可选择列表
  disableStockType: string;

  //接受到的數據
  SecurityList: Security = {};

  //表单
  validateForm: FormGroup;
  autoTips: Record<string, Record<string, string>> = {
    'zh-cn': {
      required: '該項為必填項'
    },
    en: {
      required: '該項為必填項'
    },
    default: {
      email: '不是合法Email格式'
    }
  };

  listOfSecurity: codeNameItem[] = [];

  //固定展示
  first_Trans_Date: string;

  get NAgreements() {
    return this.validateForm.get('Agreement') as FormArray;
  }

  constructor(private companyService: CompanyService,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private message: NzMessageService,
    private commonService: CommonMethodService) {
    this.validateForm = this.fb.group({
      Stock_Type: ['', [Validators.required]],
      DD_Close_Date: ['', [Validators.required]],
      Quit_Date: ['', []],
      Quit_Reason: ['', []],
      Remark: ['', []],
    });
  }

  ngOnInit(): void {
    this.getStock();
    this.requeiredSet.add(SecurityRequired.Stock_Type);
    this.requeiredSet.add(SecurityRequired.Dd_Close_Date);
    this.validateForm.get('Quit_Date').valueChanges.subscribe(data => {
      if (!!data) {
        this.requeiredSet.add(SecurityRequired.Quit_Reason);
        this.validateForm.get('Quit_Reason').setValidators(Validators.required);
      } else {
        if (this.requeiredSet.has(SecurityRequired.Quit_Reason)) this.requeiredSet.delete(SecurityRequired.Quit_Reason);
        this.validateForm.get('Quit_Reason').clearValidators();
      }
    });
    this.companyService.getCodeName('001');
    this.companyService.securityListObservable.subscribe(item => {
      this.listOfSecurity = (item as codeNameItem[]);

    });
  }

  //新增按鈕出發事件
  handleOk(): void {
    console.log('Button ok clicked!');
    if (this.validateForm.valid) {
      //寫入數據並返回
      this.insertData(this.validateForm.value);
      this.isVisible = false;
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          console.log(control);
          if (control instanceof FormArray) {
            Object.values(control.controls).forEach(controlArray => {
              if (controlArray.invalid) {
                if (controlArray instanceof FormGroup) {
                  Object.values(controlArray.controls).forEach(o => {
                    if (o.invalid) {
                      o.markAsDirty();
                      o.updateValueAndValidity({ onlySelf: true });
                    }
                  })
                }
              }
            })
          }
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  insertData(data) {
    this.SecurityList.Type = this.type;
    this.SecurityList.TypeName = this.type_Name;
    this.SecurityList.Stock_Type = data.Stock_Type;
    this.SecurityList.Stock_Type_Name = this.stockListOf.find(e => e.value == data.Stock_Type).label;
    this.SecurityList.DD_Close_Date = data.DD_Close_Date;
    this.SecurityList.Quit_Date = this.commonService.timestampToTime(data.Quit_Date);
    this.SecurityList.Quit_Reason = data.Quit_Reason;
    this.SecurityList.Remark = data.Remark;
    if (!!data.Agreement && data.Agreement.length > 0) {
      this.SecurityList.SecurityStrikeList = data.Agreement.map(e => { e.Due_Date = this.companyService.timestampToTime(e.Due_Date); return e; });
    }

    this.goBackData.emit({
      value: this.SecurityList,
      index: this.index
    });
  }

  handleCancel(event): void {
    this.isVisible = false;
    console.log('Button cancel clicked!');
  }

  //打開model觸發事件
  editVisible(data: boolean, title: string, type: Type, userRole: any, CompanyName: string, SecurityData?: Security, index?: number) {
    this.isVisible = data;
    this.userRole = userRole;
    this.index = index;
    switch (type) {
      case Type.Add:
        this.handOkTxt = '加入卡片';
        this.title = '新增Security - '
        break;
      case Type.Update:
        this.handOkTxt = '更新卡片';
        this.title = '編輯Security - '
        break;
    }

    //先处理页面栏位呈现以及是否必选逻辑
    this.type = title;
    let security = Number(title.replace(/0/g, ''));
    this.title = this.title + this.listOfSecurity.find(e => e.value == title).label
    this.type_Name = SecurityType[security];
    if (security == SecurityType.Note || security == SecurityType.Warrant) {
      this.isShowStrike = true;
      this.validateForm.addControl('Agreement', this.fb.array([
        this.fb.group({
          Due_Date: new FormControl(null, [Validators.required]),
          Strike_Price: new FormControl('', []),
          Strike_Stock_Share: new FormControl('', [])
        }),
      ]));
      console.log(this.validateForm);
    }

    //写入数据
    if (!!SecurityData) {
      this.SecurityList = SecurityData;
      if (!!SecurityData.Quit_Date) {
        this.requeiredSet.add(SecurityRequired.Quit_Date);
        this.validateForm.get('Quit_Date').setValidators(Validators.required);
      }
      else {
        if (this.requeiredSet.has(SecurityRequired.Quit_Date)) this.requeiredSet.delete(SecurityRequired.Quit_Date);
        this.validateForm.get('Quit_Date').clearValidators();
      }
      if (!!SecurityData.Security_No) {
        this.checkSecurity(SecurityData.Security_No).subscribe((res) => {
          this.isUse = res;
          if (!res) {
            this.closeDate(userRole, CompanyName);
          }
        });
      } else {
        this.closeDate(userRole, CompanyName);
      }
    } else {
      this.closeDate(userRole, CompanyName);
    }

    //股票形式必选逻辑
    this.stockTypeRequired(title);

    if (!!SecurityData) this.modifyData(SecurityData);
  }

  //写入数据
  modifyData(data: Security) {
    this.validateForm.get('Stock_Type').setValue(data.Stock_Type);
    let date = data.DD_Close_Date.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
    this.validateForm.get('DD_Close_Date').setValue(date);
    this.validateForm.get('Quit_Date').setValue(data.Quit_Date);
    this.validateForm.get('Quit_Reason').setValue(data.Quit_Reason);
    this.validateForm.get('Remark').setValue(data.Remark);
    if (!!data.SecurityStrikeList && data.SecurityStrikeList.length > 0) {
      data.SecurityStrikeList.forEach((value, index) => {
        if (index != 0) {
          (this.validateForm.get('Agreement') as FormArray).push(this.fb.group({
            Due_Date: new FormControl(null, [Validators.required]),
            Strike_Price: new FormControl('', []),
            Strike_Stock_Share: new FormControl('', [])
          }));
        }
        (this.validateForm.get('Agreement') as FormArray).controls[index].get('Due_Date').setValue(value.Due_Date);
        (this.validateForm.get('Agreement') as FormArray).controls[index].get('Strike_Price').setValue(value.Strike_Price);
        (this.validateForm.get('Agreement') as FormArray).controls[index].get('Strike_Stock_Share').setValue(value.Strike_Stock_Share);
      });
    }
  }

  //股票形式必选逻辑
  stockTypeRequired(type: string) {
    if (!this.requeiredSet.has(SecurityRequired.Stock_Type)) this.requeiredSet.add(SecurityRequired.Stock_Type);
    switch (type) {
      case '001':
        this.stockListOf = this.stockList.filter(e => e.value == '001' || e.value == '002' || e.value == '003' || e.value == '004');
        break;
      case '002':
        this.stockListOf = this.stockList.filter(e => e.value == '001' || e.value == '002' || e.value == '003' || e.value == '004' || e.value == '005');
        if (this.isUse) {
          this.disableStockType = this.stockList.find(e => e.value == this.SecurityList.Stock_Type).code_Extend_A;
        }
        break;
      case '003':
      case '004':
      case '005':
        this.stockListOf = this.stockList.filter(e => e.value == '001' || e.value == '002');
        break;
      case '008':
        this.requeiredSet.delete(SecurityRequired.Stock_Type);
        break;
    }
  }

  //根据user的部分去调整完成日
  closeDate(userRole: any, companyName: string) {
    if (userRole.isIMDMember || userRole.isIT) {
      this.getCloseDate(companyName).subscribe(data => {
        this.closeDateList = data;
      });
    }
    if (userRole.isBoardSecretariatMember) {
      this.closeDateList = ['無']
    }
  }

  //获取尽职调查完成日
  getCloseDate(companyName: string) {
    let formData = new FormData();
    formData.set('company_Name', companyName)
    return this.httpClient.post<any>(environment.apiServerURL + "DDiligence/GetCloseDate", formData);
  }

  //新增履約資料
  addAgreement() {
    (this.validateForm.get('Agreement') as FormArray).push(this.fb.group({
      Due_Date: new FormControl(null, [Validators.required]),
      Strike_Price: new FormControl('', []),
      Strike_Stock_Share: new FormControl('', [])
    }));
  }

  //移除履約資料FormArray
  removeArray(index: number) {
    (this.validateForm.get('Agreement') as FormArray).removeAt(index);
  }

  //獲取股票形式
  getStock() {
    this.companyService.getCodeName('006');
    this.stockListSub = this.companyService.stockListObservable.subscribe(item => {
      this.stockList = item as codeNameItem[];
      this.stockListOf = [...this.stockList];
    }
    );
  }

  createMessage(type: string, txt: string): void {
    this.message.create(type, txt);
  }

  //校验此卡片是否已被金額紀錄使用
  checkSecurity(security_No: string) {
    let formData = new FormData();
    formData.set('security_No', security_No)
    return this.httpClient.post<any>(environment.apiServerURL + "AmountRecord/UseSecurity", formData);
  }

  //關閉model事件
  nzAfterClose() {
    console.log('model 關閉');
    //關閉是需要恢復狀態
    this.isShowStrike = false;
    this.disableStockType = null;
    this.SecurityList = {};
    this.requeiredSet.clear();
    this.requeiredSet.add(SecurityRequired.Stock_Type);
    this.requeiredSet.add(SecurityRequired.Dd_Close_Date);
    this.validateForm.get('Quit_Date').clearValidators();
    this.validateForm.get('Quit_Reason').clearValidators();
    if (this.validateForm.contains('Agreement')) this.validateForm.removeControl('Agreement');
    this.validateForm.reset();
  }
}
