import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { environment } from '@environments/environment';

import { codeNameItem } from '../companyModel';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  userId: string = '';
  nameList = new Subject();
  fundNameList = new Subject();
  boList = new Subject();
  statusList = new Subject();
  catagoryList = new Subject();
  stageList = new Subject();
  headquarterList = new Subject();
  securityList = new Subject();
  accountList = new Subject();
  stockList = new Subject();
  fundPeriodList = new Subject();
  liabilityTypeList = new Subject();
  quarterList = new Subject();
  statementList = new Subject();
  scheduleList = new Subject();
  SurveyList = new Subject();
  MarketList = new Subject();
  FinYearList = new Subject();

  nameListObservable = this.nameList.asObservable();
  fundNameListObservable = this.fundNameList.asObservable();
  statementListObservable = this.statementList.asObservable();
  quarterListObservable = this.quarterList.asObservable();
  boListObservable = this.boList.asObservable();
  statusListObservable = this.statusList.asObservable();
  catagoryListObservable = this.catagoryList.asObservable();
  stageListObservable = this.stageList.asObservable();
  headquarterListObservable = this.headquarterList.asObservable();
  securityListObservable = this.securityList.asObservable();
  accountListObservable = this.accountList.asObservable();
  stockListObservable = this.stockList.asObservable();
  fundPeriodObservable = this.fundPeriodList.asObservable();
  liabilityTypeObservable = this.liabilityTypeList.asObservable();
  scheduleListObservable = this.scheduleList.asObservable();
  SurveyListObservable = this.SurveyList.asObservable();
  MarketListObservable = this.MarketList.asObservable();
  FinYearListObservable = this.FinYearList.asObservable();

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) {
    this.userId = sessionStorage.getItem("userID").toUpperCase();
  }

  getFundNameList() {
    this.httpClient.get<any>(environment.apiServerURL + "Fund/QueryFundNameListAll").subscribe({
      next: (res) => {
        if (res != null) {
          this.fundNameList.next([...res]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getNameList() {
    this.httpClient.get<any>(environment.apiServerURL + "Company/QueryNameListAll").subscribe({
      next: (res) => {
        if (res != null) {
          this.nameList.next([...res]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getCodeName(Kind_id: string) {
    let result: codeNameItem[] = [];
    let formData = new FormData();
    formData.set("Kind_id", Kind_id);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData).subscribe({
      next: (res) => {
        if (res != null) {
          for (let item of res) {
            let code: codeNameItem = {
              label: '',
              value: '',
              code_Extend_A: '',
              code_Extend_B: '',
              code_Extend_C: '',
              checked: false,
              disabled: true
            };
            code.label = item.code_Name;
            code.value = item.code_id;
            code.code_Extend_A = item.code_Extend_A;
            code.code_Extend_B = item.code_Extend_B;
            code.code_Extend_C = item.code_Extend_C;
            code.checked = false;
            code.disabled = true;
            result = [...result, code];
          }
          if (Kind_id == '001') this.securityList.next([...result]);
          else if (Kind_id == '002') this.statusList.next([...result]);
          else if (Kind_id == '003') this.catagoryList.next([...result]);
          else if (Kind_id == '004') this.boList.next([...result]);
          else if (Kind_id == '005') this.accountList.next([...result]);
          else if (Kind_id == '006') this.stockList.next([...result]);
          else if (Kind_id == '007') this.fundPeriodList.next([...result]);
          else if (Kind_id == '010') this.quarterList.next([...result]);
          else if (Kind_id == '011') this.statementList.next([...result]);
          else if (Kind_id == '012') this.stageList.next([...result]);
          else if (Kind_id == '013') this.liabilityTypeList.next([...result]);
          else if (Kind_id == '014') this.scheduleList.next([...result]);
          else if (Kind_id == '018') this.SurveyList.next([...result]);
          else if (Kind_id == '019') this.MarketList.next([...result]);
          else if (Kind_id == '020') this.FinYearList.next([...result]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  getheadquarter() {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryNationList").subscribe({
      next: (res) => {
        if (res != null) {
          this.headquarterList.next([...res]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  //標準時間轉換
  timestampToTime(timestamp: string): string {
    if (timestamp != null && timestamp != '') {
      let date = new Date(timestamp);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let MM = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      let dd = d < 10 ? ('0' + d) : d;
      return y + '-' + MM + '-' + dd;
    }
    else {
      return null
    }
  }
  //数组排序
  SortData(a: any, b: any): number {
    let ACloseDate = this.timestampToTime(a.DD_Close_Date.toString());
    let BCloseDate = this.timestampToTime(b.DD_Close_Date.toString());
    let ATransDate: string;
    let BTransDate: string;
    if (!!a.First_Trans_Date) ATransDate = this.timestampToTime(a.First_Trans_Date.toString());
    if (!!b.First_Trans_Date) BTransDate = this.timestampToTime(b.First_Trans_Date.toString());
    if (Number(new Date(ACloseDate)) != Number(new Date(BCloseDate))) return Number(new Date(ACloseDate)) > Number(new Date(BCloseDate)) ? 0 : -1;
    else return Number(new Date(ATransDate)) > Number(new Date(BTransDate)) ? 0 : -1;
  }
}
