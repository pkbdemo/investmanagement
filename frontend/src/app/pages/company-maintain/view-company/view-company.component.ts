import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { toNumber } from 'ng-zorro-antd/core/util';
import { ToastrService } from 'ngx-toastr';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '@environments/environment';

import { LayoutService } from '../../layout/services/layout.service';
import { IMSConstants } from 'src/app/utils/IMSConstants';
import { AuthorityControlService } from 'src/app/service/authority-control.service';
import { Security, SecurityStrike, Type } from '../companyModel';

@Component({
  selector: 'app-view-company',
  templateUrl: './view-company.component.html',
  styleUrls: ['./view-company.component.css']
})
export class ViewCompanyComponent implements OnInit {
  loginUserId = "";
  investNo: number;
  companyInfo: any;
  unitPic: string;
  securityList: Security[] = [];
  canRenameCompany = false;
  canEditCompany = false;
  statusList = [];
  tabTitle_company = "Company";
  tabTitle_financials = "Financials";
  tabTitle_FMV = "FMV&%";
  tabTitle_Shares = "參考股價";
  tabTitle_amountRecord = "金額紀錄";
  tabTitle_DDHistory = "DD History";
  tabTitle_companyFinanceMapping = "公司財務對應表";
  sessionStorageKey_selectedTabIndex = IMSConstants.sessionStorageKey.selectedTabIndex.viewCompany;
  selectedTabIndex = 0;
  selectedTabTitle = this.tabTitle_company;
  selectedCompany = true;

  companyType = Type;
  userRole: any;

  companyPanel_1 = { active: true }
  companyPanel_2 = { active: true }
  companyPanel_3 = { active: true }
  companyPanel_4 = { active: true }

  constructor(private layoutService: LayoutService,
    private route: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private keycloakService: KeycloakService,
    private authorityControlService: AuthorityControlService,
    private location: Location) { }

  async ngOnInit(): Promise<void> {
    if (await this.keycloakService.isLoggedIn()) {
      await this.keycloakService.loadUserProfile();
      this.loginUserId = this.keycloakService.getUsername().toUpperCase();
    }

    this.authorityControlService.getUserRole().then(
      userRole => {
        this.userRole = userRole;
      }
    );

    this.route.queryParams.subscribe(params => {
      let sInvestNo = params["investNo"];
      if (sInvestNo != null && sInvestNo.trim() != '') {
        this.investNo = toNumber(sInvestNo.trim());
        this.getCompanyInfo();
      }
    });

    let lastSelectedTabIndex = sessionStorage.getItem(this.sessionStorageKey_selectedTabIndex);
    if (lastSelectedTabIndex != null && lastSelectedTabIndex != "") {
      this.selectedTabIndex = Number.parseInt(lastSelectedTabIndex);
    }
  }

  toggleSmallMenu() {
    this.layoutService.toggleLeftBar();
  }

  goBack() {
    this.location.back();
  }

  updateActive() {

  }

  editCompany() {
    this.router.navigate(['companyMaintain/operate'], { queryParams: { "invest_no": this.investNo } });
  }

  getCompanyInfo() {
    this.httpClient.get<any>(environment.apiServerURL + "Company/GetCompanyDetail/" + this.investNo).subscribe({
      next: (res) => {
        console.log(res)
        this.companyInfo = res;
        if (!!res.unitPicList && res.unitPicList.length > 0) {
          this.unitPic = res.unitPicList.map(e => e.name_A).toString();
        }
        this.authorityControlService.getUserRole().then(
          userRole => {
            if (userRole.isIT || userRole.isManagerOfIMD) {
              this.canRenameCompany = true;
            }

            if (this.loginUserId == this.companyInfo.contact_Ao) {
              this.canEditCompany = true;
            }
          }
        );
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });

    this.httpClient.get<any>(environment.apiServerURL + "Company/GetCompanySecurityList/" + this.investNo).subscribe({
      next: (res) => {
        console.log(res);
        if (!!res) {
          res.forEach(element => {
            const obj: Security = {}
            obj.Type = element.type;
            obj.TypeName = element.type_Name;
            obj.DD_Close_Date = element.dD_Close_Date;
            obj.First_Trans_Date = element.first_Trans_Date;
            obj.Stock_Type = element.stock_Type;
            obj.Stock_Type_Name = element.stock_Type_Name;
            obj.Quit_Date = element.quit_Date;
            obj.Quit_Reason = element.quit_Reason;
            obj.Remark = element.remark;
            if (!!element.securityStrikeList) {
              obj.SecurityStrikeList = element.securityStrikeList.map(e => {
                let StrikeObj: SecurityStrike = {};
                StrikeObj.Due_Date = e.due_Date;
                StrikeObj.Strike_Price = e.strike_Price;
                StrikeObj.Strike_Stock_Share = e.strike_Stock_Share;
                return StrikeObj
              })
            }
            this.securityList.push(obj);
          });
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(IMSConstants.serverErrorMsg);
      }
    });
  }

  updateCompanyName(data: string) {
    if (this.companyInfo != null) {
      this.companyInfo.name = data;
    }
  }

  getCompanyStatusList() {
    this.httpClient.get<any>(environment.apiServerURL + "Company/GetCompanyStatusList/" + this.investNo).subscribe({
      next: (res) => {
        this.statusList = res;
      },
      error: (err) => {
        console.error(err);
        this.toastr.error(err.error.title);
      }
    });
  }

  selectedIndexChange(index: number): void {
    if (index == 0) {
      this.selectedCompany = true;
      this.selectedTabTitle = this.tabTitle_company;
    } else if (index == 1) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_financials;
    } else if (index == 2) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_amountRecord;
    } else if (index == 3) {
      this.selectedCompany = false;
      this.selectedTabTitle = this.tabTitle_DDHistory;
    }

    sessionStorage.setItem(this.sessionStorageKey_selectedTabIndex, index.toString());
  }
}
