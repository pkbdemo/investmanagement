export enum Type { Add, Update, Delete, View }
export enum SecurityType { PreferredShare = 1, CommonShare = 2, Note = 3, SAFE = 4, Warrant = 5, '出資額' = 8 }
export enum SecurityRequired { Stock_Type, Dd_Close_Date, Quit_Date, Quit_Reason }
export interface codeNameItem {
  name?: string;
  label: string;
  value: string;
  code_Extend_A?: string;
  code_Extend_B?: string;
  code_Extend_C?: string;
  checked?: boolean;
  disabled?: boolean;
}

export interface SecurityCodeItem {
  label?: string;
  value?: string;
  showDelete?: boolean;
}

export interface SecurityItem {
  Account: string;
  Stock_Type: string;
  Quit_Date: Date;
  Quit_Reason: string;
  Remark: string;
  Agreement: AgreementItem[];
}

export interface AgreementItem {
  Due_Date: Date;
  Strike_Price: string;
}


//新增Or修改Model
export interface RequestCompanyModify {
  Invest_No?: number;
  Name?: string;
  Nickname?: string;
  Vendor_Code?: string;
  Bo?: string;
  Bo_Pic?: string;
  Stock_Symbol?: string;
  Industry_Catagory?: string;
  Industry_Detail?: string;
  Company_Stage?: string;
  Main_Business?: string;
  Headquarter?: string;
  Status?: string;
  SecurityList?: Security[];
  Email?: string[];

  Subject?: string;
  Unit_Pic?: string[];
  Stock_Market?: string;
  Fin_Year_Rule?: string;
}

export interface Security {
  Security_No?: string;
  Type?: string;
  TypeName?: string;
  DD_Close_Date?: string;
  First_Trans_Date?: string;
  Stock_Type?: string;
  Stock_Type_Name?: string;
  Quit_Date?: string;
  Quit_Reason?: string;
  Remark?: string;
  SecurityStrikeList?: SecurityStrike[];
}


export interface SecurityStrike {
  Due_Date?: string;
  Strike_Price?: number;
  Strike_Stock_Share?: string;
}
