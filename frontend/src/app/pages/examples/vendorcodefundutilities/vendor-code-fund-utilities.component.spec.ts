import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorCodeFundUtilities } from './vendor-code-fund-utilities.component';

describe('VendorCodeFundUtilities', () => {
  let component: VendorCodeFundUtilities;
  let fixture: ComponentFixture<VendorCodeFundUtilities>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorCodeFundUtilities ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorCodeFundUtilities);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
