import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { LayoutService } from '../../layout/services/layout.service';
import { VendorCodeItem, BOPICItem } from '../../../utils/utility-component-model';

@Component({
  selector: 'app-vendor-code-fund-utilities',
  templateUrl: './vendor-code-fund-utilities.component.html',
  styleUrls: ['./vendor-code-fund-utilities.component.css']
})
export class VendorCodeFundUtilities implements OnInit {

  dataName: string = '基金全稱';

  companyName: string = 'ACBEL POLYTECH INC.';
  modeName: string = '公司'; // 基金的話要設為基金
  modeName2: string = '基金'; // 公司的話要設為公司
  investNo: string = '8';
  originName: string = 'Vertex VI';

  selectVendorCodeVisible = false;
  vendorCodeList: VendorCodeItem[] = [];

  searchVendorName: string = '';
  setOfCheckedVendorCode: string = '';

  constructor(
    private httpClient: HttpClient,
    public router: Router,
  ) { }

  ngOnInit(): void {
  }

  chosenVendorCode(data: string) {
    console.log(data);
  }

  chosenBOPIC(data: BOPICItem[]) {
    console.log(data);
  }

  updateSuccessName(data: string) {
    this.originName = data;
  }

  suggestStatus(): void {
    var testInvest_No: string = '31';
    var financialYear: string = '2021';

    let formData = new FormData();
    formData.set("invest_no", testInvest_No);
    formData.set("financialYear", financialYear);

    this.httpClient.post<any>(environment.apiServerURL + "Financials/CalculateSuggestStatus", formData).subscribe({
      next: (res) => {
        console.log(res);
      },
      error: (err) => {
        console.error(err);
      }
    });
  }

  rename(): void {
  }
}
