import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { firstValueFrom } from 'rxjs';

import { IMSConstants } from '../utils/IMSConstants';

interface UserRole {
  isIT: boolean;
  isIMDMember: boolean;
  isBoardSecretariatMember: boolean;
  isManagerOfIMD: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthorityControlService {
  private userRole: UserRole;

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) { }

  async getUserRole(): Promise<UserRole> {
    if (this.userRole == null) {
      const source$ = this.httpClient.get<UserRole>(environment.apiServerURL + "HR/GetUserRole");
      await firstValueFrom(source$).then(
        res => {
          this.userRole = res;
        },
        err => {
          console.error(err);
          this.toastr.error(IMSConstants.serverErrorMsg);
        }
      );
    }

    return this.userRole;
  }
}
