import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { codeNameItem } from '../pages/company-maintain/companyModel';

@Injectable({
  providedIn: 'root'
})
export class CommonMethodService {

  boList = new Subject();
  statusList = new Subject();
  catagoryList = new Subject();
  stageList = new Subject();
  securityList = new Subject();
  accountList = new Subject();
  stockList = new Subject();
  fundPeriodList = new Subject();
  liabilityTypeList = new Subject();
  quarterList = new Subject();
  statementList = new Subject();
  scheduleList = new Subject();
  SurveyList = new Subject();
  MarketList = new Subject();
  FinYearList = new Subject();
  currencyList = new Subject();
  leftSiderEdit = new Subject();
  SubjectList = new Subject();
  PaymentList = new Subject();

  statementListObservable = this.statementList.asObservable();
  quarterListObservable = this.quarterList.asObservable();
  boListObservable = this.boList.asObservable();
  statusListObservable = this.statusList.asObservable();
  catagoryListObservable = this.catagoryList.asObservable();
  stageListObservable = this.stageList.asObservable();
  securityListObservable = this.securityList.asObservable();
  accountListObservable = this.accountList.asObservable();
  stockListObservable = this.stockList.asObservable();
  fundPeriodObservable = this.fundPeriodList.asObservable();
  liabilityTypeObservable = this.liabilityTypeList.asObservable();
  scheduleListObservable = this.scheduleList.asObservable();
  SurveyListObservable = this.SurveyList.asObservable();
  MarketListObservable = this.MarketList.asObservable();
  FinYearListObservable = this.FinYearList.asObservable();
  subjectListObservable = this.SubjectList.asObservable();
  paymentListObservable = this.PaymentList.asObservable();

  currencyObservable = this.currencyList.asObservable();
  leftSiderEditObservable = this.leftSiderEdit.asObservable();



  constructor(private httpClient: HttpClient,
    private toastr: ToastrService,) { }
  //標準時間轉換
  timestampToTime(timestamp: string): string {
    if (timestamp != null && timestamp != '') {
      let date = new Date(timestamp);
      let y = date.getFullYear();
      let m = date.getMonth() + 1;
      let MM = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      let dd = d < 10 ? ('0' + d) : d;
      return y + '-' + MM + '-' + dd;
    }
    else {
      return null
    }
  }

  getCodeName(Kind_id: string) {
    let result: codeNameItem[] = [];
    let formData = new FormData();
    formData.set("Kind_id", Kind_id);
    this.httpClient.post<any>(environment.apiServerURL + "Utilities/SearchCodeList", formData).subscribe({
      next: (res) => {
        if (res != null) {
          for (let item of res) {
            let code: codeNameItem = {
              label: '',
              value: '',
              code_Extend_A: '',
              code_Extend_B: '',
              code_Extend_C: '',
              checked: false,
              disabled: true
            };
            code.label = item.code_Name;
            code.value = item.code_id;
            code.code_Extend_A = item.code_Extend_A;
            code.code_Extend_B = item.code_Extend_B;
            code.code_Extend_C = item.code_Extend_C;
            code.checked = false;
            code.disabled = true;
            result = [...result, code];
          }
          if (Kind_id == '001') this.securityList.next([...result]);
          else if (Kind_id == '002') this.statusList.next([...result]);
          else if (Kind_id == '003') this.catagoryList.next([...result]);
          else if (Kind_id == '004') this.boList.next([...result]);
          else if (Kind_id == '005') this.accountList.next([...result]);
          else if (Kind_id == '006') this.stockList.next([...result]);
          else if (Kind_id == '007') this.fundPeriodList.next([...result]);
          else if (Kind_id == '009') this.SubjectList.next([...result]);
          else if (Kind_id == '010') this.quarterList.next([...result]);
          else if (Kind_id == '011') this.statementList.next([...result]);
          else if (Kind_id == '012') this.stageList.next([...result]);
          else if (Kind_id == '013') this.liabilityTypeList.next([...result]);
          else if (Kind_id == '014') this.scheduleList.next([...result]);
          else if (Kind_id == '018') this.SurveyList.next([...result]);
          else if (Kind_id == '019') this.MarketList.next([...result]);
          else if (Kind_id == '020') this.FinYearList.next([...result]);
          else if (Kind_id == '023') this.PaymentList.next([...result]);
        }
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  RequestCurrencyList() {
    this.httpClient.get<any>(environment.apiServerURL + "Utilities/QueryCurrencyList").subscribe({
      next: (res) => {
        if (res != null) this.currencyList.next([...res]);
      },
      error: (err) => {
        console.error(err);
        this.toastr.error("request failed");
      }
    });
  }

  leftSider(liClass?: string) {
    this.leftSiderEdit.next(liClass);
  }


  BaseTrunString(base64) {
    if (!base64) return null;
    // 对base64转编码
    var decode = atob(base64);
    // 编码转字符串
    var str = decodeURI(decode);
    return str;
  }

  StringTrunBase(str) {
    if (!str) return null;
    // 对字符串进行编码
    var encode = encodeURI(str);
    // 对编码的字符串转化base64
    var base64 = btoa(encode);
    return base64;
  }

}
