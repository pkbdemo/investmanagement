import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { factories, IEmbedConfiguration, models, service } from 'powerbi-client';
import { environment } from '@environments/environment';

import { IMSConstants } from '../utils/IMSConstants';

const powerbi = new service.Service(factories.hpmFactory, factories.wpmpFactory, factories.routerFactory);

@Injectable({
  providedIn: 'root'
})
export class PowerbiService {

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) { }

  showDashboard(reportContainerId: string, reportId: string): void {
    let reportContainerId4Jquery = "#" + reportContainerId;

    $(reportContainerId4Jquery).height($(reportContainerId4Jquery).outerWidth() / 8 * 5);
    console.log("Frame Width: " + $(reportContainerId4Jquery).outerWidth());
    console.log("Frame Height: " + $(reportContainerId4Jquery).outerHeight());

    if (reportId != null && reportId.trim() != "") {
      this.httpClient.get<any>(environment.apiServerURL + "Dashboard/GetEmbedInfo/" + reportId).subscribe({
        next: (res) => {
          const embedConfiguration: IEmbedConfiguration = {
            type: "report",
            tokenType: models.TokenType.Embed,
            accessToken: res.embedToken,
            embedUrl: res.embedReportURL,
            /*
            // Enable this setting to remove gray shoulders from embedded report
            settings: {
                background: models.BackgroundType.Transparent
            }
            */
          };

          let reportContainer = document.getElementById(reportContainerId);

          // Embed Power BI report when Access token and Embed URL are available
          const report = powerbi.embed(reportContainer, embedConfiguration);

          // Triggers when a report schema is successfully loaded
          report.on("loaded", function () {
            console.log("Report load successful");
          });

          // Triggers when a report is successfully embedded in UI
          report.on("rendered", function () {
            console.log("Report render successful");
          });

          // Clear any other error handler event
          report.off("error");

          // Below patch of code is for handling errors that occur during embedding
          report.on("error", function (event) {
            // Use errorMsg variable to log error in any destination of choice
            console.error(event.detail);
            return;
          });
        },
        error: (err) => {
          console.error(err);
          this.toastr.error(IMSConstants.serverErrorMsg);
        }
      });
    }
  }
}
