export const environment = {
  production: false,
  apiServerRootUrl: "http://localhost:5091/",
  apiServerURL: "http://localhost:5091/api/",
  // matomoSiteId: -1,
  isLocalhost: true,
  dashboard_reportId: "fbc02cea-0e0b-4a57-a265-d00c9673e6fc",
  // keycloakConfig
};
