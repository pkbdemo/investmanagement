import { KeycloakConfig } from "keycloak-js";

// const keycloakConfig: KeycloakConfig = {
//   url: 'https://keycloak-prd.wistron.com/auth/',
//   realm: 'k8sprdwhqims',
//   clientId: 'ims'
// };

export const environment = {
  production: true,
  apiServerRootUrl: "https://ims-backend.k8sprd-whq.k8s.wistron.com/",
  apiServerURL: "https://ims-backend.k8sprd-whq.k8s.wistron.com/api/",
  // matomoSiteId: 171,
  isLocalhost: false,
  dashboard_reportId: "",
  // keycloakConfig
};
