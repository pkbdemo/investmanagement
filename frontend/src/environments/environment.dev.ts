import { KeycloakConfig } from "keycloak-js";

// const keycloakConfig: KeycloakConfig = {
//   url: 'https://keycloak-dev.wistron.com/auth',
//   realm: 'k8sdevwhqims',
//   clientId: 'ims'
// };

export const environment = {
  production: false,
  apiServerRootUrl: "https://ims-backend-dev.k8sqas-whq.k8s.wistron.com/",
  apiServerURL: "https://ims-backend-dev.k8sqas-whq.k8s.wistron.com/api/",
  // matomoSiteId: 169,
  isLocalhost: false,
  dashboard_reportId: "fbc02cea-0e0b-4a57-a265-d00c9673e6fc",
  // keycloakConfig
};
