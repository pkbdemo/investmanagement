﻿namespace ScheduleJob.Repositorys;

internal class CompanyFinancialsYtdRepository : ICompanyFinancialsYtdRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public IList<ShareholderEquityEntity> GetAllShareholderEquity()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select t1.financials_no, t1.invest_no, t1.unit, t2.amount as shareholder_equity,
                         t1.currency, t1.report_end_date, t1.next_report_start_date, t1.next_report_end_date
                       from company_financials_ytd t1, company_financials_detail t2
                       where (t1.next_report_start_date-interval'1 days')::date<=current_date
                         and current_date<=(t1.next_report_end_date-interval'1 days')::date
                         and t1.financials_no=t2.financials_no and t2.account_id={0}";
        sql = String.Format(sql, IMSConstants.AccountId_Equity);
        var shareholderEquityList = dbConn.Query<ShareholderEquityEntity>(sql).ToList();
        return shareholderEquityList;
    }

    public ShareholderEquityEntity GetShareholderEquity(int invest_no, DateTime transaction_date)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select t1.financials_no, t1.invest_no, t1.unit, t2.amount as shareholder_equity,
                         t1.currency, t1.report_end_date, t1.next_report_start_date, t1.next_report_end_date
                       from company_financials_ytd t1, company_financials_detail t2
                       where (t1.next_report_start_date-interval'1 days')::date<=:transaction_date
                         and :transaction_date<=(t1.next_report_end_date-interval'1 days')::date
                         and t1.financials_no=t2.financials_no and t2.account_id={0}
                         and invest_no=:invest_no
                       limit 1";
        sql = String.Format(sql, IMSConstants.AccountId_Equity);
        var shareholderEquity = dbConn.QuerySingleOrDefault<ShareholderEquityEntity>(sql, new { invest_no, transaction_date });
        return shareholderEquity;
    }
}
