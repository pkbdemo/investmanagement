﻿namespace ScheduleJob.Repositorys;

public class CompanyStockPriceRepository : ICompanyStockPriceRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public int InsertOrUpdateTodayNetWorth(int invest_no, decimal net_worth_price, string net_worth_currency)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"insert into company_stock_price(invest_no, transaction_date, net_worth_price, net_worth_currency, create_user, create_date, update_user, update_date)
                       values(:invest_no, current_date, :net_worth_price, :net_worth_currency, :System_User, current_timestamp, null, null)
                       on conflict(invest_no, transaction_date) do update set
                         net_worth_price=excluded.net_worth_price,
                         net_worth_currency=excluded.net_worth_currency,
                         update_user=:System_User,
                         update_date=current_timestamp";
        return dbConn.Execute(sql, new { invest_no, net_worth_price, net_worth_currency, IMSConstants.System_User });
    }

    public IList<CompanyStockPriceEntity> GetCompanyStockPriceList4UpdateNetWorth()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select stock_price_no, invest_no, transaction_date, update_date
                       from company_stock_price
                       where net_worth_price is null and update_date>(current_timestamp+'-3days')";
        var companyStockPriceList = dbConn.Query<CompanyStockPriceEntity>(sql).ToList();
        return companyStockPriceList;
    }

    public int UpdateNetWorth(int stock_price_no, decimal net_worth_price, string net_worth_currency)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"update company_stock_price
                       set net_worth_price=:net_worth_price, net_worth_currency=:net_worth_currency,
                           update_user=:System_User, update_date=current_timestamp 
                       where stock_price_no=:stock_price_no";
        return dbConn.Execute(sql, new { stock_price_no, net_worth_price, net_worth_currency, IMSConstants.System_User });
    }
}