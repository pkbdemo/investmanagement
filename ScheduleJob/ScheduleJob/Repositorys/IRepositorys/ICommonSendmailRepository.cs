﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface ICommonSendmailRepository
{
    IList<CommonSendmailEntity> GetAllUnprocessMail();
    int UpdateProcessedMail(int id, string? error_msg = null);
    int AddMail(CommonSendmailEntity MailEntity);
}
