﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface ICompanyStockPriceRepository
{
    int InsertOrUpdateTodayNetWorth(int invest_no, decimal net_worth_price, string net_worth_currency);
    IList<CompanyStockPriceEntity> GetCompanyStockPriceList4UpdateNetWorth();
    int UpdateNetWorth(int stock_price_no, decimal net_worth_price, string net_worth_currency);
}
