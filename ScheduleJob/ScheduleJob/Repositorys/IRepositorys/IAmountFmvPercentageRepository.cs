﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface IAmountFmvPercentageRepository
{
    decimal? GetShareholdingRatio(int invest_no, int year, int month);
}
