﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface IDdMainRepository
{
    IList<BoPCTEntity> QueryBoPCT();
    List<BoPCTEntity> QueryBoNameList();
}
