﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface IAmountRecordRepository
{
    IList<StockShareEntity> GetStockShares(int invest_no);
}
