﻿namespace ScheduleJob.Repositorys.IRepositorys;

public interface ICompanyFinancialsYtdRepository
{
    IList<ShareholderEquityEntity> GetAllShareholderEquity();
    ShareholderEquityEntity GetShareholderEquity(int invest_no, DateTime transaction_date);
}
