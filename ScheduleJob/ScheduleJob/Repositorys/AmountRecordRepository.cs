﻿namespace ScheduleJob.Repositorys;

public class AmountRecordRepository : IAmountRecordRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public IList<StockShareEntity> GetStockShares(int invest_no)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select t2.amount_no, t2.record_type, t3.code_name as record_type_name,
                         t2.ori_stock_share, t2.investment_date
                       from company_security t1, amount_record t2, setting_code_detail t3
                       where t1.invest_no=:invest_no and t1.security_no=t2.security_no
                         and t2.ori_stock_share is not null
                         and t2.investment_date<=current_date and t2.record_type=t3.code_id
                         and t3.kind_id='008' and t3.code_id in ('001', '002', '004')";
        var stockShareList = dbConn.Query<StockShareEntity>(sql, new { invest_no }).ToList();
        return stockShareList;
    }
}
