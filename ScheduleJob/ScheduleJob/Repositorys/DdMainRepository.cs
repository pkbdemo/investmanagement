﻿namespace ScheduleJob.Repositorys;

public class DdMainRepository : IDdMainRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public IList<BoPCTEntity> QueryBoPCT()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"with scd as 
                    (
                        select scd.kind_id, scd.code_id, scd.code_name, scd.code_extend_a
                        from setting_code_master scm inner join setting_code_detail scd
                        on scm.kind_id = scd.kind_id
                        where scd.kind_id  = '004'
                    )
                    select ds.start_date, ds.status, sc.code_name as bo_name, sc.code_extend_a as profit_center from dd_main dm
                            left join dd_stage ds on ds.dd_no = dm.dd_no  
                            left join scd sc on sc.code_id = dm.bo 
                            where ds.status  = '001'
        ";
        var boPCTByMonth = dbConn.Query<BoPCTEntity>(sql).ToList();
        return boPCTByMonth;
    }
    public List<BoPCTEntity> QueryBoNameList()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select scd.code_name as bo_name, scd.code_extend_a as profit_center
                        from setting_code_master scm inner join setting_code_detail scd
                        on scm.kind_id = scd.kind_id
                        where scd.kind_id  = '004'
        ";
        var boNameList = dbConn.Query<BoPCTEntity>(sql).ToList();
        return boNameList;
    }
}