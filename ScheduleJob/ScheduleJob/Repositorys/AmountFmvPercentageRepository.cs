﻿namespace ScheduleJob.Repositorys;

public class AmountFmvPercentageRepository : IAmountFmvPercentageRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public decimal? GetShareholdingRatio(int invest_no, int year, int month)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"select sum(shareholding_ratio) from amount_fmv_percentage
                       where invest_no=:invest_no and ""year""=:year and ""month"" =:month and fmv_type='002'";
        var shareholdingRatio = dbConn.QuerySingleOrDefault<decimal?>(sql, new { invest_no, year, month });
        return shareholdingRatio;
    }
}
