﻿namespace ScheduleJob.Repositorys;

public class CommonSendmailRepository : ICommonSendmailRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public IList<CommonSendmailEntity> GetAllUnprocessMail()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from common_sendmail where process_dt is null";
        var commonSendmailList = dbConn.Query<CommonSendmailEntity>(sql).ToList();
        return commonSendmailList;
    }

    public int UpdateProcessedMail(int id, string? error_msg = null)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "update common_sendmail set process_dt=current_timestamp, error_msg=:error_msg where id=:id";
        return dbConn.Execute(sql, new { error_msg, id });
    }

    public int AddMail(CommonSendmailEntity MailEntity)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"INSERT INTO common_sendmail
                    (      
                    Mail_From, 
                    Mail_To,
                    Mail_Cc,        
                    Mail_Bcc,          
                    Subject,          
                    Mail_Body,        
                    Create_Dt,
                    Process_Dt,       
                    Error_Msg,        
                    Attachment,   
                    File_Name,      
                    File_Size
                    )
                    VALUES (  
                    :Mail_From, 
                    :Mail_To,
                    :Mail_Cc,       
                    :Mail_Bcc,            
                    :Subject,            
                    :Mail_Body,           
                    :Create_Dt,
                    :Process_Dt,      
                    :Error_Msg,          
                    :Attachment,    
                    :File_Name,       
                    :File_Size
                    )";
        return dbConn.Execute(sql, MailEntity);
    }
}
