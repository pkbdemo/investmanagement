﻿namespace ScheduleJob.Repositorys;

public class ModificationLogRepository : IModificationLogRepository
{
    private readonly IDBHelper _dbHelper = new PostgreSQLHelper();

    public int DeleteData_OneYearAgo()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "delete from modification_log where (update_date + '1year')<current_timestamp";
        return dbConn.Execute(sql);
    }
}