﻿namespace ScheduleJob.Common;

public interface IDBHelper
{
    enum ConditionOperator
    {
        Equal,
        Like
    }
    IDbConnection GetIMSConnection();
    void CloseConnection(IDbConnection conn);
}

public class PostgreSQLHelper : IDBHelper
{
    private static string? IMSConnectionString = null;

    /// <summary>
    /// Get the finacne portal DB connection
    /// </summary>
    /// <returns></returns>
    public IDbConnection GetIMSConnection()
    {
        if (IMSConnectionString == null)
        {
            AppSettingsHelper appSettingsHelper = new AppSettingsHelper();
            string encryptPassword = appSettingsHelper.GetAppSettings("IMSConnectionStringPassword");
            string password = EncryptionHelper.AESDecrypt(encryptPassword);

            string sIMSConnectionString = appSettingsHelper.GetAppSettings("IMSConnectionString");
            IMSConnectionString = string.Format(sIMSConnectionString, password);
        }

        return new NpgsqlConnection(IMSConnectionString);
    }

    /// <summary>
    /// Close the db connection
    /// </summary>
    /// <param name="conn"></param>
    public void CloseConnection(IDbConnection conn)
    {
        if (conn.State == ConnectionState.Open || conn.State == ConnectionState.Broken)
            conn.Close();
    }
}