﻿namespace ScheduleJob.Common;

public class IMSConstants
{
    public const string System_User = "System";

    /// <summary>
    /// Account ID - shareholders' equity
    /// </summary>
    public const int AccountId_Equity = 6;
}