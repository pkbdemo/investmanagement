﻿namespace ScheduleJob.Common;

public class SendmailHelper
{
    public const string SMTP_Host = "whqsmtp.wistron.com";
    public const int SMTP_Port = 25;
    public const string DefaultMailFrom = "IMS_Support.WHQ.Wistron@wistron.com";

    private static string? JobEnvironment = null;
    private readonly AppSettingsHelper _appSettingsHelper = new AppSettingsHelper();

    public MimeMessage GetMimeMessage(string sMailFrom, string? sMailTo, string? sMailCC, string? sMailBCC, string sMailSubject, string sMailBody, byte[]? attachmentContent, string? attachmentName)
    {
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress("", sMailFrom));
        message.Subject = string.Format("[{0}] {1}", GetEnvironment(), sMailSubject);

        if (!string.IsNullOrWhiteSpace(sMailTo))
        {
            foreach (var temp in sMailTo.Split(','))
            {
                if (!string.IsNullOrWhiteSpace(temp))
                {
                    message.To.Add(new MailboxAddress("", temp));
                }
            }
        }

        if (!string.IsNullOrWhiteSpace(sMailCC))
        {
            foreach (var temp in sMailCC.Split(','))
            {
                if (!string.IsNullOrWhiteSpace(temp))
                {
                    message.Cc.Add(new MailboxAddress("", temp));
                }
            }
        }

        if (!string.IsNullOrWhiteSpace(sMailBCC))
        {
            foreach (var temp in sMailBCC.Split(','))
            {
                if (!string.IsNullOrWhiteSpace(temp))
                {
                    message.Bcc.Add(new MailboxAddress("", temp));
                }
            }
        }

        var body = new TextPart(TextFormat.Html)
        {
            Text = sMailBody
        };

        if (attachmentContent == null || attachmentContent.LongLength == 0 || string.IsNullOrWhiteSpace(attachmentName))
        {
            // If there is no attachment
            message.Body = body;
        }
        else
        {
            // If there is any attachment
            var attachment = new MimePart()
            {
                Content = new MimeContent(new MemoryStream(attachmentContent), ContentEncoding.Default),
                ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                FileName = attachmentName
            };

            var multipart = new Multipart("mixed");
            multipart.Add(body);
            multipart.Add(attachment);

            message.Body = multipart;
        }

        return message;
    }

    private string? GetEnvironment()
    {
        if (JobEnvironment == null)
        {
            JobEnvironment = _appSettingsHelper.GetAppSettings("JobEnvironment");
        }

        return JobEnvironment;
    }
}