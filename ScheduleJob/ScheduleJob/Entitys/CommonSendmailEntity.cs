﻿namespace ScheduleJob.Entitys;

public class CommonSendmailEntity
{
    public int? id { get; set; }
    public string? Mail_From { get; set; }
    public string? Mail_To { get; set; }
    public string? Mail_Cc { get; set; }
    public string? Mail_Bcc { get; set; }
    public string? Subject { get; set; }
    public string? Mail_Body { get; set; }
    public DateTime? Create_Dt { get; set; }
    public DateTime? Process_Dt { get; set; }
    public string? Error_Msg { get; set; }
    public byte[]? Attachment { get; set; }
    public string? File_Name { get; set; }
    public int? File_Size { get; set; }
}