﻿namespace ScheduleJob.Entitys;

public class CompanyStockPriceEntity
{
    public int? Stock_Price_No { get; set; }
    public int? Invest_No { get; set; }
    public DateTime? Transaction_Date { get; set; }
    public string? Public_Actual_Currency { get; set; }
    public decimal? Public_Actual_Price { get; set; }
    public string? Private_Actual_Currency { get; set; }
    public decimal? Private_Actual_Price { get; set; }
    public decimal? Discount { get; set; }
    public string? Net_Worth_Currency { get; set; }
    public decimal? Net_Worth_Price { get; set; }
    public string? History_Currency { get; set; }
    public decimal? History_Price { get; set; }
    public string? Limit_Currency { get; set; }
    public decimal? Limiy_Price { get; set; }
    public string? Create_User { get; set; }
    public DateTime? Create_Date { get; set; }
    public string? Update_User { get; set; }
    public DateTime? Update_Date { get; set; }
}
