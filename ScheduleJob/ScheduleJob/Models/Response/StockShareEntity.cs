﻿namespace ScheduleJob.Models.Response;

public class StockShareEntity
{
    public int? Amount_No { get; set; }
    public string? Record_Type { get; set; }
    public string? Record_Type_Name { get; set; }
    public decimal? Ori_Stock_Share { get; set; }
    public DateTime? Investment_Date { get; set; }
}