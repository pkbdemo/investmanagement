﻿namespace ScheduleJob.Models.Response;

public class ShareholderEquityEntity
{
    public int? Financials_No { get; set; }
    public int? Invest_No { get; set; }
    public decimal? Unit { get; set; }
    public decimal? Shareholder_Equity { get; set; }
    public string? Currency { get; set; }
    public DateTime? Report_End_Date { get; set; }
    public DateTime? Next_Report_Start_Date { get; set; }
    public DateTime? Next_Report_End_Date { get; set; }
}
