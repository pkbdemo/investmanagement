﻿namespace ScheduleJob.Models.Response;

public class BoPCTEntity
{
    public string? Bo_Name { get; set; }
    public string? Profit_Center { get; set; }
    public string? Status { get; set; }
    public DateTime? Start_Date { get; set; }
}
