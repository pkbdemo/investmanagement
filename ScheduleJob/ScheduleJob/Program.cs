﻿var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureServices(services =>
{
    services.AddQuartz(q =>
    {
        // ********** Clear Data Job configuration **********
        JobKey clearDataJobKey = new JobKey("Clear Data Job", "group1");

        q.AddJob<ClearDataJob>(options =>
        {
            options.WithIdentity(clearDataJobKey);
        });

        q.AddTrigger(options =>
        {
            // This trigger fires at 10 am on the 1th of each month
            options.ForJob(clearDataJobKey)
                .WithCronSchedule("0 0 10 1 * ?");
        });


        // ********** Mail BoPCT By Month Job configuration **********
        JobKey boPCTJobKey = new JobKey("Mail BoPCT By Month Job", "group1");

        q.AddJob<MailBoPCTByMonthJob>(options =>
        {
            options.WithIdentity(boPCTJobKey);
        });

        q.AddTrigger(options =>
        {
            // This trigger fires at 1 am on the 21th of each month
            options.ForJob(boPCTJobKey)
            .WithCronSchedule("0 0 1 21 * ?");
            //測試用，啟動后立馬執行，之後每隔1分鐘執行一次
            // .WithSimpleSchedule(x =>
            //    x.WithIntervalInMinutes(1)
            //    );
        });


        // ********** Send mail Job configuration **********
        JobKey sendMailJobKey = new JobKey("Send mail Job", "group1");

        q.AddJob<SendMailJob>(options =>
        {
            options.WithIdentity(sendMailJobKey);
        });

        q.AddTrigger(options =>
        {
            // This trigger fires every 5 minutes
            options.ForJob(sendMailJobKey)
                .WithCronSchedule("0 0/5 * * * ?");
        });


        // ********** Daily net worth calculation Job configuration **********
        JobKey calculateNetWorthJobKey = new JobKey("Calculate net worth Job", "group1");

        q.AddJob<CalculateNetWorthJob>(options =>
        {
            options.WithIdentity(calculateNetWorthJobKey);
        });

        q.AddTrigger(options =>
        {
            // This trigger fires at 1:10 am every day
            options.ForJob(calculateNetWorthJobKey)
                .WithCronSchedule("0 10 1 * * ?");
        });


        // ********** Update net worth Job configuration **********
        JobKey updateNetWorthJobKey = new JobKey("Update net worth Job", "group1");

        q.AddJob<UpdateNetWorthJob>(options =>
        {
            options.WithIdentity(updateNetWorthJobKey);
        });

        q.AddTrigger(options =>
        {
            // This trigger fires once every hour
            options.ForJob(updateNetWorthJobKey)
                .WithCronSchedule("0 50 * * * ?");
        });
    });

    services.AddQuartzHostedService(options =>
    {
        options.AwaitApplicationStarted = true;
    });
});

builder.UseSerilog((ctx, lc) =>
{
    lc.ReadFrom.Configuration(ctx.Configuration);
});

builder.Build().Run();