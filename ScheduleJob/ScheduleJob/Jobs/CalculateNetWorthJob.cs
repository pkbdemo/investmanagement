﻿namespace ScheduleJob.Jobs;

public class CalculateNetWorthJob : IJob
{
    private readonly ICompanyFinancialsYtdRepository _companyFinancialsYtdRepository = new CompanyFinancialsYtdRepository();
    private readonly IAmountRecordRepository _amountRecordRepository = new AmountRecordRepository();
    private readonly IAmountFmvPercentageRepository _amountFmvPercentageRepository = new AmountFmvPercentageRepository();
    private readonly ICompanyStockPriceRepository _companyStockPriceRepository = new CompanyStockPriceRepository();

    public Task Execute(IJobExecutionContext context)
    {
        // 淨值 = 股東權益 / (持有股數/持股比例)
        // 淨值幣別 = 當淨值有計算出資料，抓取該股東權益的財報幣別
        // 三者任一無法計算時，如無對應[Financials]財年+Quarter資料或[金額紀錄]無資料/股數為0或無對應[FMV&%]年份+月份資料，則此筆不寫入[參考股價]
        //
        // 情境:
        // 07/21執行, 至[公司維護]查看公司財年制度為一般財年制
        // 股東權益抓2022Q2的財報資料
        // 持有股數抓取<=7/21的金額紀錄資料
        // 持股比例抓2022/06的FMV&%資料

        try
        {
            Log.Information("Begin to execute the job: {0}", this.GetType().FullName);

            // Store shareholding ratio to reduce the number of database queries
            var dicShareholdingRatio = new Dictionary<string, decimal?>();

            var shareholderEquityList = _companyFinancialsYtdRepository.GetAllShareholderEquity();
            foreach (var shareholderEquityEntity in shareholderEquityList)
            {
                // 1. 股東權益 = 該公司財報資料的股東權益 x 金額單位
                //    (下季報表期初-1日 <= 當前日期 <= 下季報表期末-1日 的財報資料)
                var shareholderEquity = shareholderEquityEntity.Shareholder_Equity * shareholderEquityEntity.Unit;

                // 2. 持股比例 = 該公司的持股比例 (股東權益的報表期末年月)
                //    假設公司財年制度為一般財年制，持股比例只拿3、6、9、12月份的資料
                //    基準認月底日，3/31~6/29皆看3月的持股比例
                string key_shareholdingRatio = string.Format(
                    "{0}-{1}-{2}",
                    shareholderEquityEntity.Invest_No.Value,
                    shareholderEquityEntity.Report_End_Date.Value.Year,
                    shareholderEquityEntity.Report_End_Date.Value.Month);

                decimal? shareholdingRatio = null;
                if (dicShareholdingRatio.ContainsKey(key_shareholdingRatio))
                {
                    shareholdingRatio = dicShareholdingRatio[key_shareholdingRatio];
                }
                else
                {
                    shareholdingRatio = _amountFmvPercentageRepository.GetShareholdingRatio(
                        shareholderEquityEntity.Invest_No.Value,
                        shareholderEquityEntity.Report_End_Date.Value.Year,
                        shareholderEquityEntity.Report_End_Date.Value.Month);

                    dicShareholdingRatio.Add(key_shareholdingRatio, shareholdingRatio);
                }

                if (shareholdingRatio == null)
                {
                    continue;
                }

                // 3. 持有股數 = 該公司累積 (投資 - 處分 + 股票分割/合併) 的股數(<= 當前日期)
                var stockShareList = _amountRecordRepository.GetStockShares(shareholderEquityEntity.Invest_No.Value);
                if (stockShareList.Count <= 0)
                {
                    continue;
                }

                decimal stockShare = 0;
                foreach (var stockShareEntity in stockShareList)
                {
                    if (stockShareEntity.Record_Type == "002") // 002: 處分
                    {
                        stockShare -= stockShareEntity.Ori_Stock_Share.Value;
                    }
                    else
                    {
                        stockShare += stockShareEntity.Ori_Stock_Share.Value;
                    }
                }

                if (stockShare <= 0)
                {
                    continue;
                }

                // 4. 淨值 = 股東權益 / (持有股數/持股比例)
                decimal netWorthPrice = shareholderEquity.Value / (stockShare / shareholdingRatio.Value);

                _companyStockPriceRepository.InsertOrUpdateTodayNetWorth(shareholderEquityEntity.Invest_No.Value, netWorthPrice, shareholderEquityEntity.Currency);
            }

            Log.Information("End to execute the job: {0}", this.GetType().FullName);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "");
        }

        return Task.CompletedTask;
    }
}
