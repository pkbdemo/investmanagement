﻿namespace ScheduleJob.Jobs;

public class SendMailJob : IJob
{
    private readonly ICommonSendmailRepository _commonSendmailRepository = new CommonSendmailRepository();
    private readonly SendmailHelper _sendmailHelper = new SendmailHelper();

    public Task Execute(IJobExecutionContext context)
    {
        int? currentMailId = null;

        try
        {
            Log.Information("Begin to execute the job: {0}", this.GetType().FullName);

            IList<CommonSendmailEntity> unprocessMail = _commonSendmailRepository.GetAllUnprocessMail();

            if (unprocessMail != null && unprocessMail.Count > 0)
            {
                using (var client = new SmtpClient())
                {
                    client.Connect(SendmailHelper.SMTP_Host, SendmailHelper.SMTP_Port, false);

                    foreach (var mail in unprocessMail)
                    {
                        currentMailId = mail.id;

                        var message = _sendmailHelper.GetMimeMessage(
                            mail.Mail_From,
                            mail.Mail_To,
                            mail.Mail_Cc,
                            mail.Mail_Bcc,
                            mail.Subject,
                            mail.Mail_Body,
                            mail.Attachment,
                            mail.File_Name);
                        client.Send(message);
                        _commonSendmailRepository.UpdateProcessedMail(mail.id.Value);
                    }
                    
                    client.Disconnect(true);
                }
            }

            Log.Information("End to execute the job: {0}", this.GetType().FullName);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "");

            if (currentMailId != null)
            {
                _commonSendmailRepository.UpdateProcessedMail(currentMailId.Value, ex.ToString());
            }
        }

        return Task.CompletedTask;
    }
}
