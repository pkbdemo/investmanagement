﻿namespace ScheduleJob.Jobs;

public class MailBoPCTByMonthJob : IJob
{
    private readonly IDdMainRepository _ddMainRepository = new DdMainRepository();
    private readonly ICommonSendmailRepository _commonSendmailRepository = new CommonSendmailRepository();

    public Task Execute(IJobExecutionContext context)
    {
        try
        {
            bool changeMailBackgroundColor = true;
            decimal loadingTotal = 0;
            // decimal DirectAgainstTotal = 0;
            decimal allocTotal = 0;
            decimal finalRatioTotal = 0;
            decimal uploadTotal = 0;
            decimal maxUploadValue = 1;
            Log.Information("Begin to execute the job: {0}", this.GetType().FullName);

            //!以dd_main為主，去join dd_stage 然後判斷status為001
            var BoPCT = _ddMainRepository.QueryBoPCT();
            var BoNameList = _ddMainRepository.QueryBoNameList();
            //!且start_date落在當年度1月1號至當年度月份-1月的最後一天，符合的資料取出做統計並寄出mail
            var now = DateTime.Now;
            var firstDayCurrentMonth = new DateTime(now.Year, now.Month, 20);
            // var lastDayFromLastMonth = firstDayCurrentMonth.AddDays(-1);

            var firstDayFromYear = new DateTime(firstDayCurrentMonth.Year, 1, 1);

            var BoPCTList = (from data in BoPCT where data.Start_Date >= firstDayFromYear && data.Start_Date <= firstDayCurrentMonth select data).ToList();
            var mailBody = "Dear Freya,<br/>" + firstDayCurrentMonth.Month + @"月的情況，謝謝您！ <br/> 
            <table style='border-collapse: collapse; border=1 ; border:none; cellpadding=5';> 
                <tr> 
                    <td style='border: 1px solid black; padding:5px;'>掛帳單位</td> 
                    <td style='border: 1px solid black; text-align: center; padding:5px;'>
                        loading %
                    </td> 
                    <td style='border: 1px solid black; padding:5px; background-color:#ffe69d; font-weight: bolder;'>Profit Center</td> 
                    <td style='border: 1px solid black; padding:5px; background-color:#ffe69d; font-weight: bolder;'>Direct Against %</td> 
                    <td style='border: 1px solid black; padding:5px; background-color:#ffe69d; font-weight: bolder;'>Alloc</td> 
                    <td style='border: 1px solid black; padding:5px; background-color:#ffe69d; font-weight: bolder;'>Final Ratio</td> 
                    <td style='border: 1px solid black; padding:5px; background-color:#ffe69d; font-weight: bolder;'>Upload</td> 
                </tr>";
            var content = BoPCTList.GroupBy(fu => fu.Bo_Name)
                    .Select(g => new { ProfitCenter = g.FirstOrDefault().Profit_Center, Label = g.Key, Value = Math.Round((decimal)(decimal.Parse(g.Count().ToString()) * 100 / decimal.Parse(BoPCTList.Count().ToString())), 2, MidpointRounding.AwayFromZero) })
                    .ToList();
            foreach (var dbitem in content)
                BoNameList = BoNameList.Where(x => x.Bo_Name != dbitem.Label).ToList();
            foreach (var emptyBo in BoNameList)
                content.Add(new { ProfitCenter = emptyBo.Profit_Center, Label = emptyBo.Bo_Name, Value = decimal.Parse("0") });

            var WGILoading = (from WGIentity in content where WGIentity.Label == "WGI" select WGIentity).FirstOrDefault();
            var CORPLoading = (from WGIentity in content where WGIentity.Label == "CORP" select WGIentity).FirstOrDefault();
            var directAgainstTotal = 100 - WGILoading.Value - CORPLoading.Value;
            Dictionary<string, decimal>? keys = new Dictionary<string, decimal>();
            foreach (var item in content)
            {
                var upload = ((item.Label == "WGI" || item.Label == "CORP") ? "0.0000" : ((item.Value + Math.Round((WGILoading.Value + CORPLoading.Value) / directAgainstTotal * item.Value, 2, MidpointRounding.AwayFromZero)) / 100).ToString("0.0000"));
                if (keys.Count <= 0)
                    keys.Add(item.Label, decimal.Parse(upload));
                else
                {
                    if (keys.FirstOrDefault().Value < decimal.Parse(upload))
                    {
                        keys.Remove(keys.FirstOrDefault().Key);
                        keys.Add(item.Label, decimal.Parse(upload));
                    }
                }
            }
            foreach (var item in content)
            {
                var upload = ((item.Label == "WGI" || item.Label == "CORP") ? 0 : ((item.Value + Math.Round((WGILoading.Value + CORPLoading.Value) / directAgainstTotal * item.Value, 2, MidpointRounding.AwayFromZero)) / 100));
                if (item.Label != keys.FirstOrDefault().Key)
                {
                    maxUploadValue = maxUploadValue - upload;
                }
            }
            foreach (var item in content)
            {
                if (changeMailBackgroundColor)
                {
                    mailBody += "<tr style='background-color:#deecf7'>";
                    changeMailBackgroundColor = false;
                }
                else
                {
                    mailBody += "<tr>";
                    changeMailBackgroundColor = true;
                }
                var loading = item.Value;
                var directAgainst = ((item.Label == "WGI" || item.Label == "CORP") ? 0 : ((item.Value == 0) ? 0 : item.Value)).ToString("0.00");
                var alloc = ((item.Label == "WGI" || item.Label == "CORP") ? 0 : (Math.Round((WGILoading.Value + CORPLoading.Value) / directAgainstTotal * item.Value, 2, MidpointRounding.AwayFromZero)));
                var finalRatio = ((item.Label == "WGI" || item.Label == "CORP") ? 0 : (item.Value + Math.Round((WGILoading.Value + CORPLoading.Value) / directAgainstTotal * item.Value, 2, MidpointRounding.AwayFromZero)));
                var upload = (item.Label == keys.FirstOrDefault().Key) ? maxUploadValue : ((item.Label == "WGI" || item.Label == "CORP") ? 0 : ((item.Value + Math.Round((WGILoading.Value + CORPLoading.Value) / directAgainstTotal * item.Value, 2, MidpointRounding.AwayFromZero)) / 100));
                mailBody += "<td style='border: 1px solid black; padding:5px;'>" + item.Label + "</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + loading.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + item.ProfitCenter + "</td><td style='border: 1px solid black; padding:5px; text-align: center;'> " + directAgainst + "%</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + alloc.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + finalRatio.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + upload.ToString("0.0000") + "</td></tr> ";
                loadingTotal = loadingTotal + loading;
                allocTotal = allocTotal + alloc;
                finalRatioTotal = finalRatioTotal + finalRatio;
                uploadTotal = uploadTotal + upload;
            }
            mailBody += "<tr style='background-color:#a9cf8e'><td style='border: 1px solid black; padding:5px;'>Total:</td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + loadingTotal.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px;'></td><td style='border: 1px solid black; padding:5px; text-align: center;'>" + directAgainstTotal.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px;  text-align: center;'> " + allocTotal.ToString("0.00") + "%</td><td style='border: 1px solid black; text-align: center;'> " + finalRatioTotal.ToString("0.00") + "%</td><td style='border: 1px solid black; padding:5px;  text-align: center;'>" + uploadTotal.ToString("0.0000") + "</td></tr> ";
            mailBody += " </table> ";
            CommonSendmailEntity entity = new CommonSendmailEntity();
            entity.Mail_To = "Freya_Cheng@wistron.com";
            entity.Mail_Cc = "Mia_Jou@wistron.com";
            entity.Mail_From = SendmailHelper.DefaultMailFrom;
            entity.Subject = "當年度" + firstDayCurrentMonth.Month + "月盡職調查Kickoff各事業體%";
            entity.Mail_Body = mailBody.Replace("\n", "").Replace("\t", "").Replace("\r", "");
            entity.Create_Dt = DateTime.Now;
            _commonSendmailRepository.AddMail(entity);

            Log.Information("End to execute the job: {0}", this.GetType().FullName);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "");
        }

        return Task.CompletedTask;
    }
}