﻿namespace ScheduleJob.Jobs;

public class ClearDataJob : IJob
{
    private readonly IModificationLogRepository _modificationLogRepository = new ModificationLogRepository();

    public Task Execute(IJobExecutionContext context)
    {
        try
        {
            Log.Information("Begin to execute the job: {0}", this.GetType().FullName);

            _modificationLogRepository.DeleteData_OneYearAgo();

            Log.Information("End to execute the job: {0}", this.GetType().FullName);
        } catch (Exception ex)
        {
            Log.Error(ex, "");
        }

        return Task.CompletedTask;
    }
}
