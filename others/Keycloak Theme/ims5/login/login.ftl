<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Login to Investment Management System</title>
	<link href="${url.resourcesPath}/css/login.css" rel="stylesheet" />
</head>
<body>
	<div class="wrap">
		<div class="left">
			<form method="post" action="${url.loginAction}" onsubmit="return checkSubmit()">
				<div class="login-form">
					<div class="login-title">
						<img src="${url.resourcesPath}/img/login-logo.png" alt="Logo" class="login-logo" />
					</div>
					<div class="login-group-userid">
						<div class="login-label">使用者工號</div>
						<div class="login-input">
							<input type="text" id="username" name="username" autofocus autocomplete="off" />
						</div>
					</div>
					<div class="login-group-password">
						<div class="login-label">開機密碼</div>
						<div class="login-input">
							<input type="password" id="password" name="password" autocomplete="off" />
						</div>
					</div>
					<div class="login-msg">
						<img src="${url.resourcesPath}/img/feedback-error-sign.png" alt="Error" class="error-sign" />
						<span id="errorMsg">
							<#if message?has_content>
								${kcSanitize(message.summary)?no_esc}
							</#if>
						</span>
					</div>
					<div class="login-button">
						<input type="submit" value="Login" />
					</div>
				</div>
			</form>
		</div>
		<div class="right"></div>
	</div>

	<script type="text/javascript">
        let divLoginMsg = document.getElementsByClassName("login-msg")[0];
		let errorMsg = document.getElementById("errorMsg");
		let msg = errorMsg.innerText;

		if (msg != null) {
			msg = msg.replace(new RegExp("\\s", "g"), ""); //Replace all any invisible character, include \f \n \r \t \v
		}

		if (msg != null && msg != "") {
			divLoginMsg.style.display = "block";
		}

		function checkSubmit() {
			let username = document.getElementById("username").value;
			if (username == null || username == "") {
				errorMsg.innerText = "請輸入使用者工號";
				divLoginMsg.style.display = "block";
				return false;
			}

			let password = document.getElementById("password").value;
			if (password == null || password == "") {
				errorMsg.innerText = "請輸入開機密碼";
				divLoginMsg.style.display = "block";
				return false;
			}

			divLoginMsg.style.display = "none";

			return true;
		}
	</script>
</body>
</html>