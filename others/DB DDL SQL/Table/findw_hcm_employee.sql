CREATE TABLE findw_hcm_employee (
	emplid varchar(15) NOT NULL,
	company varchar(3) NULL,
	"name" varchar(50) NULL,
	name_a varchar(50) NULL,
	deptid varchar(6) NULL,
	descr varchar(50) NULL,
	supervisor_id varchar(11) NULL,
	termination_dt date NULL,
	email_address_a varchar(50) NULL,
	phone_a varchar(15) NULL,
	emilid varchar(70) NULL,
	seqn int4 NULL,
	updr varchar(256) NULL,
	updt timestamp NULL,
	CONSTRAINT findw_hcm_employee_pk PRIMARY KEY (emplid)
);
COMMENT ON TABLE findw_hcm_employee IS '公司員工檔';