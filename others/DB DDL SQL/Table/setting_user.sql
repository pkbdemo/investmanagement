CREATE TABLE setting_user (
	userid varchar(15) NOT NULL,
	approved bool NULL DEFAULT true,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	is_it bool NULL,
	CONSTRAINT setting_user_pkey PRIMARY KEY (userid)
);
COMMENT ON TABLE setting_user IS '使用者檔';