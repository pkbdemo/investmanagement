CREATE TABLE amount_record (
	security_no varchar(11) NOT NULL, -- 金額紀錄編號
	record_type varchar(3) NOT NULL, -- 金額記錄類型
	investment_date date NULL, -- 日期
	investment_subject varchar(3) NULL, -- 主體
	ori_currency varchar(5) NULL, -- 原幣別
	ori_amount numeric(18, 6) NULL, -- 原幣別-金額
	ori_cost numeric(18, 6) NULL, -- 原幣別-處分成本
	ori_stock_share numeric(18, 6) NULL, -- 原幣別-股數
	ori_stock_price numeric(18, 6) NULL, -- 原幣別-股價
	ori_stock_ratio numeric(7, 6) NULL, -- 原幣別-持股比例
	remark text NULL, -- 備註
	trans_currency varchar(5) NULL, -- 換算幣別
	trans_amount numeric(18, 6) NULL, -- 換算幣別-金額
	trans_cost numeric(18, 6) NULL, -- 換算幣別-處分成本
	trans_stock_price numeric(18, 6) NULL, -- 換算幣別-股價
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	amount_no serial4 NOT NULL,
	bo VARCHAR(3),
	CONSTRAINT amount_record_un UNIQUE (amount_no)
);
COMMENT ON TABLE amount_record IS '金額紀錄檔';

-- Column comments

COMMENT ON COLUMN amount_record.security_no IS '金額紀錄編號';
COMMENT ON COLUMN amount_record.record_type IS '金額記錄類型';
COMMENT ON COLUMN amount_record.investment_date IS '日期';
COMMENT ON COLUMN amount_record.investment_subject IS '主體';
COMMENT ON COLUMN amount_record.ori_currency IS '原幣別';
COMMENT ON COLUMN amount_record.ori_amount IS '原幣別-金額';
COMMENT ON COLUMN amount_record.ori_cost IS '原幣別-處分成本';
COMMENT ON COLUMN amount_record.ori_stock_share IS '原幣別-股數';
COMMENT ON COLUMN amount_record.ori_stock_price IS '原幣別-股價';
COMMENT ON COLUMN amount_record.ori_stock_ratio IS '原幣別-持股比例';
COMMENT ON COLUMN amount_record.remark IS '備註';
COMMENT ON COLUMN amount_record.trans_currency IS '換算幣別';
COMMENT ON COLUMN amount_record.trans_amount IS '換算幣別-金額';
COMMENT ON COLUMN amount_record.trans_cost IS '換算幣別-處分成本';
COMMENT ON COLUMN amount_record.trans_stock_price IS '換算幣別-股價';
COMMENT ON COLUMN amount_record.create_user IS '建立人員';
COMMENT ON COLUMN amount_record.create_date IS '建立時間';
COMMENT ON COLUMN amount_record.update_user IS '更新人員';
COMMENT ON COLUMN amount_record.update_date IS '更新時間';
COMMENT ON COLUMN amount_record.bo IS '事業體';