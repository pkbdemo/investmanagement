CREATE TABLE fund_status (
	invest_no int4 NOT NULL,
	status varchar(3) NOT NULL,
	status_date date NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT fk_fund_main_to_fund_status FOREIGN KEY (invest_no) REFERENCES fund_main(invest_no)
);
COMMENT ON TABLE fund_status IS '基金狀態檔';