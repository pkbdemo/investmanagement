CREATE TABLE common_sendmail (
	id serial4 primary key,
	mail_from varchar(100) not null,
	mail_to text not null,
	mail_cc text,
	mail_bcc text,
	subject varchar(2000),
	mail_body text not null,
	create_dt timestamp not null,
	process_dt timestamp,
	error_msg text,
	attachment bytea,
	file_name varchar(1000),
	file_size int4
);