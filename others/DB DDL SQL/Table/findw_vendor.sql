CREATE TABLE findw_vendor (
	lifnr varchar(10) NOT NULL,
	name1 varchar(35) NULL,
	seqn int4 NULL,
	updr varchar(256) NULL,
	updt timestamp NULL,
	CONSTRAINT findw_vendor_pkey PRIMARY KEY (lifnr)
);