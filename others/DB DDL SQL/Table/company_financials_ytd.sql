CREATE TABLE company_financials_ytd (
	financials_no int4 NOT NULL DEFAULT nextval('company_financials_financials_no_seq'::regclass), -- 公司財報編號
	invest_no int4 NOT NULL, -- 投資編號
	report_end_date date NOT NULL, -- 被投資公司的財報期末
	quarter varchar(3) NOT NULL, -- 季度
	report_start_date date NOT NULL, -- 被投資公司的財報期初
	financial_year int2 NOT NULL, -- 財年
	currency varchar(5) NULL, -- 幣別
	financial_statement varchar(3) NULL, -- 個體或合併
	unit numeric(12) NULL, -- 金額單位
	concerns text NULL, -- 關注事項
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	next_report_start_date date NOT NULL, -- 被投資公司的下季財報期初
	next_report_end_date date NOT NULL, -- 被投資公司的下季財報期末
	CONSTRAINT company_financials_ytd_pkey PRIMARY KEY (financials_no),
	CONSTRAINT uq_company_financials_ytd_yq UNIQUE (invest_no, financial_year, quarter),
	CONSTRAINT fk_company_main_to_company_financials_ytd FOREIGN KEY (invest_no) REFERENCES company_main(invest_no)
);
COMMENT ON TABLE company_financials_ytd IS '公司財報檔-財報類型YTD';

-- Column comments

COMMENT ON COLUMN company_financials_ytd.financials_no IS '公司財報編號';
COMMENT ON COLUMN company_financials_ytd.invest_no IS '投資編號';
COMMENT ON COLUMN company_financials_ytd.report_end_date IS '被投資公司的財報期末';
COMMENT ON COLUMN company_financials_ytd.quarter IS '季度';
COMMENT ON COLUMN company_financials_ytd.report_start_date IS '被投資公司的財報期初';
COMMENT ON COLUMN company_financials_ytd.financial_year IS '財年';
COMMENT ON COLUMN company_financials_ytd.currency IS '幣別';
COMMENT ON COLUMN company_financials_ytd.financial_statement IS '個體或合併';
COMMENT ON COLUMN company_financials_ytd.unit IS '金額單位';
COMMENT ON COLUMN company_financials_ytd.concerns IS '關注事項';
COMMENT ON COLUMN company_financials_ytd.create_user IS '建立人員';
COMMENT ON COLUMN company_financials_ytd.create_date IS '建立時間';
COMMENT ON COLUMN company_financials_ytd.update_user IS '更新人員';
COMMENT ON COLUMN company_financials_ytd.update_date IS '更新時間';
COMMENT ON COLUMN company_financials_ytd.next_report_start_date IS '被投資公司的下季財報期初';
COMMENT ON COLUMN company_financials_ytd.next_report_end_date IS '被投資公司的下季財報期末';