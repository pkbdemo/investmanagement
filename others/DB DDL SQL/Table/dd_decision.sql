CREATE TABLE dd_decision (
	dd_no int4 NOT NULL,
	decision_date date NOT NULL,
	decision_result bool NOT NULL,
	decision_comment text NOT NULL,
	attach_batch int4 NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT dd_decision_dd_no_key UNIQUE (dd_no),
	CONSTRAINT fk_dd_main_to_dd_decision FOREIGN KEY (dd_no) REFERENCES dd_main(dd_no)
);
COMMENT ON TABLE dd_decision IS '盡責調查決議明細檔';