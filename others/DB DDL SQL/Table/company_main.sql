CREATE TABLE company_main (
	invest_no int4 NOT NULL, -- 投資編號
	nickname varchar(80) NULL, -- 公司簡稱
	suggest_status varchar(3) NULL, -- 建議Status
	industry_catagory varchar(3) NULL, -- 行業別-大類
	industry_detail varchar(50) NULL, -- 行業別-細項
	company_stage varchar(3) NULL, -- 投資時公司所處階段
	main_business varchar(200) NULL, -- 主營業務
	headquarter varchar(15) NULL, -- 總部
	stock_symbol varchar(50) NULL, -- 上市/掛牌代碼
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	status_change bool NULL DEFAULT false, -- 顯示綠點
	subject varchar(200) NULL, -- 交易主體
	fin_year_rule varchar(3) NULL, -- 公司財年制度
	stock_market varchar(3) NULL, -- 股票市場種類
	CONSTRAINT company_main_invest_no_key UNIQUE (invest_no),
	CONSTRAINT company_main_nickname_key UNIQUE (nickname),
	CONSTRAINT company_main_stock_symbol_key UNIQUE (stock_symbol),
	CONSTRAINT fk_invest_main_to_company_main FOREIGN KEY (invest_no) REFERENCES invest_main(invest_no)
);
COMMENT ON TABLE company_main IS '公司主檔';

-- Column comments

COMMENT ON COLUMN company_main.invest_no IS '投資編號';
COMMENT ON COLUMN company_main.nickname IS '公司簡稱';
COMMENT ON COLUMN company_main.suggest_status IS '建議Status';
COMMENT ON COLUMN company_main.industry_catagory IS '行業別-大類';
COMMENT ON COLUMN company_main.industry_detail IS '行業別-細項';
COMMENT ON COLUMN company_main.company_stage IS '投資時公司所處階段';
COMMENT ON COLUMN company_main.main_business IS '主營業務';
COMMENT ON COLUMN company_main.headquarter IS '總部';
COMMENT ON COLUMN company_main.stock_symbol IS '上市/掛牌代碼';
COMMENT ON COLUMN company_main.create_user IS '建立人員';
COMMENT ON COLUMN company_main.create_date IS '建立時間';
COMMENT ON COLUMN company_main.update_user IS '更新人員';
COMMENT ON COLUMN company_main.update_date IS '更新時間';
COMMENT ON COLUMN company_main.status_change IS '顯示綠點';
COMMENT ON COLUMN company_main.subject IS '交易主體';
COMMENT ON COLUMN company_main.fin_year_rule IS '公司財年制度';
COMMENT ON COLUMN company_main.stock_market IS '股票市場種類';