CREATE TABLE setting_history_rate (
	currency_from varchar(5) NOT NULL,
	currency_to varchar(5) NOT NULL,
	"period" varchar(8) NOT NULL,
	exchange_rate numeric(38, 5) NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT pk_history_rate PRIMARY KEY (currency_from, currency_to, period)
);
COMMENT ON TABLE setting_history_rate IS '歷史匯率檔(專門放SAP沒有的歷史匯率資料)由央行取得';