CREATE TABLE invest_main (
	invest_no serial4 NOT NULL,
	"name" varchar(200) NOT NULL,
	category varchar(10) NULL,
	contact_ao varchar(50) NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT invest_main_pkey PRIMARY KEY (invest_no),
	CONSTRAINT uq_invest_main_name UNIQUE (name)
);
COMMENT ON TABLE invest_main IS '投資主檔';