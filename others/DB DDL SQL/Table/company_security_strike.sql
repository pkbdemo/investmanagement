CREATE TABLE company_security_strike (
	security_no varchar(11) NOT NULL,
	due_date date NOT NULL,
	strike_price numeric(13, 6) NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	strike_stock_share numeric(14, 2) NULL, -- 履約股數
	CONSTRAINT fk_company_security_to_strike FOREIGN KEY (security_no) REFERENCES company_security(security_no)
);
COMMENT ON TABLE company_security_strike IS '證券履約資料檔';

-- Column comments

COMMENT ON COLUMN company_security_strike.strike_stock_share IS '履約股數';