CREATE TABLE common_file (
	file_id serial4 NOT NULL, -- 系統檔案編號
	attach_batch int4 NOT NULL, -- 批次編號
	file_name varchar(800) NOT NULL, -- 檔案名稱
	file_path varchar(800) NOT NULL, -- 檔案路徑
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	file_size numeric NULL, -- 檔案大小
	CONSTRAINT common_file_pkey PRIMARY KEY (file_id)
);
COMMENT ON TABLE common_file IS '系統檔案檔';

-- Column comments

COMMENT ON COLUMN common_file.file_id IS '系統檔案編號';
COMMENT ON COLUMN common_file.attach_batch IS '批次編號';
COMMENT ON COLUMN common_file.file_name IS '檔案名稱';
COMMENT ON COLUMN common_file.file_path IS '檔案路徑';
COMMENT ON COLUMN common_file.create_user IS '建立人員';
COMMENT ON COLUMN common_file.create_date IS '建立時間';
COMMENT ON COLUMN common_file.update_user IS '更新人員';
COMMENT ON COLUMN common_file.update_date IS '更新時間';
COMMENT ON COLUMN common_file.file_size IS '檔案大小';