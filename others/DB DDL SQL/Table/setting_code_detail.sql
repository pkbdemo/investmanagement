CREATE TABLE setting_code_detail (
	kind_id varchar(3) NOT NULL,
	code_id varchar(3) NOT NULL,
	code_name varchar(80) NULL,
	code_description varchar(100) NULL,
	code_extend_a varchar(50) NULL,
	code_extend_b varchar(50) NULL,
	code_extend_c varchar(50) NULL,
	code_sort int2 NULL,
	enabled bool NULL DEFAULT true,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT uq_code_detail_kind_code UNIQUE (kind_id, code_id),
	CONSTRAINT fk_code_master_to_code_detail FOREIGN KEY (kind_id) REFERENCES setting_code_master(kind_id)
);
COMMENT ON TABLE setting_code_detail IS '代碼明細檔';

-- Column comments

COMMENT ON COLUMN setting_code_detail.code_extend_c IS '外部系統串接名稱';