CREATE TABLE setting_account (
	account_id serial4 NOT NULL,
	account_type varchar(80) NULL,
	account_name varchar(80) NULL,
	required bool NULL DEFAULT true,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT setting_account_pkey PRIMARY KEY (account_id)
);
COMMENT ON TABLE setting_account IS '科目檔';