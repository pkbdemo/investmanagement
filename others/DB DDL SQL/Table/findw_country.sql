CREATE TABLE findw_country (
	land1 varchar(3) NOT NULL,
	landx varchar(15) NULL,
	seqn int4 NULL,
	updr varchar(256) NULL,
	updt timestamp NULL,
	CONSTRAINT findw_country_pkey PRIMARY KEY (land1)
);
COMMENT ON TABLE findw_country IS '國家主檔';