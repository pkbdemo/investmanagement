CREATE TABLE company_status (
	invest_no int4 NOT NULL,
	status varchar(3),
	start_date date NOT NULL,
	end_date date NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT fk_company_main_to_company_status FOREIGN KEY (invest_no) REFERENCES company_main(invest_no)
);
COMMENT ON TABLE company_status IS '公司狀態檔';