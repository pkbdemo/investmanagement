CREATE TABLE fund_security (
	security_no varchar(11) NOT NULL, -- 基金每次認購編號
	invest_no int4 NOT NULL, -- 投資編號
	effective_date date NOT NULL, -- 合約生效日
	dd_close_date varchar(8) NOT NULL, -- 盡職調查完成日
	commited_currency varchar(5) NULL, -- 基金認購幣別
	commited_amount numeric(18, 6) NULL, -- 基金認購金額
	remark text NULL, -- 備註
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	CONSTRAINT fund_security_pkey PRIMARY KEY (security_no),
	CONSTRAINT uq_fund_security_no_date UNIQUE (invest_no, effective_date),
	CONSTRAINT fk_fund_main_to_fund_security FOREIGN KEY (invest_no) REFERENCES fund_main(invest_no)
);
COMMENT ON TABLE fund_security IS '基金單次認購檔';

-- Column comments

COMMENT ON COLUMN fund_security.security_no IS '基金每次認購編號';
COMMENT ON COLUMN fund_security.invest_no IS '投資編號';
COMMENT ON COLUMN fund_security.effective_date IS '合約生效日';
COMMENT ON COLUMN fund_security.dd_close_date IS '盡職調查完成日';
COMMENT ON COLUMN fund_security.commited_currency IS '基金認購幣別';
COMMENT ON COLUMN fund_security.commited_amount IS '基金認購金額';
COMMENT ON COLUMN fund_security.remark IS '備註';
COMMENT ON COLUMN fund_security.create_user IS '建立人員';
COMMENT ON COLUMN fund_security.create_date IS '建立時間';
COMMENT ON COLUMN fund_security.update_user IS '更新人員';
COMMENT ON COLUMN fund_security.update_date IS '更新時間';