CREATE TABLE dd_stage (
	dd_stage_no serial PRIMARY KEY,
	dd_no integer NOT NULL,
	status VARCHAR(3) NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	remark text,
	attach_batch integer,
	create_user VARCHAR(50),
    create_date TIMESTAMP,
	update_user VARCHAR(50),
	update_date TIMESTAMP,
	CONSTRAINT uq_dd_stage_no_status UNIQUE (dd_no, status),
	CONSTRAINT fk_dd_main_to_dd_stage FOREIGN KEY (dd_no) REFERENCES dd_main(dd_no)
);

COMMENT ON TABLE dd_stage IS '盡職調查調查階段檔';