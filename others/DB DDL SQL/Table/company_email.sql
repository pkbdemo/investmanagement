CREATE TABLE company_email (
	invest_no int4 NOT NULL,
	email varchar(100) NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT fk_company_main_to_company_email FOREIGN KEY (invest_no) REFERENCES company_main(invest_no)
);
COMMENT ON TABLE company_email IS '被投資公司窗口Email檔';