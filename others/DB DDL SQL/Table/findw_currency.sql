CREATE TABLE findw_currency (
	kurst varchar(4) NULL,
	fcurr varchar(5) NULL,
	tcurr varchar(5) NULL,
	"period" date NULL,
	exchange_rate numeric(38, 5) NULL,
	seqn numeric NULL,
	updr varchar(256) NULL,
	updt timestamp NULL,
	gdatu varchar(8) NOT NULL
);
CREATE UNIQUE INDEX findw_currency_pk ON findw_currency USING btree (kurst, fcurr, tcurr, period, gdatu);
COMMENT ON TABLE findw_currency IS '匯率檔';