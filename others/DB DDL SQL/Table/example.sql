CREATE TABLE example (
	user_id serial4 NOT NULL,
	"name" varchar NOT NULL,
	age int4 NULL,
	createby varchar NULL,
	create_date timestamp NULL,
	modifyby varchar NULL,
	modify_date timestamp NULL,
	CONSTRAINT example_pk PRIMARY KEY (user_id)
);