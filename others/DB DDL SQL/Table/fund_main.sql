CREATE TABLE fund_main (
	invest_no int4 NOT NULL, -- 投資編號
	full_name varchar(200) NULL, -- 基金全稱
	"type" varchar(3) NOT NULL, -- 基金種類
	liability_type varchar(3) NULL, -- 設立形式
	headquarter varchar(15) NULL, -- 總部
	initial_holding_ratio numeric(5, 4) NULL, -- 初始持有%
	fund_period numeric(2) NULL, -- 基金年限
	investment_period numeric(2) NULL, -- 投資期年限
	harvest_period numeric(2) NULL, -- 回收期年限
	fin_year_rule varchar(3) NULL, -- 公司財年制度
	first_trans_date date NULL, -- 初次投資日
	quit_date date NULL, -- 退出日期
	quit_reason text NULL, -- 退出原因
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	CONSTRAINT fund_main_full_name_key UNIQUE (full_name),
	CONSTRAINT fund_main_invest_no_key UNIQUE (invest_no),
	CONSTRAINT fk_invest_main_to_fund_main FOREIGN KEY (invest_no) REFERENCES invest_main(invest_no)
);
COMMENT ON TABLE fund_main IS '基金主檔';

-- Column comments

COMMENT ON COLUMN fund_main.invest_no IS '投資編號';
COMMENT ON COLUMN fund_main.full_name IS '基金全稱';
COMMENT ON COLUMN fund_main."type" IS '基金種類';
COMMENT ON COLUMN fund_main.liability_type IS '設立形式';
COMMENT ON COLUMN fund_main.headquarter IS '總部';
COMMENT ON COLUMN fund_main.initial_holding_ratio IS '初始持有%';
COMMENT ON COLUMN fund_main.fund_period IS '基金年限';
COMMENT ON COLUMN fund_main.investment_period IS '投資期年限';
COMMENT ON COLUMN fund_main.harvest_period IS '回收期年限';
COMMENT ON COLUMN fund_main.fin_year_rule IS '公司財年制度';
COMMENT ON COLUMN fund_main.first_trans_date IS '初次投資日';
COMMENT ON COLUMN fund_main.quit_date IS '退出日期';
COMMENT ON COLUMN fund_main.quit_reason IS '退出原因';
COMMENT ON COLUMN fund_main.create_user IS '建立人員';
COMMENT ON COLUMN fund_main.create_date IS '建立時間';
COMMENT ON COLUMN fund_main.update_user IS '更新人員';
COMMENT ON COLUMN fund_main.update_date IS '更新時間';