CREATE TABLE fund_financials (
	financials_no serial4 NOT NULL, -- 基金淨值編號
	invest_no int4 NOT NULL, -- 投資編號
	financial_year int2 NOT NULL, -- 財年
	quarter varchar(3) NOT NULL, -- 季度
	report_start_date date NOT NULL, -- 被投資公司的財報期初
	report_end_date date NOT NULL, -- 被投資公司的財報期末
	currency varchar(5) NULL, -- 幣別
	amount numeric(18, 6) NULL, -- 基金淨值
	concerns text NULL, -- 關注事項
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	CONSTRAINT fund_financials_pkey PRIMARY KEY (financials_no),
	CONSTRAINT uq_fund_financials_yq UNIQUE (invest_no, financial_year, quarter),
	CONSTRAINT fk_fund_main_to_fund_financials FOREIGN KEY (invest_no) REFERENCES fund_main(invest_no)
);
COMMENT ON TABLE fund_financials IS '基金淨值檔';

-- Column comments

COMMENT ON COLUMN fund_financials.financials_no IS '基金淨值編號';
COMMENT ON COLUMN fund_financials.invest_no IS '投資編號';
COMMENT ON COLUMN fund_financials.financial_year IS '財年';
COMMENT ON COLUMN fund_financials.quarter IS '季度';
COMMENT ON COLUMN fund_financials.report_start_date IS '被投資公司的財報期初';
COMMENT ON COLUMN fund_financials.report_end_date IS '被投資公司的財報期末';
COMMENT ON COLUMN fund_financials.currency IS '幣別';
COMMENT ON COLUMN fund_financials.amount IS '基金淨值';
COMMENT ON COLUMN fund_financials.concerns IS '關注事項';
COMMENT ON COLUMN fund_financials.create_user IS '建立人員';
COMMENT ON COLUMN fund_financials.create_date IS '建立時間';
COMMENT ON COLUMN fund_financials.update_user IS '更新人員';
COMMENT ON COLUMN fund_financials.update_date IS '更新時間';