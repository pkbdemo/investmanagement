CREATE TABLE dd_type (
	dd_no int4 NOT NULL,
	"type" varchar(3) NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT uq_dd_type_no_type UNIQUE (dd_no, type),
	CONSTRAINT fk_dd_main_to_dd_type FOREIGN KEY (dd_no) REFERENCES dd_main(dd_no)
);
COMMENT ON TABLE dd_type IS '盡責調查類型檔';