CREATE TABLE setting_code_master (
	kind_id varchar(3) NOT NULL,
	kind_name varchar(50) NULL,
	kind_description varchar(100) NULL,
	enabled bool NULL DEFAULT true,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT setting_code_master_pkey PRIMARY KEY (kind_id)
);
COMMENT ON TABLE setting_code_master IS '代碼主檔';