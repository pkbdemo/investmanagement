CREATE TABLE company_stock_price (
	stock_price_no serial4 NOT NULL, -- 參考股價編號
	invest_no int4 NOT NULL, -- 投資編號
	transaction_date date NOT NULL, -- 交易日期
	public_actual_currency varchar(5) NULL, -- 一般股價-實際幣別
	public_actual_price numeric(8, 2) NULL, -- 一般股價-實際股價
	private_actual_currency varchar(5) NULL, -- 私募股價-實際幣別
	private_actual_price numeric(8, 2) NULL, -- 私募股價-實際股價
	discount numeric(5, 4) NULL, -- 私/上市(%)
	net_worth_currency varchar(5) NULL, -- 淨值幣別
	net_worth_price numeric(8, 2) NULL, -- 淨值
	history_currency varchar(5) NULL, -- 歷史幣別
	history_price numeric(8, 2) NULL, -- 歷史股價
	limit_currency varchar(5) NULL, -- 指定股價幣別
	limit_price numeric(8, 2) NULL, -- 指定股價
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	CONSTRAINT company_stock_price_pkey PRIMARY KEY (stock_price_no),
	CONSTRAINT uq_company_stock_price_dt_name UNIQUE (invest_no, transaction_date),
	CONSTRAINT fk_company_main_to_company_stock_price FOREIGN KEY (invest_no) REFERENCES company_main(invest_no)
);
COMMENT ON TABLE company_stock_price IS '公司參考股價檔';

-- Column comments

COMMENT ON COLUMN company_stock_price.stock_price_no IS '參考股價編號';
COMMENT ON COLUMN company_stock_price.invest_no IS '投資編號';
COMMENT ON COLUMN company_stock_price.transaction_date IS '交易日期';
COMMENT ON COLUMN company_stock_price.public_actual_currency IS '一般股價-實際幣別';
COMMENT ON COLUMN company_stock_price.public_actual_price IS '一般股價-實際股價';
COMMENT ON COLUMN company_stock_price.private_actual_currency IS '私募股價-實際幣別';
COMMENT ON COLUMN company_stock_price.private_actual_price IS '私募股價-實際股價';
COMMENT ON COLUMN company_stock_price.discount IS '私/上市(%)';
COMMENT ON COLUMN company_stock_price.net_worth_currency IS '淨值幣別';
COMMENT ON COLUMN company_stock_price.net_worth_price IS '淨值';
COMMENT ON COLUMN company_stock_price.history_currency IS '歷史幣別';
COMMENT ON COLUMN company_stock_price.history_price IS '歷史股價';
COMMENT ON COLUMN company_stock_price.limit_currency IS '指定股價幣別';
COMMENT ON COLUMN company_stock_price.limit_price IS '指定股價';
COMMENT ON COLUMN company_stock_price.create_user IS '建立人員';
COMMENT ON COLUMN company_stock_price.create_date IS '建立時間';
COMMENT ON COLUMN company_stock_price.update_user IS '更新人員';
COMMENT ON COLUMN company_stock_price.update_date IS '更新時間';