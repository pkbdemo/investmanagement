CREATE TABLE company_financials_detail (
	financials_no int4 NOT NULL,
	account_id int4 NOT NULL,
	amount numeric(18, 6) NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT uq_company_financials_detail_account UNIQUE (financials_no, account_id),
	CONSTRAINT fk_account_to_financials_detail FOREIGN KEY (account_id) REFERENCES setting_account(account_id)
);
COMMENT ON TABLE company_financials_detail IS '公司財報明細檔';