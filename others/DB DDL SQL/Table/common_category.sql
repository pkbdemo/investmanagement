CREATE TABLE common_category (
	invest_no int4 NOT NULL,
	item_name varchar(50) NOT NULL,
	item_value varchar(200) NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT uq_common_category_key_value UNIQUE (invest_no, item_name, item_value)
);
CREATE INDEX idx_common_category_key ON common_category USING btree (invest_no, item_name);
COMMENT ON TABLE common_category IS '系統類別檔';