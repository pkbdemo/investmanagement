CREATE TABLE calculate_suggeststatus_log (
	id serial4 NOT NULL,
	calculate_no varchar(20) NOT NULL,
	log_content varchar NOT NULL,
	track_data_name varchar NULL,
	track_data varchar NULL,
	create_user varchar(50) NOT NULL,
	create_date timestamp NOT null,
	CONSTRAINT calculate_suggeststatus_log_pk PRIMARY KEY (id)
);

COMMENT ON TABLE calculate_suggeststatus_log IS '取得建議Status 計算過程log';
COMMENT ON COLUMN calculate_suggeststatus_log.calculate_no IS '計算過程編號';
COMMENT ON COLUMN calculate_suggeststatus_log.log_content IS '計算過程紀錄或每個stage起訖宣告';
COMMENT ON COLUMN calculate_suggeststatus_log.track_data_name IS '需追蹤資料的名稱';
COMMENT ON COLUMN calculate_suggeststatus_log.track_data IS '需追蹤資料';
        