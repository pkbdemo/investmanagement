CREATE TABLE company_security (
	security_no varchar(11) NOT NULL, -- 公司證券類型編號
	invest_no int4 NOT NULL, -- 投資編號
	"type" varchar(3) NOT NULL, -- 證券種類
	stock_type varchar(3) NULL, -- 股票形式
	quit_date date NULL, -- 退出日期
	quit_reason text NULL, -- 退出原因
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	remark text NULL, -- 備註
	dd_close_date varchar(8) NOT NULL, -- 盡職調查完成日
	first_trans_date date NULL, -- 初次投資日
	CONSTRAINT company_security_pkey PRIMARY KEY (security_no),
	CONSTRAINT uq_company_security_no_type_date UNIQUE (invest_no, type, dd_close_date),
	CONSTRAINT fk_company_main_to_company_security FOREIGN KEY (invest_no) REFERENCES company_main(invest_no)
);
COMMENT ON TABLE company_security IS '公司證券類型檔';

-- Column comments

COMMENT ON COLUMN company_security.security_no IS '公司證券類型編號';
COMMENT ON COLUMN company_security.invest_no IS '投資編號';
COMMENT ON COLUMN company_security."type" IS '證券種類';
COMMENT ON COLUMN company_security.stock_type IS '股票形式';
COMMENT ON COLUMN company_security.quit_date IS '退出日期';
COMMENT ON COLUMN company_security.quit_reason IS '退出原因';
COMMENT ON COLUMN company_security.create_user IS '建立人員';
COMMENT ON COLUMN company_security.create_date IS '建立時間';
COMMENT ON COLUMN company_security.update_user IS '更新人員';
COMMENT ON COLUMN company_security.update_date IS '更新時間';
COMMENT ON COLUMN company_security.remark IS '備註';
COMMENT ON COLUMN company_security.dd_close_date IS '盡職調查完成日';
COMMENT ON COLUMN company_security.first_trans_date IS '初次投資日';