CREATE TABLE amount_fmv_percentage (
	invest_no int4 NOT NULL, -- 投資編號
	"year" int2 NOT NULL, -- 年份
	"month" int2 NOT NULL, -- 月份
	fmv_type varchar(3) NOT NULL, -- 帳面價值 / 持股比例
	investment_subject varchar(3) NOT NULL, -- 主體
	currency varchar(5) NULL, -- 帳面價值-幣別
	book_value numeric(12) NULL, -- 帳面價值-金額
	shareholding_ratio numeric(5, 4) NULL, -- 持股比例-比例(%)
	remark text NULL, -- 備註
	create_user varchar(50) NULL, -- 建立人員
	create_date timestamp NULL, -- 建立時間
	update_user varchar(50) NULL, -- 更新人員
	update_date timestamp NULL, -- 更新時間
	CONSTRAINT uq_amount_fmv_percentage UNIQUE (invest_no, year, month, fmv_type, investment_subject),
	CONSTRAINT fk_invest_main_to_fmv FOREIGN KEY (invest_no) REFERENCES invest_main(invest_no)
);
COMMENT ON TABLE amount_fmv_percentage IS '帳面價值/持股比例檔';

-- Column comments

COMMENT ON COLUMN amount_fmv_percentage.invest_no IS '投資編號';
COMMENT ON COLUMN amount_fmv_percentage."year" IS '年份';
COMMENT ON COLUMN amount_fmv_percentage."month" IS '月份';
COMMENT ON COLUMN amount_fmv_percentage.fmv_type IS '帳面價值 / 持股比例';
COMMENT ON COLUMN amount_fmv_percentage.investment_subject IS '主體';
COMMENT ON COLUMN amount_fmv_percentage.currency IS '帳面價值-幣別';
COMMENT ON COLUMN amount_fmv_percentage.book_value IS '帳面價值-金額';
COMMENT ON COLUMN amount_fmv_percentage.shareholding_ratio IS '持股比例-比例(%)';
COMMENT ON COLUMN amount_fmv_percentage.remark IS '備註';
COMMENT ON COLUMN amount_fmv_percentage.create_user IS '建立人員';
COMMENT ON COLUMN amount_fmv_percentage.create_date IS '建立時間';
COMMENT ON COLUMN amount_fmv_percentage.update_user IS '更新人員';
COMMENT ON COLUMN amount_fmv_percentage.update_date IS '更新時間';