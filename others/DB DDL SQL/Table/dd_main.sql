CREATE TABLE dd_main (
	dd_no serial4 NOT NULL,
	build_date date NOT NULL,
	invest_no int4 NOT NULL,
	dept_no varchar(6) NULL,
	invest_date date NULL,
	currency varchar(5) NULL,
	invest_amount numeric(18, 6) NULL,
	frozen_type varchar(3) NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	bo varchar(3) NULL,
	memo text,
	CONSTRAINT dd_main_pkey PRIMARY KEY (dd_no),
	CONSTRAINT uq_dd_main_dt_name UNIQUE (build_date, invest_no),
	CONSTRAINT fk_invest_main_to_dd_main FOREIGN KEY (invest_no) REFERENCES invest_main(invest_no)
);
COMMENT ON TABLE dd_main IS '盡責調查主檔';

-- Column comments

COMMENT ON COLUMN dd_main.dd_no IS '盡責調查編號';
COMMENT ON COLUMN dd_main.build_date IS '建立日期';
COMMENT ON COLUMN dd_main.invest_no IS '投資編號';
COMMENT ON COLUMN dd_main.dept_no IS '掛帳單位';
COMMENT ON COLUMN dd_main.invest_date IS '投資款時間';
COMMENT ON COLUMN dd_main.currency IS '幣別';
COMMENT ON COLUMN dd_main.invest_amount IS '投資金額';
COMMENT ON COLUMN dd_main.frozen_type IS '凍結狀態';
COMMENT ON COLUMN dd_main.create_user IS '建立人員';
COMMENT ON COLUMN dd_main.create_date IS '建立時間';
COMMENT ON COLUMN dd_main.update_user IS '更新人員';
COMMENT ON COLUMN dd_main.update_date IS '更新時間';
COMMENT ON COLUMN dd_main.memo IS 'MEMO';