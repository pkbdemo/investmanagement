CREATE TABLE modification_log (
	id serial4 NOT NULL,
	update_table varchar(50) NOT NULL,
	operation varchar(50) NOT NULL,
	update_user varchar(50) NOT NULL,
	update_date timestamp NOT NULL,
	old_data varchar NULL,
	new_data varchar NULL,
	CONSTRAINT modification_log_pk PRIMARY KEY (id)
);
COMMENT ON TABLE modification_log IS '修改記錄';