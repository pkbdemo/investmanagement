CREATE TABLE findw_hcm_organization (
	deptid varchar(6) NOT NULL,
	descr varchar(50) NULL,
	manager_id varchar(11) NULL,
	seqn int4 NULL,
	updr varchar(256) NULL,
	updt timestamp NULL,
	CONSTRAINT findw_hcm_organization_pk PRIMARY KEY (deptid)
);
COMMENT ON TABLE findw_hcm_organization IS '公司部門檔';