CREATE TABLE common_finance_mapping (
	mapping_no serial4 NOT NULL,
	invest_no int4 NOT NULL,
	fin_sys_name varchar(300) NOT NULL,
	account varchar(300) NOT NULL,
	create_user varchar(50) NULL,
	create_date timestamp NULL,
	update_user varchar(50) NULL,
	update_date timestamp NULL,
	CONSTRAINT common_finance_mapping_pkey PRIMARY KEY (mapping_no),
	CONSTRAINT uq_common_finance_mapping UNIQUE (invest_no, fin_sys_name, account),
	CONSTRAINT fk_invest_main_to_common_finance_mapping FOREIGN KEY (invest_no) REFERENCES invest_main(invest_no)
);
COMMENT ON TABLE common_finance_mapping IS '公司財務對應檔';