CREATE OR REPLACE FUNCTION searchcodename(i_kind_id character varying, i_code_id character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/* ***************************************************
  ' 函數名稱 : searchcodename
  ' 功能說明 : 通过Kind_id Code_id 取得setting_code_detail DB 的CodeName资料
  ' 參數說明 :
              i_kind_id character
              i_code_id character

  ' 版本變更：
  '  xx. YYYY/MM/DD   VER     AUTHOR          COMMENTS
  '  1.  2022/05/11   1.0     Rick      		CREATE
  ********************************************************/
declare
Code_name varchar(50);
begin
	select scd.code_name into Code_name
	from setting_code_master scm inner join setting_code_detail scd
	on scm.kind_id = scd.kind_id
	where  scm.kind_id = i_kind_id  and scd.code_id = i_code_id ;
return Code_name;
end;
$function$
;
