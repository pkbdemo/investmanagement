CREATE OR REPLACE FUNCTION currentfundstatus(i_invest_no integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/* ***************************************************
  ' 函數名稱 : currentfundstatus
  ' 功能說明 : 通過i_invest_no 取得當前投資階段
  ' 參數說明 :
              i_invest_no character
  ' 版本變更：
  '  xx. YYYY/MM/DD   VER     AUTHOR          COMMENTS
  '  1.  2022/08/18   1.0     Wilson      		CREATE
  ********************************************************/
declare
result_status character varying;
begin
	select status into result_status
	from fund_status fst
	where fst.invest_no = i_invest_no
	order by fst.status_date desc
	limit 1;
return result_status;
end;
$function$
;
