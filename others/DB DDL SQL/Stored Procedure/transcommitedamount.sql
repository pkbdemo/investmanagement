CREATE OR REPLACE FUNCTION transcommitedamount(i_ori_currency character varying, i_new_currency character varying, i_effective_date character varying, i_ori_amount numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
/* ***************************************************
  ' 函數名稱 : transcommitedamount
  ' 功能說明 : 通過i_ori_currency i_new_currency i_effective_date 取得轉換過的幣別金額资料
  ' 參數說明 :
              i_ori_currency character
              i_new_currency character
			  i_effective_date character
  ' 版本變更：
  '  xx. YYYY/MM/DD   VER     AUTHOR          COMMENTS
  '  1.  2022/08/18   1.0     Wilson      		CREATE
  ********************************************************/
declare
rate numeric;
New_amount numeric;
begin
	rate := 0;
	if i_ori_currency = i_new_currency then
		rate:= i_ori_amount;
	else
		select exchange_rate into rate from v_currency_rate vcr
		where vcr.currency_from = i_new_currency and vcr.currency_to = i_ori_currency and vcr."period" = TO_DATE(i_effective_date,'YYYYMMDD') ;
	
		if not found then
			rate:=-9999999999;
		else
			if rate <= 0 then
			 	rate := -1;
		 	else
	 			rate := (i_ori_amount * rate);
			end if;
		end if;
	end if;
	
return rate;
end;
$function$
;
