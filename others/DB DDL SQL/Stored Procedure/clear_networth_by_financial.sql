CREATE OR REPLACE FUNCTION clear_networth_by_financial()
  RETURNS trigger
  LANGUAGE plpgsql
AS $function$

/* ***************************************************
  ' Function description:
    當公司Financials中的股東權益、幣別、下季財報期初、下季財報期末任一值變更時，
    清空相應的公司股票淨值，之後會由一支schedule job定時重新計算。
  ' Parameter description:
  ' Version change:
    Seq.  YYYY/MM/DD   Version  Author    Comments
    1.    2022/08/25   1.0      Martin    Create
  ********************************************************/

  BEGIN

	if tg_op='DELETE' or old.currency != new.currency
	  or old.next_report_start_date != new.next_report_start_date
	  or old.next_report_end_date != new.next_report_end_date then
	  
	  update company_stock_price
	  set net_worth_currency=null, net_worth_price=null,
	    update_user='System', update_date=current_timestamp
	  where invest_no=old.invest_no
	    and (old.next_report_start_date-interval'1 days')::date<=transaction_date
	    and transaction_date<=(old.next_report_end_date-interval'1 days')::date
	    and (net_worth_currency is not null or net_worth_price is not null);
	   
	end if;

    return new;

  END;
$function$
;