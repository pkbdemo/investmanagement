CREATE OR REPLACE FUNCTION clear_networth_by_financial_detail()
  RETURNS trigger
  LANGUAGE plpgsql
AS $function$

/* ***************************************************
  ' Function description:
    當公司Financials中的股東權益、幣別、下季財報期初、下季財報期末任一值變更時，
    清空相應的公司股票淨值，之後會由一支schedule job定時重新計算。
  ' Parameter description:
  ' Version change:
    Seq.  YYYY/MM/DD   Version  Author    Comments
    1.    2022/08/25   1.0      Martin    Create
  ********************************************************/

  declare
    accountId_ShareholderEquity int2 := 6;

  BEGIN

	if old.account_id=accountId_ShareholderEquity
	  and (tg_op='DELETE' or old.amount != new.amount) then
	  
	  update company_stock_price t1
	  set net_worth_currency=null, net_worth_price=null,
	    update_user='System', update_date=current_timestamp
	  where exists
	  (
	    select * from company_financials_ytd t2
	    where t2.financials_no=old.financials_no and t1.invest_no=t2.invest_no
	      and (t2.next_report_start_date-interval'1 days')::date<=t1.transaction_date
	      and t1.transaction_date<=(t2.next_report_end_date-interval'1 days')::date
	  )
	  and (net_worth_currency is not null or net_worth_price is not null);
	   
	end if;

    return new;

  END;
$function$
;