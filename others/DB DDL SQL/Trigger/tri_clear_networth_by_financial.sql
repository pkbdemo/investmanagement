create trigger tri_clear_networth_by_financial
    after update or delete on company_financials_ytd
    for each row
    execute procedure clear_networth_by_financial();