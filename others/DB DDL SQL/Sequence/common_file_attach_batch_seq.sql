--dd_stage,dd_decision使用：系統檔案批次編號
CREATE SEQUENCE common_file_attach_batch_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;