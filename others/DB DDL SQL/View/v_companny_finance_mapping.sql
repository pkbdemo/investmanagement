CREATE OR REPLACE VIEW v_companny_finance_mapping
AS SELECT im.contact_ao,
    im.name,
    emp.name_a AS emp_name,
    cfm.fin_sys_name,
    scd.code_name,
    im.invest_no
   FROM common_finance_mapping cfm
     JOIN invest_main im ON im.invest_no = cfm.invest_no
     JOIN findw_hcm_employee emp ON im.contact_ao::text = emp.emplid::text
     JOIN setting_code_detail scd ON cfm.account::text = scd.code_extend_c::text
  ORDER BY im.invest_no, im.name, im.contact_ao, emp.name_a, emp.deptid;