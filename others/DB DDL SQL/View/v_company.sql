-- public.v_company source

CREATE OR REPLACE VIEW v_company
AS SELECT im.invest_no,
    im.name,
    im.contact_ao,
    emp.name_a AS emp_name,
    emp.deptid,
    cm.nickname,
    cst.status,
    cm.status_change,
    searchcodename('002'::character varying, cst.status) AS status_nm,
    string_agg(DISTINCT cs.type::text, ','::text ORDER BY (cs.type::text)) AS type_list,
    string_agg(DISTINCT searchcodename('001'::character varying, cs.type)::text, ','::text) AS type_list_nm
   FROM invest_main im
     JOIN company_main cm ON im.invest_no = cm.invest_no
     JOIN company_security cs ON cm.invest_no = cs.invest_no
     LEFT JOIN company_status cst ON cm.invest_no = cst.invest_no AND cst.end_date = '9999-12-31'::date
     LEFT JOIN findw_hcm_employee emp ON upper(emp.emplid::text) = upper(im.contact_ao::text)
  GROUP BY im.invest_no, im.name, im.contact_ao, emp.name_a, emp.deptid, cm.status_change, cm.nickname, cst.status;