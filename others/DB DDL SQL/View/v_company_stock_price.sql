create or replace view v_company_stock_price
as
select t1.stock_price_no,
  t1.invest_no,
  t1.company_full_name,
  t1.pic,
  t1.transaction_date,
  t1.public_suggest_currency,
  t1.public_suggest_price,
  t1.public_actual_currency,
  t1.public_actual_price,
  t1.private_suggest_currency,
  (1 - t1.latest_discount) * t1.latest_history_price as private_suggest_price,
  t1.private_actual_currency,
  t1.private_actual_price,
  t1.discount,
  t1.discount * 100 as discount4display,
  t1.net_worth_currency,
  t1.net_worth_price,
  t1.history_currency,
  t1.history_price,
  t1.limit_currency,
  t1.limit_price,
  t1.deptid,
  t1.contact_ao
from
(
  select csp.*,
    im.name as company_full_name,
    fhe.name_a as pic,
    fhe.deptid,
    im.contact_ao,
    case
      when cs.status='004' then csp.net_worth_currency
      else
  	  coalesce
  	  (
  	    csp.history_currency,
		(
		  select csp2.limit_currency from company_stock_price csp2 
  	  	  where csp2.invest_no=csp.invest_no and csp2.transaction_date<=csp.transaction_date
  		    and csp2.limit_currency is not null and csp2.limit_currency!=''
  		  order by csp2.transaction_date desc
  		  limit 1
		)
  	  )
    end as public_suggest_currency,
    case
      when cs.end_date='9999-12-31' and cs.status='004' then csp.net_worth_price
      else
  	  coalesce
  	  (
  	    csp.history_price,
		(
		  select csp2.limit_price from company_stock_price csp2 
  		  where csp2.invest_no=csp.invest_no and csp2.transaction_date<=csp.transaction_date
  		    and csp2.limit_price is not null
  		  order by csp2.transaction_date desc
  		  limit 1
		)
  	  )
    end as public_suggest_price,
    (
      select csp2.history_currency from company_stock_price csp2 
      where csp2.invest_no=csp.invest_no and csp2.transaction_date<=csp.transaction_date
    	and csp2.history_currency is not null and csp2.history_currency!=''
      order by csp2.transaction_date desc
      limit 1
    ) as private_suggest_currency,
    (
      select csp2.history_price as latest_history_price
	  from company_stock_price csp2 
   	  where csp2.invest_no=csp.invest_no and csp2.transaction_date<=csp.transaction_date
   	    and csp2.history_price is not null
   	  order by csp2.transaction_date desc
      limit 1
    ),
    (
      select csp2.discount as latest_discount
	  from company_stock_price csp2 
      where csp2.invest_no=csp.invest_no and csp2.transaction_date<=csp.transaction_date
   	    and csp2.discount is not null
   	  order by csp2.transaction_date desc
   	  limit 1
    )
  from company_stock_price csp
  left join company_status cs on csp.invest_no=cs.invest_no and cs.end_date='9999-12-31'
  left join invest_main im on csp.invest_no=im.invest_no
  left join findw_hcm_employee fhe on fhe.emplid=im.contact_ao
) t1;