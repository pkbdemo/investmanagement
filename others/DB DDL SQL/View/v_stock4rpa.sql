create or replace view v_stock4rpa
as
select cm.stock_symbol, cm.stock_market, emp.email_address_a as imd_pic, emp.deptid
from company_main cm, invest_main im
left join findw_hcm_employee emp on im.contact_ao=emp.emplid
where cm.stock_symbol is not null and cm.stock_symbol != ''
  and cm.invest_no=im.invest_no;