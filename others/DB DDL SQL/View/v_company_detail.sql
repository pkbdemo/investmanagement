-- public.v_company_detail source

CREATE OR REPLACE VIEW v_company_detail
AS SELECT im.invest_no,
    im.name,
    cm.nickname,
    cm.subject,
    cm.suggest_status,
    searchcodename('002'::character varying, cm.suggest_status) AS suggest_status_nm,
    ( SELECT cst.status
           FROM company_status cst
          WHERE cm.invest_no = cst.invest_no AND cst.end_date = '9999-12-31'::date
          ORDER BY cst.update_date DESC
         LIMIT 1) AS status,
    searchcodename('002'::character varying, ( SELECT cst.status
           FROM company_status cst
          WHERE cm.invest_no = cst.invest_no AND cst.end_date = '9999-12-31'::date
          ORDER BY cst.update_date DESC
         LIMIT 1)) AS status_nm,
    ( SELECT string_agg(cc.item_value::text, ','::text) AS item_value
           FROM common_category cc
          WHERE cc.invest_no = im.invest_no AND cc.item_name::text = 'EMAIL'::text) AS email_value,
    im.contact_ao,
    ( SELECT emp_1.name_a
           FROM findw_hcm_employee emp_1
          WHERE emp_1.emplid::text = im.contact_ao::text) AS contact_ao_nm,
    emp.deptid,
    cm.stock_market,
    searchcodename('019'::character varying, cm.stock_market) AS stock_market_nm,
    cm.stock_symbol,
    cm.fin_year_rule,
    searchcodename('020'::character varying, cm.fin_year_rule) AS fin_year_rule_nm,
    cm.industry_catagory,
    searchcodename('003'::character varying, cm.industry_catagory) AS industry_catagory_nm,
    cm.industry_detail,
    cm.company_stage,
    searchcodename('012'::character varying, cm.company_stage) AS company_stage_nm,
    cm.main_business,
    cm.headquarter
   FROM invest_main im
     JOIN company_main cm ON im.invest_no = cm.invest_no
     JOIN findw_hcm_employee emp ON upper(emp.emplid::text) = upper(im.contact_ao::text);