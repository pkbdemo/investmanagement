CREATE OR REPLACE VIEW v_currency_rate
AS SELECT shr.currency_from,
    shr.currency_to,
    shr.period::date AS period,
    to_char(shr.period::date::timestamp with time zone, 'MM/YYYY'::text) AS period_format,
    shr.exchange_rate
   FROM setting_history_rate shr
UNION ALL
 SELECT fc.fcurr AS currency_from,
    fc.tcurr AS currency_to,
    fc.period,
    to_char(fc.period::timestamp with time zone, 'MM/YYYY'::text) AS period_format,
    fc.exchange_rate
   FROM findw_currency fc;