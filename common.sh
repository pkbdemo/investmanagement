#!/usr/bin/env bash

function get_version() {
    if test $CI_COMMIT_BRANCH = $DEV_BRANCH_NAME || test $CI_MERGE_REQUEST_TARGET_BRANCH_NAME = $DEV_BRANCH_NAME; then
	    imageTagPrefix=$IMAGE_TAG_DEV
	elif test $CI_COMMIT_BRANCH = $QAS_BRANCH_NAME || test $CI_MERGE_REQUEST_TARGET_BRANCH_NAME = $QAS_BRANCH_NAME; then
	    imageTagPrefix=$IMAGE_TAG_QAS
	elif test $CI_COMMIT_BRANCH = $PRD_BRANCH_NAME || test $CI_MERGE_REQUEST_TARGET_BRANCH_NAME = $PRD_BRANCH_NAME; then
	    imageTagPrefix=$IMAGE_TAG_PRD
	fi
	
	echo $imageTagPrefix
	
	datetimeString=`TZ=Asia/Shanghai date +%Y-%m-%dT%H-%M-%S`
    echo $datetimeString
	
	IMAGE_TAG=${imageTagPrefix}${datetimeString}
	echo $IMAGE_TAG
	
	echo "export IMAGE_TAG=${IMAGE_TAG};" >>docker_build-vars.sh
}